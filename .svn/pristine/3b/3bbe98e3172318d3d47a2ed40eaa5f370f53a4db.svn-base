package cafe;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
 * Component to hold the information about a dish.
 * 
 * @author JuLi
 * @author Adam
 * 
 */
public class DishComponent extends JPanel {

	/** Sets serial Version UID to 1L. */
	private static final long serialVersionUID = 1L;

	/** Button for remove. */
	private final JButton btnRemove = new JButton("-");

	/** Prints out the amount of food. */
	private JLabel lblQuantity = new JLabel("1");

	/** Presents price. */
	private JLabel lblPrice = new JLabel("price");

	/** The label for the description of the Dish. */
	private JLabel lblDescription = new JLabel("Description");

	/** The Name of the dish. */
	private JLabel lblName = new JLabel("Name");

	/** Where the picture is displayed. */
	private final JLabel lblPicture = new JLabel("");

	// Below are some variables for the positions of labels
	/** sets the 'Y' label position to 41. */
	private final int yPosition = 41;

	/** sets the 'X' label position to 348. */
	private final int xLblPosition = 348;

	/** sets the width to 41. */
	private final int buttonWidth = 41;

	/** sets the hight to 23. */
	private final int buttonHight = 23;

	/** The 'X' position for the removeButton. */
	private final int btnRemoveXPos = 399;

	/** sets label width to 46. */
	private final int lblWidth = 46;

	/** sets label height to 14. */
	private final int lblHeight = 14;

	/** sets label 'Y' position to 11. */
	private final int lblYPos = 11;

	/** sets the X position of the remove button to 374. */
	private final int btnRemoveX = 374;

	/** sets the 'X' position of Quantity to 250. */
	private final int lblQuantityXPos = 250;

	/** sets the label 'X' position to 109. */
	private final int lblX = 109;

	/** sets the description 'Y' position to 36. */
	private final int descriptionY = 36;

	/** sets the description width to 201. */
	private final int descriptionWidth = 201;

	/** sets the description height to 34. */
	private final int descriptionHeight = 34;

	/** sets width of picture to 99. */
	private final int picWidth = 99;

	/** sets hight of picture to 70. */
	private final int picHight = 70;

    /**
     * Create the panel. sets the label values to the dish values that are
     * passed to the method
     * 
     * @param name
     *            - dish name.
     * @param description
     *            - dish description.
     * @param price
     *            - dish price.
     * @param quantity
     *            - dish quantity
     * @param aListener
     *            - Action listener for dish.
     * @param image
     *            - dish image
     */
    public DishComponent(final String name, final String description,
            final float price, final int quantity,
            final ActionListener aListener, final BufferedImage image) {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] { 99, 165, 50, 164, 41, 41, 0 };
        gridBagLayout.rowHeights = new int[] { 25, 34, 0 };
        gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, Double.MIN_VALUE };
        gridBagLayout.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
        setLayout(gridBagLayout);
        /*
         * aListener from controller - should decrease quantity and/or remove
         * item and refresh labels and refresh price
         */

        JButton btnAdd = new JButton("+");

        btnAdd.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                // for now it just increases the
                // number displayed on the label

                int temp = Integer.parseInt(lblQuantity.getText()) + 1;
                lblQuantity.setText(Integer.toString(temp));
            }
        });
        btnAdd.addActionListener(aListener);
        int temp = Integer.parseInt(lblQuantity.getText()) + 1;

        GridBagConstraints gbc_lblPicture = new GridBagConstraints();
        gbc_lblPicture.fill = GridBagConstraints.BOTH;
        gbc_lblPicture.insets = new Insets(0, 0, 0, 5);
        gbc_lblPicture.gridheight = 2;
        gbc_lblPicture.gridx = 0;
        gbc_lblPicture.gridy = 0;
        add(lblPicture, gbc_lblPicture);

        if (image != null) {
            lblPicture.setIcon(new ImageIcon(image));
        }

        GridBagConstraints gbc_lblName = new GridBagConstraints();
        gbc_lblName.anchor = GridBagConstraints.SOUTH;
        gbc_lblName.fill = GridBagConstraints.HORIZONTAL;
        gbc_lblName.insets = new Insets(0, 0, 5, 5);
        gbc_lblName.gridx = 1;
        gbc_lblName.gridy = 0;
        add(lblName, gbc_lblName);

        lblName.setText(name);

        GridBagConstraints gbc_lblPrice = new GridBagConstraints();
        gbc_lblPrice.anchor = GridBagConstraints.SOUTHWEST;
        gbc_lblPrice.insets = new Insets(0, 0, 5, 5);
        gbc_lblPrice.gridx = 3;
        gbc_lblPrice.gridy = 0;
        add(lblPrice, gbc_lblPrice);
        lblPrice.setText(Float.toString(price));
        lblQuantity.setHorizontalAlignment(SwingConstants.CENTER);

        GridBagConstraints gbc_lblQuantity = new GridBagConstraints();
        gbc_lblQuantity.anchor = GridBagConstraints.SOUTH;
        gbc_lblQuantity.insets = new Insets(0, 0, 5, 0);
        gbc_lblQuantity.gridwidth = 2;
        gbc_lblQuantity.gridx = 4;
        gbc_lblQuantity.gridy = 0;
        add(lblQuantity, gbc_lblQuantity);
        lblDescription.setFont(new Font("Tahoma", Font.PLAIN, 11));
        lblDescription.setVerticalAlignment(SwingConstants.TOP);

        GridBagConstraints gbc_lblDescription = new GridBagConstraints();
        gbc_lblDescription.fill = GridBagConstraints.BOTH;
        gbc_lblDescription.insets = new Insets(0, 0, 0, 5);
        gbc_lblDescription.gridwidth = 3;
        gbc_lblDescription.gridx = 1;
        gbc_lblDescription.gridy = 1;
        add(lblDescription, gbc_lblDescription);
        lblDescription.setText(description);
        GridBagConstraints gbc_btnAdd = new GridBagConstraints();
        gbc_btnAdd.anchor = GridBagConstraints.WEST;
        gbc_btnAdd.insets = new Insets(0, 0, 0, 5);
        gbc_btnAdd.gridx = 4;
        gbc_btnAdd.gridy = 1;
        add(btnAdd, gbc_btnAdd);

        btnRemove.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent arg0) {
                // temporary, just decreasing quantity

                int temp = Integer.parseInt(lblQuantity.getText()) - 1;
                lblQuantity.setText(Integer.toString(temp));
            }
        });

        btnRemove.addActionListener(aListener);

        GridBagConstraints gbc_btnRemove = new GridBagConstraints();
        gbc_btnRemove.fill = GridBagConstraints.HORIZONTAL;
        gbc_btnRemove.gridx = 5;
        gbc_btnRemove.gridy = 1;
        add(btnRemove, gbc_btnRemove);

    }
}
