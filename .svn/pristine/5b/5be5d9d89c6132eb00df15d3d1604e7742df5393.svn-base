package cafe.manager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import cafe.dao.EmployeeDAO;
import cafe.dao.IngredientDAO;
import cafe.dao.TableDAO;

/**
 * Observes user events from the manager view(s) and directs behaviour accordingly.
 * @author Robert Kardjaliev
 *
 */
public class ManagerController {
	/**
	 * The controller for the Waiters for Tables GUI.
	 */
	private WaitersForTablesController tablesController;
	/**
	 * The model for the Waiters for Tables GUI.
	 */
	private WaitersForTablesModel tablesModel;
	/**
	 * The view for the Waiters for Tables GUI.
	 */
	private WaitersForTablesView tablesView;

	/**
	 * The View of the Manager GUI.
	 */
	private ManagerView view;
	
	/**
	 * The stock level controller.
	 */
	private StockLevelController stockController;
	
	/**
	 * The stock level view.
	 */
	private StockLevelView stockView;
	/**
	 * The stock level model.
	 */
	private StockLevelModel stockModel;
	
	/**
	 * Hides the view associated with this controller.
	 */
	public final void hideView() {
		view.setVisible(false);
	}

	/**
	 * Shows the view associated with this controller.
	 */
	public final void showView() {
		view.setVisible(true);
	}
	
	/**
	 * Observes the button to change to Stock Level View.
	 * @param listener
	 * 				the listener.
	 */
    public final void addStockLevelListener(final ActionListener listener) {
        view.addStockLevelListener(listener);        
    }
    /**
     * Observes the button to change to Waiters For Tables View.
     * @param listener
     * 				the listener
     */
    public final void addWaitersForTablesListener(final ActionListener listener) {
    	view.addWaitersForTablesListener(listener);
    }
	/**
	 * Instantiates the fields of the Manager Controller.
	 * @param view
	 *            The View
	 * @param tableData
	 * 				the Table data access object needed
	 * to allow modification of {@link Table}s from the database.
	 * @param employeeData
	 * 				the Emlpoyee data access object required for modification
	 * of {@link Employee}s from the database.
	 * @param ingredientData 
	 * 				the Ingredient data access object required for modification of
	 * {@link Ingredient}s from the database.
	 */

	public ManagerController(final ManagerView view, final TableDAO tableData,
			final EmployeeDAO employeeData, final IngredientDAO ingredientData) {
		this.view = view;
		tablesView = new WaitersForTablesView();
		tablesModel = new WaitersForTablesModel(tableData, employeeData);
		tablesController = new WaitersForTablesController(tablesView, tablesModel);
		stockView = new StockLevelView();
		stockModel = new StockLevelModel(ingredientData);
		stockController = new StockLevelController(stockView, stockModel);
		addWaitersForTablesListener(new ShowWaitersForTablesListener());
		addStockLevelListener(new ShowStockLevelListener());
		tablesController.addBackButtonListener(new BackFromTablesListener());
	}
	
	/**
	 * A listener for the back button in the Waiters for Tables view.
	 *
	 */
	private class BackFromTablesListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			tablesController.hideView();
			showView();
			
		}
	}
    
    /**
     * Listens for button click events originating from the Waiters For Tables button.
     *  It hides the Manager View and displays the Waiter for Tables's view.
     */
	private class ShowWaitersForTablesListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			hideView();
			tablesController.showView();
		}
	}
    /**
     * Listens for button click events originating from the Waiters For Tables button.
     *  It hides the Manager View and displays the Waiter for Tables's view.
     */
	private class ShowStockLevelListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			hideView();
			stockController.showView();
		}
	}
}
