import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Provides a driver for the client's application, handling the relationship between the menu,
 * order status display, and payment processing.
 * 
 * @author Michael Winter
 */
public class ClientApplicationController {
	/**
	 * Initialises and runs the application. The actual launching of the application is delegated to
	 * this method in order to ensure initialisation occurs on Swing's event-dispatching thread.
	 */
	private static void launch() {
		try {
			if (SwingUtilities.isEventDispatchThread()) {
				final ClientApplicationController application = new ClientApplicationController();
				application.run();
			} else {
				SwingUtilities.invokeAndWait(new Runnable() {
					@Override
					public void run() {
						launch();
					}
				});
			}
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * The main entry point for the application.
	 * 
	 * @param args the command line arguments passed to the application
	 */
	public static void main(String[] args) {
		launch();
	}

	/**
	 * The currently active order. This instance retains the state of client's selection during
	 * their visit and is reset once the client has paid and left.
	 */
	//private Order currentOrder;
	/**
	 * The controller for the client menu.
	 */
	private MenuController menuController;
	/**
	 * The controller for the client order display.
	 */
	private OrderController orderController;
	/**
	 * The controller for the client payment system.
	 */
	//private PaymentController paymentController;


	/**
	 * Prepares the UI and any resources required prior to starting the application. The constructor
	 * must be invoked on the event-dispatching thread.
	 */
	private ClientApplicationController() {
		final MenuModel menuModel = new MenuModel();
		final MenuGUI menuView = new MenuGUI();
		menuController = new MenuController(menuModel, menuView);
		menuController.addDishAddedListener(new DishAddedListener());
		menuController.addShowOrderListener(new ShowOrderListener());
		menuController.addWaiterCallListener(new WaiterCallListener());

		final Order orderModel = new Order();
		final OrderGUI orderView = new OrderGUI();
		orderController = new OrderController(orderModel, orderView);
		orderController.addShowMenuListener(new ShowMenuListener());
	}
	
	/**
	 * Starts the application.
	 */
	public void run() {
		menuController.showView();
	}
	
	
	/**
	 * Listens for order change events originating from the Add to Order button associated with
	 * each dish, adding them to the current order.
	 */
	private class DishAddedListener implements ChangeListener {
		@Override
		public void stateChanged(final ChangeEvent e) {
			final Dish addedDish = (Dish) e.getSource();
			//currentOrder.addDish(addedDish);
			System.out.println("Added " + addedDish.getName() + " to order.");
		}
	}
	
	/**
	 * Listens for button click events originating from the Order button, hiding the menu view
	 * and displaying the order status view.
	 */
	private class ShowOrderListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			menuController.hideView();
			orderController.showView();
		}
	}
	
	/**
	 * Listens for button click events originating from the Menu button, hiding the order view
	 * and displaying the menu.
	 */
	private class ShowMenuListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			menuController.showView();
			orderController.hideView();
		}
	}
	
	
	/**
	 * Listens for button click events originating from the Call for Assistance button, hiding
	 * alerting the waiters that a table requires attention.
	 */
	private class WaiterCallListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("Service request sent...");
		}
	}
}
