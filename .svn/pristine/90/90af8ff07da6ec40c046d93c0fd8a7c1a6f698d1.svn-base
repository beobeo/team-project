package cafe.dao.postgres;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import cafe.Payment;
import cafe.dao.DAOFactory;
import cafe.dao.PaymentDAO;
import cafe.dao.postgres.DefaultTestDAOFactoryImpl;

/**
 * Unit test for {@link PaymentDAOImpl} class.
 * @author Robert Kardjaliev
 */
public class PaymentDAOTestImpl {	
	/** An instance of the DAO factory. */
	private DAOFactory factory = DAOFactory.getInstance("zyvc215", "IYIP1845",
			DefaultTestDAOFactoryImpl.class);
	/**
	 * An instance of a Payment database access object.
	 */
	private PaymentDAO paymentData = factory.getPaymentDAO();

	/**
	 * A payment object.
	 */
	private Payment payment = new Payment(1, 1, null, 55.50f);

	/**
	 * A payment object loaded from the database.
	 */
	Payment testPayment = paymentData.findPayment(1);

	/**
	 * Checks whether the {@code ID} of the local {@link Payment} is the same as
	 * the one on the database.
	 */
	@Test
	public final void testPaymentId() {
		assertEquals("Test 1: Test for existing integer payment's id.", 
				payment.getId(), testPayment.getId());
	}
	
	/**
	 * Checks whether the {@code Sitting ID} of the local {@link Payment} is the same as
	 * the one on the database.
	 */
	@Test
	public final void testPaymentSittingId() {
		assertEquals("Test 2: Test for existing integer payment's sitting id", 
				payment.getSittingId(), testPayment.getSittingId());
	}
	
	/**
	 * Checks whether you can have a null String for {@code card} for a {@link Payment}
	 * for a payment with cash in the database.
	 */
	@Test
	public final void testForNoCard() {
		assertTrue("Test 3: Test for non-existing card", testPayment.getCard()==null);
	}

	/**
	 * Checks whether the {@code amount} paid of the local {@link Payment} is the same as
	 * the one on the database.
	 */
	@Test
	public final void testAmountPaid() {
		assertEquals("Test 4: Test for existing float payment price", testPayment.getAmount(), payment.getAmount(), 0.01f);
	}
}