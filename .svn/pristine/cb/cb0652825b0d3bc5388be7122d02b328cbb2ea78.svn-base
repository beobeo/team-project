package cafe.staff;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import cafe.Application;
import cafe.client.ClientApplication;
import cafe.dao.DAOFactory;
import cafe.dao.DishDAO;
import cafe.dao.EmployeeDAO;
import cafe.dao.IngredientDAO;
import cafe.dao.OrderDAO;
import cafe.dao.TableDAO;
import cafe.dao.postgres.DAOFactoryImpl;
import cafe.staff.editStock.EditStockController;
import cafe.staff.editStock.EditStockGUI;
import cafe.staff.editStock.EditStockModel;
import cafe.staff.kitchen.KitchenController;
import cafe.staff.kitchen.KitchenModel;
import cafe.staff.kitchen.KitchenView;
import cafe.staff.manager.ManagerController;
import cafe.staff.manager.ManagerView;
import cafe.staff.waiter.WaiterController;
import cafe.staff.waiter.WaiterGUI;
import cafe.staff.waiter.WaiterModel;

/**
 * @author Jonathan Hercock
 * @author Robert Kardjaliev
 *
 */
public final class StaffApplication extends Application {
	/**
	 * A logging instance used to record errors and debugging information during
	 * execution.
	 */
	private static final Logger LOGGER = Logger.getLogger(StaffApplication.class.getName());
    /**
     * The controller for the edit stock screen.
     */
    private EditStockController stockController;
    /**
     * The Controller for the employee's Log In Screen.
     */
    private LogInController loginController;
    
    /**
     * The Controller for the Waiter's view.
     */
    private WaiterController waiterController;
    
    /**
     * The Controller for the Kitchen's view.
     */
    private KitchenController kitchenController;
    
    /**
     * The Controller for the Manager's view.
     */
    private ManagerController managerController;
    
    /**
     * Determines if the last page was managers page or kitchen staffs page
     * for the back button on edit stock interface.
     */
    private Boolean managerKitchen = false;
	/**
	 * The data access object used to obtain and persist {@code Dish} instances.
	 */
	private DishDAO dishData;
	/**
	 * The data access object used to obtain and persist {@code Employee} instances.
	 */
	private EmployeeDAO employeeData;
	/**
	 * The data access object used to obtain and persist {@code Order} instances.
	 */
	private OrderDAO orderData;
	/**
	 * The data access object used to obtain and persist {@code Table} instances.
	 */
	private TableDAO tableData;

    
    /**
     * The main entry point for the application.
     * 
     * @param args the command line arguments passed to the application.
     */
    public static void main(final String[] args) {
        launch(StaffApplication.class, args);
    }
    
    /*
	 * (non-Javadoc)
	 * 
	 * @see cafe.Application#initialise(java.lang.String[])
	 */
	@Override
	protected void initialise(final String[] args) {
        DAOFactory factory = DAOFactory.getInstance("zyvc215", "IYIP1845",
                DAOFactoryImpl.class);
        orderData = factory.getOrderDAO();
        tableData = factory.getTableDAO();
        dishData = factory.getDishDAO();
        employeeData = factory.getEmployeeDAO();
        
        final IngredientDAO ingredientData = factory.getIngredientDAO();
        
        final EditStockModel editStockModel = new EditStockModel(ingredientData);
        final EditStockGUI editStockGUI = new EditStockGUI(editStockModel.getIngredientList());
        stockController = new EditStockController(editStockGUI, editStockModel);
        stockController.addUpdateStockListener(new UpdateStockListener());  
        stockController.addBackButtonListener(new BackButtonListener());
    }
	
	/**
	 * Starts the application.
	 */
    @Override
	protected void run() {
        final LogInScreen loginScreen = new LogInScreen();
        loginController = new LogInController(loginScreen);
        loginController.addManagementLogInListener(new ManagementLogInListener());
        loginController.addKitchenLogInListener(new KitchenLogInListener());
        loginController.addWaiterLogInListener(new WaiterLogInListener());
        loginController.showView();
    }
    
    
    /**
     * Listens for button click events originating from the Management button.
     *  It hides the log in screen and (TODO:) displays the Manager's view.
     */
    private class ManagementLogInListener implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent e) {
            final ManagerView managerView = new ManagerView();
            managerController = new ManagerController(managerView, tableData, employeeData, dishData);
            managerController.addStockLevelListener(new EditStockListener());

            loginController.hideView();
            managerController.showView();
            managerKitchen = true;
        }
    }
    
    /**
     * Listens for button click events originating from the Kitchen Staff button.
     *  It hides the log in screen and displays the Kitchen Staff's view.
     */
    private class KitchenLogInListener implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent e) {
        	final KitchenView kitchenScreen = new KitchenView();
            final KitchenModel kitchenModel = new KitchenModel(orderData);
            kitchenController = new KitchenController(kitchenModel, kitchenScreen);
            kitchenController.addEditStockListener(new EditStockListener());
            
        	loginController.hideView();
        	kitchenController.showView();
        	managerKitchen = false;
        }
    }   
    
    /**
     * Listens for button click events originating from the Kitchen Staff button.
     *  It hides the log in screen and (TODO:) displays the Kitchen Staff's view.     *
     */
    private class WaiterLogInListener implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent e) {
        	final WaiterGUI waiterScreen = new WaiterGUI();
            final WaiterModel waiterModel = new WaiterModel(orderData);
            waiterController = new WaiterController(waiterScreen, waiterModel);
            
            loginController.hideView();
            waiterController.showView();
        }
    }
    
    /**
     * 
     * @author Hannah Cooper
     * Listens for button click of update stock on edit stock page. If action is performed then the 
     * selected ingredient will be updated to the number in which is inputted by the user
     * in the textField.
     */
    public class UpdateStockListener implements ActionListener {
    	@Override
		public final void actionPerformed(final ActionEvent e) {
    		if (!EditStockGUI.getTextField().getText().equals("")) {
    			int text = Integer.parseInt(EditStockGUI.getTextField().getText());
        		EditStockGUI.selectedItem.setStock(text);
        		EditStockModel.changeStockLevel(EditStockGUI.selectedItem.getId(), text); 
    		} else {
    			
    		}
    		EditStockGUI.level.setText(String.valueOf(EditStockGUI.selectedItem.getStock()));

    		EditStockGUI.panel.repaint();
    	}
    }
    /**
     * 
     * @author Hannah Cooper
     * Listener for back button on edit stock GUI.
     * Reads the value of the boolean to determine which page to go back to.
     */
    public class BackButtonListener implements ActionListener {
		@Override
		public final void actionPerformed(final ActionEvent e) {
			if (managerKitchen) {
				stockController.hideView();
			    managerController.showView();
			} else {
				stockController.hideView();
				kitchenController.showView();
			}
		}	
    }
    
    /**
	 * 
	 * @author Hannah Cooper
	 * Listener for the edit stock button in KitchenView
	 *
	 */
	private class EditStockListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			if (kitchenController != null) {
				kitchenController.hideView();
			}
			stockController.showView();
		}
	}
}
