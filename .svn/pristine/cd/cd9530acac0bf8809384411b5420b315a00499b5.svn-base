package cafe;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.sql.Timestamp;
import java.util.Collections;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

/**
 * Presents a graphical window to kitchen staff from which they can view orders,
 * and update their progress.
 * 
 * @author Robert Kardjaliev
 * @author Michael Winter
 */
public final class KitchenGUI extends JFrame {
    /**
     * Provides a container within the scroll pane into which displayed orders
     * are rendered.
     */
    private JPanel container;
    
    public static void main(String args[]) {
        new KitchenGUI().setVisible(true);
    }
    
    /**
     * Create the frame.
     */
    public KitchenGUI() {
        initGUI();
        GridBagConstraints panelGridBagConstraints = new GridBagConstraints();
        panelGridBagConstraints.anchor = GridBagConstraints.NORTH;
        panelGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        panelGridBagConstraints.insets = new Insets(0, 0, 5, 0);
        panelGridBagConstraints.gridx = 0;
        panelGridBagConstraints.gridy = 0;
        container.add(new OrderListView(new Order(0, null, OrderState.CONFIRMED, new Timestamp(
                        System.currentTimeMillis()), null, null, null,
                        Collections.singletonList(new OrderItem(new Dish(1,
                                "Lasagne", null, 7.65f, 867, "Main course",
                                null, true,
                                Collections.<Ingredient, Integer> emptyMap(),
                                Collections.<DietaryInformation> emptySet()),
                                1, false)))), panelGridBagConstraints);
    }
    
    
    /**
     * Initialises UI components.
     */
    //CHECKSTYLE:OFF
    private void initGUI() {
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 416, 463);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
        
        final JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportBorder(new EmptyBorder(1, 0, 1, 0));
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        contentPane.add(scrollPane);
        
        container = new JPanel();
        scrollPane.setViewportView(container);
        GridBagLayout containerGridBagLayout = new GridBagLayout();
        containerGridBagLayout.columnWidths = new int[] {380};
        containerGridBagLayout.rowHeights = new int[]{0, 0, 0};
        containerGridBagLayout.columnWeights = new double[]{0.0};
        containerGridBagLayout.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
        container.setLayout(containerGridBagLayout);
        
        JPanel panel = new JPanel();
        panel.setVisible(false);
        panel.setBorder(new CompoundBorder(new EmptyBorder(2, 2, 2, 2), new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Sample Order #1", TitledBorder.LEADING, TitledBorder.TOP, null, null)));
        GridBagLayout panelGridBagLayout = new GridBagLayout();
        panelGridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
        panelGridBagLayout.rowHeights = new int[]{0, 0, 0, 0};
        panelGridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        panelGridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        panel.setLayout(panelGridBagLayout);
        
        JLabel lblDishNamesAnd = new JLabel("Dish names and Ingredients");
        GridBagConstraints lblDishNamesAndGridBagConstraints = new GridBagConstraints();
        lblDishNamesAndGridBagConstraints.anchor = GridBagConstraints.WEST;
        lblDishNamesAndGridBagConstraints.insets = new Insets(0, 0, 5, 5);
        lblDishNamesAndGridBagConstraints.gridx = 0;
        lblDishNamesAndGridBagConstraints.gridy = 0;
        panel.add(lblDishNamesAnd, lblDishNamesAndGridBagConstraints);
        
        JLabel lblATimerPerhaps = new JLabel("A timer perhaps?");
        GridBagConstraints lblATimerPerhapsGridBagConstraints = new GridBagConstraints();
        lblATimerPerhapsGridBagConstraints.anchor = GridBagConstraints.WEST;
        lblATimerPerhapsGridBagConstraints.insets = new Insets(0, 0, 5, 5);
        lblATimerPerhapsGridBagConstraints.gridheight = 2;
        lblATimerPerhapsGridBagConstraints.gridx = 1;
        lblATimerPerhapsGridBagConstraints.gridy = 0;
        panel.add(lblATimerPerhaps, lblATimerPerhapsGridBagConstraints);
        
        JButton btnViewOrderDetails = new JButton("View Order Details");
        GridBagConstraints btnViewOrderDetailsGridBagConstraints = new GridBagConstraints();
        btnViewOrderDetailsGridBagConstraints.fill = GridBagConstraints.BOTH;
        btnViewOrderDetailsGridBagConstraints.insets = new Insets(0, 0, 5, 0);
        btnViewOrderDetailsGridBagConstraints.gridx = 2;
        btnViewOrderDetailsGridBagConstraints.gridy = 0;
        panel.add(btnViewOrderDetails, btnViewOrderDetailsGridBagConstraints);
        
        JLabel lblProgressBarFor = new JLabel("Progress Bar for Order #1");
        GridBagConstraints lblProgressBarForGridBagConstraints = new GridBagConstraints();
        lblProgressBarForGridBagConstraints.anchor = GridBagConstraints.NORTH;
        lblProgressBarForGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        lblProgressBarForGridBagConstraints.insets = new Insets(0, 0, 5, 5);
        lblProgressBarForGridBagConstraints.gridx = 0;
        lblProgressBarForGridBagConstraints.gridy = 1;
        panel.add(lblProgressBarFor, lblProgressBarForGridBagConstraints);
        
        JButton btnNewButton = new JButton("Update progress");
        GridBagConstraints btnNewButtonGridBagConstraints = new GridBagConstraints();
        btnNewButtonGridBagConstraints.insets = new Insets(0, 0, 5, 0);
        btnNewButtonGridBagConstraints.fill = GridBagConstraints.BOTH;
        btnNewButtonGridBagConstraints.gridx = 2;
        btnNewButtonGridBagConstraints.gridy = 1;
        panel.add(btnNewButton, btnNewButtonGridBagConstraints);
        
        JProgressBar progressBar = new JProgressBar();
        GridBagConstraints progressBarGridBagConstraints = new GridBagConstraints();
        progressBarGridBagConstraints.anchor = GridBagConstraints.NORTH;
        progressBarGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        progressBarGridBagConstraints.insets = new Insets(0, 0, 0, 5);
        progressBarGridBagConstraints.gridwidth = 3;
        progressBarGridBagConstraints.gridx = 0;
        progressBarGridBagConstraints.gridy = 2;
        panel.add(progressBar, progressBarGridBagConstraints);
        GridBagConstraints panelGridBagConstraints = new GridBagConstraints();
        panelGridBagConstraints.anchor = GridBagConstraints.NORTH;
        panelGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        panelGridBagConstraints.insets = new Insets(0, 0, 5, 0);
        panelGridBagConstraints.gridx = 0;
        panelGridBagConstraints.gridy = 0;
        container.add(panel, panelGridBagConstraints);
        
        final JPanel panel_1 = new JPanel();
        panel_1.setVisible(false);
        panel_1.setBorder(new CompoundBorder(new EmptyBorder(2, 2, 2, 2), new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Sample Order #2", TitledBorder.LEADING, TitledBorder.TOP, null, null)));
        GridBagConstraints panel_1GridBagConstraints = new GridBagConstraints();
        panel_1GridBagConstraints.anchor = GridBagConstraints.NORTH;
        panel_1GridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        panel_1GridBagConstraints.gridx = 0;
        panel_1GridBagConstraints.gridy = 1;
        container.add(panel_1, panel_1GridBagConstraints);
        GridBagLayout panel_1GridBagLayout = new GridBagLayout();
        panel_1GridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
        panel_1GridBagLayout.rowHeights = new int[]{0, 0, 0, 0};
        panel_1GridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        panel_1GridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
        panel_1.setLayout(panel_1GridBagLayout);
        
        final JLabel label = new JLabel("Dish names and Ingredients");
        GridBagConstraints labelGridBagConstraints = new GridBagConstraints();
        labelGridBagConstraints.anchor = GridBagConstraints.WEST;
        labelGridBagConstraints.insets = new Insets(0, 0, 5, 5);
        labelGridBagConstraints.gridx = 0;
        labelGridBagConstraints.gridy = 0;
        panel_1.add(label, labelGridBagConstraints);
        
        final JLabel label_1 = new JLabel("A timer perhaps?");
        GridBagConstraints label_1GridBagConstraints = new GridBagConstraints();
        label_1GridBagConstraints.anchor = GridBagConstraints.WEST;
        label_1GridBagConstraints.gridheight = 2;
        label_1GridBagConstraints.insets = new Insets(0, 0, 5, 5);
        label_1GridBagConstraints.gridx = 1;
        label_1GridBagConstraints.gridy = 0;
        panel_1.add(label_1, label_1GridBagConstraints);
        
        final JButton button = new JButton("View Order Details");
        GridBagConstraints buttonGridBagConstraints = new GridBagConstraints();
        buttonGridBagConstraints.fill = GridBagConstraints.BOTH;
        buttonGridBagConstraints.insets = new Insets(0, 0, 5, 0);
        buttonGridBagConstraints.gridx = 2;
        buttonGridBagConstraints.gridy = 0;
        panel_1.add(button, buttonGridBagConstraints);
        
        final JLabel labelProgressBarFor = new JLabel("Progress Bar for Order #2");
        GridBagConstraints labelProgressBarForGridBagConstraints = new GridBagConstraints();
        labelProgressBarForGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        labelProgressBarForGridBagConstraints.anchor = GridBagConstraints.NORTH;
        labelProgressBarForGridBagConstraints.insets = new Insets(0, 0, 5, 5);
        labelProgressBarForGridBagConstraints.gridx = 0;
        labelProgressBarForGridBagConstraints.gridy = 1;
        panel_1.add(labelProgressBarFor, labelProgressBarForGridBagConstraints);
        
        final JButton button_1 = new JButton("Update progress");
        GridBagConstraints button_1GridBagConstraints = new GridBagConstraints();
        button_1GridBagConstraints.fill = GridBagConstraints.BOTH;
        button_1GridBagConstraints.insets = new Insets(0, 0, 5, 0);
        button_1GridBagConstraints.gridx = 2;
        button_1GridBagConstraints.gridy = 1;
        panel_1.add(button_1, button_1GridBagConstraints);
        
        final JProgressBar progressBar_1 = new JProgressBar();
        GridBagConstraints progressBar_1GridBagConstraints = new GridBagConstraints();
        progressBar_1GridBagConstraints.insets = new Insets(0, 0, 0, 5);
        progressBar_1GridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        progressBar_1GridBagConstraints.anchor = GridBagConstraints.NORTH;
        progressBar_1GridBagConstraints.gridwidth = 3;
        progressBar_1GridBagConstraints.gridx = 0;
        progressBar_1GridBagConstraints.gridy = 2;
        panel_1.add(progressBar_1, progressBar_1GridBagConstraints);
    }
    //CHECKSTYLE:ON
}

