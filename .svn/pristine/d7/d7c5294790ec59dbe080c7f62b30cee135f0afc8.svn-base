package cafe.dao;

import cafe.Sitting;

/**
 * A data access object used to manipulate persisted {@link Sitting} objects.
 * 
 * @author Michael Winter
 */
public interface SittingDAO {
    /**
     * Given an identifier, this method loads and returns a new instance of the
     * specified {@link Sitting}.
     * 
     * @param id
     *            the identifier.
     * @return the specified {@code Sitting}.
     * @throws DataAccessException
     *             if an error occurs while accessing the data store.
     */
    Sitting findSitting(final int id);
}
