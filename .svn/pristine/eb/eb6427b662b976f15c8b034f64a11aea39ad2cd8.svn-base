package cafe;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import cafe.dao.DishDAO;

/**
 * Obtains available dishes from the database.
 * 
 * TODO: Provide filtering and sorting.
 * 
 * @author Michael Winter
 * @author Robert Kardjaliev
 */
public class MenuModel {
	/**
	 * The list of dishes last retrieved from the database.
	 */
	private List<Dish> dishes;
	/**
	 * A dish data access object.
	 */
	private DishDAO dishData;
	
	/**
	 * Creates the menu model.
	 * @param dishData - the data access object
	 * @throws IOException 
	 */
	public MenuModel(final DishDAO dishData) throws IOException {
		this.dishData = dishData;
		loadDishes();
	}
	
	/**
	 * Obtained an ordered list of dishes available from the menu. The returned list is immutable.
	 * 
	 * @return an unmodifiable, ordered list of available dishes.
	 * @throws IOException 
	 */
	public final List<Dish> getDishes() throws IOException {
		return Collections.unmodifiableList(dishes);
	}
	
	/**
	 * Loads the dishes with the visible parameter set to true from the
	 * database in order to display them when later called.
	 * @throws IOException 
	 */
	private void loadDishes() throws IOException {
	    dishes = dishData.selectAvailableDishes();

	    /*
        Dish dish = null;
        List<Dish> dishes = null;
        Map<Ingredient, Integer> ingredients = null;

        ingredients = new HashMap<>();
        ingredients.put(new Ingredient(1, "Minced beef", 0, 2000), 120);
        ingredients.put(new Ingredient(2, "Maris Piper potatoes", 0, 100), 1);
        ingredients.put(new Ingredient(3, "Cheddar cheese", 0, 1000), 50);
        ingredients.put(new Ingredient(4, "Egg pasta", 0, 100), 5);
        dish = new Dish(1, "Lasagne",
                "Homemade lasagne. Served with chips and salad.", 8.95f, 1080,
                "Main course", true, ingredients, null);
		dishes.add(dish);

        ingredients = new HashMap<>();
        ingredients.put(new Ingredient(5, "Red Onion", 0, 50), 1);
        ingredients.put(new Ingredient(6, "Nutmeg", 0, 100), 1);
        ingredients.put(new Ingredient(3, "Cheddar cheese", 0, 1000), 25);
        ingredients.put(new Ingredient(7, "Egg", 0, 120), 3);
        ingredients.put(new Ingredient(8, "Tomatoes", 0, 50), 2);
        dish = new Dish(2, "Butternut squash ravioli",
                "Homemade Butternut squash ravioli with fried sage and tomatoes.", 7.45f, 900,
                "Main course", true, ingredients, null);
        dishes.add(dish);

        ingredients = new HashMap<>();
        ingredients.put(new Ingredient(9, "Mozzarella cheese", 0, 1000), 50);
        ingredients.put(new Ingredient(10, "Ricotta cheese", 0, 1000), 50);
        ingredients.put(new Ingredient(3, "Cheddar cheese", 0, 1000), 50);
        ingredients.put(new Ingredient(11, "Gorgonzola cheese", 0, 1000), 50);
        dish = new Dish(3, "4 Cheese Pizza",
                "Freshly made base with 4 toppings of cheese, Mozzarella, Ricotta, Gorgonzola and Cheddar.", 8.95f, 1000,
                "Main course", true, ingredients, null);
        dishes.add(dish);

        ingredients = new HashMap<>();
        ingredients.put(new Ingredient(12, "Salmon", 0, 500), 1);
        ingredients.put(new Ingredient(13, "Garlic", 0, 100), 1);
        ingredients.put(new Ingredient(14, "Mixed herbs", 0, 250), 1);
        dish = new Dish(4, "Grilled Salmon",
                "Grilled Salmon on a bed of salad.", 7.95f, 850,
                "Main course", true, ingredients, null);
        dishes.add(dish);
        */
    }
}
