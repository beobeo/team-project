package cafe;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.util.Collections;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;

/**
 * 
 * @author Michael Winter
 */
public class OrderDetailView extends JDialog {
    private JTable itemTable;

    public static void main(String args[]) {
        new OrderDetailView(new OrderDetailTableModel(
                new Order(0, null, OrderState.CONFIRMED, new Timestamp(
                        System.currentTimeMillis()), null, null, null,
                        Collections.singletonList(new OrderItem(new Dish(1,
                                "Lasagne", null, 7.65f, 867, "Main course",
                                null, true,
                                Collections.<Ingredient, Integer> emptyMap(),
                                Collections.<DietaryInformation> emptySet()),
                                1, false))))).setVisible(true);;
    }
    /**
     * Create the dialog.
     */
    public OrderDetailView(final OrderDetailTableModel tableModel) {
        initGUI();
        itemTable.setModel(tableModel);
    }
    
    private void initGUI() {
        setTitle("Order Details");
        setResizable(false);
        setModalityType(ModalityType.APPLICATION_MODAL);
        setType(Type.UTILITY);
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        {
            final JScrollPane scrollPane = new JScrollPane();
            scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            scrollPane.setBorder(null);
            getContentPane().add(scrollPane, BorderLayout.CENTER);
            {
                itemTable = new JTable();
                itemTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
                itemTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                itemTable.setShowVerticalLines(false);
                itemTable.setFillsViewportHeight(true);
                scrollPane.setViewportView(itemTable);
            }
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("OK");
                okButton.addActionListener(new OkButtonActionListener());
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
            }
        }
    }

    private class OkButtonActionListener implements ActionListener {
        public void actionPerformed(final ActionEvent e) {
            setVisible(false);
            dispose();
        }
    }
}
