
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



/**
 * The Class OrderController.
 */
public class OrderController{


	//private Order model;
	/** The view. */
	private OrderGUI view;


	/**
	 * Listener for the first remove button.
	 * When pressed it will remove the first panel
	 * from the view.
	 */
	

	class ButtonListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent a) {
			view.hidePanel(1);
		}

	}

	/**
	 * Listener for the second remove button.
	 * When pressed it will remove the second panel
	 * from the view.
	 */
	class ButtonListenerA implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent b) {
			view.hidePanel(2);
		}
	}

	/**
	 * Listener for the third remove button.
	 * When pressed it will remove the third panel
	 * from the view.
	 */
	class ButtonListenerB implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent c) {
			view.hidePanel(3);
		}
	}


/**
 * Instantiates the fields of the Order Controller.
 */
public OrderController() { 
	//model = new Order(); 
	view = new OrderGUI(); 
	view.setVisible(true); 

	view.addButtonListener(new ButtonListener());
	view.addButtonListenerA(new ButtonListenerA());
	view.addButtonListenerB(new ButtonListenerB());

} 

/**
 * Main method which creates the controller.
 */
public static void main(final String[] args) { 
	@SuppressWarnings("unused") 
	OrderController controller = new OrderController(); 
} 

}
