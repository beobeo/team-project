import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.postgresql.util.Base64;

/**
 * Represents a connection to a database, and provides methods for creating and
 * executing queries against it.
 * 
 * @author Michael Winter
 */
final class Database {
    /**
     * A map for storing pooled connections that share the same the
     * authentication credentials.
     */
    private static final Map<String, Database> INSTANCES = new HashMap<>();
    /**
     * The JDBC connection string for the project database. The full host name
     * is used to allow remote connections.
     */
    private static final String URI = "jdbc:postgresql://teaching.cs.rhul.ac.uk:29503/teamproject";
    /**
     * The number of seconds that a connection validation attempt will wait.
     * This value must be greater-than or equal-to zero (0).
     */
    private static final int VALIDATION_TIMEOUT = 1;

    /**
     * Creates a connection to the database, if necessary, using the specified
     * user name and password. Connections returned by this method will be valid
     * at the time of the call.
     * 
     * @param username
     *            the name used to authenticate access to the database.
     * @param password
     *            the password used to authenticate the given user.
     * @return a connection to the database.
     * @throws AuthenticationException
     *             if the user name is unknown or the password is incorrect.
     * @throws ConnectionException
     *             if a connection cannot be established to the database server.
     */
    public static synchronized Database getInstance(final String username,
            final String password) throws ConnectionException {
        // Hash credentials before storing them in the connection map.
        MessageDigest hash = null;
        try {
            hash = MessageDigest.getInstance("SHA-1");
            hash.update((username + '#' + password).getBytes("UTF-8"));
        } catch (final Exception e) {
            // Impossible: Implementation of SHA-1 and support for the UTF-8
            // character set is guaranteed.
        }
        final String encodedCredentials = Base64.encodeBytes(hash.digest());

        // Determine whether there's an existing, valid connection to the
        // database using the provided credentials.
        Database instance = null;
        if (INSTANCES.containsKey(encodedCredentials)) {
            instance = INSTANCES.get(encodedCredentials);
            try {
                if (instance.connection.isClosed()
                        || !instance.connection.isValid(VALIDATION_TIMEOUT)) {
                    instance.connection.close();
                    instance = null;
                }
            } catch (final SQLException e) {
                // None of isClosed(), isValid(int) and close() should raise an
                // exception: VALIDATION_TIMEOUT is an acceptable value, and
                // isClosed() and close() should always succeed.
                throw new Error("Unanticipated SQL exception", e);
            }
        }
        // If there isn't a valid connection, create one and store it.
        if (instance == null) {
            instance = new Database(username, password);
            INSTANCES.put(encodedCredentials, instance);
        }
        return instance;
    }

    /**
     * The database connection wrapped by this object.
     */
    private final Connection connection;

    /**
     * Constructs a connection to the database. Transactions executed over this
     * connection will have a serialisable <a href=
     * "http://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels"
     * >isolation level</a> but by default each statement will be followed by an
     * implicit commit.
     * 
     * @param username
     *            the name used to authenticate access to the database.
     * @param password
     *            the password used to authenticate the given user.
     * @throws AuthenticationException
     *             if the user name is unknown or the password is incorrect.
     * @throws ConnectionException
     *             if a connection cannot be established to the database server.
     * @see #setAutoCommit(boolean)
     */
    private Database(final String username, final String password)
            throws ConnectionException {
        try {
            connection = DriverManager.getConnection(URI, username, password);
        } catch (final SQLException e) {
            switch (e.getSQLState()) {
            case "08001": // Client connection failure
                throw new ConnectionException(
                        "Unable to connect to the database", e);
            case "28000": // Invalid user name
                throw new AuthenticationException("Invalid user name", e);
            case "28P01": // Invalid password
                throw new AuthenticationException("Invalid password", e);
            default:
                // Re-raise the exception
                throw new RuntimeException(e);
            }
        }
        try {
            connection
                    .setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
        } catch (final SQLException e) {
            // This exception should be impossible: the connection will be
            // newly-established (therefore valid) and the DBMS supports
            // serialisable transactions.
            throw new Error("Unanticipated SQL error", e);
        }
    }

    /**
     * Closes the connection. If the connection is already closed, this is a
     * no-op.
     * 
     * <p>
     * An application must explicitly commit or roll-back an active transaction
     * prior to calling the {@code close} method. If the {@code close} method is
     * called and there is an active transaction, the results are
     * implementation-defined.
     */
    public void close() {
        try {
            connection.close();
        } catch (final SQLException e) {
            // Impossible: The close() method is always valid.
            throw new Error(e);
        }
    }
    
    /**
     * Makes all changes made since the previous commit/rollback permanent and
     * releases any database locks currently held over this connection. This
     * method can only be used when auto-commit mode has been disabled.
     * 
     * @throws IllegalStateException
     *             if the database connection is closed or auto-commit mode is
     *             enabled.
     * @throws SQLException
     *             if a database access error occurs.
     */
    public void commit() throws SQLException {
        checkConnectionClosed();
        if (connection.getAutoCommit()) {
            throw new IllegalStateException("Auto-commit mode is enabled");
        }
        connection.commit();
    }
    
    /**
     * Creates a {@link Statement} object for sending SQL statements to the
     * database. SQL statements without parameters are normally executed using
     * {@code Statement} objects but if the same SQL statement is executed many
     * times, it may be more efficient to use a {@link PreparedStatement}
     * object.
     * 
     * <p>
     * Result sets created using the returned {@code Statement} object will by
     * default be type {@link java.sql.ResultSet#TYPE_FORWARD_ONLY
     * TYPE_FORWARD_ONLY} and have a concurrency level of
     * {@link java.sql.ResultSet#CONCUR_READ_ONLY CONCUR_READ_ONLY}.
     * 
     * @return a new {@code Statement} object.
     * @throws IllegalStateException
     *             if the database connection is closed.
     * @throws SQLException
     *             if a database access error occurs.
     */
    public Statement createStatement() throws SQLException {
        checkConnectionClosed();
        return connection.createStatement();
    }
    
    /**
     * Creates a {@link PreparedStatement} object that has the capability to
     * retrieve auto-generated keys.
     * 
     * <p>
     * This method is optimised for handling parametric SQL statements that
     * benefit from pre-compilation.
     * 
     * <p>
     * Result sets created using the returned {@code PreparedStatement} object
     * will by default be type {@link java.sql.ResultSet#TYPE_FORWARD_ONLY
     * TYPE_FORWARD_ONLY} and have a concurrency level of
     * {@link java.sql.ResultSet#CONCUR_READ_ONLY CONCUR_READ_ONLY}.
     * 
     * @param sql
     *            an SQL statement that may contain one or more '?' IN parameter
     *            placeholders.
     * @return a new {@code PreparedStatement} object, containing the
     *         pre-compiled SQL statement, that will have the capability of
     *         returning auto-generated keys.
     * @throws IllegalStateException
     *             if the database connection is closed.
     * @throws SQLException
     *             if a database access error occurs.
     */
    public PreparedStatement prepareStatement(final String sql) throws SQLException {
        checkConnectionClosed();
        return connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
    }
    
    /**
     * Undoes all changes made in the current transaction and releases any
     * database locks currently held on this connection. This method can only be
     * used when auto-commit mode has been disabled.
     * 
     * @throws IllegalStateException
     *             if the database connection is closed or auto-commit mode is
     *             enabled.
     * @throws SQLException
     *             if a database access error occurs.
     */
    public void rollback() throws SQLException {
        checkConnectionClosed();
        if (connection.getAutoCommit()) {
            throw new IllegalStateException("Auto-commit mode is enabled");
        }
        connection.rollback();
    }
       

    /**
     * Sets this connection's auto-commit mode to the given state. If a
     * connection is in auto-commit mode, then all its SQL statements will be
     * executed and committed as individual transactions. Otherwise, its SQL
     * statements are grouped into transactions that are terminated by a call to
     * either {@link #commit()} or {@link #rollback()}. By default, new
     * connections are in auto-commit mode.
     * 
     * <p>
     * The commit occurs when the statement completes. The time when the
     * statement completes depends on the type of SQL Statement:
     * 
     * <ul>
     * <li>For DML statements, such as Insert, Update or Delete, and DDL
     * statements, the statement is complete as soon as it has finished
     * executing.</li>
     * <li>For Select statements, the statement is complete when the associated
     * result set is closed.</li>
     * <li>For statements that return multiple results, the statement is
     * complete when all of the associated result sets have been closed, and all
     * update counts and output parameters have been retrieved.</li>
     * </ul>
     * 
     * <p>
     * If this method is called during a transaction and the auto-commit mode is
     * changed, the transaction is committed. If the auto-commit mode is not
     * changed, the call is a no-op.
     * 
     * @param enabled
     *            {@code true} to turn on auto-commit mode; {@code false}
     *            otherwise.
     * @throws IllegalStateException
     *             if the database connection is closed.
     * @throws SQLException
     *             if a database access error occurs or auto-commit is enabled
     *             during a distributed transaction.
     */
    public void setAutoCommit(final boolean enabled) throws SQLException {
        checkConnectionClosed();
        connection.setAutoCommit(enabled);
    }

    /**
     * Checks whether the connection to the database is still open.
     * 
     * @throws IllegalStateException
     *             if the database connection is closed.
     */
    private void checkConnectionClosed() {
        boolean closed = false;
        try {
            closed = connection.isClosed();
        } catch (final SQLException e) {
            // Impossible: The isClosed() method should never thrown an exception.
            throw new Error(e);
        }
        if (closed) {
            throw new IllegalStateException("The database connection is closed");
        }
    }
}
