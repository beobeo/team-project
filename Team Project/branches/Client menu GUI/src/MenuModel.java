import java.util.Collections;
import java.util.List;

/**
 * Obtains available dishes from the database.
 * 
 * TODO: Provide filtering and sorting.
 * 
 * @author Michael Winter
 */
public class MenuModel {
	/**
	 * The list of dishes last retrieved from the database.
	 */
	private List<Dish> dishes = new java.util.LinkedList<>();
	
	/**
	 * Creates the model.
	 */
	public MenuModel() {
		loadDishes();
	}
	
	/**
	 * Obtained an ordered list of dishes available from the menu. The returned list is immutable.
	 * 
	 * @return an unmodifiable, ordered list of available dishes.
	 */
	public List<Dish> getDishes() {
		return Collections.unmodifiableList(dishes);
	}
	
	
	/**
	 * Generates a dummy set of dishes.
	 * 
	 * TODO: Load items from the database.
	 */
	private void loadDishes() {
		Dish dish = null;
		Ingredient ingredient = null;
		
		dish = new Dish();
		dish.setName("Lasagne 1");
		dish.setDescription("Homemade lasagne. Served with chips and salad.");
		dish.setCategory("Main course");
		dish.setCalories(1080);
		dish.setPrice(8.95f);
		ingredient = new Ingredient();
		ingredient.setName("Minced beef");
		dish.addIngredient(ingredient, 120);
		ingredient = new Ingredient();
		ingredient.setName("Maris Piper potatoes");
		dish.addIngredient(ingredient, 1);
		ingredient = new Ingredient();
		ingredient.setName("Cheddar cheese");
		dish.addIngredient(ingredient, 50);
		ingredient = new Ingredient();
		ingredient.setName("Egg pasta");
		dish.addIngredient(ingredient, 5);
		dishes.add(dish);

		dish = new Dish();
		dish.setName("Lasagne 2");
		dish.setDescription("Homemade lasagne. Served with chips and salad.");
		dish.setCategory("Main course");
		dish.setCalories(1080);
		dish.setPrice(8.95f);
		ingredient = new Ingredient();
		ingredient.setName("Minced beef");
		dish.addIngredient(ingredient, 120);
		dishes.add(dish);

		dish = new Dish();
		dish.setName("Lasagne 3");
		dish.setDescription("Homemade lasagne. Served with chips and salad.");
		dish.setCategory("Main course");
		dish.setCalories(1080);
		dish.setPrice(8.95f);
		ingredient = new Ingredient();
		ingredient.setName("Minced beef");
		dish.addIngredient(ingredient, 120);
		dishes.add(dish);

		dish = new Dish();
		dish.setName("Lasagne 4");
		dish.setDescription("Homemade lasagne. Served with chips and salad.");
		dish.setCategory("Main course");
		dish.setCalories(1080);
		dish.setPrice(8.95f);
		ingredient = new Ingredient();
		ingredient.setName("Minced beef");
		dish.addIngredient(ingredient, 120);
		dishes.add(dish);
	}
}
