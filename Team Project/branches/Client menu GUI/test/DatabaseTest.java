import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

import java.sql.SQLException;

import org.junit.Test;

/**
 * Unit tests for the {@link Database} class.
 * 
 * @author Michael Winter
 */
public class DatabaseTest {
    /**
     * The user name used for authenticating connections to the database.
     */
    private static final String USER = "zyvc215";
    /**
     * The password used for authenticating connections to the database.
     */
    private static final String PASSWORD = "IYIP1845";

    /**
     * Tests that a connection with valid credentials return an instance without
     * raising any exceptions.
     * 
     * <p>
     * Note: It's assumed that incorrect credentials will be handled correctly
     * by the DBMS.
     * 
     * @throws ConnectionException
     *             if a connection to the database cannot be established for
     *             network reasons.
     */
    @Test
    public void establishConnection() throws ConnectionException {
        Database.getInstance(USER, PASSWORD);
    }

    /**
     * Tests that repeating a connection attempt with the same credentials
     * results in the same object: that is, connections are saved.
     * 
     * @throws ConnectionException
     *             if a connection to the database cannot be established for
     *             network reasons.
     */
    @Test
    public void pooledConnections() throws ConnectionException {
        final Database db = Database.getInstance(USER, PASSWORD);
        assertSame(db, Database.getInstance(USER, PASSWORD));
    }

    /**
     * Tests that returned connections are valid. If a connection is closed, a
     * new one should be established.
     * 
     * @throws ConnectionException
     *             if a connection to the database cannot be established for
     *             network reasons.
     */
    @Test
    public void recreateClosedConnection() throws ConnectionException {
        final Database closedInstance = Database.getInstance(USER, PASSWORD);
        closedInstance.close();
        assertNotSame(closedInstance, Database.getInstance(USER, PASSWORD));
    }

    /**
     * Tests that attempting to create a {@link java.sql.Statement Statement} on
     * a closed connection raises an {@code IllegalStateException}.
     * 
     * @throws ConnectionException
     *             if a connection to the database cannot be established for
     *             network reasons.
     * @throws SQLException
     *             if a database access error occurs.
     */
    @Test(expected = IllegalStateException.class)
    public void statementCreationWhileClosedRaisesException()
            throws ConnectionException, SQLException {
        final Database closedInstance = Database.getInstance(USER, PASSWORD);
        closedInstance.close();
        closedInstance.createStatement();
    }

    /**
     * Tests that attempting to create a {@link java.sql.PreparedStatement
     * PreparedStatement} on a closed connection raises an
     * {@code IllegalStateException}.
     * 
     * @throws ConnectionException
     *             if a connection to the database cannot be established for
     *             network reasons.
     * @throws SQLException
     *             if a database access error occurs.
     */
    @Test(expected = IllegalStateException.class)
    public void preparedStatementCreationWhileClosedRaisesException()
            throws ConnectionException, SQLException {
        final Database closedInstance = Database.getInstance(USER, PASSWORD);
        closedInstance.close();
        closedInstance.prepareStatement("SELECT 1;");
    }
    
    /**
     * Tests that attempting to commit on a closed connection raises an
     * {@code IllegalStateException}.
     * 
     * @throws ConnectionException
     *             if a connection to the database cannot be established for
     *             network reasons.
     * @throws SQLException
     *             if a database access error occurs.
     */
    @Test(expected = IllegalStateException.class)
    public void commitWhileClosedRaisesException() throws ConnectionException,
            SQLException {
        final Database closedInstance = Database.getInstance(USER, PASSWORD);
        closedInstance.close();
        closedInstance.commit();
    }
    
    /**
     * Tests that attempting to commit while auto-commit is enabled raises an
     * {@code IllegalStateException}.
     * 
     * @throws ConnectionException
     *             if a connection to the database cannot be established for
     *             network reasons.
     * @throws SQLException
     *             if a database access error occurs.
     */
    @Test(expected = IllegalStateException.class)
    public void commitWhileAutoCommittingRaisesException() throws ConnectionException,
            SQLException {
        final Database instance = Database.getInstance(USER, PASSWORD);
        instance.commit();
    }

    /**
     * Tests that attempting to rollback on a closed connection raises an
     * {@code IllegalStateException}.
     * 
     * @throws ConnectionException
     *             if a connection to the database cannot be established for
     *             network reasons.
     * @throws SQLException
     *             if a database access error occurs.
     */
    @Test(expected = IllegalStateException.class)
    public void rollbackWhileClosedRaisesException() throws ConnectionException,
            SQLException {
        final Database closedInstance = Database.getInstance(USER, PASSWORD);
        closedInstance.close();
        closedInstance.rollback();
    }

    /**
     * Tests that attempting to rollback while auto-commit is enabled raises an
     * {@code IllegalStateException}.
     * 
     * @throws ConnectionException
     *             if a connection to the database cannot be established for
     *             network reasons.
     * @throws SQLException
     *             if a database access error occurs.
     */
    @Test(expected = IllegalStateException.class)
    public void rollbackWhileAutoCommittingRaisesException() throws ConnectionException,
            SQLException {
        final Database instance = Database.getInstance(USER, PASSWORD);
        instance.rollback();
    }
}
