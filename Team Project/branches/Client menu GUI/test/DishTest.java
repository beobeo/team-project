import static org.junit.Assert.*;

import org.junit.Test;


public class DishTest {
	/**
	 * A visible dish should be available.
	 */
	@Test
	public void visibleDishIsAvailable() {
		final Dish dish = new Dish();
		assertTrue(dish.isAvailable());
	}
	
	/**
	 * If a dish is marked as not visible, it should not be considered available.
	 */
	@Test
	public void invisibleDishIsNotAvailable() {
		final Dish dish = new Dish();
		dish.setVisible(false);
		assertFalse(dish.isAvailable());
	}
	
	/**
	 * If an ingredient is not in stock, it should not be considered available.
	 */
	@Test
	public void dishWithInsufficientIngredientsIsNotAvailable() {
		final Dish dish = new Dish();
		final Ingredient ingredient = new Ingredient();
		ingredient.setStock(0);
		dish.addIngredient(ingredient, 1);
		assertFalse(dish.isAvailable());
	}

	/**
	 * All ingredients necessary to prepare a dish must be in stock for for that dish to be
	 * considered available.
	 */
	@Test
	public void dishWithSomeInsufficientIngredientsIsNotAvailable() {
		final Dish dish = new Dish();
		final Ingredient stockedIngredient = new Ingredient();
		final Ingredient unstockedIngredient = new Ingredient();
		stockedIngredient.setStock(1);
		unstockedIngredient.setStock(0);
		dish.addIngredient(stockedIngredient, 1);
		dish.addIngredient(unstockedIngredient, 1);
		assertFalse(dish.isAvailable());
	}

	/**
	 * If a dish is visible and all of its ingredients are in stock, it should be considered
	 * available.
	 */
	@Test
	public void dishWithSufficientIngredientsIsAvailable() {
		final Dish dish = new Dish();
		final Ingredient stockedIngredient = new Ingredient();
		stockedIngredient.setStock(1);
		dish.addIngredient(stockedIngredient, 1);
		assertTrue(dish.isAvailable());
	}
}
