import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;


public class OrderTesting {


	@Test
	public void test() {
		
		Order orderTest = new Order();
		Dish dish = new Dish();
		Dish dish1 = new Dish();
		dish.setPrice(3.40f);
		dish1.setPrice(2.40f);
		
		orderTest.addDish(dish);
		orderTest.addDish(dish1);
		
		assertEquals("Test for correct addition of dish prices", 5.80 , orderTest.getTotalPrice(), 0.01f);
		
	}

}
