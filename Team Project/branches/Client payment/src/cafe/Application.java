package cafe;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

/**
 * 
 * @author Michael Winter
 */
public abstract class Application {
	/**
	 * A logging instance used to record errors and debugging information during
	 * execution.
	 */
	private static final Logger LOGGER = Logger.getLogger(Application.class.getName());
	/**
	 * The running instance of this application.
	 */
	private static Application application;
	
	
	/**
	 * Initialises and runs an instance of the given application. This method
	 * ensures initialisation occurs on Swing's event-dispatching thread.
	 *
	 * @param <T>
	 *            a subclass of {@code Application}.
	 * @param applicationClass
	 *            the application implementation to launch.
	 * @param args
	 *            the command-line arguments received during invocation.
	 */
	public static synchronized <T extends Application> void launch(
			final Class<T> applicationClass, final String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					application = applicationClass.newInstance();
					application.initialise(args);
					application.run();
				} catch (final Exception e) {
					final String message = String.format("Application %s failed to launch",
							applicationClass);
					LOGGER.log(Level.SEVERE, message, e);
					throw new Error(message, e);
				}
			}
		});
	}
	
	/**
	 * Returns an instance of this application.
	 * 
	 * @return the application.
	 * @throws IllegalStateException if the application has not yet been initialised.
	 */
	public static synchronized Application getInstance() {
		if (application == null) {
			throw new IllegalStateException("Application not yet initialised");
		}
		return application;
	}
	
	
	/**
	 * Prepares any resources required prior to starting the application. The
	 * default implementation of this method does nothing.
	 * 
	 * @param args
	 *            the command-line arguments received during invocation.
	 */
	protected void initialise(final String[] args) {
	}

	/**
     * Prepares the UI and shows the initial view.
     */
	protected abstract void run();
}
