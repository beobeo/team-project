package cafe.client.payment;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;


/**
 * A controller for the payment GUI. It observes user behaviour and directs behaviour
 * accordingly.
 * 
 * @author Michael Winter
 */
public final class PaymentController {
	/**
	 * The client payment model.
	 */
	private PaymentModel model;
	/**
	 * The client payment view.
	 */
	private PaymentView view;
	/**
	 * A list of event listeners that observe payment completion events.
	 */
	private final List<PaymentListener> listeners = new ArrayList<>();
	/**
	 * Tracks whether the payment method is by cash ({@code false}) or by card ({@code true}).
	 */
	private boolean usingCard = false;

	
	/**
	 * Constructs a controller for the given model and view.
	 * 
	 * @param model the model.
	 * @param view the view.
	 */
	public PaymentController(final PaymentModel model, final PaymentView view) {
		this.model = model;
		this.view = view;
		
		this.view.setPrice(model.getTotalPrice());
		this.view.addPayButtonListener(new PayButtonListener());
		this.view.addPaymentMethodButtonListener(new PaymentMethodListener());
	}

	/**
	 * Shows the view associated with this controller.
	 */
	public void showView() {
		view.setVisible(true);
	}

	/**
	 * Hides the view associated with this controller.
	 */
	public void hideView() {
		view.setVisible(false);
	}

	/**
	 * Adds an {@linkplain PaymentListener payment completion listener} to the
	 * listeners observing the end of the payment process.
	 * 
	 * @param listener
	 *            the listener.
	 */
	public void addPaymentCompletedListener(final PaymentListener listener) {
		listeners.add(listener);
	}
	
	
	/**
	 * Notifies interested parties that payment has been completed successfully.
	 * The paid-for sitting is passed as the source object.
	 * 
	 * @param e
	 *            the event.
	 */
	private void firePaymentCompletedEvent(final PaymentEvent e) {
		for (PaymentListener listener : listeners) {
			listener.paymentCompleted(e);
		}
	}
	
		
	/**
	 * Listens for button click events originating from the Pay button.
	 */
	private final class PayButtonListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			if (view.isInputValid()) {
				final String cardNumber = view.getCardNumber();
				final PaymentEvent event = new PaymentEvent(model.getSitting(),
						cardNumber);
				model.addPayment(cardNumber);
				view.setVisible(false);
				view.dispose();
				firePaymentCompletedEvent(event);
			} else {
				JOptionPane.showMessageDialog(
						view,
						"The card number you have entered is invalid."
								+ "\nPlease supply your 16-digit card number"
								+ " or choose cash payment.",
						"Invalid card number",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
		
	/**
	 * Listens for button click events originating from the Payment Method radio
	 * buttons, enabling and disabling associated inputs as appropriate.
	 */
	private final class PaymentMethodListener implements ActionListener {
	@Override
		public void actionPerformed(final ActionEvent e) {
			if (e.getActionCommand().equals("Card")) {
				usingCard = true;
			} else {
				usingCard = false;
			}
			view.enableCardNumberField(usingCard);
		}
	}
}
