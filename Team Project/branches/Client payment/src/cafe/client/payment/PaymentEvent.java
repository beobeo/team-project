package cafe.client.payment;

import java.util.EventObject;

import cafe.Sitting;

/**
 * {@code PaymentEvent} is used to notify interested parties that payment for a
 * sitting has been completed.
 * 
 * @author Michael Winter
 */
public final class PaymentEvent extends EventObject {
	/**
	 * Unique serialisation identifier.
	 */
	private static final long serialVersionUID = 5717996123000761761L;
	/**
	 * The card number used in the transaction.
	 */
	private final String cardNumber;

	
	/**
	 * Constructs a payment completion event.
	 * 
	 * @param source
	 *            The {@link Sitting} on which the {@code PaymentEvent} initially
	 *            occurred.
	 * @param cardNumber
	 *            The credit card number used during the transaction, or {@code null}
	 *            if payment was by cash.
	 */
	public PaymentEvent(final Sitting source, final String cardNumber) {
		super(source);
		this.cardNumber = cardNumber;
	}
	
	/**
	 * Returns whether the payment was made by card.
	 * 
	 * @return {@code true} if the payment was by card; {@code false} if it was using cash.
	 */
	public boolean usingCard() {
		return cardNumber != null;
	}
	
	/**
	 * Returns the card number used in the transaction, if applicable.
	 * 
	 * @return the card number, or {@code null} if payment was using cash.
	 */
	public String getCardNumber() {
		return cardNumber;
	}
}
