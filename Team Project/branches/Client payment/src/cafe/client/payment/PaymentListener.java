package cafe.client.payment;

import java.util.EventListener;

/**
 * Defines the interface for an object that listens for completion of the payment process.
 * 
 * @author Michael Winter
 */
public interface PaymentListener extends EventListener {
	/**
	 * Invoked after payment has been completed for a sitting.
	 * 
	 * @param e
	 *            a payment completed event.
	 */
	void paymentCompleted(PaymentEvent e);
}
