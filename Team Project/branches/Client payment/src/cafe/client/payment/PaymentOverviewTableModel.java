package cafe.client.payment;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.table.AbstractTableModel;

import cafe.Dish;

/**
 * Provides the mechanism for populating tabular data within the payment view
 * used by clients.
 * 
 * @author Michael Winter
 */
public final class PaymentOverviewTableModel extends AbstractTableModel {
	/**
	 * Unique serialisation identifier.
	 */
	private static final long serialVersionUID = -5135992384262588382L;
	/**
	 * The number of columns represented by the model.
	 */
	private static final int COLUMN_COUNT = 3;
	/**
	 * The heading for each column represented by the model.
	 */
	private static final String[] COLUMN_HEADINGS = { "Dish", "Quantity",
			"Price (£)" };
	/**
	 * A mapping of the dishes ordered during the sitting to the total quantity ordered.
	 */
	private Map<Dish, Integer> orderedDishes = new HashMap<>();

	/**
	 * Constructs an {@code PaymentOverviewTableModel} populated using the specified
	 * {@code Map}.
	 * 
	 * @param dishQuantities
	 *            a mapping of the dishes ordered during the sitting to the total quantity ordered.
	 */
	public PaymentOverviewTableModel(final Map<Dish, Integer> dishQuantities) {
		orderedDishes.putAll(dishQuantities);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return orderedDishes.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return COLUMN_COUNT;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		final Iterator<Entry<Dish, Integer>> iterator = orderedDishes.entrySet().iterator();
		for (int i = 0; i < rowIndex; ++i) {
			iterator.next();
		}
		final Entry<Dish, Integer> item = iterator.next();
		
		Object value;
		switch (columnIndex) {
		case 0: // Dish
			value = item.getKey().getName();
			break;
		case 1: // Quantity
			value = item.getValue();
			break;
		case 2: // Price
			value = String.format("%03.2f", item.getKey().getPrice() * item.getValue());
			break;
		default:
			value = null;
		}
		return value;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(final int column) {
		return COLUMN_HEADINGS[column];
	}
	
	/**
	 * Sets the mapping of dishes ordered to the number of each dish ordered.
	 * 
	 * @param map the mapping.
	 */
	public void setDishQuantities(final Map<Dish, Integer> map) {
		orderedDishes.clear();
		orderedDishes.putAll(map);
	}
}
