package cafe.client.payment;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

import javax.swing.ButtonGroup;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;

/**
 * Presents a graphical window to the customer for payment.
 * 
 * @author Michael Winter
 */
public final class PaymentView extends JDialog {
	/**
	 * Unique serialisation identifier.
	 */
	private static final long serialVersionUID = 8830889410240081933L;
	/**
	 * The table in which order information will be displayed.
	 */
	private JTable itemTable;
	/**
	 * A radio button that, if selected, indicates that the client wants to pay
	 * by card.
	 */
	private JRadioButton rdbtnCard;
	/**
	 * A radio button that, if selected, indicates that the client wants to pay
	 * by cash.
	 */
	private JRadioButton rdbtnCash;
	/**
	 * The label used to display the formatted total price for the sitting.
	 */
	private JLabel lblPrice;
	/**
	 * A text input field used to obtain the credit card number of the client
	 * should they opt to pay by card.
	 */
	private JTextField txtCardNumber;
	/**
	 * The button used to proceed with payment and mark the sitting as complete.
	 */
	private JButton btnPay;

	/**
	 * Create the frame.
	 * 
	 * @param tableModel
	 *            the {@code TableModel} used to access the database.
	 */
	public PaymentView(final TableModel tableModel) {
		initComponents();
		itemTable.setModel(tableModel);
	}

	/**
	 * Sets whether the card number field is enabled.
	 * 
	 * @param enabled
	 *            {@code true} if the field should be enabled; {@code false}
	 *            otherwise.
	 */
	public void enableCardNumberField(final boolean enabled) {
		txtCardNumber.setEnabled(enabled);
	}

	/**
	 * Returns the card number provided by the user.
	 * 
	 * @return the entered card number.
	 */
	public String getCardNumber() {
		return txtCardNumber.getText().replaceAll("[^0-9]", "");
	}

	/**
	 * Returns whether the current input is valid.
	 * 
	 * @return {@code true} if all inputs are valid; {@code false} otherwise.
	 */
	public boolean isInputValid() {
		return rdbtnCash.isSelected()
				|| CardNumberVerifier.isValid(getCardNumber());
	}

	/**
	 * Adds an action listener to the Pay button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public void addPayButtonListener(final ActionListener listener) {
		btnPay.addActionListener(listener);
	}

	/**
	 * Adds an action listener to the Payment Method radio buttons.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public void addPaymentMethodButtonListener(final ActionListener listener) {
		rdbtnCard.addActionListener(listener);
		rdbtnCash.addActionListener(listener);
	}

	/**
	 * Sets the price an order costs in the price label.
	 * 
	 * @param price
	 *            the price to display.
	 */
	public void setPrice(final float price) {
		lblPrice.setText(String.format("£%03.2f", price));
	}

	/**
	 * Initialises UI components.
	 */
	private void initComponents() {
		// CHECKSTYLE:OFF
		setTitle("Payment");
		setModal(true);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setType(Type.UTILITY);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 402, 353);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		final JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);

		itemTable = new JTable();
		itemTable.setFillsViewportHeight(true);
		scrollPane.setViewportView(itemTable);

		final JPanel controlsPanel = new JPanel();
		controlsPanel.setBorder(new EmptyBorder(5, 0, 0, 0));
		contentPane.add(controlsPanel, BorderLayout.SOUTH);
		GridBagLayout controlsPanelGridBagLayout = new GridBagLayout();
		controlsPanelGridBagLayout.columnWidths = new int[] { 90, 27, 100, 0 };
		controlsPanelGridBagLayout.rowHeights = new int[] { 23, 0, 0, 0, 0 };
		controlsPanelGridBagLayout.columnWeights = new double[] { 0.0, 1.0,
				0.0, Double.MIN_VALUE };
		controlsPanelGridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0,
				0.0, Double.MIN_VALUE };
		controlsPanel.setLayout(controlsPanelGridBagLayout);

		JLabel lblTotal = new JLabel("Total:");
		GridBagConstraints gbc_lblTotal = new GridBagConstraints();
		gbc_lblTotal.anchor = GridBagConstraints.EAST;
		gbc_lblTotal.insets = new Insets(0, 0, 5, 5);
		gbc_lblTotal.gridx = 0;
		gbc_lblTotal.gridy = 0;
		controlsPanel.add(lblTotal, gbc_lblTotal);

		lblPrice = new JLabel("£0.00");
		GridBagConstraints gbc_lblPrice = new GridBagConstraints();
		gbc_lblPrice.anchor = GridBagConstraints.WEST;
		gbc_lblPrice.insets = new Insets(0, 0, 5, 5);
		gbc_lblPrice.gridx = 1;
		gbc_lblPrice.gridy = 0;
		controlsPanel.add(lblPrice, gbc_lblPrice);

		JLabel lblPaymentMethod = new JLabel("Payment method:");
		GridBagConstraints gbc_lblPaymentMethod = new GridBagConstraints();
		gbc_lblPaymentMethod.anchor = GridBagConstraints.EAST;
		gbc_lblPaymentMethod.insets = new Insets(0, 0, 5, 5);
		gbc_lblPaymentMethod.gridx = 0;
		gbc_lblPaymentMethod.gridy = 1;
		controlsPanel.add(lblPaymentMethod, gbc_lblPaymentMethod);

		final ButtonGroup paymentMethodGroup = new ButtonGroup();
		rdbtnCash = new JRadioButton("Cash");
		paymentMethodGroup.add(rdbtnCash);
		rdbtnCash.setSelected(true);
		rdbtnCash.setMnemonic('C');
		GridBagConstraints gbc_rdbtnCash = new GridBagConstraints();
		gbc_rdbtnCash.anchor = GridBagConstraints.WEST;
		gbc_rdbtnCash.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnCash.gridx = 1;
		gbc_rdbtnCash.gridy = 1;
		controlsPanel.add(rdbtnCash, gbc_rdbtnCash);

		rdbtnCard = new JRadioButton("Credit/Debit card");
		rdbtnCard.setActionCommand("Card");
		paymentMethodGroup.add(rdbtnCard);
		rdbtnCard.setMnemonic('D');
		GridBagConstraints gbc_rdbtnCard = new GridBagConstraints();
		gbc_rdbtnCard.anchor = GridBagConstraints.WEST;
		gbc_rdbtnCard.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnCard.gridx = 1;
		gbc_rdbtnCard.gridy = 2;
		controlsPanel.add(rdbtnCard, gbc_rdbtnCard);

		final JLabel lblCardNumber = new JLabel("Card number:");
		lblCardNumber.setDisplayedMnemonic('n');
		GridBagConstraints gbc_lblCardNumber = new GridBagConstraints();
		gbc_lblCardNumber.insets = new Insets(0, 0, 0, 5);
		gbc_lblCardNumber.anchor = GridBagConstraints.EAST;
		gbc_lblCardNumber.gridx = 0;
		gbc_lblCardNumber.gridy = 3;
		controlsPanel.add(lblCardNumber, gbc_lblCardNumber);

		txtCardNumber = new JTextField();
		lblCardNumber.setLabelFor(txtCardNumber);
		txtCardNumber.setEnabled(false);
		txtCardNumber.setText("");
		txtCardNumber.setColumns(19);
		txtCardNumber.setInputVerifier(new CardNumberVerifier());
		GridBagConstraints gbc_txtCardNumber = new GridBagConstraints();
		gbc_txtCardNumber.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtCardNumber.insets = new Insets(0, 20, 0, 5);
		gbc_txtCardNumber.gridx = 1;
		gbc_txtCardNumber.gridy = 3;
		controlsPanel.add(txtCardNumber, gbc_txtCardNumber);

		btnPay = new JButton("Pay");
		btnPay.setMnemonic('P');
		GridBagConstraints gbc_btnPay = new GridBagConstraints();
		gbc_btnPay.gridheight = 4;
		gbc_btnPay.gridx = 2;
		gbc_btnPay.gridy = 0;
		controlsPanel.add(btnPay, gbc_btnPay);
		// CHECKSTYLE:ON
	}

	/**
	 * Creates a debit/credit card number formatter for use in displaying and
	 * validating user input.
	 * 
	 * @return a card number formatter.
	 */
	private static class CardNumberVerifier extends InputVerifier {
		/**
		 * The pattern that a card number should match.
		 */
		private static final Pattern PATTERN = Pattern
				.compile("\\d{4}\\s*\\d{4}\\s*\\d{4}\\s*\\d{4}");
		/**
		 * The background colour used to indicate a badly-formatted input value.
		 */
		private static final Color WARNING = Color
				.getHSBColor(0.0f, 0.6f, 0.9f);
		/**
		 * The background colour that was used prior to setting the warning
		 * colour.
		 */
		private Color previousBackground = null;

		/**
		 * Compares the given string against the expected format.
		 * 
		 * @param string
		 *            the string to examine.
		 * @return {@code true} if the string matches the expected format;
		 *         {@code false} otherwise.
		 */
		public static boolean isValid(final String string) {
			return PATTERN.matcher(string).matches();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see javax.swing.InputVerifier#verify(javax.swing.JComponent)
		 */
		@Override
		public boolean verify(final JComponent input) {
			final JTextField textField = (JTextField) input;
			if (!isValid(textField.getText())) {
				if (previousBackground == null) {
					previousBackground = textField.getBackground();
				}
				textField.setBackground(WARNING);
				return false;
			} else if (previousBackground != null) {
				textField.setBackground(previousBackground);
				previousBackground = null;
			}
			return true;
		}
	}
}