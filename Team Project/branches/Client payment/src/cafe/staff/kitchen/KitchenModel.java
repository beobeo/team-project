package cafe.staff.kitchen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import cafe.Order;
import cafe.OrderEvent;
import cafe.OrderEventListener;
import cafe.dao.OrderDAO;

/**
 * Obtains orders from the database.
 * 
 * @author Michael Winter
 */
public final class KitchenModel {
	/**
	 * The default frequency, in seconds, at which the model re-queries the
	 * database.
	 */
	private static final int DEFAULT_REFRESH_INTERVAL = 15;
	/**
	 * A list of event listeners that observe order change events caused by the
	 * Add to Order buttons.
	 */
	private final List<OrderEventListener> listeners = new ArrayList<>();
	/**
	 * The list of dishes last retrieved from the database.
	 */
	private final List<Order> orders = new ArrayList<>();
	/**
	 * An Order data access object.
	 */
	private OrderDAO orderDataSource;
	/**
	 * The current refresh interval.
	 */
	private int refreshInterval = 0;
	/**
	 * The scheduler services used to perform refreshes.
	 */
	private ScheduledExecutorService refreshScheduler = Executors
			.newSingleThreadScheduledExecutor();
	/**
	 * The task used for scheduling updates.
	 */
	private ScheduledFuture<?> refreshTask;

	/**
	 * Creates the Kitchen model.
	 * 
	 * @param orderDataSource
	 *            the data access object used to obtain order information.
	 */
	public KitchenModel(final OrderDAO orderDataSource) {
		this.orderDataSource = orderDataSource;
		setRefreshInterval(DEFAULT_REFRESH_INTERVAL);
	}

	/**
	 * Obtains an ordered list of OrderItem objects. The returned list is
	 * immutable.
	 * 
	 * @return an unmodifiable, ordered list of current Orders.
	 */
	public List<Order> getOrders() {
		return Collections.unmodifiableList(orders);
	}

	/**
	 * Updates the list of currently active orders.
	 */
	public void refresh() {
		refreshScheduler.execute(new Runnable() {
			@Override
			public void run() {
				doRefresh();
			}
		});
	}

	/**
	 * Updates the specified order within the data store.
	 * 
	 * @param order
	 *            the order to update.
	 */
	public void updateOrder(final Order order) {
		orderDataSource.updateOrder(order);
		fireOrderChangedEvent(new OrderEvent(order));
	}

	/**
	 * Sets the interval at which the model re-queries the database for changes
	 * in order state. If the interval is set to {@code 0}, future updates are
	 * disabled.
	 * 
	 * @param seconds
	 *            the time between updates (in seconds); or {@code 0} to disable
	 *            automatic updates.
	 * @throws IllegalArgumentException
	 *             if {@code seconds} is less than zero.
	 */
	public void setRefreshInterval(final int seconds) {
		if (seconds < 0) {
			throw new IllegalArgumentException();
		}
		if (seconds != refreshInterval) {
			long remainingTime = 0;
			if (refreshTask != null) {
				remainingTime = refreshTask.getDelay(TimeUnit.SECONDS);
				refreshTask.cancel(false);
			}

			refreshInterval = seconds;
			if (refreshInterval > 0) {
				refreshTask = refreshScheduler.scheduleAtFixedRate(
						new Runnable() {
							@Override
							public void run() {
								doRefresh();
							}
						}, Math.max(refreshInterval - remainingTime, 0),
						refreshInterval, TimeUnit.SECONDS);
			}
		}
	}

	/**
	 * Adds an {@linkplain OrderEventListener order event listener} for
	 * {@code OrderEvent}s fired after the list of active orders changes in some
	 * way.
	 * 
	 * @param listener
	 *            the listener.
	 */
	public void addOrderModelListener(final OrderEventListener listener) {
		listeners.add(listener);
	}

	/**
	 * Notifies interested parties that an order has become available through
	 * this model. The added order is passed as the source object.
	 * 
	 * @param e
	 *            the event.
	 */
	void fireOrderAddedEvent(final OrderEvent e) {
		for (OrderEventListener listener : listeners) {
			listener.orderAdded(e);
		}
	}

	/**
	 * Notifies interested parties that an existing order has changed state. The
	 * changed order is passed as the source object.
	 * 
	 * @param e
	 *            the event.
	 */
	void fireOrderChangedEvent(final OrderEvent e) {
		for (OrderEventListener listener : listeners) {
			listener.orderChanged(e);
		}
	}

	/**
	 * Notifies interested parties that an existing order has been removed. The
	 * removed order is passed as the source object.
	 * 
	 * @param e
	 *            the event.
	 */
	void fireOrderRemovedEvent(final OrderEvent e) {
		for (OrderEventListener listener : listeners) {
			listener.orderRemoved(e);
		}
	}

	/**
	 * Performs a comparison between the existing list of orders and a
	 * newly-obtained list from the backing data store looking for new, removed
	 * and altered items.
	 */
	private void doRefresh() {
		final List<Order> updatedOrders = orderDataSource
				.selectUndeliveredOrders();
		final Iterator<Order> existingOrderIterator = orders.iterator();
		final Iterator<Order> updatedOrderIterator = updatedOrders.iterator();

		if (updatedOrderIterator.hasNext() && existingOrderIterator.hasNext()) {
			Order existingOrder = existingOrderIterator.next();
			Order updatedOrder = updatedOrderIterator.next();
			do {
				if (existingOrder.equals(updatedOrder)) {
					if (existingOrder.getProgress() != updatedOrder
							.getProgress()) {
						fireOrderChangedEvent(new OrderEvent(updatedOrder));
					}
					if (existingOrderIterator.hasNext()
							&& updatedOrderIterator.hasNext()) {
						existingOrder = existingOrderIterator.next();
						updatedOrder = updatedOrderIterator.next();
					}
				} else {
					// If this existing order doesn't appear at this point in
					// the updated set, it was removed; otherwise, the updated
					// order was inserted.
					if (existingOrder.compareTo(updatedOrder) < 0) {
						fireOrderRemovedEvent(new OrderEvent(existingOrder));
						if (existingOrderIterator.hasNext()) {
							existingOrder = existingOrderIterator.next();
						}
					} else {
						fireOrderAddedEvent(new OrderEvent(updatedOrder));
						if (updatedOrderIterator.hasNext()) {
							updatedOrder = updatedOrderIterator.next();
						}
					}
				}
			} while (updatedOrderIterator.hasNext()
					&& existingOrderIterator.hasNext());
		}

		while (existingOrderIterator.hasNext()) {
			fireOrderRemovedEvent(new OrderEvent(existingOrderIterator.next()));
		}

		while (updatedOrderIterator.hasNext()) {
			fireOrderAddedEvent(new OrderEvent(updatedOrderIterator.next()));
		}

		orders.clear();
		orders.addAll(updatedOrders);
	}
}
