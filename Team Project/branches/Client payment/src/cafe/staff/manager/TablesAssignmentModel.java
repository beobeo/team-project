package cafe.staff.manager;

import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 * Provides the mechanism for populating the table within the Tables Assignment
 * view used by the manager(s).
 * 
 * @author Robert Kardjaliev
 */
public final class TablesAssignmentModel extends AbstractTableModel {
	/**
	 * Unique serialisation identifier.
	 */
	private static final long serialVersionUID = -6314730557977100210L;
	/**
	 * A list of the {@code Table} numbers.
	 */
	private List<Integer> tableNumbers;
	/** 
	 * A list of the names of {@link Waiter}s. 
	 */
	private List<String> waiters;

	/**
	 * Constructs an {@code TablesAssignmentModel} using the specified
	 * lists of {@code Waiter} names and {@code Table} numbers.
	 * 
	 * @param tableNumbers
	 *            the list of table numbers
	 * @param waiters
	 * 			the list of waiter names
	 */
	public TablesAssignmentModel(final List<Integer> tableNumbers, final List<String> waiters) {
		this.tableNumbers = tableNumbers;
		this.waiters = waiters;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return tableNumbers.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return 2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	//CHECKSTYLE:OFF
	@Override
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		return (columnIndex == 0) ? tableNumbers.get(rowIndex) : 
			waiters.get(rowIndex);
	}

	@Override
	public String getColumnName(final int column) {
		return (column == 0) ? "Table Number" : "Waiter Name";
	}

}