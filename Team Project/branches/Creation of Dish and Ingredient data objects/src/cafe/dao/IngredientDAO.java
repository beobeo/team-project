package cafe.dao;

import java.io.IOException;

import cafe.Ingredient;

/**
 * A data access object used to manipulate persisted {@link Ingredient} objects.
 * 
 * TODO: Wrap IOException exceptions as another, more friendly exception where
 * possible. Lookup-type methods should only need to raise an exception if the
 * connection is lost.
 * 
 * @author Michael Winter
 */
public interface IngredientDAO {
    /**
     * Given an identifier, this method loads and returns a new instance of the
     * specified {@link Ingredient}.
     * 
     * @param id
     *            the identifier.
     * @return the specified {@code Ingredient}.
     * @throws IOException
     *             if an error occurs while accessing the data store.
     */
    Ingredient findIngredient(int id) throws IOException;
}
