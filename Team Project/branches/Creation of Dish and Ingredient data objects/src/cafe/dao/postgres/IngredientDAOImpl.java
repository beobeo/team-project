package cafe.dao.postgres;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import cafe.Ingredient;
import cafe.dao.AuthenticationException;
import cafe.dao.IngredientDAO;
import cafe.dao.JdbcConnectionFactory;

/**
 * A data access object used to manipulate {@link Ingredient} objects that have
 * persisted to a PostgreSQL database.
 * 
 * TODO: Wrap IOException exceptions as another, more friendly exception where
 * possible. Lookup-type methods should only need to raise an exception if the
 * connection is lost.
 * 
 * @author Michael Winter
 */
public final class IngredientDAOImpl implements IngredientDAO {
    /**
     * The source of JDBC connections used to perform queries against the
     * underlying database.
     */
    private JdbcConnectionFactory connectionFactory;
    
    /**
     * Constructs a data access object for {@link Ingredient}s that will
     * operate over the given connection.
     * 
     * @param connectionFactory
     *            the factory object that will provide a database connection.
     * @throws NullPointerException
     *             if the specified factory is {@code null}.
     */
    public IngredientDAOImpl(final JdbcConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.IngredientDAO#findIngredient(int)
     */
    @Override
    public Ingredient findIngredient(final int id) throws IOException {
        Connection connection = null;
        try {
            connection = connectionFactory.getConnection();
        } catch (final AuthenticationException e) {
            // Ignore: Authentication would have been verified earlier in the program.
        }
        Ingredient ingredient = null;
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT name, cost, stock FROM ingredient WHERE id = ?")) {
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                ingredient = new Ingredient(id, result.getString("name"),
                        result.getFloat("cost"), result.getInt("stock"));
            }
        } catch (final SQLException e) {
            throw new IOException(e);
        }
        return ingredient;
    }
}
