package cafe.dao;

import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

import org.junit.Test;

/**
 * Unit tests for the {@link DAOFactory} class.
 * 
 * @author Michael Winter
 */
public final class DAOFactoryTest {
    /**
     * Tests that an instance of a valid implementation can be created without
     * raising any exceptions.
     * 
     * @throws ConnectionException
     *             if a connection to the database cannot be established for
     *             network reasons.
     * @throws AuthenticationException 
     */
    @Test
    public void instantiateFactory() throws ConnectionException, AuthenticationException {
        DAOFactory.getInstance(null, null, MockDAOFactoryImpl.class);
    }

    /**
     * Tests that repeating an instantiation attempt with the same
     * implementation type results in the same object: that is, implementations
     * are caches.
     * 
     * @throws ConnectionException
     *             if a connection to the database cannot be established for
     *             network reasons.
     * @throws AuthenticationException 
     */
    @Test
    public void cachedImplementations() throws ConnectionException, AuthenticationException {
        final DAOFactory factory = DAOFactory.getInstance(null, null,
                MockDAOFactoryImpl.class);
        assertSame(factory,
                DAOFactory.getInstance(null, null, MockDAOFactoryImpl.class));
    }

    /**
     * Tests that using a different implementation during instance retrieval
     * will result in a new instance.
     * 
     * @throws ConnectionException
     *             if a connection to the database cannot be established for
     *             network reasons.
     * @throws AuthenticationException 
     */
    @Test
    public void differentImplementationsCreateDifferentInstances()
            throws ConnectionException, AuthenticationException {
        final DAOFactory factory = DAOFactory.getInstance(null, null,
                MockDAOFactoryImpl.class);
        assertNotSame(factory,
                DAOFactory.getInstance("", "", AnotherMockDAOFactoryImpl.class));
    }

    /**
     * Tests that using different credentials during instance retrieval will
     * result in a new instance.
     * 
     * @throws ConnectionException
     *             if a connection to the database cannot be established for
     *             network reasons.
     * @throws AuthenticationException 
     */
    @Test
    public void differentCredentialsCreateDifferentInstances()
            throws ConnectionException, AuthenticationException {
        final DAOFactory factory = DAOFactory.getInstance(null, null,
                MockDAOFactoryImpl.class);
        assertNotSame(factory,
                DAOFactory.getInstance("", "", MockDAOFactoryImpl.class));
    }

    /**
     * A mock factory implementation used to test basic features of the
     * {@link DAOFactory} class.
     */
    static class MockDAOFactoryImpl extends DAOFactory {
        /*
         * (non-Javadoc)
         * 
         * @see cafe.dao.DAOFactory#getDishDAO()
         */
        @Override
        public DishDAO getDishDAO() {
            return null;
        }

        /*
         * (non-Javadoc)
         * 
         * @see cafe.dao.DAOFactory#getIngredientDAO()
         */
        @Override
        public IngredientDAO getIngredientDAO() {
            return null;
        }

        /*
         * (non-Javadoc)
         * 
         * @see cafe.dao.DAOFactory#setCredentials(java.lang.String,
         * java.lang.String)
         */
        @Override
        protected void setCredentials(final String user, final String password)
                throws ConnectionException {
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.AutoCloseable#close()
         */
        @Override
        public void close() {
        }
    }

    /**
     * Another mock factory implementation used to test basic features of the
     * {@link DAOFactory} class.
     */
    static class AnotherMockDAOFactoryImpl extends MockDAOFactoryImpl {
    }
}
