package cafe.dao.postgres;

import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

import java.sql.Connection;

import org.junit.Test;

import cafe.dao.AuthenticationException;
import cafe.dao.ConnectionException;
import cafe.dao.DAOFactory;

/**
 * Unit tests for the {@link DAOFactory} class.
 * 
 * @author Michael Winter
 */
public final class DAOFactoryImplTest {
    /**
     * The user name used for authenticating connections to the database.
     */
    private static final String USER = "zyvc215";
    /**
     * The password used for authenticating connections to the database.
     */
    private static final String PASSWORD = "IYIP1845";

    /**
     * Tests that a connection with valid credentials return an instance without
     * raising any exceptions.
     * 
     * <p>
     * Note: It's assumed that incorrect credentials will be handled correctly
     * by the DBMS.
     * 
     * @throws ConnectionException
     *             if a connection to the database cannot be established for
     *             network reasons.
     * @throws AuthenticationException 
     */
    @Test
    public void establishConnection() throws ConnectionException, AuthenticationException {
        DAOFactory.getInstance(USER, PASSWORD, DAOFactoryImpl.class);
    }

    /**
     * Tests that repeating a connection attempt with the same credentials
     * results in the same object: that is, connections are saved.
     * 
     * @throws ConnectionException
     *             if a connection to the database cannot be established for
     *             network reasons.
     * @throws AuthenticationException 
     */
    @Test
    public void pooledConnections() throws ConnectionException, AuthenticationException {
        final DAOFactoryImpl factory = (DAOFactoryImpl) DAOFactory.getInstance(
                USER, PASSWORD, DAOFactoryImpl.class);
        final Connection connection = factory.getConnection();
        assertSame(connection, factory.getConnection());
    }

    /**
     * Tests that returned connections are valid. If a connection is closed, a
     * new one should be established.
     * 
     * @throws ConnectionException
     *             if a connection to the database cannot be established for
     *             network reasons.
     * @throws AuthenticationException 
     */
    @Test
    public void recreateClosedConnection() throws ConnectionException, AuthenticationException {
        final DAOFactoryImpl factory = (DAOFactoryImpl) DAOFactory.getInstance(
                USER, PASSWORD, DAOFactoryImpl.class);
        final Connection closedConnection = factory.getConnection();
        factory.close();
        assertNotSame(closedConnection, factory.getConnection());
    }
}
