package cafe;

/**
 * Represents a designated table for serving customers.
 * 
 * @author Michael Winter
 */
public final class Table {
    /**
     * The unique identifier for this instance.
     */
    private int id;
    /**
     * The waiter that normally serves customers at this table.
     */
    private Employee waiter;
    
    
    /**
     * Constructs a new {@code Table} instance.
     * 
     * @param id
     *            the unique identifier for this instance.
     * @param waiter
     *            the waiter that normally serves this table.
     */
    public Table(final int id, final Employee waiter) {
        this.id = id;
        this.waiter = waiter;
    }
    
    /**
     * Returns the unique identifier for this table.
     * 
     * @return the identifier.
     */
    public int getId() {
        return id;
    }
    
    /**
     * Returns the {@link Employee} that normally serves this table.
     * 
     * @return the waiter.
     */
    public Employee getWaiter() {
        return waiter;
    }
}
