/**
 * Defines classes and subpackages that will be used by the customers.
 * 
 * @author Robert Kardjaliev
 *
 */
package cafe.client;