package cafe.client.trackOrder;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import cafe.client.payment.PaymentController;
import cafe.client.payment.PaymentView;


/**
 * Observes user events from the Track Order View and directs behaviour accordingly, 
 * invoking the model where appropriate and referring events to higher-level observing
 * controllers.
 * 
 * @author Robert Kardjaliev
 * @author Jonathan Hercock
 * @author Hannah Cooper
 */



public class TrackOrderController {
	/**
	 * Hides the view associated with this controller.
	 */
	public final void hideView() {
		view.setVisible(false);
	}

	/**
	 * Shows the view associated with this controller.
	 */
	public final void showView() {
		view.setVisible(true);
	}
	/** 
	 * An integer that will act as 10 minute a timer in 
	 * {@code refreshProgress()}. 
	 */
	private int timer;
	// CHECKSTYLE IGNORE MagicNumberCheck FOR NEXT 2 LINES
	/** An integer variable used for the timer. */	
	private int minute = 60;

	/** The model. */
	private TrackOrderModel model;
	/** The view. */
	private TrackOrderView view;

	/** A payment controller. */
	private PaymentController paymentController;
	/** A payment view. */
	private PaymentView paymentView;

	/**
	 * Instantiates the fields of the Track Order Controller.
	 *
	 * @param view - The View.
	 * @param model - The Model.
	 */
	public TrackOrderController(final TrackOrderView view, final TrackOrderModel model) { 
		this.view = view; 
		this.model = model;
		paymentView = new PaymentView();
		setPaymentController(new PaymentController(paymentView));
		setProgressValue();
		refreshProgress();
	}

	/**
	 * Adds a listener to the Edit Order button.
	 * @param listener
	 * 				the listener
	 */
	public final void addEditOrderListener(final ActionListener listener) {
		view.addEditOrderListener(listener);
	}
	/**
	 * Adds a listener to the Cancel Order button.
	 * @param listener 
	 * 				the listener
	 */
	public final void addCancelOrderListener(final ActionListener listener) {
		view.addCancelOrderListener(listener);
	}

	/**
	 * Adds a listener to the Cancel Order button.
	 * @param listener 
	 * 				the listener to be added
	 */
	public final void addPaymentButtonListener(final ActionListener listener) {
		view.addPaymentButtonListener(listener);
	}

	/**
	 * Adds a listener to the Call for Assistance button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public final void addWaiterCallListener(final ActionListener listener) {
		view.addWaiterCallListener(listener);
	}

	/**
	 * Sets the progress bar percentage in the view and displays the
	 * OrderState enum as a String in order to display the stage 
	 * the order is currently at.
	 */
	public final void setProgressValue() {
		int progress = model.getOrderProgress();
		String progressLabel = model.getOrderState().toString();
		view.setProgressValue(progress);
		view.setProgressLabel(progressLabel);
	}

	/**
	 * Listens for Payment button clicks. When pressed, it'll close the current
	 * view and open the payment one.
	 */
	private class PaymentButtonListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			hideView();
			getPaymentController().showView();
		}
	}
	/**
	 * Reloads the progress of the Order, sets buttons to enabled/disabled where appropriate
	 * and displays appropriate text to the client in the view about the progress and
	 * approximate time remaining until its completion.
	 */
	public final void refreshProgress() {
		final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		Runnable refresh = new Runnable() {
			// CHECKSTYLE IGNORE MagicNumberCheck FOR NEXT 13 LINES
			private int count = 0;
			public void run() {
				setProgressValue();
				if (getTimer() / minute >= 2 && model.getOrderProgress() <= 60) {
					if (count == 1) {
						count = 0;
					}
					view.setTimeLabel(getTimer() / minute + " minutes");
					setTimer(getTimer() - 1);
				} else if (getTimer() / minute < 2 && model.getOrderProgress() <= 60) {
					if (count == 0) {
						count = 1;
						setTimer(minute);
					}
					view.setTimeLabel("Your order's taking longer "
							+ "than expected.");
				}
				// CHECKSTYLE IGNORE MagicNumberCheck FOR NEXT 22 LINES
				switch(model.getOrderProgress()) {
				case 20:
					view.cancelOrderEnabled(true);
					view.editOrderEnabled(true);
					break;
				case 40: 
					break;
				case 60:
					view.cancelOrderEnabled(false);
					break;
				case 80:
					if (getTimer() >= 1) {
						view.setTimeLabel(getTimer() + " seconds"); 
						setTimer(getTimer() - 1);
					} else {
						view.setTimeLabel("Your order should be here "
								+ "soon.");
					}
					view.editOrderEnabled(false);
					break;
				case 100:
					view.setTimeLabel("Your order has arrived.");
					view.paymentButtonEnabled(true);
					addPaymentButtonListener(new PaymentButtonListener());
					executor.shutdown();
					break;
				default:
					break;
				}
			}
		};
		executor.scheduleAtFixedRate(refresh, 0, 1, TimeUnit.SECONDS);

	}

	/**
	 * Gets the payment controller.
	 * @return
	 * 			the payment controller.
	 */
	public final PaymentController getPaymentController() {
		return paymentController;
	}

	/**
	 * Sets the payment controller.
	 * @param paymentController
	 * 			the payment controller.
	 */
	public final void setPaymentController(final PaymentController paymentController) {
		this.paymentController = paymentController;
	}

	/**
	 * Gets the current value of {@code timer}.
	 * @return the {@code timer} - integer representation of
	 * the time in seconds
	 */
	public final int getTimer() {
		return timer;
	}

	/**
	 * Sets the seconds the timer should have.
	 * @param timer
	 * 			the variable keeping track of the seconds.
	 */
	public final void setTimer(final int timer) {
		this.timer = timer;
	}


}