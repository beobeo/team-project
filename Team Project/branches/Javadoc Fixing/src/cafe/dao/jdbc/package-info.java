/**
 * Defines data access objects specific to JDBC-based implementations.
 * 
 * @author Michael Winter
 */
package cafe.dao.jdbc;