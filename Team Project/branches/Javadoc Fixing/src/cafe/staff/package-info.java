/**
 * Defines classes and subpackages that will be used by the staff.
 * 
 * @author Robert Kardjaliev
 */
package cafe.staff;