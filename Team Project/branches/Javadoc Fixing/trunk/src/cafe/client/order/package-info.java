/**
 * Defines classes related to the Order user interface for customers.
 * 
 * @author Robert Kardjaliev
 *
 */
package cafe.client.order;