package cafe.client.payment;

import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.swing.table.AbstractTableModel;

import cafe.Dish;
import cafe.Order;
import cafe.OrderItem;
import cafe.Sitting;
import cafe.dao.OrderDAO;

/**
 * Provides the mechanism for populating tabular data within the payment view
 * used by clients.
 * 
 * @author Michael Winter
 */
public final class PaymentOverviewTableModel extends AbstractTableModel {
	/**
	 * Unique serialisation identifier.
	 */
	private static final long serialVersionUID = -5135992384262588382L;
	/**
	 * The number of columns represented by the model.
	 */
	private static final int COLUMN_COUNT = 3;
	/**
	 * The heading for each column represented by the model.
	 */
	private static final String[] COLUMN_HEADINGS = { "Dish", "Quantity",
			"Price" };
	/**
	 * The sitting to be queried for ordered dishes.
	 */
	private Sitting sitting;
	/**
	 * A mapping of the dishes ordered during the sitting to the total quantity ordered.
	 */
	private SortedMap<Dish, Integer> orderedDishes = new TreeMap<>();

	/**
	 * Constructs an {@code PaymentOverviewTableModel} using the specified
	 * {@code Sitting}.
	 * 
	 * @param sitting
	 *            the sitting from which order information will be retrieved.
	 * @param orderDataSource
	 *            the data access object used to obtain information about orders
	 *            made during the sitting.
	 */
	public PaymentOverviewTableModel(final Sitting sitting,
			final OrderDAO orderDataSource) {
		this.sitting = sitting;

		buildDishQuantityMap(orderDataSource);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return orderedDishes.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return COLUMN_COUNT;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(final int rowIndex, final int columnIndex) {
		final Iterator<Entry<Dish, Integer>> iterator = orderedDishes.entrySet().iterator();
		for (int i = 0; i < rowIndex; ++i) {
			iterator.next();
		}
		final Entry<Dish, Integer> item = iterator.next();
		
		Object value;
		switch (columnIndex) {
		case 0: // Dish
			value = item.getKey().getName();
			break;
		case 1: // Quantity
			value = item.getValue();
			break;
		case 2: // Price
			value = item.getKey().getPrice() * item.getValue();
			break;
		default:
			value = null;
		}
		return value;
	}

	@Override
	public String getColumnName(final int column) {
		return COLUMN_HEADINGS[column];
	}

	
	/**
	 * Constructs the map of ordered dishes to the quantity ordered across a sitting.
	 * 
	 * @param orderDataSource
	 *            the data access object used to obtain information about orders
	 *            made during the sitting.
	 */
	private void buildDishQuantityMap(final OrderDAO orderDataSource) {
		final List<Order> orders = orderDataSource
				.selectOrdersBySitting(sitting.getId());
		for (Order order : orders) {
			final List<OrderItem> items = order.getItems();
			for (OrderItem item : items) {
				final Dish dish = item.getDish();
				final int quantity = item.getQuantity();
				if (!orderedDishes.containsKey(dish)) {
					orderedDishes.put(dish, quantity);
				} else {
					orderedDishes.put(dish, quantity + orderedDishes.get(dish));
				}
			}
		}
	}
}
