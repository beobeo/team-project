/**
 * Defines classes for the Payment user interface
 * that will be used by the client.
 * 
 * @author Robert Kardjaliev
 */
package cafe.client.payment;