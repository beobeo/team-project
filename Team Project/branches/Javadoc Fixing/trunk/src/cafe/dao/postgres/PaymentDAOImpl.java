package cafe.dao.postgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import cafe.Payment;
import cafe.dao.DataAccessException;
import cafe.dao.PaymentDAO;
import cafe.dao.jdbc.ConnectionFactory;

/**
 * A data access object used to manipulate payments on the database.
 * 
 * @author Robert Kardjaliev
 */
public final class PaymentDAOImpl implements PaymentDAO {
	/**
	 * The source of JDBC connections used to perform queries against the
	 * underlying database.
	 */
	private ConnectionFactory connectionFactory;

	/**
	 * Constructs a data access object for payments that will operate over
	 * the given connection.
	 * 
	 * @param connectionFactory
	 *            the factory object that will provide a database connection.
	 * @throws NullPointerException
	 *             if the specified factory or data access objects are {@code null}.
	 */
	public PaymentDAOImpl(final ConnectionFactory connectionFactory) {
		if (connectionFactory == null) {
			throw new NullPointerException("connectionFactory is null");
		}
		this.connectionFactory = connectionFactory;
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.PaymentDAO#makePayment(int, int)
	 */
	@Override
	public void makePayment(final int sittingId, final int amount) {
		final Connection connection = connectionFactory.getConnection();
		try (PreparedStatement statement = connection.prepareStatement(
				"INSERT INTO payment (sitting_id, amount) VALUES(?, ?)")) {
			statement.setInt(1, sittingId);
			statement.setInt(2, amount);
			statement.executeUpdate();
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.PaymentDAO#makePayment(int)
	 */
	@Override
	public void makePayment(final int sittingId, final String card, final int amount) {
		final Connection connection = connectionFactory.getConnection();
		try (PreparedStatement statement = connection.prepareStatement(
				"INSERT INTO payment (sitting_id, card, amount) VALUES(?, ?, ?)")) {
			statement.setInt(1, sittingId);
			statement.setString(2, card);
			// CHECKSTYLE IGNORE MagicNumberCheck FOR NEXT 1 LINES
			statement.setInt(3, amount);
			statement.executeUpdate();
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.PaymentDAO#findPayment(int)
	 */
	@Override
	public Payment findPayment(final int id) {
		Payment payment = null;
		final Connection connection = connectionFactory.getConnection();
		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT * FROM payment WHERE id = ?")) {
			statement.setInt(1, id);
			ResultSet result = statement.executeQuery();
			if (result.next()) {
				payment = new Payment(id, result.getInt("sitting_id"), 
						result.getString("card"), result.getFloat("amount"));
			}
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}
		return payment;
	}
}