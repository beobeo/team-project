package cafe.staff;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * 
 * @author Jonathan Hercock
 *
 */
public class LogInScreen extends JFrame {

    /**
     *  Serial ID for CheckStyle purposes.
     */
    private static final long serialVersionUID = -3492289270567953976L;
    /**  The Main GUI Panel.    */
    private JPanel contentPane;
    /**  The button that allows the Management Screen to be reached.  */
    private JButton btnManagement;
    /**  The button that allows the Kitchen Staff's Screen to be reached.  */
    private JButton btnKitchenStaff;
    /**  The button that allows the Waiting Staff's Screen to be reached.   */
    private JButton btnWaitingStaff;

    /**
     * Launch the application.
     * @param args the set of arguments.
     */
    public static void main(final String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    LogInScreen frame = new LogInScreen();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    ///CHECKSTYLE:OFF
    public LogInScreen() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JLabel lblLogIn = new JLabel("LOG IN");
        lblLogIn.setFont(new Font("Tahoma", Font.BOLD, 15));
        lblLogIn.setBounds(10, 11, 81, 25);
        contentPane.add(lblLogIn);
        
        btnKitchenStaff = new JButton();
        btnKitchenStaff.setFont(new Font("Tahoma", Font.BOLD, 13));
        btnKitchenStaff.setText("Kitchen Staff");
        btnKitchenStaff.setBounds(127, 46, 180, 49);
        contentPane.add(btnKitchenStaff);
        
        btnManagement = new JButton();
        btnManagement.setFont(new Font("Tahoma", Font.BOLD, 13));
        btnManagement.setText("Management");
        btnManagement.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            }
        });
        btnManagement.setBounds(127, 166, 180, 49);
        contentPane.add(btnManagement);
        
        btnWaitingStaff = new JButton();
        btnWaitingStaff.setFont(new Font("Tahoma", Font.BOLD, 13));
        btnWaitingStaff.setText("Waiting Staff");
        btnWaitingStaff.setBounds(127, 106, 180, 49);
        contentPane.add(btnWaitingStaff);
    }
    //CHECKSTYLE:ON
    
    /** Method to add an action listener to the Management button.
     * 
     * @param listener - the listener for the Management log in button.
     */
    public final void addManagementLogInListener(final ActionListener listener) {
        btnManagement.addActionListener(listener);
    }
    
    /** Method to add an action listener to the Kitchen log in button.
     * 
     * @param listener - listener for the Kitchen Staff button. 
     */
    
    public final void addKitchenStaffLogInListener(final ActionListener listener) {
        btnKitchenStaff.addActionListener(listener);
    }
    
    /** Method to add an action listener to the Waiter log in button.
     * 
     * @param listener - listener for the Waiting Staff button. 
     */
    
    public final void addWaitingStaffLogInListener(final ActionListener listener) {
        btnWaitingStaff.addActionListener(listener);
    }
    
}
