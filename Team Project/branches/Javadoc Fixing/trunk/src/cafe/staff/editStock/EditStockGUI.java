package cafe.staff.editStock;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import cafe.Ingredient;



/**
 * 
 * @author Hannah Cooper
 *
 */
//CHECKSTYLE:OFF

public class EditStockGUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton button;

	public static JLabel level;

	private JButton backButton;
	//static JLabel level;

	private static JFormattedTextField textField;
	static JLabel selection;
	public static Ingredient selectedItem;
	public static JPanel panel;
	static ArrayList<Ingredient> values;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public EditStockGUI(ArrayList<Ingredient> a) {
				
		
				values = a;
		        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		        setBounds(100, 100, 710, 400);
		        contentPane = new JPanel();
		        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		        setContentPane(contentPane);
		        contentPane.setLayout(null);
		        
		        JLabel lblEdit = new JLabel("EDIT STOCK");
		        lblEdit.setFont(new Font("Tahoma", Font.BOLD, 16));
		        lblEdit.setBounds(20, 11, 300, 31);
		        contentPane.add(lblEdit);
		        
		        final JSplitPane splitPane = new JSplitPane();
		        splitPane.setBounds(40, 63, 448, 196);
		        splitPane.setVisible(true);
		        contentPane.add(splitPane);
		        
		        JScrollPane scrollPane = new JScrollPane();
		        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		        splitPane.setLeftComponent(scrollPane);
		        
		        panel = new JPanel();
			    splitPane.setRightComponent(panel);
			    panel.setLayout(null);
			    
			    final JLabel currentStockLevel = new JLabel("Current Stock Level:");
			    currentStockLevel.setFont(new Font("Tahoma", Font.BOLD, 14));
			    currentStockLevel.setBounds(0, 50, 200, 50);
			    
			    level = new JLabel();
			    level.setFont(new Font("Tahoma", Font.BOLD, 14));
			    level.setBounds(150, 50, 200, 50);
			 
			    panel.add(level);
			    
			    final JLabel newStockLevel = new JLabel("New Stock Level:");
			    newStockLevel.setFont(new Font("Tahoma", Font.BOLD, 14));
			    newStockLevel.setBounds(0, 100, 150, 20);

			    
			    setTextField(new JFormattedTextField(NumberFormat.getInstance()));
			    getTextField().setFont(new Font("Tahoma", Font.BOLD, 14));
			    getTextField().setBounds(120, 100, 100, 20);

			    backButton = new JButton("Back");
			    backButton.setFont(new Font("Tahoma", Font.BOLD, 14));
			    backButton.setBounds(42, 300, 130, 20);
			    contentPane.add(backButton);
			    
			    button = new JButton("Update");
			    button.setFont(new Font("Tahoma", Font.BOLD, 14));
			    button.setBounds(130, 160, 130, 20);

		        final JList list = new JList();
		        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		        list.setFont(new Font("Tahoma", Font.BOLD, 13));
		        list.setVisibleRowCount(4);
		        list.setValueIsAdjusting(true);
		        list.setModel(new DefaultListModel() {
				
					private static final long serialVersionUID = 1L;
					
		            public int getSize() {
		                return values.size();
		            }
		            public Ingredient getElementAt(int index) {
		                return values.get(index);
		            }
		            
		        });
		        MouseListener mouseListener = new MouseAdapter(){
		        	public void mouseClicked(MouseEvent e) {
		        
			    	   if (e.getClickCount() == 1) {

			    		   panel.removeAll();
			               selectedItem = (Ingredient) list.getSelectedValue();
			               selection = new JLabel(selectedItem.getName());
				 	       selection.setFont(new Font("Tahoma", Font.BOLD, 16));
				 	       selection.setBounds(120, 0, 500, 20);
				 	      
				 	       panel.add(button);
				 	       panel.add(getTextField());
				 	       panel.add(newStockLevel);
				 	       panel.add(level);
				 	       level.setText(String.valueOf(selectedItem.getStock()));
			               panel.add(selection);
			               panel.add(currentStockLevel);
			               panel.repaint();   
			    	   }
		        	}
		        };
			 
			   list.addMouseListener(mouseListener);     
		       list.setSelectedIndex(0);   		       
		       scrollPane.setViewportView(list);
	 }

	
	public final void addUpdateStockListener(final ActionListener listener) {
		button.addActionListener(listener);
	}
	
	public final void addBackButtonListener(final ActionListener listener) {
		backButton.addActionListener(listener);
	}
	
	public static JTextField getTextField() {
		return textField;
	}

	public void setTextField(JFormattedTextField textField) {
		EditStockGUI.textField = textField;
	}
	
	public void setValues(ArrayList<Ingredient> a){
		values = a;
	}
}
//CHECKSTYLE:ON

