package cafe;
/**
 * Represents an ingredient used to prepare dishes and tracks the stock level of each.
 *
 * TODO: Add add/take stock methods?
 * 
 * @author Michael Winter
 */
public final class Ingredient {
	/**
	 * The unique identifier for this instance.
	 */
	private int id;
	/**
	 * The name of the ingredient.
	 */
	private String name;
	/**
	 * The current stock level of the ingredient.
	 */
	private int stock;
	/**
	 * The unit cost of the ingredient.
	 */
	private float cost;
	
	/**
     * 
     * @param id - the unique integer id of an ingredient
     * @param name - the name of the ingredient
     * @param cost - the cost of an ingredient
     * @param stock - the amount in stock of an ingredient
     */
    public Ingredient(final int id, final String name, final float cost, final int stock) {
        this.id = id;
        this.name = name;
        this.cost = cost;
        this.stock = stock;
    }

    /**
	 * Returns the unit cost of this ingredient (in pounds stirling).
	 * 
	 * @return the cost.
	 */
	public float getCost() {
		return cost;
	}
	
	/**
	 * Returns the name of this ingredient.
	 * 
	 * @return the name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the stock level of this ingredient.
	 * 
	 * @return the current stock level.
	 */
	public int getStock() {
		return stock;
	}
	
	/**
	 * Returns whether the required quantity of this ingredient is available in stock.
	 * 
	 * @param quantity the needed amount.
	 * @return {@code true} if there is a sufficient amount of this ingredient in stock;
	 * 		{@code false} otherwise.
	 */
	public boolean isAvailable(final int quantity) {
		return stock >= quantity;
	}
	
	
	/**
	 * Returns the unique identifier for this dish.
	 * 
	 * @return the identifier.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the price of this ingredient. This must be greater than zero.
	 * 
	 * @param amount the cost (must be > 0).
	 * @throws IllegalArgumentException if {@code amount} is negative.
	 */
	void setCost(final float amount) {
		if (amount <= 0) {
			throw new IllegalArgumentException();
		}
		cost = amount;
	}
	
	/**
	 * Sets the name of this ingredient.
	 * 
	 * @param string the name (must not be {@code null}).
	 * @throws NullPointerException if {@code string} is {@code null}.
	 */
	void setName(final String string) {
		if (string == null) {
			throw new NullPointerException();
		}
		name = string;
	}
	
	/**
	 * Sets the current stock level for this ingredient. This must be greater than, or equal to,
	 * zero.
	 * 
	 * @param amount the current stock level (must be >= 0).
	 * @throws IllegalArgumentException if {@code value} is negative.
	 */
	void setStock(final int amount) {
		if (amount < 0) {
			throw new IllegalArgumentException();
		}
		stock = amount;
	}
}
