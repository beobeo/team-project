--
-- Types
--
CREATE TYPE EMPLOYEE_ROLE AS ENUM (
	'Manager',
	'Waiter',
	'Kitchen'
);

CREATE TYPE ORDER_PROGRESS AS ENUM (
	'PENDING',    -- Placed by a customer but not yet sent to the kitchen
	'CONFIRMED',  -- Confirmed to the kitchen by a waiter
	'PREPARING',  -- Under preparation
	'READY',      -- Ready for delivery
	'DELIVERED',  -- At the table
	'CANCELLED'
);


--
-- Food
--
CREATE TABLE "dish_category" (
	"id" SERIAL,
	"name" CHARACTER VARYING(25) NOT NULL,
	PRIMARY KEY ("id"),
	UNIQUE ("name")
);
INSERT INTO "dish_category"	("id", "name") VALUES (0, 'Unspecified');

CREATE TABLE "dish" (
	"id" SERIAL,
	"name" CHARACTER VARYING(50) NOT NULL,
	"description" CHARACTER VARYING(150) NOT NULL,
	"calories" INTEGER NOT NULL CHECK ("calories" > 0),
	"price" NUMERIC(6,2) NOT NULL CHECK ("price" > 0),   -- The total cost per dish
	"category_id" INTEGER NOT NULL DEFAULT 0,
	"image" BYTEA,
	"visible" BOOLEAN NOT NULL DEFAULT TRUE,             -- Enables hiding a dish without deleting
	                                                     -- it
	PRIMARY KEY ("id"),
	UNIQUE ("name"),
	FOREIGN KEY ("category") REFERENCES "dish_category" ("id") ON DELETE SET DEFAULT
);

CREATE TABLE "dietary_information" (
	"id" SERIAL,
	"description" CHARACTER VARYING(50),
	PRIMARY KEY ("id"),
	UNIQUE ("description")
);
INSERT INTO "dietary_information" ("description") VALUES ('Suitable for vegetarians');

CREATE TABLE "dish_information" (
	"dish_id" INTEGER NOT NULL,                          -- The dish being described
	"information_id" INTEGER NOT NULL,                   -- The information applicable to the dish
	PRIMARY KEY ("dish_id", "information_id"),
	FOREIGN KEY ("dish_id") REFERENCES "dish" ("id") ON DELETE CASCADE,
	FOREIGN KEY ("information_id") REFERENCES "dietary_information" ("id") ON DELETE CASCADE
);

CREATE TABLE "ingredient" (
	"id" SERIAL,
	"name" CHARACTER VARYING(30) NOT NULL,
	"price" NUMERIC(6,2) NOT NULL CHECK ("price" > 0),   -- The per-unit cost of an ingredient
	"stock" INTEGER NOT NULL CHECK ("stock" >= 0),       -- The current available units
	PRIMARY KEY ("id"),
	UNIQUE ("name")
);

CREATE TABLE "dish_ingredient" (
	"dish_id" INTEGER NOT NULL,
	"ingredient_id" INTEGER NOT NULL,
	"quantity" INTEGER NOT NULL CHECK ("quantity" > 0),  -- The number of units of the ingredient
	                                                     -- required by the dish
	PRIMARY KEY ("dish_id", "ingredient_id"),
	FOREIGN KEY ("dish_id") REFERENCES "dish" ("id") ON DELETE CASCADE,
	FOREIGN KEY ("ingredient_id") REFERENCES "ingredient" ("id") ON DELETE RESTRICT
);


--
-- Administration
--
CREATE TABLE "customer" (
	"id" SERIAL,
	"title" CHARACTER(4) NOT NULL,
	"first_name" CHARACTER VARYING(25) NOT NULL,
	"last_name" CHARACTER VARYING(25) NOT NULL,
	"email" CHARACTER VARYING(50) NOT NULL,
	"password" CHARACTER(40) NOT NULL,                   -- Hashed
	"visits" INTEGER NOT NULL CHECK ("visits" >= 0) DEFAULT 0,
	"subscribed" BOOLEAN NOT NULL DEFAULT FALSE,
	PRIMARY KEY ("id"),
	UNIQUE ("email")
);

CREATE TABLE "dietary_preference" (
	"customer_id" INTEGER NOT NULL,
	"preference_id" INTEGER NOT NULL,                    -- The dietary filter the customer wants
	                                                     -- to apply by default
	PRIMARY KEY ("customer_id", "preference_id"),
	FOREIGN KEY ("customer_id") REFERENCES "customer" ("id") ON DELETE CASCADE,
	FOREIGN KEY ("preference_id") REFERENCES "dietary_information" ("id") ON DELETE CASCADE
);

CREATE TABLE "employee" (
    "id" SERIAL,
    "first_name" CHARACTER VARYING(25) NOT NULL,
    "last_name" CHARACTER VARYING(25) NOT NULL,
    "role" EMPLOYEE_ROLE NOT NULL,
    "password" CHARACTER(40) NOT NULL,                   -- Hashed
    PRIMARY KEY ("id")
);

CREATE TABLE "table" (
	"id" INTEGER NOT NULL,
	"waiter_id" INTEGER NOT NULL,                        -- The waiter that usually serves this
	                                                     -- table
	PRIMARY KEY ("id"),
	FOREIGN KEY ("waiter_id") REFERENCES "employee" ("id") ON DELETE RESTRICT
);

CREATE TABLE "service_call" (
    "table_id" INTEGER NOT NULL,
    "time_placed" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "time_answered" TIMESTAMP CHECK ("time_answered" > "time_placed") DEFAULT NULL,
    PRIMARY KEY ("table_id", "time_placed"),
    FOREIGN KEY ("table_id") REFERENCES "table" ("id")
);


--
-- Sittings
--
-- A sitting may be made in advance or ad-hoc (for instance, passing trade). Each sitting
-- encapsulates a set of orders over a given period; payments must cover all of the uncancelled
-- orders.
--
CREATE TABLE "sitting" (
	"id" SERIAL,
	"table_id" INTEGER NOT NULL,
	"customer_id" INTEGER DEFAULT NULL,
	"placed" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	"reserved_from" TIMESTAMP NOT NULL,
	"reserved_until" TIMESTAMP NOT NULL CHECK ("reserved_until" > "reserved_from"),
	PRIMARY KEY (id),
	FOREIGN KEY (table_id) REFERENCES "table" ("id"),
	FOREIGN KEY (customer_id) REFERENCES "customer" ("id") ON DELETE SET NULL
);


--
-- Orders
--
CREATE TABLE "order" (
	"id" SERIAL,
	"sitting_id" INTEGER NOT NULL,
	"progress" ORDER_PROGRESS NOT NULL DEFAULT 'PENDING',
	"time_placed" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, -- When the client places a desired order
	"time_ready" TIMESTAMP DEFAULT NULL,                 -- When the order has been prepared
	"time_delivered" TIMESTAMP DEFAULT NULL,             -- When the order reaches the table
	"taken_by" INTEGER DEFAULT NULL,                     -- The waiter that took the order
	PRIMARY KEY ("id"),
	FOREIGN KEY ("sitting_id") REFERENCES "sitting" ("id"),
	FOREIGN KEY ("taken_by") REFERENCES "employee" ("id") ON DELETE SET NULL
);

CREATE TABLE "order_item" (
	"order_id" INTEGER NOT NULL,
	"dish_id" INTEGER NOT NULL,
	"quantity" INTEGER NOT NULL CHECK ("quantity" > 0) DEFAULT 1,
	"suggested" BOOLEAN NOT NULL DEFAULT FALSE,          -- Whether this was suggested by the
	                                                     -- waiter
	PRIMARY KEY ("order_id", "dish_id"),
	FOREIGN KEY ("order_id") REFERENCES "order" ("id") ON DELETE CASCADE,
	FOREIGN KEY ("dish_id") REFERENCES "dish" ("id") ON DELETE RESTRICT
);

CREATE TABLE "payment" (
	"id" SERIAL,
	"sitting_id" INTEGER NOT NULL,
	"card" CHARACTER(16) DEFAULT NULL,                   -- Card number or NULL for cash
	"amount" NUMERIC(6,2) NOT NULL CHECK ("amount" > 0),
	PRIMARY KEY ("id"),
	UNIQUE ("sitting_id", "card"),
	FOREIGN KEY ("sitting_id") REFERENCES "sitting" ("id") ON DELETE RESTRICT
);
