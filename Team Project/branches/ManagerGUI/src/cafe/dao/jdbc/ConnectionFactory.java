package cafe.dao.jdbc;

import java.sql.Connection;

import cafe.dao.AuthenticationException;

/**
 * Represents a type that is able to provide connections to a database using JDBC.
 * 
 * @author Michael Winter
 */
public interface ConnectionFactory {
    /**
     * Returns a JDCB connection to a database. This connection may be either a
     * new or an existing instance but in either case, it will always be valid
     * at the point of return.
     * 
     * @return the connection.
     * @throws ConnectionException
     *             if an error occurs whilst attempting to connect to the
     *             database.
     * @throws AuthenticationException
     *            if authentication is required and the user name is unknown or
     *            {@code null}, or the password is incorrect or {@code null}.
     */
    Connection getConnection();
}
