package cafe.manager;

/**
 * Observes events from management views and directs behaviour accordingly,
 * invoking the model where appropriate and referring events to higher-level
 * observing controllers.
 * @author Robert Kardjaliev
 */
public class ManagerController {
    /**  The model.  */
    private ManagerModel model;
    /** The view.    */
    private ManagerView view;
    
    /**
     * Constructor for the controller.
     * @param model
     * 			the model
     * @param view
     * 			the view
     */
    public ManagerController(final ManagerModel model, final ManagerView view) {
        this.model = model;
        this.view = view;
    }
    
    /**
     * Shows the view associated with this controller.
     */
    public final void showView() {
        view.setVisible(true);
    }
    
	/**
	 * Hides the view associated with this controller.
	 */
	public final void hideView() {
		view.setVisible(false);
	}
	
	/**
	 * @param args 
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
