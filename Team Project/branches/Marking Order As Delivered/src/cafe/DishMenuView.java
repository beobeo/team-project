package cafe;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.Set;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;

/**
 * @author Michael Winter
 *
 */
public class DishMenuView extends JPanel {
	
	/** Unique serialisation identifier. */
	private static final long serialVersionUID = -644015779923976660L;
	
	/**The name of the dish. */
	private JLabel name;
	
	/**The price of the dish.*/
	private JLabel price;
	
	/** A brief description of the dish. */
	private JTextArea description;
	
	/** The number of calories in the dish. */
	private JLabel calories;
	
	/** A list of ingredients used to prepare the dish. */
	private JTextArea ingredients;
	
	/** A photograph of the dish. */
	private JLabel image;
	
	/** The Add to Order button. */
	private JButton addButton;
	
	/** A list of dietary and allergy information for the dish. */
	private JTextArea dietaryInformation;
	/**
	 * Icon which image in DishMenuView takes.
	 */
	private Icon icon;

	/**
	 * Create the panel.
	 * @param dish - 
	 */
	public DishMenuView(final Dish dish) {
		setOpaque(false);
		initGUI();
		
		BufferedImage dish1 = dish.getImage();
		
		setCalories(dish.getCalories());
		setDescription(dish.getDescription());
		setIngredients(dish.getIngredients());
		setInformation(dish.getDietaryInformation());
		setName(dish.getName());
        setPrice(dish.getPrice());
        if (dish1 != null) {
        	image.setIcon(new ImageIcon(dish1));
        }
	}
	
	/**
	 * Sets a formatted display of the number of calories in the dish.
	 *  
	 * @param value the number of calories in the dish.
	 */
	public final void setCalories(final int value) {
		calories.setText(String.format("%1$1d kcal", value));
	}
	
	/**
	 * Sets a formatted display of the price of this dish.
	 * 
	 * @param value the price.
	 */
	public final void setPrice(final float value) {
		price.setText(String.format("£%1$3.2f", value));
	}
	
	/**
	 * Sets the description of this dish.
	 * 
	 * @param string the description.
	 */
	public final void setDescription(final String string) {
		description.setText(string);
	}
    
    /**
     * Sets and formats the list of ingredients used to prepare this dish.
     * 
     * @param ingredientSet the ingredients.
     */
    public final void setIngredients(final Set<Ingredient> ingredientSet) {
        final StringBuilder ingredientList = new StringBuilder();
        for (final Ingredient ingredient : ingredientSet) {
            ingredientList.append(ingredient.getName());
            ingredientList.append(", ");
        }
        ingredientList.setLength(ingredientList.length() - 2);
        ingredients.setText(ingredientList.toString());
    }
    
    /**
     * Sets and formats the allergy and dietary information for this dish.
     * 
     * @param informationSet the information.
     */
    public final void setInformation(final Set<DietaryInformation> informationSet) {
        if (!informationSet.isEmpty()) {
            final StringBuilder informationList = new StringBuilder();
            for (final DietaryInformation information : informationSet) {
                informationList.append(information.getName());
                informationList.append(", ");
            }
            informationList.setLength(informationList.length() - 2);
            dietaryInformation.setText(informationList.toString());
            dietaryInformation.setVisible(true);
        } else {
            dietaryInformation.setVisible(false);
        }
    }
	
	/**
	 * Sets the name of the dish.
	 * 
	 * @param string the name.
	 */
	public final void setName(final String string) {
		name.setText(string);
	}
	
	/**
	 * sets the dish image as an icon.
	 * @param bufferedImage - image type
	 */
	public final void setImage(final BufferedImage bufferedImage) {
		image.setIcon(new ImageIcon(bufferedImage));
	}
	/**
	 * Adds an action listener to the Add to Order button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public final void addOrderListener(final ActionListener listener) {
		addButton.addActionListener(listener);
	}
	
	/**
	 * Constructs the view.
	 */
	//CHECKSTYLE:OFF
	private void initGUI() {
		setMaximumSize(new Dimension(483, 32767));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{49, 28, 63, 0, 100, 0};
		gridBagLayout.rowHeights = new int[]{21, 20, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		name = new JLabel("Dish name");
		name.setFont(UIManager.getFont("Label.font"));
		GridBagConstraints gbc_name = new GridBagConstraints();
		gbc_name.gridwidth = 2;
		gbc_name.anchor = GridBagConstraints.BASELINE_LEADING;
		gbc_name.insets = new Insets(0, 0, 5, 5);
		gbc_name.gridx = 0;
		gbc_name.gridy = 0;
		add(name, gbc_name);
		
		price = new JLabel("Cost");
		GridBagConstraints gbc_cost = new GridBagConstraints();
		gbc_cost.anchor = GridBagConstraints.BASELINE_TRAILING;
		gbc_cost.insets = new Insets(0, 0, 5, 5);
		gbc_cost.gridx = 2;
		gbc_cost.gridy = 0;
		add(price, gbc_cost);
		
		addButton = new JButton("Add to Order");
		addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		addButton.setFont(new Font("Tahoma", Font.PLAIN, 9));
		GridBagConstraints gbc_addButton = new GridBagConstraints();
		gbc_addButton.anchor = GridBagConstraints.BASELINE_TRAILING;
		gbc_addButton.insets = new Insets(0, 0, 5, 5);
		gbc_addButton.gridx = 3;
		gbc_addButton.gridy = 0;
		add(addButton, gbc_addButton);
		
		image = new JLabel(icon);
		image.setBounds(new Rectangle(0, 0, 100, 0));
		image.setFocusable(false);
		GridBagConstraints gbc_image = new GridBagConstraints();
		gbc_image.insets = new Insets(0, 0, 5, 0);
		gbc_image.anchor = GridBagConstraints.NORTH;
		gbc_image.gridheight = 3;
		gbc_image.gridx = 4;
		gbc_image.gridy = 0;
		add(image, gbc_image);
		
		description = new JTextArea("Description");
		description.setBorder(null);
		description.setEditable(false);
		description.setFont(new Font("Tahoma", Font.PLAIN, 11));
		description.setLineWrap(true);
		description.setOpaque(false);
		description.setWrapStyleWord(true);
		GridBagConstraints gbc_description = new GridBagConstraints();
		gbc_description.fill = GridBagConstraints.HORIZONTAL;
		gbc_description.anchor = GridBagConstraints.BASELINE;
		gbc_description.gridwidth = 3;
		gbc_description.insets = new Insets(0, 0, 5, 5);
		gbc_description.gridx = 0;
		gbc_description.gridy = 1;
		add(description, gbc_description);
		
		calories = new JLabel("Calories");
		GridBagConstraints gbc_calories = new GridBagConstraints();
		gbc_calories.anchor = GridBagConstraints.BASELINE_TRAILING;
		gbc_calories.insets = new Insets(0, 0, 5, 5);
		gbc_calories.gridx = 3;
		gbc_calories.gridy = 1;
		add(calories, gbc_calories);
		
		JLabel lblContains = new JLabel("Contains:");
		lblContains.setFont(lblContains.getFont().deriveFont(lblContains.getFont().getSize() - 1f));
		GridBagConstraints gbc_lblContains = new GridBagConstraints();
		gbc_lblContains.anchor = GridBagConstraints.BASELINE_LEADING;
		gbc_lblContains.insets = new Insets(0, 0, 5, 5);
		gbc_lblContains.gridx = 0;
		gbc_lblContains.gridy = 2;
		add(lblContains, gbc_lblContains);
		
		ingredients = new JTextArea("Ingredients");
		ingredients.setBorder(null);
		ingredients.setWrapStyleWord(true);
		ingredients.setLineWrap(true);
		ingredients.setOpaque(false);
		ingredients.setEditable(false);
		ingredients.setFont(new Font("Tahoma", Font.PLAIN, 11));
		GridBagConstraints gbc_ingredients = new GridBagConstraints();
		gbc_ingredients.fill = GridBagConstraints.HORIZONTAL;
		gbc_ingredients.anchor = GridBagConstraints.BASELINE;
		gbc_ingredients.gridwidth = 3;
		gbc_ingredients.insets = new Insets(0, 0, 5, 5);
		gbc_ingredients.gridx = 1;
		gbc_ingredients.gridy = 2;
		add(ingredients, gbc_ingredients);

		dietaryInformation = new JTextArea("");
		dietaryInformation.setBorder(null);
		dietaryInformation.setFont(new Font("Tahoma", Font.PLAIN, 11));
		dietaryInformation.setEditable(false);
		dietaryInformation.setLineWrap(true);
		dietaryInformation.setWrapStyleWord(true);
		dietaryInformation.setOpaque(false);
        GridBagConstraints informationGridBagConstraints = new GridBagConstraints();
        informationGridBagConstraints.anchor = GridBagConstraints.BASELINE;
        informationGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        informationGridBagConstraints.gridwidth = 5;
        informationGridBagConstraints.insets = new Insets(0, 0, 0, 5);
        informationGridBagConstraints.gridx = 0;
        informationGridBagConstraints.gridy = 3;
        add(dietaryInformation, informationGridBagConstraints);
	}
	//CHECKSTYLE:ON
}
