package cafe;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import cafe.dao.DAOFactory;
import cafe.dao.postgres.DAOFactoryImpl;

/**
 * Image writer class to test image is writing to GUI.
 * 
 * @author Hannah Cooper
 * 
 */
final class ImageWriter {
	/**
	 * .
	 */
	private ImageWriter() {
	}

	/**
	 * 
	 * @param args
	 *            - for main method
	 * @throws SQLException
	 *             - SQL exception
	 * @throws IOException
	 *             - IO exception
	 */
	public static void main(final String[] args) throws SQLException,
			IOException {
		DAOFactoryImpl factory = (DAOFactoryImpl) DAOFactory.getInstance(
				"zyvc215", "IYIP1845",

				DAOFactoryImpl.class);

		File file = new File("images/chicPizza.jpg");
		FileInputStream fis = new FileInputStream(file);
		PreparedStatement ps = factory.getConnection().prepareStatement(
				"UPDATE DISH set image = ? WHERE ID = 2");
		ps.setBinaryStream(1, fis, (int) file.length());

		ps.executeUpdate();
		ps.close();
		fis.close();
	}
}
