package cafe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;

import cafe.dao.DAOFactory;
import cafe.dao.DishDAO;
import cafe.dao.EmployeeDAO;
import cafe.dao.IngredientDAO;
import cafe.dao.OrderDAO;
import cafe.dao.TableDAO;
import cafe.dao.postgres.DAOFactoryImpl;
import cafe.kitchen.KitchenController;
import cafe.kitchen.KitchenModel;
import cafe.kitchen.KitchenView;
import cafe.manager.ManagerController;
import cafe.manager.ManagerView;
import cafe.waiter.WaiterController;
import cafe.waiter.WaiterGUI;
import cafe.waiter.WaiterModel;

/**
 * @author Jonathan Hercock
 *
 */
public final class StaffApplicationController {

    /** A carbon copy of the Client method (including the following javadoc message) :
     * Initialises and runs the application. The actual launching of the
     * application is delegated to this method in order to ensure initialisation
     * occurs on Swing's event-dispatching thread.  
     */
    private static void launch() {
        try {
            if (SwingUtilities.isEventDispatchThread()) {
                final StaffApplicationController application = new StaffApplicationController();
                application.run();
            } else {
                SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                        launch();
                    }
                });
            }
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }
    /**
     * The controller for the edit stock screen.
     */
    private EditStockController stockController;
    /**
     * The Controller for the employee's Log In Screen.
     */
    private LogInController loginController;
    
    /**
     * The Controller for the Waiter's view.
     */
    private WaiterController waiterController;
    
    /**
     * The Controller for the Kitchen's view.
     */
    private KitchenController kitchenController;
    
    /**
     * The Controller for the Manager's view.
     */
    private ManagerController managerController;
    
    /** The constructor for the Application. Adds all relevant listeners to 
     *  a new instance of the controllers.
     */
    private StaffApplicationController() {
        final LogInScreen loginScreen = new LogInScreen();
        loginController = new LogInController(loginScreen);
        loginController.addManagementLogInListener(new ManagementLogInListener());
        loginController.addKitchenLogInListener(new KitchenLogInListener());
        loginController.addWaiterLogInListener(new WaiterLogInListener());
        
        DAOFactory factory = DAOFactory.getInstance("zyvc215", "IYIP1845",
                DAOFactoryImpl.class);        
        final TableDAO tableData = factory.getTableDAO();
        final OrderDAO orderData = factory.getOrderDAO();
        final DishDAO dishData = factory.getDishDAO();
        final EmployeeDAO employeeData = factory.getEmployeeDAO();
        final IngredientDAO ingredientData = factory.getIngredientDAO();
        
        final KitchenView kitchenScreen = new KitchenView();
        final KitchenModel kitchenModel = new KitchenModel(orderData);
        kitchenController = new KitchenController(kitchenModel, kitchenScreen);
        kitchenController.addEditStockListener(new EditStockListener());
        
        final WaiterGUI waiterScreen = new WaiterGUI();
        final WaiterModel waiterModel = new WaiterModel(orderData);
        waiterController = new WaiterController(waiterScreen, waiterModel);
        final ManagerView managerView = new ManagerView();
        managerController = new ManagerController(managerView, tableData, employeeData, dishData);
        
        final EditStockModel editStockModel = new EditStockModel(ingredientData);
        final EditStockGUI editStockGUI = new EditStockGUI(editStockModel.getIngredientList());
        stockController = new EditStockController(editStockGUI, editStockModel);
        stockController.addUpdateStockListener(new UpdateStockListener());  
       
        managerController.addStockLevelListener(new EditStockListener());
        
    }      
    
    /**
     * Listens for button click events originating from the Management button.
     *  It hides the log in screen and (TODO:) displays the Manager's view.
     */
    private class ManagementLogInListener implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent e) {
            loginController.hideView();
            managerController.showView();
        }
    }
    
    /**
     * Listens for button click events originating from the Kitchen Staff button.
     *  It hides the log in screen and displays the Kitchen Staff's view.
     */
    private class KitchenLogInListener implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent e) {
            loginController.hideView();
          kitchenController.showView();
        }
    }   
    
    /**
     * Listens for button click events originating from the Kitchen Staff button.
     *  It hides the log in screen and (TODO:) displays the Kitchen Staff's view.     *
     */
    private class WaiterLogInListener implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent e) {
            loginController.hideView();
            waiterController.showView();
           
        }
    }
    /**
     * 
     * @author Hannah Cooper
     * Listens for button click of update stock on edit stock page. If action is performed then the 
     * selected ingredient will be updated to the number in which is inputted by the user
     * in the textField.
     */
    public class UpdateStockListener implements ActionListener {
    	@Override
		public final void actionPerformed(final ActionEvent e) {
    		int text = Integer.parseInt(EditStockGUI.getTextField().getText());
    		
    		EditStockGUI.selectedItem.setStock(text);
    		EditStockModel.changeStockLevel(EditStockGUI.selectedItem.getId(), text); 
    		EditStockGUI.level.setText(String.valueOf(EditStockGUI.selectedItem.getStock()));

    		EditStockGUI.panel.repaint();
    	}
    }
    
    /**
	 * 
	 * @author Hannah Cooper
	 * Listener for the edit stock button in KitchenView
	 *
	 */
	private class EditStockListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			kitchenController.hideView();
			stockController.showView();

			
		}
	}
    
    /** Starts up the application.     */
    private void run() {
        loginController.showView();
    }

    /**
     * The main entry point for the application.
     * 
     * @param args the command line arguments passed to the application.
     */
    public static void main(final String[] args) {
        launch();
    }

}
