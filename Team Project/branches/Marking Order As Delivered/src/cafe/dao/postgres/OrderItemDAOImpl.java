package cafe.dao.postgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cafe.OrderItem;
import cafe.dao.DataAccessException;
import cafe.dao.DishDAO;
import cafe.dao.OrderItemDAO;
import cafe.dao.jdbc.ConnectionFactory;

/**
 * A data access object used to manipulate {@link OrderItem} objects that have
 * persisted to a PostgreSQL database.
 * 
 * @author Robert Kardjaliev
 * @author Michael Winter
 */
public final class OrderItemDAOImpl implements OrderItemDAO {
	/**
	 * The source of JDBC connections used to perform queries against the
	 * underlying database.
	 */
	private ConnectionFactory connectionFactory;
	/**
	 * The data access object used to obtain {@link Dish} instances.
	 */
	private final DishDAO dishDAO;
    
    
    /**
     * Constructs a data access object for {@link OrderItem}s that will
     * operate over the given connection.
     * 
     * @param dishDAO
     *            the data access object used to provide {@link Dish} instances.
     * @param connectionFactory
     *            the factory object that will provide a database connection.
     */
    public OrderItemDAOImpl(final DishDAO dishDAO, final ConnectionFactory connectionFactory) {
        if (dishDAO == null) {
            throw new NullPointerException("dishDAO is null");
        }
        if (connectionFactory == null) {
            throw new NullPointerException("connectionFactory is null");
        }
        this.dishDAO = dishDAO;
        this.connectionFactory = connectionFactory;
    }

	
    /*
     * (non-Javadoc)
     * @see cafe.dao.selectOrderItems#findOrderItem(int)
     */
    @Override
    public List<OrderItem> selectOrderItems(final int id) {
        final Connection connection = connectionFactory.getConnection();
        final List<OrderItem> items = new ArrayList<>();
        try (final PreparedStatement statement = connection.prepareStatement(
                "SELECT dish_id, quantity, suggested"
                + " FROM order_item"
                + " WHERE order_id = ?")) {
            statement.setInt(1, id);
            final ResultSet result = statement.executeQuery();
            while (result.next()) {
                items.add(new OrderItem(dishDAO.findDish(result.getInt("dish_id")),
                        result.getInt("quantity"), result.getBoolean("suggested")));
            }
        } catch (final SQLException e) {
            throw new DataAccessException(e);
        }
        return items;
    }
	
	/*
	 * (non-Javadoc)
	 * @see cafe.dao.OrderItemDAO#findOrderItem(int, int)
	 */
	@Override
	public OrderItem findOrderItem(final int orderId, final int dishId) {
		final Connection connection = connectionFactory.getConnection();
		OrderItem orderItem = null;
		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT quantity, suggested FROM order_item WHERE order_id = ? AND dish_id = ?")) {
			statement.setInt(1, orderId);
			statement.setInt(2, dishId);
			ResultSet result = statement.executeQuery();
			if (result.next()) {
				orderItem = new OrderItem(dishDAO.findDish(dishId),
						result.getInt("quantity"),
						result.getBoolean("suggested"));
			}
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}
		return orderItem;
	}

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.OrderItemDAO#createOrderItem(int, int, int, boolean)
     */
    @Override
    public void createOrderItem(final int orderId, final int dishId,
            final int quantity, final boolean suggested) {
        final Connection connection = connectionFactory.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO order_item"
                        + " (order_id, dish_id, quantity, suggested)"
                        + " VALUES (?, ?, ?, ?)",
                PreparedStatement.RETURN_GENERATED_KEYS)) {
            // CHECKSTYLE IGNORE MagicNumberCheck FOR NEXT 4 LINES
            statement.setInt(1, orderId);
            statement.setInt(2, dishId);
            statement.setInt(3, quantity);
            statement.setBoolean(4, suggested);
            statement.executeUpdate();
        } catch (final SQLException e) {
            throw new DataAccessException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.OrderItemDAO#deleteOrderItem(int, int)
     */
    @Override
    public void deleteOrderItem(final int orderId, final int dishId) {
        final Connection connection = connectionFactory.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM order_item"
                        + " WHERE order_id = ? AND dish_id = ?")) {
            // CHECKSTYLE IGNORE MagicNumberCheck FOR NEXT 2 LINES
            statement.setInt(1, orderId);
            statement.setInt(2, dishId);
            statement.executeUpdate();
        } catch (final SQLException e) {
            throw new DataAccessException(e);
        }
    }


    /* (non-Javadoc)
     * @see cafe.dao.OrderItemDAO#updateOrderItem(cafe.OrderItem)
     */
    @Override
    public void updateOrderItem(final int orderId, final OrderItem item) {
        final Connection connection = connectionFactory.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE order_item"
                        + " SET quantity = ?, suggested = ?"
                        + " WHERE order_id = ? AND dish_id = ?")) {
            // CHECKSTYLE IGNORE MagicNumberCheck FOR NEXT 4 LINES
            statement.setInt(1, item.getQuantity());
            statement.setBoolean(2, item.isSuggested());
            statement.setInt(3, orderId);
            statement.setInt(4, item.getDish().getId());
            statement.executeUpdate();
        } catch (final SQLException e) {
            throw new DataAccessException(e);
        }
    }
}