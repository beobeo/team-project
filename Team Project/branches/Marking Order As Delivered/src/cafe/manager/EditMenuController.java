package cafe.manager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

/**
 * Observes user events from the {@link EditMenuView view} and directs behaviour accordingly,
 * invoking the {@link EditMenuView model} where appropriate and referring events to higher-level
 * observing controllers.
 * @author Robert Kardjaliev
 *
 */
public class EditMenuController {
	/** The view for the Edit Menu GUI. */
	private EditMenuView view;

	/**
	 * A boolean variable to keep track of the current visibility of a dish.
	 */
	private boolean dishVisibility;
	/** The model for the Edit Menu GUI. */
	private EditMenuModel model;
	/**
	 * Hides the view associated with this controller.
	 */
	public final void hideView() {
		view.setVisible(false);
	}

	/**
	 * Adds a listener to the back button.
	 * @param listener
	 * 				the listener.
	 */
	public final void addBackButtonListener(final ActionListener listener) {
		view.addBackButtonListener(listener);        
	}
	/**
	 * Adds a listener to the Change Visibility Button.
	 * @param listener
	 * 				the listener.
	 */
	public final void addVisibleListener(final ActionListener listener) {
		view.addVisibleListener(listener);        
	}

	/**
	 * Adds a listener to the {@code dishNameComboBox} in the view.
	 * @param listener
	 * 				the listener.
	 */
	public final void addComboListener(final ActionListener listener) {
		view.addComboListener(listener);        
	}
	/**
	 * Shows the view associated with this controller.
	 */
	public final void showView() {
		view.setVisible(true);
	}

	/**
	 * Initializes the fiels for this controller.
	 * @param view 
	 * 			The View.
	 * @param model
	 * 			The Model.
	 */
	public EditMenuController(final EditMenuView view, final EditMenuModel model) {
		this.view = view;
		this.model = model;
		setDishNameComboBox();
		setDishVisibility();
		view.setLabel(" ");
	}
	
	/**
	 * Gets the currently selected {@code Dish} and displays its current
	 * visibility.
	 */
	private void setDishVisibility() {
		dishVisibility = model.getVisible(view.getDishName());
		if (dishVisibility) {
			view.setVisibleLabel("Visible");
		} else {
			view.setVisibleLabel("Not visible");
		}
	}
	/**
	 * Adds all the {@code Dish} names to the dish name combo box in the view.
	 */
	private void setDishNameComboBox() {
		Set<String> names = new HashSet<String>(model.getDishNames());
		for (String str: names) {
			view.addDishNameComboBoxItems(str);
			view.addVisibleListener(new SetVisibleButtonListener());
			view.addComboListener(new ComboBoxListener());
		}
	}
	/**
	 * Listens for combo box selections. When a new item is selected it
	 * sets the label displaying the previous action performed to empty.
	 *
	 */
	private class ComboBoxListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			setDishVisibility();
			view.setLabel(" ");
	}
	}
	
	/**
	 * Listens for Change Visibility button clicks. When pressed, it takes the currently
	 * selected {@code Dish} and changes its visibility to the oposite in the database
	 * and displays it as appropriate in the labels.
	 *
	 */
	private class SetVisibleButtonListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			model.setVisible(view.getDishName(), !dishVisibility);
			if (!dishVisibility) {
				view.setLabel(view.getDishName() + " changed to visible ");
			} else {
				view.setLabel(view.getDishName() + " changed to not visible ");
			}
			setDishVisibility();
		}

	}	
}