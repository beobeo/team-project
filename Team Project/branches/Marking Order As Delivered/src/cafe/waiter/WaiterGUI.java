package cafe.waiter;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

/**
 * 
 * @author Jonathan Hercock
 * @author Adam Lumber
 */
public class WaiterGUI extends JFrame {
    
    /**
     * The container for the Waiter View.
     */
    private JPanel contentPane;
    /**
     *  The container for the Orders.
     */
    private JPanel callContainer;
    /**
     * A scrollpane.
     */
    private JScrollPane scrollPane;
   
    /** Example of a Waiter, used to feed a name and ID to the GUI. */
    public static final Waiter TEST_WAITER = new Waiter(1234321, "John",
            "Smith", true, null);

    /**
     *  Automated ID.
     */
    private static final long serialVersionUID = 1L;


    /**
     * 
     * Launch the application.
     * 
     * @param args
     *            Arguments.
     */
    public static void main(final String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    WaiterGUI frame = new WaiterGUI();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Specifies how listed orders are arranged in this UI.
     */
    private static final GridBagConstraints CONSTRAINTS = new GridBagConstraints();
    
    //CHECKSTYLE:OFF 
    static {
        CONSTRAINTS.anchor = GridBagConstraints.NORTHWEST;
        CONSTRAINTS.fill = GridBagConstraints.HORIZONTAL;
        CONSTRAINTS.insets = new Insets(0, 0, 10, 0);
        CONSTRAINTS.gridx = 0;
        CONSTRAINTS.gridy = GridBagConstraints.RELATIVE;
    }
  //CHECKSTYLE:ON
    
    /**
     * Adds the given {@code OrderListItemView} to the current list of order panels.
     * 
     * @param listView
     *            the view to add.
     */
    public final void addListItem(final WaiterOLIView listView) {
        callContainer.add(listView, CONSTRAINTS);
        callContainer.revalidate();
        callContainer.repaint();
    }

    
    /**
     * Removes the given {@code OrderListItemView} from the current list of order panels.
     * 
     * @param waiterOLIView
     *            the view to remove.
     */
    public final void removeListItem(final WaiterOLIView waiterOLIView) {
        callContainer.remove(waiterOLIView);
        callContainer.revalidate();
        callContainer.repaint();
    }

    /**
     * Create the frame.
     */
    //CHECKSTYLE:OFF
    @SuppressWarnings({ })
    public WaiterGUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 710, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JLabel lblWaiter = new JLabel("WAITER");
        lblWaiter.setFont(new Font("Tahoma", Font.BOLD, 16));
        lblWaiter.setBounds(20, 11, 78, 31);
        contentPane.add(lblWaiter);
        
        JLabel lblWaiterName = new JLabel("Logged in as:");
        lblWaiterName.setFont(new Font("Tahoma", Font.ITALIC, 11));
        lblWaiterName.setBounds(154, 21, 78, 14);
        contentPane.add(lblWaiterName);
        
        JLabel label = new JLabel();
        label.setForeground(Color.BLUE);
        label.setText(TEST_WAITER.getName());
        label.setBounds(230, 21, 78, 14);
        contentPane.add(label);
        
        JButton btnLogOut = new JButton("Log Out");
        btnLogOut.setBounds(595, 12, 89, 23);
        btnLogOut.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0)
            {
                
                System.exit(0);
            }
        });
       
        contentPane.add(btnLogOut);
        
        JLabel lblId = new JLabel("ID:");
        lblId.setFont(new Font("Tahoma", Font.ITALIC, 11));
        lblId.setBounds(318, 21, 26, 14);
        contentPane.add(lblId);
        
        JLabel label_1 = new JLabel();
        label_1.setForeground(Color.BLUE);
        label_1.setText(String.valueOf(TEST_WAITER.getId()));
        label_1.setBounds(345, 21, 78, 14);
        contentPane.add(label_1);
        
        JButton btnCallList = new JButton("Call List");
        btnCallList.setBounds(274, 328, 130, 23);
        contentPane.add(btnCallList);
        
        JLabel lblAvailability = new JLabel("Available:");
        lblAvailability.setFont(new Font("Tahoma", Font.ITALIC, 11));
        lblAvailability.setBounds(410, 21, 78, 14);
        contentPane.add(lblAvailability);
        
        JLabel label_2 = new JLabel();
        if (TEST_WAITER.getAvailable() == false)
        {
            label_2.setText("No");  
        }
        if (TEST_WAITER.getAvailable() != false)
        {
            label_2.setText("Yes");  
        }
        label_2.setForeground(Color.BLUE);
        label_2.setBounds(467, 21, 78, 14);
        contentPane.add(label_2);
        
        callContainer = new JPanel();
        callContainer.setBounds(30, 53, 194, 254);
        scrollPane = new JScrollPane();
        scrollPane.setBounds(30, 60, 614, 244);
        contentPane.add(scrollPane);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setViewportView(callContainer);
        
        
        btnCallList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                if (scrollPane.isVisible() == true) {scrollPane.setVisible(false);}
                else if (scrollPane.isVisible() == false) {scrollPane.setVisible(true);}
                    
            }
        });
    
    }
}

  //CHECKSTYLE:ON

