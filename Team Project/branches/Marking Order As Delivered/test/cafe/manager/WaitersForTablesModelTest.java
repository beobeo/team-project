package cafe.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import cafe.dao.DAOFactory;
import cafe.dao.TableDAO;
import cafe.dao.EmployeeDAO;
import cafe.dao.postgres.DefaultTestDAOFactoryImpl;

/**
 * Unit test for the Waiters for Tables {@link WaitersForTables mode}.
 * @author Robert Kardjaliev
 */
public class WaitersForTablesModelTest {
	/** An instance of the DAO factory. */
    private DAOFactory factory = DAOFactory.getInstance("zyvc215", "IYIP1845",
            DefaultTestDAOFactoryImpl.class);
	/** A table data access {@link TableDAO object}. */
    private TableDAO tableData = factory.getTableDAO();
	/** An employee data access {@link EmployeeDAO object}. */
    private EmployeeDAO emlpoyeeData = factory.getEmployeeDAO();
    /**
     * A {@code Waiter}'s first name.
     */
    private String name = "Harry";
    
    /**A {@code Table} number. */
    private int tableNum;
    
    private WaitersForTablesModel model = new WaitersForTablesModel(tableData, emlpoyeeData);
	@Before
	public final void setUp() {
		tableNum = tableData.createTable();
		tableData.assignWaiterToTable(1, "David");
	}
	
    
	/**
	 * Checks whether the last {@code Table} in the database(the one created at the setUp()) will get removed.
	 */
	@Test
	public final void checkOrderState() {
		model.removeTable();
		assertEquals("Test 1: Check for correct removal of a table.", model.getTableId(), tableNum);
	}
	
	/**
	 * Checks whether removal of a {@code Table} will decrease the size of the {@code Table Numbers'} list.
	 */
	@Test
	public final void testTableListSize() {
		 model.setAllTableNums();
		int tableListSize = tableData.getAllTables().size();
		model.removeTable();
		assertEquals("Test 2: Removal of table decreases the number of total tables in database.",
				tableListSize-1, tableData.getAllTables().size());
	}
	/**
	 * Checks whether correct assignment of {@code Waiter} to {@code Table} occurs.
	 */
	@Test
	public final void assigningWaiterToTable() {
		model.assignWaiterToTable(1, name);
		assertTrue("Test 3: Assignment of waiter to table.",
				tableData.findTable(1).getWaiter().getFirstName().equals(name));
	}
	
}
