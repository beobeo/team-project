package cafe.trackorder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import cafe.Order;
import cafe.OrderState;
import cafe.dao.DAOFactory;
import cafe.dao.OrderDAO;
import cafe.dao.postgres.DefaultTestDAOFactoryImpl;
import cafe.trackorder.TrackOrderModel;

/**
 * Unit test for the {@code TrackOrder} model class.
 * @author Robert Kardjaliev
 */
public class TrackOrderModelTest {
	/** An instance of the DAO factory. */
    private DAOFactory factory = DAOFactory.getInstance("zyvc215", "IYIP1845",
            DefaultTestDAOFactoryImpl.class);
	/** An Order data access {@link OrderDAO object}. */
    private OrderDAO orderData = factory.getOrderDAO();
	
	/** The {@link Order}'s ID. */
	private int id = 2;
	
	/** An Order State {@link OrderState object}. */
	private OrderState state;
	/**
	 * A {@link TrackOrderModel} instance.
	 */
	private TrackOrderModel test = new TrackOrderModel(orderData, id);
	
	/**
	 * Checking whether the {@link OrderState} of the {@link Order} is what this 
	 * test was designed for. Might get changed or deleted in the database.
	 */
	@Test
	public final void checkOrderState() {
		state = OrderState.READY;
		assertTrue("Test 1: Test for correct calculation of Order progress.", 
				state.compareTo(test.getOrderState()) == 0);
	}
	
	/**
	 * Checking whether correct {@code progress} will be assigned to the progress bar
	 * when loading the {@link Order} from the database.
	 */
	@Test
	public final void testProgressBar() {
		// CHECKSTYLE IGNORE MagicNumberCheck FOR NEXT 2 LINES
		assertEquals("Test 2: Test for correct calculation of Order progress.", 80,
				test.getOrderProgress(), 0.01f);
	}
}
