package cafe;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.UIManager;

import java.awt.Insets;

/**
 * Presents a graphical window to the client from which they can view menu items, add those items
 * to an order, and call for assistance from the attending waiters.
 * 
 * @author Michael Winter
 * @author Jonny Hercock
 */
public class MenuGUI extends JFrame {
	/**
	 * Represents the number of dishes that should be displayed at once in the
	 * view due to size limitations.
	 */
	private static int DISHES_PER_PAGE = 3;

	/**
	 * Unique serialisation identifier.
	 */
	private static final long serialVersionUID = 2922450639815942656L;

	/**
	 * Returns the number of dishes that are expected to fit in the container component.
	 * 
	 * @return the maximum number of dishes that should be added to the view.
	 */
	public static int getDishesPerPage() {
    	return DISHES_PER_PAGE;
    }
	
	
    /**
	 * Provides a container within the (normally hidden) scroll pane into which displayed menu
	 * items are rendered.
	 */
    private JPanel dishContainer;
    /**
     * Allows the client to move to later pages in the menu.
     */
    private JButton nextPage;
    /**
     * Allows the client to move to earlier pages in the menu.
     */
    private JButton previousPage;
    /**
     * Alerts attending waiters that this table requires assistance.
     */
    private JButton requestAssistance;

    /**
     * Hides this view and notifies the application controller that the order status view needs to
     * be displayed.
     */
    private JButton showOrder;
    
	/**
     * Create the frame.
     */
    public MenuGUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 693, 483);
        JPanel contentPane = new JPanel();
        contentPane.setBackground(SystemColor.activeCaption);
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        showOrder = new JButton("Order");
        showOrder.setBounds(578, 11, 89, 23);
        contentPane.add(showOrder);
        
        JButton showMenu = new JButton("Menu");
        showMenu.setEnabled(false);
        showMenu.setBounds(479, 11, 89, 23);
        contentPane.add(showMenu);
        
        requestAssistance = new JButton("Call for Assistance");
        requestAssistance.setBounds(10, 403, 152, 30);
        contentPane.add(requestAssistance);
        
        JLabel label_1 = new JLabel();
        label_1.setBounds(510, 150, 50, 50);
        label_1.setIcon(new ImageIcon("Images/pepPizza.jpg"));
        contentPane.add(label_1);
        
        JLabel label_2 = new JLabel();
        label_2.setBounds(510, 50, 50, 50);
        label_2.setIcon(new ImageIcon("Images/pepPizza.jpg"));
        contentPane.add(label_2);
        
        JLabel label_3 = new JLabel();
        label_3.setBounds(510, 240, 50, 50);
        label_3.setIcon(new ImageIcon("Images/pepPizza.jpg"));
        contentPane.add(label_3);
        
        JScrollPane dishContainerScroller = new JScrollPane();
        dishContainerScroller.setBackground(SystemColor.activeCaption);
        dishContainerScroller.setBorder(null);
        dishContainerScroller.setHorizontalScrollBarPolicy(
        		ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        dishContainerScroller.setBounds(63, 54, 546, 312);
        contentPane.add(dishContainerScroller);
        
        dishContainer = new JPanel();
        dishContainer.setBackground(SystemColor.activeCaption);
        dishContainer.setMaximumSize(new Dimension(546, 32767));
        dishContainer.setBorder(null);
        dishContainerScroller.setViewportView(dishContainer);
        dishContainer.setLayout(new GridLayout(3, 1, 0, 2));
        /*
        JTextPane txtpnSampleDish = new JTextPane();
        txtpnSampleDish.setVisible(false);
        dishContainer.setLayout(new GridLayout(3, 1, 0, 0));
        dishContainer.add(txtpnSampleDish);
        txtpnSampleDish.setForeground(SystemColor.text);
        txtpnSampleDish.setFont(new Font("Tahoma", Font.PLAIN, 12));
        txtpnSampleDish.setBackground(SystemColor.activeCaption);
        txtpnSampleDish.setText("Example Dish");
        
        JTextPane txtpnDescription = new JTextPane();
        txtpnDescription.setVisible(false);
        dishContainer.add(txtpnDescription);
        txtpnDescription.setFont(new Font("Tahoma", Font.ITALIC, 11));
        txtpnDescription.setBackground(SystemColor.activeCaption);
        txtpnDescription.setText("Description                                       ");
        
        JTextPane txtpnExampleDish = new JTextPane();
        txtpnExampleDish.setVisible(false);
        dishContainer.add(txtpnExampleDish);
        txtpnExampleDish.setFont(new Font("Tahoma", Font.PLAIN, 12));
        txtpnExampleDish.setForeground(SystemColor.text);
        txtpnExampleDish.setBackground(SystemColor.activeCaption);
        txtpnExampleDish.setText("Lasagne");
        
        JTextPane txtpnDescriptionIngredients = new JTextPane();
        txtpnDescriptionIngredients.setVisible(false);
        dishContainer.add(txtpnDescriptionIngredients);
        txtpnDescriptionIngredients.setFont(new Font("Tahoma", Font.ITALIC, 11));
        txtpnDescriptionIngredients.setBackground(SystemColor.activeCaption);
        txtpnDescriptionIngredients.setText("Homemade Lasagne. Served with chips and salad.");
        
        JTextPane txtpnDishPicture = new JTextPane();
        txtpnDishPicture.setVisible(false);
        dishContainer.add(txtpnDishPicture);
        txtpnDishPicture.setText("Dish Picture");
        
        JTextPane textPane_1 = new JTextPane();
        textPane_1.setVisible(false);
        dishContainer.add(textPane_1);
        textPane_1.setText("Dish Picture");
        
        JLabel lblDishPrice = new JLabel("Dish Price");
        lblDishPrice.setVisible(false);
        dishContainer.add(lblDishPrice);
        lblDishPrice.setForeground(SystemColor.window);
        
        JLabel label = new JLabel("\u00A38.95");
        label.setVisible(false);
        dishContainer.add(label);
        label.setForeground(SystemColor.text);
        
        JButton btnAddToOrder = new JButton("Add to Order");
        btnAddToOrder.setVisible(false);
        dishContainer.add(btnAddToOrder);
        btnAddToOrder.setFont(new Font("Tahoma", Font.PLAIN, 9));
        
        JButton button = new JButton("Add to Order");
        button.setVisible(false);
        dishContainer.add(button);
        button.setFont(new Font("Tahoma", Font.PLAIN, 9));
        
        JTextPane textPane = new JTextPane();
        textPane.setVisible(false);
        dishContainer.add(textPane);
        textPane.setForeground(SystemColor.text);
        textPane.setFont(new Font("Tahoma", Font.PLAIN, 12));
        textPane.setBackground(SystemColor.activeCaption);
        textPane.setText("Example Dish");
        
        JTextPane txtpnDescription_1 = new JTextPane();
        txtpnDescription_1.setVisible(false);
        dishContainer.add(txtpnDescription_1);
        txtpnDescription_1.setFont(new Font("Tahoma", Font.ITALIC, 11));
        txtpnDescription_1.setBackground(SystemColor.activeCaption);
        txtpnDescription_1.setText(" Description                                     ");
        
        JLabel label_1 = new JLabel("Dish Price");
        label_1.setVisible(false);
        dishContainer.add(label_1);
        label_1.setForeground(SystemColor.text);
        
        JButton button_1 = new JButton("Add to Order");
        button_1.setVisible(false);
        dishContainer.add(button_1);
        button_1.setFont(new Font("Tahoma", Font.PLAIN, 9));
        
        JTextPane textPane_3 = new JTextPane();
        textPane_3.setVisible(false);
        dishContainer.add(textPane_3);
        textPane_3.setText("Dish Picture");
        
        JTextPane txtpnBeefEggCheese = new JTextPane();
        txtpnBeefEggCheese.setVisible(false);
        dishContainer.add(txtpnBeefEggCheese);
        txtpnBeefEggCheese.setFont(new Font("Tahoma", Font.ITALIC, 11));
        txtpnBeefEggCheese.setBackground(SystemColor.activeCaption);
        txtpnBeefEggCheese.setText("Contains: Beef, Egg, Cheese");
        
        JTextPane txtpnIngredients = new JTextPane();
        txtpnIngredients.setVisible(false);
        dishContainer.add(txtpnIngredients);
        txtpnIngredients.setFont(new Font("Tahoma", Font.ITALIC, 11));
        txtpnIngredients.setText("Ingredients");
        txtpnIngredients.setBackground(SystemColor.activeCaption);
        
        JTextPane textPane_4 = new JTextPane();
        textPane_4.setVisible(false);
        dishContainer.add(textPane_4);
        textPane_4.setFont(new Font("Tahoma", Font.ITALIC, 11));
        textPane_4.setText("Ingredients");
        textPane_4.setBackground(SystemColor.activeCaption);
        
        JLabel lblMains = new JLabel("Mains (1/5)");
        lblMains.setVisible(false);
        dishContainer.add(lblMains);
        lblMains.setFont(new Font("Tahoma", Font.BOLD, 12));
        
        JLabel lblCalories = new JLabel("Calories");
        lblCalories.setVisible(false);
        dishContainer.add(lblCalories);
        lblCalories.setForeground(Color.WHITE);
        
        JLabel lblkcl = new JLabel("1080kcal");
        lblkcl.setVisible(false);
        dishContainer.add(lblkcl);
        lblkcl.setForeground(Color.WHITE);
        
        JLabel label_3 = new JLabel("Calories");
        label_3.setVisible(false);
        dishContainer.add(label_3);
        label_3.setForeground(Color.WHITE);
        */
        JLabel menu = new JLabel("MENU");
        menu.setFont(new Font("Tahoma", Font.BOLD, 15));
        menu.setBounds(10, 13, 46, 14);
        contentPane.add(menu);
        
        nextPage = new JButton(">");
        nextPage.setMargin(new Insets(1, 1, 1, 1));
        nextPage.setBorder(UIManager.getBorder("Button.border"));
        nextPage.setEnabled(false);
        nextPage.setActionCommand("next");
        nextPage.setBounds(581, 377, 28, 22);
        contentPane.add(nextPage);
        
        previousPage = new JButton("<");
        previousPage.setMargin(new Insets(1, 1, 1, 1));
        previousPage.setBorder(UIManager.getBorder("Button.border"));
        previousPage.setActionCommand("previous");
        previousPage.setEnabled(false);
        previousPage.setBounds(540, 377, 28, 22);
        contentPane.add(previousPage);
        
        JMenuBar sortingMenuBar = new JMenuBar();
        sortingMenuBar.setOpaque(false);
        sortingMenuBar.setBorderPainted(false);
        sortingMenuBar.setBounds(75, 11, 97, 21);
        contentPane.add(sortingMenuBar);
        
        final JMenu sortingMenu = new JMenu("Sort by");
        sortingMenu.setEnabled(false);
        sortingMenu.setMnemonic('S');
        sortingMenuBar.add(sortingMenu);
        
        JRadioButtonMenuItem rdbtnmntmCalories = new JRadioButtonMenuItem("Calories");
        rdbtnmntmCalories.setSelected(true);
        sortingMenu.add(rdbtnmntmCalories);
        
        JRadioButtonMenuItem rdbtnmntmPopularity = new JRadioButtonMenuItem("Popularity");
        sortingMenu.add(rdbtnmntmPopularity);
        
        JRadioButtonMenuItem rdbtnmntmPrice = new JRadioButtonMenuItem("Price");
        sortingMenu.add(rdbtnmntmPrice);
        
        JMenuBar filterMenuBar = new JMenuBar();
        filterMenuBar.setOpaque(false);
        filterMenuBar.setBorderPainted(false);
        filterMenuBar.setBounds(255, 11, 97, 21);
        contentPane.add(filterMenuBar);
        
        JMenu filterMenu = new JMenu("Show only");
        filterMenu.setMnemonic('O');
        filterMenuBar.add(filterMenu);
        
        JRadioButtonMenuItem radioButtonMenuItem = new JRadioButtonMenuItem("Meal Deals");
        radioButtonMenuItem.setVisible(false);
        filterMenu.add(radioButtonMenuItem);
        
        JSeparator separator = new JSeparator();
        separator.setVisible(false);
        filterMenu.add(separator);
        
        JRadioButtonMenuItem radioButtonMenuItem_1 =
        		new JRadioButtonMenuItem("Desserts");
        radioButtonMenuItem_1.setVisible(false);
        filterMenu.add(radioButtonMenuItem_1);
        
        JRadioButtonMenuItem radioButtonMenuItem_2 =
        		new JRadioButtonMenuItem("Mains");
        radioButtonMenuItem_2.setSelected(true);
        filterMenu.add(radioButtonMenuItem_2);
        
        JRadioButtonMenuItem radioButtonMenuItem_3 =
        		new JRadioButtonMenuItem("Starters");
        radioButtonMenuItem_3.setVisible(false);
        filterMenu.add(radioButtonMenuItem_3);
        
        JSeparator separator_1 = new JSeparator();
        separator_1.setVisible(false);
        filterMenu.add(separator_1);
        
        JRadioButtonMenuItem radioButtonMenuItem_4 = 
        		new JRadioButtonMenuItem("Fish & Seafood");
        radioButtonMenuItem_4.setVisible(false);
        filterMenu.add(radioButtonMenuItem_4);
        
        JRadioButtonMenuItem radioButtonMenuItem_5 =
        		new JRadioButtonMenuItem("Halal Meats ");
        radioButtonMenuItem_5.setVisible(false);
        filterMenu.add(radioButtonMenuItem_5);
        
        JRadioButtonMenuItem radioButtonMenuItem_6 =
        		new JRadioButtonMenuItem("Lactose Free");
        radioButtonMenuItem_6.setVisible(false);
        filterMenu.add(radioButtonMenuItem_6);
        
        JRadioButtonMenuItem radioButtonMenuItem_7 =
        		new JRadioButtonMenuItem("Vegan");
        radioButtonMenuItem_7.setVisible(false);
        filterMenu.add(radioButtonMenuItem_7);
        
        JRadioButtonMenuItem radioButtonMenuItem_8 =
        		new JRadioButtonMenuItem("Vegetarian");
        radioButtonMenuItem_8.setVisible(false);
        filterMenu.add(radioButtonMenuItem_8);
        
    }    
    
	/**
	 * Adds an action listener to the Next Page button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
    public final void addNextPageListener(final ActionListener listener) {
    	nextPage.addActionListener(listener);
    }
    
	/**
	 * Adds an action listener to the Previous Page button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
    public final void addPreviousPageListener(final ActionListener listener) {
    	previousPage.addActionListener(listener);
    }
    
	/**
	 * Adds an action listener to the Order button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
    public final void addShowOrderListener(final ActionListener listener) {
    	showOrder.addActionListener(listener);
    }
    
	/**
	 * Adds an action listener to the Call for Assistance button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
    public final void addWaiterCallListener(final ActionListener listener) {
    	requestAssistance.addActionListener(listener);
    }
    
	/**
	 * Enables (or disables) the Next Page button.
	 * 
	 * @param listener
	 *             {@code true} to enable the button; {@code false} otherwise.
	 */
    public final void enableNextPageButton(final boolean enabled) {
    	nextPage.setEnabled(enabled);
    }
    
    
    /**
	 * Enables (or disables) the Previous Page button.
	 * 
	 * @param listener
	 *             {@code true} to enable the button; {@code false} otherwise.
	 */
    public final void enablePreviousPageButton(final boolean enabled) {
    	previousPage.setEnabled(enabled);
    }
    
    /**
     * Sets which dishes should be displayed by this view and redraws the display.
     * 
     * @param dishes
     * 			   an ordered list containing the dishes to display.
     */
    public final void setDishes(final List<DishMenuView> dishes) {
    	dishContainer.removeAll();

    	for (final DishMenuView dish : dishes) {
    		dishContainer.add(dish);
    	}
    	
    	revalidate();
    	repaint();
    }
}
