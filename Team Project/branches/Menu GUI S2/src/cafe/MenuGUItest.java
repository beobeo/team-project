package cafe;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

public class MenuGUItest extends JFrame {

    private JPanel contentPane;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MenuGUItest frame = new MenuGUItest();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public MenuGUItest() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JLabel lblNewLabel = new JLabel();
        lblNewLabel.setBounds(25, 50, 50, 50);
        lblNewLabel.setIcon(new ImageIcon("Images/chicPizza.jpg"));
        //lblNewLabel.setVisible(true);
        contentPane.add(lblNewLabel);     
        
        
        JLabel label = new JLabel();
        
        label.setBounds(25, 120, 50, 50);
        label.setIcon(new ImageIcon("Images/grisalm.jpg"));
        //lblNewLabel.setVisible(true);
        contentPane.add(label);
        
        JLabel label_1 = new JLabel();
        label_1.setBounds(26, 181, 50, 50);
        label_1.setIcon(new ImageIcon("Images/pepPizza.jpg"));
        contentPane.add(label_1);
    }
}
