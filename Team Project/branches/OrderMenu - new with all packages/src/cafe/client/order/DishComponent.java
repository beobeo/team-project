package cafe.client.order;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import cafe.OrderItem;
import javax.swing.JTextArea;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.SystemColor;
import java.awt.Rectangle;

/**
 * Component to hold the information about a dish.
 * 
 * @author JuLi
 * @author Adam
 * 
 */
public class DishComponent extends JPanel {

	/** Sets serial Version UID to 1L. */
	private static final long serialVersionUID = 1L;

	/** Button for remove. */
	private final JButton btnRemove = new JButton("-");

	/** Prints out the amount of food. */
	private JLabel lblQuantity = new JLabel("");

	/** Presents price. */
	private JLabel lblPrice = new JLabel("price");

	/** The Name of the dish. */
	private JLabel lblName = new JLabel("Name");

	/** Where the picture is displayed. */
	private final JLabel lblPicture = new JLabel("");
	/**
	 * Increase quantity button.
	 */
	private final JButton btnAdd = new JButton("+");
	private final JTextArea textArea = new JTextArea();

	/**
	 * Creates the panel and sets the label values to the dish values that are
	 * passed to the method.
	 * 
	 * @param orderItem
	 * 					- orderItem contains a {@code Dish} 
	 */
	// CHECKSTYLE:OFF
	public DishComponent(final OrderItem orderItem) {
		final String name = orderItem.getDish().getName();
		final String description = orderItem.getDish().getDescription();
		 
		final float price = orderItem.getDish().getPrice();
		final int quantity = orderItem.getQuantity();
		final BufferedImage image = orderItem.getDish().getImage();
		//final Dish dish = orderItem.getDish();
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 99, 90, 50, 29, 41, 41, 0 };
		gridBagLayout.rowHeights = new int[] { 25, 49, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0,
				0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);
		GridBagConstraints gbc_lblPicture = new GridBagConstraints();
		gbc_lblPicture.fill = GridBagConstraints.BOTH;
		gbc_lblPicture.insets = new Insets(0, 0, 0, 5);
		gbc_lblPicture.gridheight = 2;
		gbc_lblPicture.gridx = 0;
		gbc_lblPicture.gridy = 0;
		add(lblPicture, gbc_lblPicture);

		if (image != null) {
			lblPicture.setIcon(new ImageIcon(image));
		}

		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.SOUTH;
		gbc_lblName.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 1;
		gbc_lblName.gridy = 0;
		add(lblName, gbc_lblName);

		lblName.setText(name);

		GridBagConstraints gbc_lblPrice = new GridBagConstraints();
		gbc_lblPrice.insets = new Insets(0, 0, 5, 5);
		gbc_lblPrice.gridx = 3;
		gbc_lblPrice.gridy = 0;
		lblPrice.setAlignmentX(Component.CENTER_ALIGNMENT);
		add(lblPrice, gbc_lblPrice);
		lblPrice.setText(Float.toString(price));
		lblQuantity.setHorizontalAlignment(SwingConstants.CENTER);

		GridBagConstraints gbc_lblQuantity = new GridBagConstraints();
		gbc_lblQuantity.insets = new Insets(0, 0, 5, 0);
		gbc_lblQuantity.gridwidth = 2;
		gbc_lblQuantity.gridx = 4;
		gbc_lblQuantity.gridy = 0;
		add(lblQuantity, gbc_lblQuantity);
		lblQuantity.setText(Integer.toString(quantity));
		
		GridBagConstraints gbc_textArea = new GridBagConstraints();
		gbc_textArea.gridwidth = 2;
		gbc_textArea.insets = new Insets(0, 0, 0, 5);
		gbc_textArea.fill = GridBagConstraints.HORIZONTAL;
		gbc_textArea.anchor = GridBagConstraints.BASELINE;
		gbc_textArea.gridx = 1;
		gbc_textArea.gridy = 1;
		textArea.setBounds(new Rectangle(0, 0, 269, 0));
		textArea.setBackground(SystemColor.menu);
		textArea.setAlignmentY(Component.TOP_ALIGNMENT);
		textArea.setAlignmentX(Component.LEFT_ALIGNMENT);
		textArea.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textArea.setTabSize(2);
		textArea.setMaximumSize(new Dimension(4, 16));
		textArea.setMinimumSize(new Dimension(4, 16));
		textArea.setLineWrap(true);
		textArea.setBorder(null);
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);
		add(textArea, gbc_textArea);
		textArea.setText(description);
		
		GridBagConstraints gbc_btnAdd = new GridBagConstraints();
		gbc_btnAdd.insets = new Insets(0, 0, 0, 5);
		gbc_btnAdd.gridx = 4;
		gbc_btnAdd.gridy = 1;
		add(btnAdd, gbc_btnAdd);

		GridBagConstraints gbc_btnRemove = new GridBagConstraints();
		gbc_btnRemove.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnRemove.gridx = 5;
		gbc_btnRemove.gridy = 1;
		add(btnRemove, gbc_btnRemove);

	}
	//CHECKSTYLE:ON
	/**
	 * Listener for the remove and add buttons.
	 * 
	 * @param 	listener
	 * 					- action listener
	 */
	public final void addQuantityListener(final ActionListener listener) {
		btnRemove.addActionListener(listener);
		btnAdd.addActionListener(listener);
	}
}
