package cafe.client.payment;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
/**
 * Presents a graphical window to the customer for payment.
 * 
 * @author Robert Kardjaliev
 */
public class PaymentView extends JFrame {

	/**
	 * Unique serialisation identifier.
	 */
	private static final long serialVersionUID = 8830889410240081933L;
	 /**  The Main GUI Panel.    */
    private JPanel contentPane;
    /**  The button for paying with cash.  */
    private JButton btnPayCash;
    /** The button for paying by card. */
	private JButton btnPayCard;
	/** The label that will display the amount to be paid for an order. */
	private JLabel priceLabel;
	/** The label that will display a message when payment is done. */
	private JLabel msgLabel;

    /**
     * Create the frame.
     */
    ///CHECKSTYLE:OFF
    public PaymentView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        btnPayCash = new JButton();
        btnPayCash.setFont(new Font("Tahoma", Font.BOLD, 13));
        btnPayCash.setText("Pay with cash");
        btnPayCash.setBounds(64, 177, 135, 46);
        contentPane.add(btnPayCash);
        
        btnPayCard = new JButton();
        btnPayCard.setText("Pay with card");
        btnPayCard.setFont(new Font("Tahoma", Font.BOLD, 13));
        btnPayCard.setBounds(220, 177, 135, 46);
        contentPane.add(btnPayCard);
        
        JLabel priceTxtLabel = new JLabel("Price:");
        priceTxtLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
        priceTxtLabel.setBounds(153, 146, 46, 20);
        contentPane.add(priceTxtLabel);
        
        priceLabel = new JLabel("");
        priceLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
        priceLabel.setBounds(209, 146, 46, 20);
        contentPane.add(priceLabel);
        
        msgLabel = new JLabel("");
        msgLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
        msgLabel.setBounds(153, 234, 135, 14);
        contentPane.add(msgLabel);
        
    }
    //CHECKSTYLE:ON
    
    /**
     * Adds a listener to pay with cash button.
     * @param listener - 
     * 				the listener for the pay with cash button.
     */
    public final void addPayWithCashListener(final ActionListener listener) {
        btnPayCash.addActionListener(listener);
    }
    /**
     * Adds a listener to pay with card button.
     * @param listener - 
     * 				the listener for the pay with card button.
     */
    public final void addPayWithCardListener(final ActionListener listener) {
        btnPayCard.addActionListener(listener);
    }
    
    /**
     * Sets the price an order costs in the price label.
     * @param fl
     * 			the float representation of the price.
     */
    public final void setPrice(final Float fl) {
        priceLabel.setText(fl.toString());
    }
    
    /**
     * Sets the price an order costs in the price label.
     * @param str
     * 			the String message to be displayed.
     */
    public final void setMessage(final String str) {
        msgLabel.setText(str);
    }
}