/**
 * Defines data access objects and their related factories for all create-read-update-delete (CRUD).
 * persistence operations.
 * 
 * @author Michael Winter
 */
package cafe.dao;