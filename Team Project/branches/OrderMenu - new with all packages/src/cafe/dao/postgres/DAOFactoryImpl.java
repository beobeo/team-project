package cafe.dao.postgres;

import cafe.dao.DishDAO;
import cafe.dao.EmployeeDAO;
import cafe.dao.IngredientDAO;
import cafe.dao.OrderDAO;
import cafe.dao.OrderItemDAO;
import cafe.dao.PaymentDAO;
import cafe.dao.SittingDAO;
import cafe.dao.TableDAO;
import cafe.dao.jdbc.AbstractDAOFactoryImpl;

/**
 * This class is a factory that is responsible for creating data access objects
 * (DAOs) used to interact with an underlying PostgreSQL database.
 * 
 * @author Michael Winter
 */
public final class DAOFactoryImpl extends AbstractDAOFactoryImpl {
    /**
     * The JDBC connection string for the project database. The full host name
     * is used to allow remote connections.
     */
    private static final String URI = "jdbc:postgresql://teaching.cs.rhul.ac.uk:29503/teamproject";

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#getDishDAO()
     */
    @Override
    public DishDAO getDishDAO() {
        return new DishDAOImpl(getIngredientDAO(), this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#getIngredientDAO()
     */
    @Override
    public IngredientDAO getIngredientDAO() {
        return new IngredientDAOImpl(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#getSittingDAO()
     */
    @Override
    public SittingDAO getSittingDAO() {
        return new SittingDAOImpl(getTableDAO(), this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#getSittingDAO()
     */
    @Override
    public TableDAO getTableDAO() {
        return new TableDAOImpl(getEmployeeDAO(), this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#getOrderDAO()
     */
    @Override
    public OrderDAO getOrderDAO() {
        return new OrderDAOImpl(getOrderItemDAO(), getSittingDAO(), getEmployeeDAO(), this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#getOrderItemDAO()
     */
    @Override
    public OrderItemDAO getOrderItemDAO() {
        return new OrderItemDAOImpl(getDishDAO(), this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#getEmployeeDAO()
     */
    @Override
    public EmployeeDAO getEmployeeDAO() {
        return new EmployeeDAOImpl(this);
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#getPaymentDAO()
     */
	@Override
	public PaymentDAO getPaymentDAO() {
		return new PaymentDAOImpl(this);
	}

    
    /* (non-Javadoc)
     * @see cafe.dao.jdbc.AbstractDAOFactoryImpl#getConnectionUri()
     */
    @Override
    protected String getConnectionUri() {
        return URI;
    }


}
