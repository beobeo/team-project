package cafe.dao.postgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cafe.Employee;
import cafe.dao.DataAccessException;
import cafe.dao.EmployeeDAO;
import cafe.dao.jdbc.ConnectionFactory;

/**
 * A data access object used to manipulate {@link Employee} objects that have
 * persisted to a PostgreSQL database.
 * 
 * @author Michael Winter
 */
public final class EmployeeDAOImpl implements EmployeeDAO {
	/**
	 * The source of JDBC connections used to perform queries against the
	 * underlying database.
	 */
	private ConnectionFactory connectionFactory;

	/**
	 * Constructs a data access object for {@link Employee} objects that will
	 * operate over the given connection.
	 * 
	 * @param connectionFactory
	 *            the factory object that will provide a database connection.
	 * @throws NullPointerException
	 *             if the specified factory is {@code null}.
	 */
	public EmployeeDAOImpl(final ConnectionFactory connectionFactory) {
		this.connectionFactory = connectionFactory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.EmployeeDAO#findEmployee(int)
	 */
	@Override
	public Employee findEmployee(final int id) {
		final Connection connection = connectionFactory.getConnection();
		Employee employee = null;
		try (PreparedStatement statement = connection
				.prepareStatement("SELECT first_name, last_name, role, password"
						+ " FROM employee" + " WHERE id = ?")) {
			statement.setInt(1, id);
			ResultSet result = statement.executeQuery();
			if (result.next()) {
				// TODO: Complete implementation of method
				employee = new Employee(id, result.getString("first_name"),
						result.getString("last_name"));
			}
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}
		return employee;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.EmployeeDAO#getAllWaiterNames()
	 */
	@Override
	public List<String> getAllWaiterNames() {
		final Connection connection = connectionFactory.getConnection();
		List<String> employees = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT first_name FROM employee WHERE role = \'Waiter\'"
		+ " ORDER BY first_name DESC")) {
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				employees.add(result.getString("first_name"));
			}
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}
		return employees;
	}
}
