package cafe.dao.postgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cafe.Ingredient;
import cafe.dao.DataAccessException;
import cafe.dao.IngredientDAO;
import cafe.dao.jdbc.ConnectionFactory;

/**
 * A data access object used to manipulate {@link Ingredient} objects that have
 * persisted to a PostgreSQL database.
 * 
 * @author Michael Winter
 */
public final class IngredientDAOImpl implements IngredientDAO {
    /**
<<<<<<< .working
=======
     * Given a Dish identifier and a connection, this method returns the
     * ingredients (and quantity) necessary to prepare that dish as a map.
     * 
     * @param dish
     *            the dish to be prepared.
     * @param connection
     *            a connection over which the query will be performed.
     * @return the mapping of ingredients-to-quantity required for the specified
     *         dish.
     */
    static Map<Ingredient, Integer> selectDishIngredients(final int dish,
            final Connection connection) {
        final Map<Ingredient, Integer> ingredients = new HashMap<>();
        try (final PreparedStatement statement = connection.prepareStatement(
                "SELECT id, name, price, stock, quantity"
                + " FROM ingredient"
                + " INNER JOIN dish_ingredient ON id = ingredient_id"
                + " WHERE dish_id = ?"
                + " ORDER BY price")) {
            statement.setInt(1, dish);
            final ResultSet result = statement.executeQuery();
            while (result.next()) {
                ingredients.put(new Ingredient(result.getInt("id"),
                        result.getString("name"), result.getFloat("price"),
                        result.getInt("stock")), result.getInt("quantity"));
            }
        } catch (final SQLException e) {
            throw new DataAccessException(e);
        }
        return ingredients;
    }
    
    
    /**
>>>>>>> .merge-right.r257
     * The source of JDBC connections used to perform queries against the
     * underlying database.
     */
    private ConnectionFactory connectionFactory;
    
    
    /**
     * Constructs a data access object for {@link Ingredient}s that will
     * operate over the given connection.
     * 
     * @param connectionFactory
     *            the factory object that will provide a database connection.
     * @throws NullPointerException
     *             if the specified factory is {@code null}.
     */
    public IngredientDAOImpl(final ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    
    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.IngredientDAO#findIngredient(int)
     */
    @Override
    public Ingredient findIngredient(final int id) {
        final Connection connection = connectionFactory.getConnection();
        Ingredient ingredient = null;
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT name, price, stock FROM ingredient WHERE id = ?")) {
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                ingredient = new Ingredient(id, result.getString("name"),
                        result.getFloat("price"), result.getInt("stock"));
            }
        } catch (final SQLException e) {
            throw new DataAccessException(e);
        }
        return ingredient;
    }
    
    /**
     * Given a Dish identifier and a connection, this method returns the
     * ingredients (and quantity) necessary to prepare that dish as a map.
     * 
     * @param dishId
     *            the dish to be prepared.
     * @return the mapping of ingredients-to-quantity required for the specified
     *         dish.
     * @throws DataAccessException
     *             if an error occurs while accessing the data store.
     */
    @Override
    public Map<Ingredient, Integer> selectDishIngredients(final int dishId) {
        final Connection connection = connectionFactory.getConnection();
        final Map<Ingredient, Integer> ingredients = new HashMap<>();
        try (final PreparedStatement statement = connection.prepareStatement(
                "SELECT id, name, price, stock, quantity"
                + " FROM ingredient"
                + " INNER JOIN dish_ingredient ON id = ingredient_id"
                + " WHERE dish_id = ?"
                + " ORDER BY price")) {
            statement.setInt(1, dishId);
            final ResultSet result = statement.executeQuery();
            while (result.next()) {
                ingredients.put(new Ingredient(result.getInt("id"),
                        result.getString("name"), result.getFloat("price"),
                        result.getInt("stock")), result.getInt("quantity"));
            }
        } catch (final SQLException e) {
            throw new DataAccessException(e);
        }
        return ingredients;
    }
    
    /**
     * @return :returns all ingredients currently in the database.
     * 
     */
    public ArrayList<Ingredient> selectIngredients() {
        final Connection connection = connectionFactory.getConnection();
        final ArrayList<Ingredient> ingredients = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT id, name, price, stock FROM ingredient")) {
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                ingredients.add(new Ingredient(result.getInt("id"), result.getString("name"),
                        result.getFloat("price"), result.getInt("stock")));
            }
        } catch (final SQLException e) {
            throw new DataAccessException(e);
        }
        return ingredients;
    }
    /**
     * Changes the stock level of the ingredient with the id provided to the number provided.
     * @param id - id of ingredient
     * @param number - number the user wishes to change the stock level to
     */
    public void changeStockLevel(final int id, final int number) {
    	final Connection connection = connectionFactory.getConnection();
    	Statement s = null;
    	try {
    		s = connection.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	String sql = "UPDATE ingredient SET stock=" + String.valueOf(number)
    			+ "WHERE id= " + String.valueOf(id) + ";";
    	
    	try {
			s.execute(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    				
    }

}
