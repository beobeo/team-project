package cafe.staff.manager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import cafe.dao.DishDAO;
import cafe.dao.EmployeeDAO;
import cafe.dao.TableDAO;

/**
 * Observes user events from the manager views and directs behaviour accordingly.
 * @author Robert Kardjaliev
 *
 */
public class ManagerController {
	/**
	 * The controller for the Waiters for Tables GUI.
	 */
	private WaitersForTablesController tablesController;
	/**
	 * The controller for the Edit Menu GUI.
	 */
	private EditMenuController editController;

	/**
	 * The View of the Manager GUI.
	 */
	private ManagerView view;
	
	/**
	 * Hides the view associated with this controller.
	 */
	public final void hideView() {
		view.setVisible(false);
	}

	/**
	 * Shows the view associated with this controller.
	 */
	public final void showView() {
		view.setVisible(true);
	}

	
	/**
	 * Observes the button to change to Stock Level View.
	 * @param listener
	 * 				the listener.
	 */
    public final void addStockLevelListener(final ActionListener listener) {
        view.addStockLevelListener(listener);        
    }
    /**
     * Observes the button to change to Waiters For Tables View.
     * @param listener
     * 				the listener
     */
    public final void addWaitersForTablesListener(final ActionListener listener) {
    	view.addWaitersForTablesListener(listener);
    }
    
    /**
     * Observes the button to change to the Edit Menu View.
     * @param listener
     * 				the listener
     */
    public final void addEditMenuListener(final ActionListener listener) {
    	view.addEditMenuListener(listener);
    }
	/**
	 * Instantiates the fields of the Manager Controller.
	 * @param view
	 *            The View
	 * @param tableData
	 * 				the Table data access object needed
	 * to allow modification of {@link Table}s from the database.
	 * @param employeeData
	 * 				the Emlpoyee data access object required for modification
	 * of {@link Employee}s from the database.
	 * @param dishData 
	 */

	public ManagerController(final ManagerView view, final TableDAO tableData,
			final EmployeeDAO employeeData, final DishDAO dishData) {
		this.view = view;
		WaitersForTablesView tablesView = new WaitersForTablesView();
		WaitersForTablesModel tablesModel = new WaitersForTablesModel(tableData, employeeData);
		EditMenuView editView = new EditMenuView();
		EditMenuModel editModel = new EditMenuModel(dishData);
		editController = new EditMenuController(editView, editModel);
		editController.addBackButtonListener(new BackFromEditMenuListener());
		tablesController = new WaitersForTablesController(tablesView, tablesModel);
		addWaitersForTablesListener(new ShowWaitersForTablesListener());
		tablesController.addBackButtonListener(new BackFromTablesListener());
		addEditMenuListener(new EditMenuListener());
		addStockLevelListener(new EditStockListener());
	}
	
	/**
	 * A listener for the back button in the Waiters for Tables view.
	 *
	 */
	private class BackFromTablesListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			tablesController.hideView();
			showView();			
		}
	}
    
    /**
     * Listens for button click events originating from the Waiters For Tables button.
     *  It hides the Manager View and displays the Waiter for Tables's view.
     */
	private class ShowWaitersForTablesListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			hideView();
			tablesController.showView();
		}
	}
	
    /**
     * Listens for button click events originating from the Edit Menu button.
     *  It hides the Manager View and displays the Edit Menu view.
     */
	private class EditMenuListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			hideView();
			editController.showView();
		}
	}
	/**
	 * A listener for the back button in the Waiters for Tables view.
	 *
	 */
	private class BackFromEditMenuListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			editController.hideView();
			showView();			
		}
	}
	/**
	 * 
	 * @author Hannah Cooper
	 *	Listener for stock button in managers view to be pressed
	 */
	private class EditStockListener implements ActionListener {

		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			hideView();
		}
		
	}
}
