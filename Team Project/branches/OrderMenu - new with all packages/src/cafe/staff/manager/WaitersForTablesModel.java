package cafe.staff.manager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cafe.dao.EmployeeDAO;
import cafe.dao.TableDAO;
import cafe.Table;

/**
 * Obtains a list of tables and a list of waiters
 * from the database.
 * 
 * @author Robert Kardjaliev
 */
public final class WaitersForTablesModel {
	/** A table data access {@link TableDAO object}. */
	private TableDAO tableData;
	/** A table data access {@link EmployeeDAO object}. */
	private EmployeeDAO employeeData;
	/** A {@link Table}'s ID. */
	private int tableId;
	/** A list of {@link Table}s. */
	private List<Table> tables;
	/**A list of {@code Waiter}'s names from the {@link Table}s' table in the database.*/
	private List<String> waitersFromTables = new ArrayList<>();
	/**A list of all {@code Waiters' first names}.*/
	private List<String> waiters = new ArrayList<>();
	/**A list of {@link Table} ID numbers.*/
	private List<Integer> tableNumbers = new ArrayList<>();
	
	/**
	 * Initializes the field for the Waiters For Tables Model.
	 * @param tableData
	 * 				the Table data access object needed
	 * to allow modification of {@link Table Tables} from the database.
	 * @param employeeData
	 * 				the Emlpoyee data access object required for modification
	 * of {@link Employee Employees} from the database.
	 */
	public WaitersForTablesModel(final TableDAO tableData, final EmployeeDAO employeeData) {
		this.tableData = tableData;
		this.employeeData = employeeData;

		refresh();
	}

	/**
	 * A function that causes the lists to reload.
	 */
	public void refresh() {
		getTablesList();
		setAllWaitersFromTables();
		setAllTableNums();
		setAllWaiters();
	}
	/**
	 * Assigns a {@code Waiter} to a {@link Table} in the database using input from the
	 * view.
	 * @param id
	 * 			the {@link Table}'s ID.
	 * @param name
	 * 			the {@code Waiter}'s first name.
	 */
	public void assignWaiterToTable(final int id, final String name) {
		tableData.assignWaiterToTable(id, name);		
	}

	/**
	 * Gets a list of all the {@link Table}s from the database.
	 */
	private void getTablesList() {
		tables = tableData.getAllTables();
	}

	/**
	 * Gets a list of all the first names of waiters from the {@link Table}'s table from the database and 
	 * copies it into the local waiters list. It will be in the same order as the 
	 * {@link Table}s, so the names will repeat in order to get displayed properly in the 
	 * {@link TablesAssignmentView} later on.
	 */
	private void setAllWaitersFromTables() {
		final Iterator<Table> iterator = tables.iterator();
		final List<String> list = new ArrayList<>();
		while (iterator.hasNext()) {
			list.add(iterator.next().getWaiter().getFirstName());
		}
		waitersFromTables = list;
	}

	/**
	 * Gets a list of all the first names of Waiters from the Employee table from the database
	 * in order to get displayed to the waiter JComboBox in the View.
	 */
	private void setAllWaiters() {
		waiters = employeeData.getAllWaiterNames();
	}

	/**
	 * Makes a List containing all the {@link Table} numbers.
	 */
	public void setAllTableNums() {
		final List<Integer> nums = new ArrayList<>();
		for (Table t: tables) {
			nums.add(t.getId());
		}
		tableNumbers = nums;
	}

	/**
	 * Creates a new {@link Table} in the database.
	 */
	public void createTable() {
		setTableId(tableData.createTable());		
	}
	/**
	 * Removes the last {@link Table} from the database.
	 */
	public void removeTable() {
		setTableId(tableData.removeTable());

	}
	/**
	 * Gets the ID of the removed {@link Table}.
	 * @return
	 * 		the {@link Table}'s id.
	 */
	public int getTableId() {
		return tableId;
	}
	/**
	 * Sets the ID of the removed {@link Table}.
	 * @param tableId
	 * 			the {@link Table}'s id.
	 */
	public void setTableId(final int tableId) {
		this.tableId = tableId;
	}

	/**
	 * Returns the list of the {@code Waiter}s' first names who are assigned to {@link Table}s.
	 * @return
	 * 			the list of {@code Waiter} names.
	 */
	public List<String> getWaitersFromTables() {
		return waitersFromTables;
	}
	
	/**
	 * Returns the list of all {@code Waiter} first names regardless of whether they're
	 * assigned to a table. They are getting taken from the {@code Employee} table from the
	 * database.
	 * @return
	 * 			the list of {@code Waiter} names.
	 */
	public List<String> getWaitersFromEmployees() {
		return waiters;
	}
	/**
	 * Returns the list of {@link Table} (id)numbers.
	 * @return
	 * 			the list of {@link Table} (@code id) numbers.
	 */
	public List<Integer> getTableNumbers() {
		return tableNumbers;
	}
}
