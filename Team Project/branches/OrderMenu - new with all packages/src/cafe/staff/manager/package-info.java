/**
 * Defines classes related to the manager GUI.
 * 
 * @author Robert Kardjaliev
 */
package cafe.staff.manager;