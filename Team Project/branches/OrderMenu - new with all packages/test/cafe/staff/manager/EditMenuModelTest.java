package cafe.staff.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.Test;


import cafe.dao.DAOFactory;
import cafe.dao.DishDAO;
import cafe.dao.postgres.DefaultTestDAOFactoryImpl;
import cafe.staff.manager.EditMenuModel;

/**
 * Unit test for the {@link EditMenuModel}.
 * @author Robert Kardjaliev
 */
public class EditMenuModelTest {
	/** An instance of the DAO factory. */
    private DAOFactory factory = DAOFactory.getInstance("zyvc215", "IYIP1845",
            DefaultTestDAOFactoryImpl.class);
	/** An Order data access {@link DishDAO object}. */
    private DishDAO dishData = factory.getDishDAO();
    /** An Edit Menu {@link EditMenuModel model}.  */
    private EditMenuModel model = new EditMenuModel(dishData);
    /** A {@code Dish}'s name.  */
    private String dishName = "Lasagne";
    /** A boolean value for whether a {@code Dish} is visible.*/
    private boolean visibility = true;
    
	@Before
	public final void setUp() {
		dishData.setVisible(dishName, visibility);
	}
	
	/**
	 * Checks whether the visibility of a {@code Dish} is what this test was designed for.
	 */
	@Test
	public final void checkOrderState() {		
		assertEquals("Test 1: Check for correct visibility of a Dish.", model.getVisible(dishName),visibility);
	}
	
	/**
	 * Checks for correct change of {@code Dish} visibility.
	 */
	@Test
	public final void testVisibleChange() {
		model.setVisible(dishName, false);
		assertFalse("Test 2: Test for correct correct change of visibility.", model.getVisible(dishName) == visibility);
	}
}
