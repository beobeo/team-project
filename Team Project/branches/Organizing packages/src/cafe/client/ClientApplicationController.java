package cafe.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import cafe.Order;
import cafe.Table;
import cafe.client.menu.MenuController;
import cafe.client.menu.MenuGUI;
import cafe.client.menu.MenuModel;
import cafe.client.order.OrderController;
import cafe.client.order.OrderGUI;
import cafe.client.order.OrderModel;
import cafe.client.trackOrder.TrackOrderController;
import cafe.client.trackOrder.TrackOrderModel;
import cafe.client.trackOrder.TrackOrderView;
import cafe.dao.AuthenticationException;
import cafe.dao.DAOFactory;
import cafe.dao.DataAccessException;
import cafe.dao.DishDAO;
import cafe.dao.OrderDAO;
import cafe.dao.SittingDAO;
import cafe.dao.postgres.DAOFactoryImpl;

/**
 * Provides a driver for the client's application, handling the relationship
 * between the menu, order status display, and payment processing.
 * 
 * @author Michael Winter
 * @author Jonathan Hercock
 * @author Hannah Cooper
 */
public final class ClientApplicationController {
    /**
     * The currently active order. This instance retains the state of client's
     * selection during their visit and is reset once the client has paid and
     * left.
     */
	//private Order currentOrder;
	/**
	 * The controller for the client menu.
	 */
	private MenuController menuController;
	/**
	 * The controller for the client order display.
	 */
	private OrderController orderController;
	/**
	 * The controller for the client track order display.
	 */
	private TrackOrderController trackOrderController;
    /**
     * The controller for the client payment system.
     */
    // private PaymentController paymentController;
	// TODO: Remove this field
	private OrderGUI orderView;
	
    /**
     * The model for the {@link OrderGUI}.
     */
	private OrderModel orderModel;
	
	/**
	 * An {@link OrderDAO} object.
	 */
	private OrderDAO orderDataSource;
	/**
	 * The ID of the {@link Order}.
	 */
	private int orderId;

	
	/**
	 * The main entry point for the application. The application expects that 
	 * 
	 * @param args the command line arguments passed to the application.
	 */
	public static void main(final String[] args) {
		final int tableId = parseArguments(args);
		launch(tableId);
	}


	/**
	 * Initialises and runs the application. The actual launching of the
	 * application is delegated to this method in order to ensure initialisation
	 * occurs on Swing's event-dispatching thread.
	 * 
	 * @param tableId
	 *            the identifier of the table on which this application is
	 *            running.
	 */
	private static void launch(final int tableId) {
		try {
			if (SwingUtilities.isEventDispatchThread()) {
				final ClientApplicationController application = new ClientApplicationController(
						tableId);
				application.run();
			} else {
				SwingUtilities.invokeAndWait(new Runnable() {
					@Override
					public void run() {
						launch(tableId);
					}
				});
			}
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	/**
	 * Interprets command line arguments received when the application was
	 * invoked.
	 * 
	 * @param args
	 *            the arguments received from the command line as a series of
	 *            words or quoted strings.
	 * @return the identifier of the table on which this application is running.
	 */
	private static int parseArguments(final String[] args) {
		return 1;
	}


    /**
     * Prepares the UI and any resources required prior to starting the
     * application. The constructor must be invoked on the event-dispatching
     * thread.
     * 
     * @param tableId
     *            the identifier of the table on which this application is
     *            running.
     */
    private ClientApplicationController(final int tableId) {
        DAOFactory factory;
        try {
            /*
             * Initialise the data object factory using the PostgreSQL
             * implementation, using the given credentials to authenticate the
             * database connection. These details could be provided by the user
             * when the application loads, rather than hard-coded. For example,
             * guest details would only allow users read access to the menu,
             * insert access to service requests, and insert/update to orders.
             * If a waiter logs in, they also can read and update service
             * requests, insert and update bookings, etc.
             */
            factory = DAOFactory.getInstance("zyvc215", "IYIP1845",
                    DAOFactoryImpl.class);

            final Table table = factory.getTableDAO().findTable(tableId);
            if (table == null) {
                throw new IllegalArgumentException("Table does not exist (id: "
                        + tableId + ")");
            }

            final SittingDAO sittingDataSource = factory.getSittingDAO();
            final int sittingId = sittingDataSource.createSitting(table.getId());
            // final Sitting sitting = sittingDataSource.findSitting(sittingId);

            orderDataSource = factory.getOrderDAO();
            orderId = orderDataSource.createOrder(sittingId);
            final Order order = orderDataSource.findOrder(orderId);

            /*
             * Obtain the data object for dish information. The menu model
             * should only take data sources that it actually requires, which is
             * why the model now only takes the dish data object as an argument.
             * If it requires more information from other parts of the database,
             * then those access objects should be created separately and passed
             * in (not the whole factory: see dependency injection).
             */
            final DishDAO dishData = factory.getDishDAO();
            orderModel = new OrderModel(order,
                    orderDataSource, factory.getOrderItemDAO(),
                    factory.getIngredientDAO());
            orderView = new OrderGUI();
            orderController = new OrderController(orderModel, orderView);
            orderController.addShowMenuListener(new ShowMenuListener());
            orderController.addWaiterCallListener(new WaiterCallListener());

            final MenuGUI menuView = new MenuGUI();
            final MenuModel menuModel = new MenuModel(dishData);
            menuController = new MenuController(menuModel, menuView);
            
            final TrackOrderView trackView = new TrackOrderView();
            final TrackOrderModel trackModel = new TrackOrderModel(orderDataSource, orderId);
            //TODO - GET THE ORDER ID WHEN CONFIRMING AN ORDER
            trackOrderController = new TrackOrderController(trackView, trackModel); 
        } catch (final AuthenticationException e) {
            JOptionPane.showMessageDialog(
                    null,
                    "The user name or password used to access information is incorrect."
                            + "\nPlease speak to a member of staff for assistance.",
                    "Bad user name/password", JOptionPane.ERROR_MESSAGE);
            // TODO: Re-prompt authentication (if appropriate)
            return;
        } catch (final DataAccessException e) {
            JOptionPane.showMessageDialog(
                    null,
                    "The application is unable to get some required information and must close."
                            + "\nPlease speak to a member of staff for assistance.",
                    "Connection failed", JOptionPane.ERROR_MESSAGE);
            // TODO: Add logging
            return;
        }
        orderController.addShowTrackOrderViewListener(new ShowTrackOrderViewListener());

        menuController.addDishAddedListener(orderController.getOrderItemListener());
        menuController.addShowOrderListener(new ShowOrderListener());
        menuController.addWaiterCallListener(new WaiterCallListener());
        
        trackOrderController.addCancelOrderListener(new CancelOrder());
        trackOrderController.addEditOrderListener(new EditOrder());
    }
	
	/**
	 * Starts the application.
	 */
	public void run() {
		menuController.showView();
	}
	
	
	/**
	 * Listens for button click events originating from the Order button, hiding the menu view
	 * and displaying the order status view.
	 */
	private class ShowOrderListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			orderController.showView();
			menuController.hideView();
		}
	}
	
	/**
	 * Listens for button click events originating from the Menu button, hiding the order view
	 * and displaying the menu.
	 */
	private class ShowMenuListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			menuController.showView();
			orderController.hideView();
		}
	}
	
	
	
	/**
	 * Listens for button click events originating from the Track My Order button, 
	 * hiding the order view and displaying the Track Order view.
	 * 
	 *
	 */
	private class ShowTrackOrderViewListener implements ActionListener {
	    @Override
	    public void actionPerformed(final ActionEvent e) {
	        orderController.hideView();
	        trackOrderController.showView();
	        trackOrderController.getPaymentController().setPaymentPrice(orderModel.getTotalPrice());
	    }
	}
	
	
	/**
	 * Listens for button click events originating from the Call for Assistance button,
	 * alerting the waiters that a table requires attention.
	 */
	private class WaiterCallListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
		    // TODO: Implement write to database
			System.out.println("Service request sent...");
		}
	}
		/**
		 * 
		 * @author Hannah Cooper
		 * ActionListener for Cancel Order button of TrackOrder GUI. 
		 * Actions include:
		 * presenting user with a confirm pop-up once the button is clicked 
		 * and changes the user back to the menu screen.
		 */
		class CancelOrder implements ActionListener {
	
			@Override
			public void actionPerformed(final ActionEvent e) {
				
				int option = JOptionPane.showConfirmDialog(
						null, "Are you sure you want to cancel your order?");
				
				if (option == 0) {
					trackOrderController.hideView();
					menuController.showView();
					orderView.makeConfirmOrderVisible();
					orderDataSource.cancelOrder(orderId);
				}
				
				//To Do:
				//change order status to cancelled in database
				
			}
			
		}
	    
		/**
		 * 
		 * @author Hannah Cooper
		 * ActionListener for EditOrder button of TrackOrder GUI. 
		 * Actions include:
		 * presenting user with a confirm pop-up once the button is clicked 
		 * and changes the user back to order screen so they can begin to edit their order again.
		 */
	class EditOrder implements ActionListener {
	
		@Override
		public void actionPerformed(final ActionEvent e) {
			
			int option1 = JOptionPane.showConfirmDialog(
					null, "Editing your order will increase"
							+ " the time that it will take for"
							+ " your food to be delivered."
							+ " Would you like to continue?");
			
			if (option1 == 0) {
				trackOrderController.hideView();
				orderController.showView();
				orderView.makeConfirmOrderVisible();
				orderModel.newProgress();
			}
			
			
			//To Do:
			//change status of order on database

		}
		
	}
	
	
}
