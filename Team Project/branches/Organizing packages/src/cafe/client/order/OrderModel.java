package cafe.client.order;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import cafe.Dish;
import cafe.DishesUnavailableException;
import cafe.Ingredient;
import cafe.Order;
import cafe.OrderItem;
import cafe.OrderState;
import cafe.dao.IngredientDAO;
import cafe.dao.OrderDAO;
import cafe.dao.OrderItemDAO;

/**
 * The Model for Order. Gets {@link Order Orders}, {@link Ingredient Ingredients},
 * and {@link OrderItems orderItems} and contains the methods that perform
 * actions on them.
 * 
 * @author Robert Kardjaliev
 */
public final class OrderModel {
	/** the variable in which the total price of the order is saved. */
	private float totalPrice = 0;
	/** The list of dishes ordered. */
	private List<OrderItem> orderItems = new ArrayList<OrderItem>();
	/**
	 * The order instance to be modelled by the interface.
	 */
	private Order order;
    /**
     * An Order data access object used to load and persist order instances
     * to the data store.
     */
    private final OrderDAO orderDataSource;
    /**
     * An OrderItem data access object used to load and persist item instances
     * to the data store.
     */
    private final OrderItemDAO orderItemDataSource;
    /**
     * An Ingredient data access object used to load ingredient instances
     * from the data store.
     */
    private final IngredientDAO ingredientDataSource;

    /**
     * Creates the Kitchen model.
     * 
     * @param order
     *            the order to be modelled.
     * @param orderDataSource
     *            the data access object used to load and persist order
     *            instances.
     * @param orderItemDataSource
     *            the data access object used to load and persist item
     *            instances.
     * @param ingredientDataSource
     *            the data access object used to load and persist ingredient
     *            instances.
     */
    public OrderModel(final Order order, final OrderDAO orderDataSource,
            final OrderItemDAO orderItemDataSource,
            final IngredientDAO ingredientDataSource) {
        this.setOrder(order);
        this.orderDataSource = orderDataSource;
        this.orderItemDataSource = orderItemDataSource;
        this.ingredientDataSource = ingredientDataSource;
    }
	

	/**
	 * Fills up the List.
	 * 
	 */
	public void populateOrder() {
		// database connection start
		// get orders and fill up arrayList
		// close connection
	}
	
    /**
     * Marks an order such that it is visible to waiters. This action denotes
     * the moment where a customer is satisfied with their order and wishes to
     * have it confirmed by a waiter.
     * 
     * @return {@code true} if the order was placed; otherwise {@code false} if
     *         a dish is unavailable.
     * @throws DishesUnavailableException 
     * @throws IllegalStateException
     *             if this method is called after an item has already been
     *             placed or cancelled.
     * @throws DishesUnavailableException
     *             if one or more dishes has insufficient ingredients.
     */
	public boolean placeOrder() throws DishesUnavailableException {
	    if (getOrder().getProgress() != OrderState.NEW) {
	        throw new IllegalStateException();
	    }
	    
	    final List<Dish> unavailableDishes = new ArrayList<>();
	    final Iterator<OrderItem> iterator = orderItems.iterator();
        while (iterator.hasNext()) {
            final OrderItem item = iterator.next();
	        final Dish dish = item.getDish();
	        final int quantity = item.getQuantity();
	        final int available = validateDishAvailability(dish.getId(), quantity);
	        if (available < quantity) {
	            if (available > 0) {
	                item.setQuantity(available);
	                orderItemDataSource.updateOrderItem(getOrder().getId(), item);
	            } else {
	                iterator.remove();
                    orderItemDataSource.deleteOrderItem(getOrder().getId(), dish.getId());
	            }
	            unavailableDishes.add(dish);
	        }
	    }
	    if (!unavailableDishes.isEmpty()) {
	        throw new DishesUnavailableException(unavailableDishes);
	    }

	    getOrder().advanceProgress();
	    orderDataSource.updateOrder(getOrder());
	    return true;
	}

	/**
	 * Iterates over entire list of dishes and adds up their prices.
	 * 
	 * @return the total price of the order
	 */
	public float getTotalPrice() {
		int size = orderItems.size();
		for (int i = 0; i < size; i++) {
			totalPrice += orderItems.get(i).getDish().getPrice()
					* orderItems.get(i).getQuantity();
		}
		return totalPrice;
	}

	/**
	 * Removes a dish from the list of dishes.
	 * 
     * @param dish
     *            the dish to be removed.
	 */
	public void removeItem(final Dish dish) {
		final Iterator<OrderItem> iterator = orderItems.iterator();
		while (iterator.hasNext()) {
		    final OrderItem item = iterator.next();
            if (dish.equals(item.getDish())) {
                if (item.getQuantity() == 1) {
                    iterator.remove();
                    orderItemDataSource.deleteOrderItem(getOrder().getId(), dish.getId());
                } else {
                    item.setQuantity(item.getQuantity() - 1);
                    orderItemDataSource.updateOrderItem(getOrder().getId(), item);
                }
                break;
            }
		}
	}


    /**
     * Adds a item to the order list, returning whether there were sufficient
     * ingredients to prepare the dish.
     * 
     * @param dish
     *            the dish to be added.
     * @return {@code true} if the dish was added the order; otherwise
     *         {@code false} if there were insufficient ingredients to prepare
     *         the dish.
     * @throws DishesUnavailableException
     *             if one or more dishes has insufficient ingredients.
     */
    public boolean addItem(final Dish dish) throws DishesUnavailableException {
        for (final OrderItem item : orderItems) {
            if (dish.equals(item.getDish())) {
                final int quantity = item.getQuantity() + 1;
                if (validateDishAvailability(dish.getId(), quantity) < quantity) {
                    throw new DishesUnavailableException(Collections.singletonList(dish));
                }
                item.setQuantity(quantity);
                orderItemDataSource.updateOrderItem(getOrder().getId(), item);
                return true;
            }
        }
        if (validateDishAvailability(dish.getId(), 1) < 1) {
            throw new DishesUnavailableException(Collections.singletonList(dish));
        }
        orderItemDataSource.createOrderItem(getOrder().getId(), dish.getId(), 1,
                false);
        orderItems.add(orderItemDataSource.findOrderItem(getOrder().getId(),
                dish.getId()));
        return true;
    }

	/**
	 * Returns the list of orderItems (containing the dishes).
	 * 
	 * @return orderItems - list of type OrderItem
	 */
	public List<OrderItem> getDishes() {
		return orderItems;
	}
	
	
    /**
     * Checks that each ingredient needed for the specified dish is available to
     * prepare the given number of dishes, returning the maximum number that
     * could be prepared with current stock if the requested quantity cannot be
     * fulfilled.
     * 
     * @param dishId
     *            the identifier of the dish to validate.
     * @param quantity
     *            the number of such dishes that are required.
     * @return {@link Integer#MAX_VALUE} if at least {@code quantity} dishes can
     *         be prepared, otherwise the maximum number of dishes that can be
     *         made with the current stock.
     */
    private int validateDishAvailability(final int dishId, final int quantity) {
        final Map<Ingredient, Integer> requiredIngredients
                = ingredientDataSource.selectDishIngredients(dishId);
        int available = Integer.MAX_VALUE;
        for (final Entry<Ingredient, Integer> ingredientRequirement
                : requiredIngredients.entrySet()) {
            final Ingredient ingredient = ingredientRequirement.getKey();
            final int requirement = ingredientRequirement.getValue();
            if (!ingredient.isAvailable(requirement * quantity)) {
                available = Math.min(available, ingredient.getStock() / requirement);
            }
        }
        return available;
    }
    
	/**
	 * Sets the {@link OrderState} of an {@link Order} to NEW.
	 */
	public void newProgress() {
		getOrder().newProgress();
	}

	/**
	 * Gets the current {@link Order} this Model is working on.
	 * @return
	 * 		The {@link Order}.
	 */
	public Order getOrder() {
		return order;
	}

	/**
	 * Sets the {@link Order} this Model will work on.
	 * @param order
	 * 			The {@link Order}.
	 */
	public void setOrder(final Order order) {
		this.order = order;
	}
}
