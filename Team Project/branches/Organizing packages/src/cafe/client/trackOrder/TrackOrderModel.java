package cafe.client.trackOrder;

import cafe.Order;
import cafe.OrderState;
import cafe.dao.OrderDAO;

/**
 * The Track Order Model that keeps track of
 * an {@link Order}'s progress.
 * 
 * @author Robert Kardjaliev
 *
 */
public class TrackOrderModel {
	/** An Order data access object. */
	private OrderDAO orderData;
	/** An {@link Order} object.	 */
	private Order theOrder;
	/** The {@link OrderState} variable that will get assigned
	 * the progress from the {@link Order}.
	 */
	private OrderState progress;

	/** The {@link Order}'s ID. */
	private int id;

	/** An integer representation of the order progress. */
	private int percentage = 0;
	/**
	 * TrackOrderModel constructor.
	 * @param orderData
	 * 				The Order data access object needed
	 * to load the {@link Order}'s progress from the database.
	 * @param id - the Order's ID
	 */
	public TrackOrderModel(final OrderDAO orderData, final int id) {
		this.orderData = orderData;
		this.id = id;
	}

	/**
	 * This method sets what {@link Order} will be tracked.
	 *  
	 */
	public final void setOrder() {
		theOrder = orderData.findOrder(id);
	}

	/**	
	 * Method that gets the {@link OrderState}(progress) of an {@link Order}.
	 * Needs to call setOrder() again for refreshing to work.
	 * @return the OrderState of the Order.
	 */
	public final OrderState getOrderState() {
		setOrder();
		progress = theOrder.getProgress();
		return progress;
	}

	/**
	 * Method that gets the progress {@link OrderState} and turns it
	 * into an integer representation.
	 * @return An integer representation of the {@link Order}'s progress.
	 */
	public final int getOrderProgress() {
		switch(getOrderState()) {
		// CHECKSTYLE IGNORE MagicNumberCheck FOR NEXT 9 LINES
		case PENDING: percentage = 20;
		break;
		case CONFIRMED: percentage = 40;
		break;
		case PREPARING: percentage = 60;
		break;
		case READY: percentage = 80;
		break;
		case DELIVERED: percentage = 100;
		break;
		default:
			break;
		}
		return percentage;		
	}
	/*
	/**
	 * Sets the {@link OrderState} of an {@link Order} to NEW.
	 */
	/*public final void newProgress() {
		orderData.newProgress(id);
		setOrder();
	} */
}