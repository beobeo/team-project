package cafe.dao;

import java.util.List;

import cafe.Dish;

/**
 * A data access object used to manipulate persisted {@link Dish} objects.
 * 
 * @author Michael Winter
 * @author Robert Kardjaliev
 */
public interface DishDAO {
    /**
     * Given an identifier, this method loads and returns a new instance of the
     * specified {@link Dish}.
     * 
     * @param id
     *            the identifier.
     * @return the specified {@code Dish}.
     * 
     */
    Dish findDish(int id);
    
    /**
     * @param course - dish category as in main, dessert, etc
     * @return the dishes in the selected course
     * @throws DataAccessException
     *             if an error occurs while accessing the data store.
     */
    List<Dish> selectDishesByCourse(String course);
    
    /**
     * 
     * @return the set of dishes set to visible from the database
     * @throws DataAccessException
     *             if an error occurs while accessing the data store.
     */
    List<Dish> selectAvailableDishes();
    
    /**
     * Gets the list of all {@link Dish} names from the database.
     * @return the list of names.
     * @throws DataAccessException
     *             if an error occurs while accessing the data store.
     */
	List<String> getAllDishNames();

	/**
	 * Sets a {@link Dish}'s visibility given its name and a boolean value.
	 * @param name
	 * 			the {@link Dish}'s name.
	 * @param visible
	 * 			the boolean value for visible.
	 */
	void setVisible(String name, boolean visible);

	/**
	 * Given a {@link Dish}'s name it gets the visibility of that {@link Dish}.
	 * @param name
	 * 			the {@link Dish}'s name
	 * @return
	 * 			the {@link Dish}'s visibility.
	 */
	boolean getVisible(String name);
}
