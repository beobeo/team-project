package cafe.staff.manager;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;

/**
 * Presents a graphical window to the manager(s) from which he can choose
 * to add/remove dishes from the menu.
 * @author Robert Kardjaliev
 *
 */
public class EditMenuView extends JFrame {

	/** Unique serialisation identifier. */
	private static final long serialVersionUID = 8830889410240081933L;
	 /**  The Main GUI Panel.    */
    private JPanel contentPane;
    /**  The button that allows to return to the Manager View.  */
    private JButton btnBack;
    /**  The button that sets the currently selected dish to visible.  */
	private JButton btnVisible;
    /** 
     * A label that will add interactivty to the view, so that
     * a manager can see the effect his button clicks have.
     */
	private JLabel actionLabel;
	/**
	 * The combo box that will contain all the {@link Dish} names currently on the
	 * database.
	 */
    private JComboBox<String> dishNameComboBox;
    /**
     * Shows the current visibility of a {@link Dish}.
     */
	private JLabel visibleLabel;


    /**
     * Create the frame.
     */
    ///CHECKSTYLE:OFF
    public EditMenuView() {
    	setTitle("Edit Menu");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 468, 327);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        btnBack = new JButton();
        btnBack.setFont(new Font("Tahoma", Font.BOLD, 10));
        btnBack.setText("Back");
        btnBack.setBounds(10, 11, 63, 21);
        contentPane.add(btnBack);
        
        btnVisible = new JButton();
        btnVisible.setText("Change Visibility");
        btnVisible.setFont(new Font("Dialog", Font.BOLD, 10));
        btnVisible.setBounds(148, 192, 133, 40);
        contentPane.add(btnVisible);
        
        actionLabel = new JLabel("");
        actionLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
        actionLabel.setBounds(82, 243, 370, 25);
        contentPane.add(actionLabel);
        
        dishNameComboBox = new JComboBox<String>();
        dishNameComboBox.setBounds(105, 124, 236, 20);
        contentPane.add(dishNameComboBox);
        
        visibleLabel = new JLabel("");
        visibleLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
        visibleLabel.setBounds(180, 155, 122, 25);
        contentPane.add(visibleLabel);
        
    }
    //CHECKSTYLE:ON
    /**
     * Adds a listener that allows to change back to the Manager View.
     * @param listener 
     * 				the listener for the Back button.
     */
    public final void addBackButtonListener(final ActionListener listener) {
        btnBack.addActionListener(listener);
    }
    
    /**
     * Adds a listener which, when pressed, changes the currently selected {@code Dish}'s
     * visibility in the database.
     * @param listener 
     * 				the listener for the Set Visible button.
     */
    public final void addVisibleListener(final ActionListener listener) {
        btnVisible.addActionListener(listener);
    }
    /**
     * Adds a listener to the {@code dishNameComboBox}.
     * @param listener
     * 				the listener.
     */
    public final void addComboListener(final ActionListener listener) {
        dishNameComboBox.addActionListener(listener);
    }
    
    /**
     * Adds {@link Dish} names to the dishNameComboBox one by one.
     * @param name
     * 			a {@link Dish}'s name.
     */
    public final void addDishNameComboBoxItems(final String name) {
    	dishNameComboBox.addItem(name);
    }
    
    /**
     * Gets the currently selected {@link Dish}'s name.
     * @return
     * 			the {@link Dish} name as a String.
     */
    public final String getDishName() {
    	return dishNameComboBox.getSelectedItem().toString();
    }
    /**
     * Sets the label to display the given String.
     * @param str
     * 			the String.
     */
	public final void setLabel(final String str) {
		actionLabel.setText(str);		
	}
	/**
	 * Sets the label showing the {@link Dish}'s current visibility.
	 * @param string
	 * 			the String message.
	 */
	public final void setVisibleLabel(final String string) {
		visibleLabel.setText(string);
		
	}
}