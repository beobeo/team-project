package cafe.staff.manager;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
/**
 * Presents a graphical window to the manager(s) from which he can choose
 * to which view he wants to go: {@link WaitersForTables Assigning Waiters to Tables},
 *  {@link EditStockGUI Editing Stock}. or {@link EditMenuView Removing Dishes}.
 * 
 * @author Robert Kardjaliev
 */
public class ManagerView extends JFrame {

	/**
	 * Unique serialisation identifier.
	 */
	private static final long serialVersionUID = 8830889410240081933L;
	 /**  The Main GUI Panel.    */
    private JPanel contentPane;
    /**  The button that allows the Stock Level View to be reached.  */
    private JButton btnStockLevel;
    /**  The button that allows the Waiters for Tables View to be reached.  */
    private JButton btnWaitersForTables;
    /**  The button that allows the Edit Menu View to be reached.  */
	private JButton btnEditMenu;

    /**
     * Create the frame.
     */
    ///CHECKSTYLE:OFF
    public ManagerView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        btnWaitersForTables = new JButton();
        btnWaitersForTables.setFont(new Font("Tahoma", Font.BOLD, 13));
        btnWaitersForTables.setText("Waiters for Tables");
        btnWaitersForTables.setBounds(127, 41, 180, 49);
        contentPane.add(btnWaitersForTables);
        
        btnStockLevel = new JButton();
        btnStockLevel.setFont(new Font("Tahoma", Font.BOLD, 13));
        btnStockLevel.setText("Stock Levels");
        btnStockLevel.setBounds(127, 101, 180, 49);
        contentPane.add(btnStockLevel);
        
        btnEditMenu = new JButton();
        btnEditMenu.setText("Edit Available Dishes");
        btnEditMenu.setFont(new Font("Tahoma", Font.BOLD, 13));
        btnEditMenu.setBounds(127, 161, 180, 49);
        contentPane.add(btnEditMenu);
        
    }
    //CHECKSTYLE:ON
    
    /**
     * Adds a listener that allows to change to the Stock Level View.
     * @param listener - 
     * 				the listener for the Stock Level view button.
     */
    public final void addStockLevelListener(final ActionListener listener) {
        btnStockLevel.addActionListener(listener);
    }
    
    /**
     * Adds a listener that allows to change to the Waiters for Tables View.
     * @param listener - 
     * 				listener for the Waiters for Tables View button. 
     */
    
    public final void addWaitersForTablesListener(final ActionListener listener) {
        btnWaitersForTables.addActionListener(listener);
    }
    /**
     * Adds a listener that allows to change to the Edit Menu View.
     * @param listener - 
     * 				the listener for the Edit Menu button.
     */
    public final void addEditMenuListener(final ActionListener listener) {
        btnEditMenu.addActionListener(listener);
    }
}