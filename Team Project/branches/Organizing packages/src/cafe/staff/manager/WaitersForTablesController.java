package cafe.staff.manager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

/**
 * Observes user events from the WaitersForTablesView and directs behaviour accordingly,
 * invoking the model where appropriate and referring events to higher-level
 * observing controllers.
 * @author Robert Kardjaliev
 *
 */
public class WaitersForTablesController {
	/**
	 * The view for the Waiters for Tables GUI.
	 */
	private WaitersForTablesView view;
	/**
	 * The model for the Waiters for Tables GUI.
	 */
	private WaitersForTablesModel model;
	
	/**
	 * {@link TablesAssignmentView}. A view to list all the tables and waiters assigned to them.
	 */
	private TablesAssignmentView assignView;

	/**
	 * A model for setting up the {@link TablesAssignmentView}.
	 */
	private TablesAssignmentModel assignModel;


	/**
	 * Hides the view associated with this controller.
	 */
	public final void hideView() {
		view.setVisible(false);
	}

	/**
	 * Shows the view associated with this controller.
	 */
	public final void showView() {
		view.setVisible(true);
	}
	/**
	 * Adds a listener to observe the button for changing back to Manager View.
	 * @param listener
	 * 				the listener.
	 */

	public final void addBackButtonListener(final ActionListener listener) {
		view.addBackButtonListener(listener);        
	}
	/**
	 * Adds a listener to observe the button that assigns Waiters to Tables.
	 * @param listener
	 * 				the listener
	 */
	public final void addAssignButtonListener(final ActionListener listener) {
		view.addAssignButtonListener(listener);
	}

	/**
	 * Adds a listener to observe the Add table button to add more tables to the database.
	 * @param listener
	 * 				the listener.
	 */	
	public final void addTableListener(final ActionListener listener) {
		view.addTableListener(listener);
	}

	/**
	 * Adds a listener to observe the Remove Table button which removes tables from the database.
	 * @param listener
	 * 				the listener.
	 */    	
	public final void addRemoveTableListener(final ActionListener listener) {
		view.addRemoveTableListener(listener);
	}

	/**
	 * Adds a listener to observe the View Table Assignment button.
	 * @param listener
	 * 				the listener.
	 */    	
	public final void addTableAssignListener(final ActionListener listener) {
		view.addTableAssignListener(listener);
	}

	/**
	 * Initializes the fields for this controller.
	 * @param view 
	 * 			The View.
	 * @param model
	 * 			The Model.
	 */
	public WaitersForTablesController(final WaitersForTablesView view, 
			final WaitersForTablesModel model) {
		this.view = view;
		this.model = model;
		refresh();
		
		addTableListener(new AddTableListener());
		addRemoveTableListener(new RemoveTableListener());  
		addAssignButtonListener(new AssignButtonListener());
		addTableAssignListener(new AddTableAssignListener());


	}

	/**
	 * Causes the lists in the combo boxes to reload and 
	 * creates a new Tables Assignment {@link TablesAssignmentView view} and 
	 * {@link TablesAssignmentView model} with the updated lists.
	 */
	public final void refresh() {
		view.getWaiterComboBox().removeAllItems();
		view.getTableNumberComboBox().removeAllItems();
		model.refresh();
		setTableComboBoxItems();
		setWaiterComboBoxItems();
		assignModel = new TablesAssignmentModel(model.getTableNumbers(),
				model.getWaitersFromTables());
		assignView = new TablesAssignmentView(assignModel);
		addOkayButtonListener(new OkayButtonListener());
	}
	/**
	 * Adds table numbers to table combo box in the view.
	 */
	public final void setTableComboBoxItems() {
		for (int i: model.getTableNumbers()) {
			view.addTableComboBoxItems(i);
		}
	}
	/**
	 * Adds the waiter names to the waiter combo box in the view.
	 */
	public final void setWaiterComboBoxItems() {
		Set<String> waiters = new HashSet<String>(model.getWaitersFromEmployees());
		for (String str: waiters) {
			view.addWaiterComboBoxItems(str);
		}
	}
	
	/**
	 * Adds a listener to the OK button in the {@link TablesAssignmentView} that closes
	 * that view.
	 * @param listener
	 * 				the listener.
	 */
	public final void addOkayButtonListener(final ActionListener listener) {
		assignView.addOkButtonActionListener(listener);
	}
	

	/**
	 * Listens for Assign button clicks. When the Assign button is pressed, after 
	 * selecting a table number and waiter name from the drop boxes, it will 
	 * assign a waiter to a table in the database and display it in the label 
	 */
	private class AssignButtonListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			model.assignWaiterToTable(view.getTableCombo(), view.getWaiterCombo());
			view.setText(view.getWaiterCombo() + " assigned to table # " 
			+ view.getTableCombo());
			refresh();
		}
	}

	/**
	 * When the View Table Assignment is pressed it opens the window showing
	 * all tables and waiters assigned to them.
	 *
	 */
	private class AddTableAssignListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {			
			assignView.setVisible(true);
		}
	}
	
	/**
	 * Closes TablesAssignmentView when pressed.
	 *
	 */
	private class OkayButtonListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			assignView.setVisible(false);
		}
	}

	/**
	 * When the Add table button is clicked it creates a new {@link Table}.
	 */
	private class AddTableListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			model.createTable();
			view.setText("Table " + model.getTableId() + " created.");
			refresh();
		}
	}

	/**
	 * When the delete table button is clicked it
	 * deletes the last {@link Table}(ordered by id) in the database.
	 */
	private class RemoveTableListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			model.removeTable();
			view.setText("Table " + model.getTableId() + " removed.");
			refresh();
		}
	}

}
