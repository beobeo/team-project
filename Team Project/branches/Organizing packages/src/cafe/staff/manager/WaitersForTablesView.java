package cafe.staff.manager;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;

/**
 * Presents a graphical window to the manager(s) from which he can choose
 * to add/remove tables or assign waiters to tables.
 * @author Robert Kardjaliev
 *
 */
public class WaitersForTablesView extends JFrame {

	/** Unique serialisation identifier. */
	private static final long serialVersionUID = 8830889410240081933L;
	 /**  The Main GUI Panel.    */
    private JPanel contentPane;
    /**  The button that allows to return to the Manager View.  */
    private JButton btnBack;
    /**  The button that allows the Waiters for Tables View to be reached.  */
    private JButton btnAssign;
    /**  The button that allows new tables to be created.  */
	private JButton btnAddTable;
	/**  The button that allows tables to be removed.  */
    private JButton btnRemoveTable;
    /** 
     * A label that will add interactivty to the view, so that
     * a manager can see his button clicks have an effect.
     */
	private JLabel actionLabel;
	
	/**
	 * Button for opening current tables assignment window.
	 */
	private JButton btnTableAssign;
	/**
	 * A combo box holding all the table numbers.
	 */
	private JComboBox<Integer> tableNumberComboBox;
	/**
	 * A combo box holding all the waiters.
	 */
	private JComboBox<String> waiterComboBox;

    /**
     * Create the frame.
     */
    ///CHECKSTYLE:OFF
    public WaitersForTablesView() {
    	setTitle("Waiters For Tables");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 468, 327);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        btnAssign = new JButton();
        btnAssign.setFont(new Font("Tahoma", Font.BOLD, 13));
        btnAssign.setText("Assign");
        btnAssign.setBounds(180, 116, 80, 25);
        contentPane.add(btnAssign);
        
        btnBack = new JButton();
        btnBack.setFont(new Font("Tahoma", Font.BOLD, 10));
        btnBack.setText("Back");
        btnBack.setBounds(10, 11, 63, 21);
        contentPane.add(btnBack);
        
        btnAddTable = new JButton();
        btnAddTable.setText("Add Table");
        btnAddTable.setFont(new Font("Dialog", Font.BOLD, 10));
        btnAddTable.setBounds(105, 191, 112, 40);
        contentPane.add(btnAddTable);
        
        btnRemoveTable = new JButton();
        btnRemoveTable.setText("Remove Table");
        btnRemoveTable.setFont(new Font("Dialog", Font.BOLD, 10));
        btnRemoveTable.setBounds(229, 191, 112, 40);
        contentPane.add(btnRemoveTable);
        
        actionLabel = new JLabel("");
        actionLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
        actionLabel.setBounds(93, 243, 270, 25);
        contentPane.add(actionLabel);
        
        btnTableAssign = new JButton();
        btnTableAssign.setText("View Table Assignment");
        btnTableAssign.setFont(new Font("Dialog", Font.BOLD, 10));
        btnTableAssign.setBounds(303, 6, 149, 30);
        contentPane.add(btnTableAssign);
        
        setTableNumberComboBox(new JComboBox<Integer>());
        getTableNumberComboBox().setBounds(272, 118, 93, 20);
        contentPane.add(getTableNumberComboBox());
        
        setWaiterComboBox(new JComboBox<String>());
        getWaiterComboBox().setBounds(75, 118, 93, 20);
        contentPane.add(getWaiterComboBox());
        
    }
    //CHECKSTYLE:ON
    
    /**
     * Adds table numbers to the tableNumberComboBox one by one.
     * @param num 
     * 			the Table numbers.
     */
    public final void addTableComboBoxItems(final int num) {
    	getTableNumberComboBox().addItem(num);
    }
    
    /**
     * Gets the Table number selected from the combo box.
     * @return
     * 		the Table number.
     */
    public final int getTableCombo() {
    	return (int) getTableNumberComboBox().getSelectedItem();
    }
    
    /**
     * Gets the Waiter selected from the combo box.
     * @return
     * 		the Waiter's first name.
     */
    public final String getWaiterCombo() {
    	return getWaiterComboBox().getSelectedItem().toString();
    }
    
    /**
     * Adds Waiter names to the waiterComboBox one by one.
     * @param name
     * 			a Waiter's first name.
     */
    public final void addWaiterComboBoxItems(final String name) {
    	getWaiterComboBox().addItem(name);
    }
    /**
     * Adds a listener to change back to the Manager View.
     * @param listener 
     * 				the listener for the {@code Back} button.
     */
    public final void addBackButtonListener(final ActionListener listener) {
        btnBack.addActionListener(listener);
    }
    
    /**
     * Adds a listener to the {@code Assign} button.
     * @param listener 
     * 				listener for the {@code Assign} button. 
     */
    
    public final void addAssignButtonListener(final ActionListener listener) {
        btnAssign.addActionListener(listener);
    }
    
    /**
     * Adds a listener to the {@code Add Table} button.
     * @param listener 
     * 				listener for the {@code Add Table} button. 
     */
    
    public final void addTableListener(final ActionListener listener) {
        btnAddTable.addActionListener(listener);
    }
    
    /**
     * Adds a listener to the {@code Remove Table} button.
     * @param listener 
     * 				listener for the {@code Remove Table} button. 
     */
    
    public final void addRemoveTableListener(final ActionListener listener) {
        btnRemoveTable.addActionListener(listener);
    }
    
    /**
     * Adds a listener to the {@code View Table Assignment} button.
     * @param listener 
     * 				listener for the {@code View Table Assignment} button. 
     */
    
    public final void addTableAssignListener(final ActionListener listener) {
        btnTableAssign.addActionListener(listener);
    }
    
    /**
     * A method that will set a different message depending on 
     * whether the {@code Add} or {@code Remove} buttons are pressed.
     * @param msg
     * 			the message.
     */
    public final void setText(final String msg) {
    	actionLabel.setText(msg);
    }
    
    /**
     * Gets the Waiter combo box.
     * @return
     * 			the Waiter JComboBox.
     */
	public final JComboBox<String> getWaiterComboBox() {
		return waiterComboBox;
	}

	/**
	 * Sets the Waiter combo box.
	 * @param waiterComboBox
	 * 			the Waiter JComboBox.
	 */
	public final void setWaiterComboBox(final JComboBox<String> waiterComboBox) {
		this.waiterComboBox = waiterComboBox;
	}

    /**
     * Gets the Tables combo box.
     * @return
     * 			the Table numbers JComboBox.
     */
	public final JComboBox<Integer> getTableNumberComboBox() {
		return tableNumberComboBox;
	}

	/**
	 * Sets the Table numbers combo box.
	 * @param tableNumberComboBox
	 * 			the Table numbers' JComboBox.
	 */
	public final void setTableNumberComboBox(final JComboBox<Integer> tableNumberComboBox) {
		this.tableNumberComboBox = tableNumberComboBox;
	}
}