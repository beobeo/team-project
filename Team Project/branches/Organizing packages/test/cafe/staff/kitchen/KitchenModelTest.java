package cafe.staff.kitchen;

import static org.junit.Assert.fail;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import cafe.Order;
import cafe.OrderEvent;
import cafe.OrderEventListener;
import cafe.OrderItem;
import cafe.OrderState;
import cafe.dao.OrderDAO;
import cafe.staff.kitchen.KitchenModel;

/**
 * Unit tests for the {@link KitchenModel} class.
 * 
 * @author Michael Winter
 */
public final class KitchenModelTest {
    /**
     * The upper limit on test duration (in milliseconds) that involve
     * concurrent event notifications.
     */
    private static final int MAXIMUM_WAIT = 250;
	/**
	 * Used as a wait guard during concurrent tests. Is {@code true} if the test
	 * is regarded as complete (though not necessarily passed); {@code false}
	 * otherwise.
	 */
	private volatile boolean finished = false;
	/**
	 * Indicates whether the wrong event was received during the test.
	 */
	private volatile boolean wrongEventReceived = false;
	/**
	 * The monitor object used for synchronisation during concurrent tests.
	 */
	private final Object monitor = new Object();
	
	
	/**
	 * Configures the test fixture.
	 */
	@Before
	public void setUp() {
		finished = false;
		wrongEventReceived = false;
	}
	
    /**
     * Asserts that when an order has been added to the data source, this is
     * determined upon refresh by the model.
     * 
     * @throws InterruptedException
     *             if an interruption is signalled while awaiting event capture.
     */
	@Test(timeout = MAXIMUM_WAIT)
	public void addedOrderFiresAddedEvent() throws InterruptedException {
		final MockOrderDAO orderData = new MockOrderDAO();
		final KitchenModel model = new KitchenModel(orderData);
        model.setRefreshInterval(0);
		model.addOrderModelListener(new AbstractOrderEventListener() {
            @Override
            public void orderAdded(final OrderEvent e) {
                clearWait();
            }
		});
		
		orderData.addConfirmedOrder();
		model.refresh();
        awaitEvent();
	}
	
    /**
     * Asserts that when a second order has been added to the data source, this is
     * also determinable upon refresh by the model.
     * 
     * @throws InterruptedException
     *             if an interruption is signalled while awaiting event capture.
     */
	@Test(timeout = MAXIMUM_WAIT)
	public void secondAddedOrderFiresAddedEvent() throws InterruptedException {
		final MockOrderDAO orderData = new MockOrderDAO();
		final KitchenModel model = new KitchenModel(orderData);
        model.setRefreshInterval(0);
		model.addOrderModelListener(new AbstractOrderEventListener() {
		    private final int expectedEvents = 2;
		    private volatile int fired = 0;
		    
            @Override
            public void orderAdded(final OrderEvent e) {
                ++fired;
                if (fired == expectedEvents) {
                    clearWait();
                }
            }
		});

		// Populate the model
		orderData.addConfirmedOrder();
		model.refresh();
		
		// Test addded event
        orderData.addConfirmedOrder();
        finished = false;
		model.refresh();
        awaitEvent();
	}

    /**
     * Checks whether a change in progression state of an order can be
     * discovered by the model.
     * 
     * @throws InterruptedException
     *             if an interruption is signalled while awaiting event capture.
     */
    @Test(timeout = MAXIMUM_WAIT)
    public void advancingOrderFiresChangedEvent() throws InterruptedException {
        final MockOrderDAO orderData = new MockOrderDAO();
        final KitchenModel model = new KitchenModel(orderData);
        model.setRefreshInterval(0);
        model.addOrderModelListener(new AbstractOrderEventListener() {
            private final int expectedEvents = 2;
            private volatile boolean initialised = false;
            private volatile int fired = 0;
            
            @Override
            public void orderAdded(final OrderEvent e) {
                ++fired;
                if (!initialised) {
                    if (fired == expectedEvents) {
                        initialised = true;
                        clearWait();
                    }
                } else {
                    super.orderAdded(e);
                }
            }

            @Override
            public void orderChanged(final OrderEvent e) {
                if (initialised) {
                    clearWait();
                } else {
                    super.orderChanged(e);
                }
            }
        });

        // Populate the model and synchronise
        orderData.addConfirmedOrder();
        orderData.addConfirmedOrder();
        model.refresh();
        awaitEvent();

        // Test changed event
        orderData.advanceOrder(0);
        finished = false;
        model.refresh();
        awaitEvent();
    }

    /**
     * Asserts that removing orders from the data store can be detected by the
     * model.
     * 
     * @throws InterruptedException
     *             if an interruption is signalled while awaiting event capture.
     */
    @Test(timeout = MAXIMUM_WAIT)
    public void removingOrdersFiresRemovedEvent() throws InterruptedException {
        final MockOrderDAO orderData = new MockOrderDAO();
        final KitchenModel model = new KitchenModel(orderData);
        model.setRefreshInterval(0);
        model.addOrderModelListener(new AbstractOrderEventListener() {
            private final int expectedEvents = 3;
            private volatile boolean initialised = false;
            private volatile int fired = 0;
            
            @Override
            public void orderAdded(final OrderEvent e) {
                ++fired;
                if (!initialised) {
                    if (fired == expectedEvents) {
                        initialised = true;
                        clearWait();
                    }
                } else {
                    super.orderAdded(e);
                }
            }

            @Override
            public void orderRemoved(final OrderEvent e) {
                if (initialised) {
                    clearWait();
                } else {
                    super.orderRemoved(e);
                }
            }
        });

        // Populate the model
        orderData.addConfirmedOrder();
        orderData.addConfirmedOrder();
        orderData.addConfirmedOrder();
        model.refresh();
        awaitEvent();
        
        // Remove the middle order
        orderData.removeOrder(1);
        finished = false;
        model.refresh();
        awaitEvent();

        // Remove the first order
        orderData.removeOrder(0);
        finished = false;
        model.refresh();
        awaitEvent();

        // Remove the last order
        orderData.removeOrder(0);
        finished = false;
        model.refresh();
        awaitEvent();
    }

    
    /**
     * Requires that the current thread waits until an anticipated event has
     * been received and the test can continue.
     * 
     * @throws InterruptedException
     *             if an interruption is signalled while awaiting event capture.
     */
    private void awaitEvent() throws InterruptedException {
        synchronized (monitor) {
            while (!finished) {
                monitor.wait();
            }
        }
        if (wrongEventReceived) {
            fail();
        }
    }

    /**
     * Indicates that a wait condition has been met (though not necessarily
     * successfully) and that any waiting thread should proceed.
     */
    private void clearWait() {
        finished = true;
        synchronized (monitor) {
            monitor.notifyAll();
        }
    }

    
    /**
	 * A mock data source for testing purposes.
	 */
	private static class MockOrderDAO implements OrderDAO {
	    /**
	     * The unique identifier for the next generated fake object.
	     */
		private int nextId = 1;
		/**
		 * The orders that this data source can return.
		 */
		private List<Order> orders = new ArrayList<>();
		
        /**
         * Creates a new fake object with a unique identifier, the current time
         * as placement time, and in a confirmed state.
         */
		public void addConfirmedOrder() {
			orders.add(new Order(nextId, null, OrderState.CONFIRMED, new Timestamp(
					System.currentTimeMillis()), null, null, null, Collections
					.<OrderItem> emptyList()));
			++nextId;
		}
		
        /**
         * Updates the specified order (in terms of its position in the list,
         * rather than its own properties) such that it will take on the next
         * progression state.
         * 
         * @param index
         *            the order to update.
         */
        public void advanceOrder(final int index) {
            orders.get(index).advanceProgress();
        }
        
        /**
         * Removes the specified order.
         * 
         * @param index
         *            the order to remove.
         */
        public void removeOrder(final int index) {
            orders.remove(index);
        }
        
		@Override
		public Order findOrder(final int id) {
			throw new UnsupportedOperationException();
		}

        @Override
        public List<Order> selectUndeliveredOrders() {
            List<Order> list = new ArrayList<>(orders.size());
            for (Order order : orders) {
                list.add(new Order(order.getId(), order.getSitting(),
                        order.getProgress(), order.getWhenPlaced(),
                        order.getWhenReady(), order.getWhenDelivered(),
                        order.getWaiter(), order.getItems()));
            }
            return list;
        }

		@Override
		public int createOrder(final int sitting) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int createOrder(final int sitting, final int waiter) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void updateOrder(final Order order) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void newProgress(int id) {
			throw new UnsupportedOperationException();			
		}

		@Override
		public void cancelOrder(int id) {
			throw new UnsupportedOperationException();
		}

        @Override
        public List<Order> selectAllOrders() {
            // TODO Auto-generated method stub
            return null;
        }
	}
	
	
    /**
     * Provides an adapter for implementing OrderEventListeners. The default
     * implementation for each event is to raise a test failure error.
     */
    private abstract class AbstractOrderEventListener implements
            OrderEventListener {
        /*
         * (non-Javadoc)
         * @see cafe.OrderEventListener#orderAdded(cafe.OrderEvent)
         */
        @Override
        public void orderAdded(final OrderEvent e) {
            wrongEventReceived = true;
            System.out.println("No order should be regarded as added in this test.");
            clearWait();
        }

        /*
         * (non-Javadoc)
         * @see cafe.OrderEventListener#orderChanged(cafe.OrderEvent)
         */
        @Override
        public void orderChanged(final OrderEvent e) {
            wrongEventReceived = true;
            System.out.println("No order should be regarded as changed in this test.");
            clearWait();
        }

        /*
         * (non-Javadoc)
         * @see cafe.OrderEventListener#orderRemoved(cafe.OrderEvent)
         */
        @Override
        public void orderRemoved(final OrderEvent e) {
            wrongEventReceived = true;
            System.out.println("No order should be regarded as removed in this test.");
            clearWait();
        }
    }
}
