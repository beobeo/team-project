package cafe;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JScrollPane;

import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ScrollPaneConstants;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import javax.swing.BoxLayout;

/**
 * 
 * @author Robert Kardjaliev
 * @author Jonny Hercock
 * @author Hannah Cooper
 * @author Adam Lumber
 * 
 */
public class OrderGUI extends JFrame {

	/** sets the Serial Version to 1L. */
	private static final long serialVersionUID = 1L;

	/** creates content pane. */
	private JPanel contentPane;

	/** Creates a button to call for assistance. */
	private final JButton btnCallAssistance = new JButton("Call for Assistance");

	/** Sets title label for order. */
	private final JLabel lblTitle = new JLabel("ORDER");

	/** Sets label for total price. */
	private final JLabel lblPrice = new JLabel("Total Price");

	/** Creates an area for total price to be placed. */
	private final JLabel lblTotalPrice = new JLabel("");

	/** Creates a confirm order button. */
	static final JButton btnConfirmOrder = new JButton("Confirm Order");

	/** Creates a menu button. */
	private final JButton btnMenu = new JButton("Menu");

	/** Creates an order button. */
	private final JButton btnOrder = new JButton("Order");

	/** Makes a scroll pane. */
	private final JScrollPane orderScroller = new JScrollPane();

	/** Creates a list of dish component. */
	private ArrayList<DishComponent> dishComponents = new ArrayList<DishComponent>();
	/**
	 * Track order button.
	 */
	private final JButton btnTrackOrder = new JButton("Track Order");

	/**
	 * JPanel holding the components to be displayed in the scrollPane.
	 */
	private final JPanel orderPanel = new JPanel();

	// CHECKSTYLE:OFF
	/**
	 * Create the frame.
	 */
	public OrderGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 520);
		contentPane = new JPanel();
		contentPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});
		contentPane.setBackground(SystemColor.menu);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 135, 127, 127, 82, 1, 55,
				46, 0 };
		gbl_contentPane.rowHeights = new int[] { 23, 380, 30, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_lblTitle = new GridBagConstraints();
		gbc_lblTitle.insets = new Insets(0, 0, 5, 5);
		gbc_lblTitle.gridx = 0;
		gbc_lblTitle.gridy = 0;
		contentPane.add(lblTitle, gbc_lblTitle);
		btnOrder.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
			}
		});
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
			}
		});
		GridBagConstraints gbc_btnMenu = new GridBagConstraints();
		gbc_btnMenu.anchor = GridBagConstraints.NORTH;
		gbc_btnMenu.gridwidth = 2;
		gbc_btnMenu.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnMenu.insets = new Insets(0, 0, 5, 5);
		gbc_btnMenu.gridx = 3;
		gbc_btnMenu.gridy = 0;
		contentPane.add(btnMenu, gbc_btnMenu);

		btnOrder.setEnabled(false);
		GridBagConstraints gbc_btnOrder = new GridBagConstraints();
		gbc_btnOrder.anchor = GridBagConstraints.NORTH;
		gbc_btnOrder.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnOrder.insets = new Insets(0, 0, 5, 0);
		gbc_btnOrder.gridwidth = 2;
		gbc_btnOrder.gridx = 5;
		gbc_btnOrder.gridy = 0;
		contentPane.add(btnOrder, gbc_btnOrder);
		orderScroller.setBorder(null);
		orderScroller

				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		GridBagConstraints gbc_orderScroller = new GridBagConstraints();
		gbc_orderScroller.fill = GridBagConstraints.BOTH;
		gbc_orderScroller.insets = new Insets(5, 2, 5, 0);
		gbc_orderScroller.gridwidth = 7;
		gbc_orderScroller.gridx = 0;
		gbc_orderScroller.gridy = 1;
		contentPane.add(orderScroller, gbc_orderScroller);
		orderPanel.setBorder(null);
		orderPanel.setOpaque(false);
		orderScroller.setViewportView(orderPanel);
		orderPanel.setLayout(new GridLayout(10, 1, 0, 2)); // changed to 10 from
															// 5
		btnCallAssistance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});

		GridBagConstraints gbc_btnCallAssistance = new GridBagConstraints();
		gbc_btnCallAssistance.fill = GridBagConstraints.BOTH;
		gbc_btnCallAssistance.insets = new Insets(0, 0, 0, 5);
		gbc_btnCallAssistance.gridx = 0;
		gbc_btnCallAssistance.gridy = 2;
		contentPane.add(btnCallAssistance, gbc_btnCallAssistance);

		GridBagConstraints gbc_btnTrackOrder = new GridBagConstraints();
		gbc_btnTrackOrder.fill = GridBagConstraints.BOTH;
		gbc_btnTrackOrder.insets = new Insets(0, 0, 0, 5);
		gbc_btnTrackOrder.gridx = 1;
		gbc_btnTrackOrder.gridy = 2;
		btnTrackOrder.setEnabled(false);
		contentPane.add(btnTrackOrder, gbc_btnTrackOrder);
		btnConfirmOrder.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
			}
		});
		GridBagConstraints gbc_btnConfirmOrder = new GridBagConstraints();
		gbc_btnConfirmOrder.fill = GridBagConstraints.BOTH;
		gbc_btnConfirmOrder.insets = new Insets(0, 0, 0, 5);
		gbc_btnConfirmOrder.gridx = 2;
		gbc_btnConfirmOrder.gridy = 2;
		contentPane.add(btnConfirmOrder, gbc_btnConfirmOrder);

		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblPrice = new GridBagConstraints();
		gbc_lblPrice.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblPrice.insets = new Insets(0, 0, 0, 5);
		gbc_lblPrice.gridwidth = 2;
		gbc_lblPrice.gridx = 4;
		gbc_lblPrice.gridy = 2;
		contentPane.add(lblPrice, gbc_lblPrice);

		GridBagConstraints gbc_lblTotalPrice = new GridBagConstraints();
		gbc_lblTotalPrice.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblTotalPrice.gridx = 6;
		gbc_lblTotalPrice.gridy = 2;
		contentPane.add(lblTotalPrice, gbc_lblTotalPrice);

	}

	/**
	 * populates the GUI.
	 * 
	 * @param orderItems
	 *            - list of dishes
	 * @param aListener
	 *            - action listener
	 */
	public final void populateGUI(final List<OrderItem> orderItems,
			final float totalPrice, final ActionListener aListener) {
		populateDishComponents(orderItems, aListener);
		populateScrollingPane();
		lblTotalPrice.setText(Float.toString(totalPrice));
		revalidate();
		repaint();
	}

	/**
	 * Sets whether the Confirm Order button is enabled or disabled.
	 * 
	 * @param enabled
	 *            {@code true} to enable the button; {@code false} otherwise.
	 */
	public void enableConfirmOrderButton(final boolean enabled) {
		btnConfirmOrder.setEnabled(enabled);
	}

	/**
	 * Sets whether the Track Order button is enabled or disabled.
	 * 
	 * @param enabled
	 *            {@code true} to enable the button; {@code false} otherwise.
	 */
	public void enableTrackOrderButton(final boolean enabled) {
		btnTrackOrder.setEnabled(enabled);
	}

	public void makeConfirmOrderVisible() {
		btnConfirmOrder.setEnabled(true);
		btnTrackOrder.setEnabled(false);
	}

	/**
	 * Adds an action listener to the Track Order button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public final void addShowTrackOrderViewListener(
			final ActionListener listener) {
		btnTrackOrder.addActionListener(listener);
	}

	/**
	 * Adds an action listener to the Call for Assistance button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public final void addWaiterCallListener(final ActionListener listener) {
		btnCallAssistance.addActionListener(listener);
	}

	/**
	 * Adds an action listener to the Confirm Order button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public final void addConfirmOrderListener(final ActionListener listener) {
		btnConfirmOrder.addActionListener(listener);
	}

	/**
	 * Adds an action listener to the Menu button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public final void addShowMenuListener(final ActionListener listener) {
		btnMenu.addActionListener(listener);
	}

	/**
	 * Populates the DishComponent ArrayList with the dishes that are to be
	 * ordered.
	 * 
	 * @param aListener
	 *            - action listener
	 * @param orderItems
	 *            - list of dishes to be displayed
	 */
	private final void populateDishComponents(final List<OrderItem> orderItems,
			final ActionListener aListener) {
		dishComponents.clear();
		for (OrderItem i : orderItems) {
			// dishComponents.add(new DishComponent(
			// i.getDish().getName(),i.getDish().getDescription(),
			// i.getQuantity(), aListener, i.getDish().getImage(),
			// i.getDish()));

			dishComponents.add(new DishComponent(i.getDish().getName(), i
					.getDish().getDescription(), i.getDish().getPrice(), i
					.getQuantity(), aListener, i.getDish().getImage(), i
					.getDish()));
		}
	}

	/**
	 * Populates the scrolling pane with the dish component ArrayList.
	 * 
	 */
	private final void populateScrollingPane() {
		orderPanel.removeAll();

		for (DishComponent dc : dishComponents) {
			orderPanel.add(dc);
		}
		orderScroller.setViewportView(orderPanel);
	}

}
