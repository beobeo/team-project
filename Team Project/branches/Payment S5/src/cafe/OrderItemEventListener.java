package cafe;

import java.util.EventListener;

/**
 * Defines the interface for an object that listens to changes in an
 * {@link Order}.
 * 
 * @author Michael Winter
 */
public interface OrderItemEventListener extends EventListener {
	/**
	 * Invoked after an item has been added to an order.
	 * 
	 * @param e an order item added event.
	 */
	void orderItemAdded(OrderItemEvent e);
}
