package cafe;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JScrollPane;

import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ScrollPaneConstants;

/**
 * 
 * @author Robert Kardjaliev
 * @author Jonny Hercock
 * @author Hannah Cooper
 * @author Adam Lumber
 * 
 */
public class OrderGUI extends JFrame {

	/** sets the Serial Version to 1L. */
	private static final long serialVersionUID = 1L;

	/** creates content pane. */
	private JPanel contentPane;

	/** Creates a button to call for assistance. */
	private final JButton btnCallAssistance = new JButton("Call for Assistance");

	/** Sets title label for order. */
	private final JLabel lblTitle = new JLabel("ORDER");

	/** Sets label for total price. */
	private final JLabel lblPrice = new JLabel("Total Price");

	/** Creates an area for total price to be placed. */
	private final JLabel lblTotalPrice = new JLabel("");

	/** Creates a confirm order button. */
	private final JButton btnConfirmOrder = new JButton("Confirm Order");

	/** Creates a menu button. */
	private final JButton btnMenu = new JButton("Menu");

	/** Creates an order button. */
	private final JButton btnOrder = new JButton("Order");

	/** Makes a scroll pane. */
	private final JScrollPane scrollPane = new JScrollPane();

	/** Creates a list of dish component. */
	private ArrayList<DishComponent> dishComponents = new ArrayList<DishComponent>();
	/**
	 * Track order button.
	 */
	private final JButton btnTrackOrder = new JButton("trackOrder");
//CHECKSTYLE:OFF
	/**
	 * Create the frame.
	 */
	public OrderGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 693, 463);
		contentPane = new JPanel();
		contentPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});
		contentPane.setBackground(SystemColor.activeCaption);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		scrollPane
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		scrollPane.setBounds(10, 45, 657, 324);
		contentPane.add(scrollPane);

		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblTitle.setBounds(10, 13, 72, 14);
		contentPane.add(lblTitle);
		btnCallAssistance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnCallAssistance.setBounds(12, 386, 127, 30);

		contentPane.add(btnCallAssistance);

		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPrice.setBounds(552, 392, 67, 14);
		contentPane.add(lblPrice);

		lblTotalPrice.setBounds(620, 392, 46, 14);
		contentPane.add(lblTotalPrice);
		btnConfirmOrder.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
			}
		});

		btnConfirmOrder.setBounds(336, 386, 127, 30);
		contentPane.add(btnConfirmOrder);
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
			}
		});

		btnMenu.setBounds(478, 11, 89, 23);
		contentPane.add(btnMenu);
		btnOrder.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
			}
		});

		btnOrder.setEnabled(false);
		btnOrder.setBounds(578, 11, 89, 23);
		contentPane.add(btnOrder);
		btnTrackOrder.setBounds(164, 386, 127, 30);
		
		contentPane.add(btnTrackOrder);

	}

	/**
	 * populates the GUI.
	 * 
	 * @param orderItems
	 *            - list of dishes
	 * @param aListener
	 *            - action listener
	 */
	public final void populateGUI(final List<OrderItem> orderItems,
			final ActionListener aListener) {
		populateDishComponents(orderItems, aListener);
		populateScrollingPane();

		revalidate();
		repaint();
	}
	
    /**
     * Sets whether the Confirm Order button is enabled or disabled.
     * 
     * @param enabled
     *            {@code true} to enable the button; {@code false} otherwise.
     */
    public void enableConfirmOrderButton(final boolean enabled) {
        btnConfirmOrder.setEnabled(enabled);
    }

    /**
     * Sets whether the Track Order button is enabled or disabled.
     * 
     * @param enabled
     *            {@code true} to enable the button; {@code false} otherwise.
     */
    public void enableTrackOrderButton(final boolean enabled) {
        btnTrackOrder.setEnabled(enabled);
    }

	public void makeConfirmOrderVisible(){
    	btnConfirmOrder.setEnabled(true);
    	btnTrackOrder.setEnabled(false);
    }
	
	
	 /**
     * Adds an action listener to the Track Order button.
     * 
     * @param listener
     *            the action listener to be added.
     */
    public final void addShowTrackOrderViewListener(final ActionListener listener) {
        btnTrackOrder.addActionListener(listener);
    }
	
	
	/**
     * Adds an action listener to the Call for Assistance button.
     * 
     * @param listener
     *            the action listener to be added.
     */
    public final void addWaiterCallListener(final ActionListener listener) {
        btnCallAssistance.addActionListener(listener);
    }
	
	
    /**
     * Adds an action listener to the Confirm Order button.
     * 
     * @param listener
     *            the action listener to be added.
     */
    public final void addConfirmOrderListener(final ActionListener listener) {
        btnConfirmOrder.addActionListener(listener);
    }
	
	
	/**
	 * Adds an action listener to the Menu button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public final void addShowMenuListener(final ActionListener listener) {
		btnMenu.addActionListener(listener);
	}

	/**
	 * Populates the DishComponent ArrayList with the dishes that are to be
	 * ordered.
	 * 
	 * @param aListener
	 *            - action listener
	 * @param orderItems
	 *            - list of dishes to be displayed
	 */
	private final void populateDishComponents(final List<OrderItem> orderItems,
			final ActionListener aListener) {
		for (OrderItem i : orderItems) {
			dishComponents.add(new DishComponent(i.getDish().getName(), i
					.getDish().getDescription(), i.getDish().getPrice(), i
					.getQuantity(), aListener));
			// d.getImage(); when it's finished
		}
	}

	/**
	 * Populates the scrolling pane with the dish component ArrayList.
	 * 
	 */
	private final void populateScrollingPane() {
		for (DishComponent dc : dishComponents) {
			scrollPane.getViewport().add(dc);
		}
	}
	
	public final void enableTrackOrder() {
		btnTrackOrder.setEnabled(true);
	}
	
	public final void disableTrackOrder() {
		btnTrackOrder.setEnabled(false);
	}
	
	public final void enableConfirmOrder() {
		btnConfirmOrder.setEnabled(true);
	}
	
	public final void disableConfirmOrder() {
		btnConfirmOrder.setEnabled(false);
	}
}
