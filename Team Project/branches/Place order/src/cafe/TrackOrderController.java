/**
 * 
 */
package cafe;

import java.awt.event.ActionListener;
//import java.io.IOException;

/**
 * 
 * @author Jonathan Hercock
 * @author Hannah Cooper
 * The Class TrackOrderController.
 */



public class TrackOrderController {
    /**
     * Hides the view associated with this controller.
     */
    public final void hideView() {
        view.setVisible(false);
    }
    
    /**
     * Shows the view associated with this controller.
     */
    public final void showView() {
        view.setVisible(true);
    }

    /** The model. */
    //private TrackOrderModel model;
    /** The view. */
    private TrackMyOrderGUI view;
    
    //private ClientApplicationController clientApp;

    /**
     * Instantiates the fields of the Track Order Controller.
     *
     * @param view - The View
     */
    public TrackOrderController(final TrackMyOrderGUI view) { 
        this.view = view; 
        //view.addEditOrderListener(new addEditOrderListener());
    }
    
    /**
     * 
     * @param listener - Edit Order Listener
     */
    public final void addEditOrderListener(final ActionListener listener) {
    	view.addEditOrderListener(listener);
    }
    /**
     * 
     * @param listener - cancel order listener
     */
    public final void addCancelOrderListener(final ActionListener listener) {
    	view.addCancelOrderListener(listener);
    }
}
        


