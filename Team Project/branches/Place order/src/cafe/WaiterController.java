package cafe;

/**
 * 
 */

/**
 * @author Jonathan Hercock
 *
 */
public class WaiterController {
    
    /**
     *  The Waiter's interface.
     */
    private static WaiterGUI view;

    /** A constructor for the given view.
     * 
     * @param view - the log in view.
     */
    public WaiterController(final WaiterGUI view) {
        WaiterController.view = view;
    }
    
    /**
     * Shows the view associated with this controller.
     */
    public final void showView() {
       view.setVisible(true);
    }

    /**
     * Hides the view associated with this controller. 
     */
    public final void hideView() {
        WaiterController.view.setVisible(false);
    }

}
