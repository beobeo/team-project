package cafe.dao;

import cafe.Employee;

/**
 * A data access object used to manipulate persisted {@link Employee} objects.
 * 
 * @author Michael Winter
 */
public interface EmployeeDAO {
    /**
     * Given an identifier, this method loads and returns a new instance of the
     * specified {@link Employee}.
     * 
     * @param id
     *            the identifier.
     * @return the specified {@code Sitting}.
     * 
     */
    Employee findEmployee(int id);
}
