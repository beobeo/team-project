package cafe.dao;

import java.util.List;

import cafe.Order;

/**
 * A data access object used to manipulate persisted {@link Order} objects.
 * 
 * @author Robert Kardjaliev
 * @author Michael Winter
 */
public interface OrderDAO {
	/**
	 * Given an identifier, this method loads and returns a new instance of the
	 * specified {@link Order}.
	 * 
	 * @param id
	 *            an {@link Order}'s ID.
	 * @return the specified {@link Order}.
	 * @throws DataAccessException
	 *             if an error occurs while accessing the data store.
	 */
	Order findOrder(int id);
	
	/**
	 * Gives a list containing all {@link Order}s with their numbers
	 * and the table they were set from.
	 * 
	 * @return A list of all the {@link Order}s.
	 * @throws DataAccessException
	 *             if an error occurs while accessing the data store.
	 */
	List<Order> selectUndeliveredOrders();
	
    /**
     * Creates a new {@link Order} relating to the specified {@link Sitting},
     * returning its identifier. This is equivalent to calling
     * {@code createOrder(sitting, -1)}.
     * 
     * @param sitting
     *            the identifier of the sitting to which this order relates.
     * @return the identifier for the new order.
     *
     */
	int createOrder(int sitting);
    
    /**
     * Creates a new {@link Order} related to the specified {@link Sitting} on
     * behalf of a {@link Waiter}, returning its identifier.
     * 
     * @param sitting
     *            the identifier of the sitting to which this order relates.
     * @param waiter
     *            the identifier of the waiter that took this order or
     *            {@code -1} if it was placed by a customer.
     * @return the identifier for the new order.
     *
     */
    int createOrder(int sitting, int waiter);
    
    /**
     * Updates an {@code Order} that has been previously persisted. 
     * 
     * @param order the {@code Order} to update. 
     */
    void updateOrder(Order order);
}
