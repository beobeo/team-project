package cafe;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cafe.dao.DAOFactory;
import cafe.dao.DishDAO;
import cafe.dao.EmployeeDAO;
import cafe.dao.IngredientDAO;
import cafe.dao.OrderDAO;
import cafe.dao.OrderItemDAO;
import cafe.dao.SittingDAO;
import cafe.dao.TableDAO;
import cafe.dao.postgres.AbstractDAOFactoryTestImpl;
import cafe.dao.postgres.DishDAOImpl;
import cafe.dao.postgres.EmployeeDAOImpl;
import cafe.dao.postgres.IngredientDAOImpl;
import cafe.dao.postgres.OrderDAOImpl;
import cafe.dao.postgres.OrderItemDAOImpl;
import cafe.dao.postgres.SittingDAOImpl;
import cafe.dao.postgres.TableDAOImpl;

/**
 * Unit tests for the {@link Order} class.
 * 
 * @author Hannah Cooper
 * @author Michael Winter
 */
public final class OrderModelTest {
    /**
     * The order number used in this test fixture. This order must exist in the
     * database.
     */
    private static final int ORDER_ID = 1;
    /**
     * The data access object used to retrieve dishes used by this test fixture.
     */
    private static DishDAO dishDataSource = null;
    /**
     * The data access object used to retrieve ingredients used by this test
     * fixture.
     */
    private static IngredientDAO ingredientDataSource = null;
    /**
     * The data access object used to retrieve orders manipulated by this test
     * fixture.
     */
    private static OrderDAO orderDataSource = null;
    /**
     * The data access object used to retrieve order items created by this test
     * fixture.
     */
    private static OrderItemDAO orderItemDataSource = null;
    /**
     * A list of dishes that are added to the database. This list is used to
     * clean up after each test to avoid duplicate key errors and unnecessary
     * database bloat.
     */
    private List<Dish> insertedDishes = new ArrayList<>();
    /**
     * The order model under test.
     */
    private OrderModel model;
    
    
    /**
     * Initialises the data sources to be used by the test fixture.
     */
    @BeforeClass
    public static void initialiseDataSource() {
        final DAOFactory factory = DAOFactory.getInstance("zyvc215", "IYIP1845",
                OrderModelTestDAOFactoryImpl.class);
        dishDataSource = factory.getDishDAO();
        ingredientDataSource = factory.getIngredientDAO();
        orderDataSource = factory.getOrderDAO();
        orderItemDataSource = factory.getOrderItemDAO();
    }
    
    /**
     * Constructs the object under test for each unit test.
     */
    @Before
    public void setup() {
        model = new OrderModel(orderDataSource.findOrder(ORDER_ID),
                orderDataSource, orderItemDataSource, ingredientDataSource);
    }
    
    /**
     * Removes any items added to the database after each test.
     */
    @After
    public void cleanupItems() {
        for (final Dish dish : insertedDishes) {
            orderItemDataSource.deleteOrderItem(ORDER_ID, dish.getId());
        }
        insertedDishes.clear();
    }
    
    /**
     * Asserts that a bare model contains no order items at initialisation.
     */
	@Test
	public void modelInitiallyEmpty() {
	    assertTrue(model.getDishes().isEmpty());
	}
    
    /**
     * Checks that an order item can be successfully inserted by the model and
     * that the model retains that item.
     * 
     * @throws DishesUnavailableException
     *             if one or more dishes has insufficient ingredients.
     */
    @Test
    public void addingDishIncreasesItemListLength() throws DishesUnavailableException {
        final int dishId = 1;
        final Dish dish = dishDataSource.findDish(dishId);
        insertedDishes.add(dish);
        model.addItem(dish);
        assertFalse(model.getDishes().isEmpty());
    }
    
    /**
     * Ensures that rather than duplicating a dish does not result in an attempt
     * to add another database record (this will fail due to key conflicts).
     * 
     * @throws DishesUnavailableException
     *             if one or more dishes has insufficient ingredients.
     */
    @Test
    public void addingSameDishNotIncreasingItemListLength() throws DishesUnavailableException {
        final int dishId = 1;
        final Dish dish = dishDataSource.findDish(dishId);
        final int expectedItems = 1;
        insertedDishes.add(dish);
        model.addItem(dish);
        model.addItem(dish);
        assertEquals(expectedItems, model.getDishes().size());
    }
    
    /**
     * As an extension to the previous test, this checks that the duplicate dish
     * results in an increase in the quantity ordered of that dish.
     * 
     * @throws DishesUnavailableException
     *             if one or more dishes has insufficient ingredients.
     */
    @Test
    public void addingSameDishIncreasesItemQuantity() throws DishesUnavailableException {
        final int dishId = 1;
        final Dish dish = dishDataSource.findDish(dishId);
        final int expectedQuantity = 2;
        insertedDishes.add(dish);
        model.addItem(dish);
        model.addItem(dish);
        assertEquals(expectedQuantity, model.getDishes().get(0).getQuantity());
    }

    /**
     * Asserts that adding a different dish to an order with existing items
     * extends that order.
     * 
     * @throws DishesUnavailableException
     *             if one or more dishes has insufficient ingredients.
     */
    @Test
    public void addingDifferentDishIncreasesItemListLength() throws DishesUnavailableException {
        final int firstDishId = 1;
        final Dish firstDish = dishDataSource.findDish(firstDishId);
        final int secondDishId = 2;
        final Dish secondDish = dishDataSource.findDish(secondDishId);
        final int expectedItems = 2;
        insertedDishes.add(firstDish);
        insertedDishes.add(secondDish);
        model.addItem(firstDish);
        model.addItem(secondDish);
        assertEquals(expectedItems, model.getDishes().size());
    }
	
    /**
     * Checks that the summation of the total price of an order is calculated
     * correctly.
     * 
     * @throws DishesUnavailableException
     *             if one or more dishes has insufficient ingredients.
     */
    @Test
    public void totalPrice() throws DishesUnavailableException {
        final int firstDishId = 1;
        final Dish firstDish = dishDataSource.findDish(firstDishId);
        final int secondDishId = 2;
        final Dish secondDish = dishDataSource.findDish(secondDishId);
        
        final float expectedPrice = firstDish.getPrice() + secondDish.getPrice();
        final float delta = 0.001f;
        insertedDishes.add(firstDish);
        insertedDishes.add(secondDish);

        model.addItem(firstDish);
        model.addItem(secondDish);

        assertEquals("Test for correct addition of dish prices", expectedPrice,
                model.getTotalPrice(), delta);
    }
    
    /**
     * Asserts that adding an unavailable dish raises an exception.
     * 
     * @throws DishesUnavailableException
     *             if one or more dishes has insufficient ingredients.
     */
    @Test(expected = DishesUnavailableException.class)
    public void addingUnavailableDishRaisesException() throws DishesUnavailableException {
        final int dishId = 4;
        final Dish dish = dishDataSource.findDish(dishId);
        model.addItem(dish);
        
        // Allow for the dish to removed in case the test fails and addition succeeds.
        insertedDishes.add(dish);
    }
    
    /**
     * Asserts that placing an order with an unavailable dish raises an exception.
     * TODO: Uncomment test once IngredientDAO.updateIngredient() is available.
     * 
     * @throws DishesUnavailableException
     *             if one or more dishes has insufficient ingredients.
     */
    /*
    @Test(expected = DishesUnavailableException.class)
    public void placingUnavailableOrderRaisesException() throws DishesUnavailableException {
        final int dishId = 9;
        final int ingredientId = 11;
        final Dish dish = dishDataSource.findDish(dishId);
        final Ingredient ingredient = ingredientDataSource.findIngredient(ingredientId);

        insertedDishes.add(dish);
        model.addItem(dish);
        model.addItem(dish);
        
        // Simulate a reduction in stock after adding an item.
        ingredient.setStock(30);
        ingredientDataSource.updateIngredient(ingredient);
        
        try {
            model.placeOrder();
        } finally {
            // Restore the stock levels for subsequent tests.
            ingredient.setStock(1000);
            ingredientDataSource.updateIngredient(ingredient);
        }
    }
    */
    
    /**
     * As an extension of the previous test, this ensures that placing an order that
     * is only partially fulfilable modifies the order such that it can be placed
     * but still raises an exception.
     * TODO: Uncomment test once IngredientDAO.updateIngredient() is available.
     * 
     * @throws DishesUnavailableException
     *             if one or more dishes has insufficient ingredients.
     */
    /*
    @Test(expected = DishesUnavailableException.class)
    public void placingOrderWithLowStockChangesOrder()
            throws DishesUnavailableException {
        final int dishId = 9;
        final int ingredientId = 11;
        final int initialOrderQuantity = 2;
        final int expectedOrderQuantity = 1;
        final Dish dish = dishDataSource.findDish(dishId);
        final Ingredient ingredient = ingredientDataSource.findIngredient(ingredientId);

        insertedDishes.add(dish);
        model.addItem(dish);
        model.addItem(dish);
        assertEquals(initialOrderQuantity,
                model.getDishes().get(0).getQuantity());

        // Simulate a reduction in stock after adding an item.
        ingredient.setStock(30);
        ingredientDataSource.updateIngredient(ingredient);

        try {
            model.placeOrder();
        } finally {
            // Restore the stock levels for subsequent tests.
            ingredient.setStock(1000);
            ingredientDataSource.updateIngredient(ingredient);

            assertEquals(expectedOrderQuantity,
                    model.getDishes().get(0).getQuantity());
        }
    }
    */
    
	
    /**
     * A simplified DAOFactory implementation that uses the real data access
     * object implementations, but targets the test database.
     */
	public static class OrderModelTestDAOFactoryImpl extends AbstractDAOFactoryTestImpl {
        /* (non-Javadoc)
         * @see cafe.dao.DAOFactory#getDishDAO()
         */
        @Override
        public DishDAO getDishDAO() {
            return new DishDAOImpl(getIngredientDAO(), this);
        }

        /* (non-Javadoc)
         * @see cafe.dao.DAOFactory#getIngredientDAO()
         */
        @Override
        public IngredientDAO getIngredientDAO() {
            return new IngredientDAOImpl(this);
        }

        /* (non-Javadoc)
         * @see cafe.dao.DAOFactory#getSittingDAO()
         */
        @Override
        public SittingDAO getSittingDAO() {
            return new SittingDAOImpl(getTableDAO(), this);
        }

        /* (non-Javadoc)
         * @see cafe.dao.DAOFactory#getTableDAO()
         */
        @Override
        public TableDAO getTableDAO() {
            return new TableDAOImpl(getEmployeeDAO(), this);
        }

        /* (non-Javadoc)
         * @see cafe.dao.DAOFactory#getOrderDAO()
         */
        @Override
        public OrderDAO getOrderDAO() {
            return new OrderDAOImpl(getOrderItemDAO(), getSittingDAO(), getEmployeeDAO(), this);
        }

        /* (non-Javadoc)
         * @see cafe.dao.DAOFactory#getOrderItemDAO()
         */
        @Override
        public OrderItemDAO getOrderItemDAO() {
            return new OrderItemDAOImpl(getDishDAO(), this);
        }

        /* (non-Javadoc)
         * @see cafe.dao.DAOFactory#getEmployeeDAO()
         */
        @Override
        public EmployeeDAO getEmployeeDAO() {
            return new EmployeeDAOImpl(this);
        }
	}
}
