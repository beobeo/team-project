package cafe.client.payment;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;

/**
 * Presents a graphical window to the customer for payment.
 * 
 * @author Robert Kardjaliev
 * @author Michael Winter
 */
public class PaymentView extends JFrame {
	/**
	 * Unique serialisation identifier.
	 */
	private static final long serialVersionUID = 8830889410240081933L;
	/** The button for paying with cash. */
	private JButton btnPayCash;
	/** The button for paying by card. */
	private JButton btnPayCard;
	/** The label that will display the amount to be paid for an order. */
	private JLabel priceLabel;
	/** The label that will display a message when payment is done. */
	private JLabel msgLabel;
	/**
	 * The table in which order information will be displayed.
	 */
	private JTable itemTable;

	/**
	 * Create the frame.
	 * 
	 * @param tableModel
	 *            the {@code TableModel} used to access the database.
	 */
	public PaymentView(final TableModel tableModel) {
		initComponents();
		itemTable.setModel(tableModel);
	}

	/**
	 * Adds a listener to pay with cash button.
	 * 
	 * @param listener
	 *            - the listener for the pay with cash button.
	 */
	public final void addPayWithCashListener(final ActionListener listener) {
		btnPayCash.addActionListener(listener);
	}

	/**
	 * Adds a listener to pay with card button.
	 * 
	 * @param listener
	 *            - the listener for the pay with card button.
	 */
	public final void addPayWithCardListener(final ActionListener listener) {
		btnPayCard.addActionListener(listener);
	}

	/**
	 * Sets the price an order costs in the price label.
	 * 
	 * @param fl
	 *            the float representation of the price.
	 */
	public final void setPrice(final Float fl) {
		priceLabel.setText(fl.toString());
	}

	/**
	 * Sets the price an order costs in the price label.
	 * 
	 * @param str
	 *            the String message to be displayed.
	 */
	public final void setMessage(final String str) {
		msgLabel.setText(str);
	}

	// CHECKSTYLE:OFF
	private void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		final JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);

		itemTable = new JTable();
		itemTable.setFillsViewportHeight(true);
		scrollPane.setViewportView(itemTable);

		final JPanel controlsPanel = new JPanel();
		contentPane.add(controlsPanel, BorderLayout.SOUTH);

		JLabel priceTxtLabel = new JLabel("Price:");
		controlsPanel.add(priceTxtLabel);

		priceLabel = new JLabel("");
		controlsPanel.add(priceLabel);

		btnPayCash = new JButton();
		controlsPanel.add(btnPayCash);
		btnPayCash.setText("Pay with cash");

		btnPayCard = new JButton();
		controlsPanel.add(btnPayCard);
		btnPayCard.setText("Pay with card");

		msgLabel = new JLabel("");
		controlsPanel.add(msgLabel);
	}
	// CHECKSTYLE:ON
}