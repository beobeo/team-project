package cafe.dao;

import java.util.ArrayList;
import java.util.Map;

import cafe.Ingredient;

/**
 * A data access object used to manipulate persisted {@link Ingredient} objects.
 * 
 * @author Michael Winter
 * @author Hannah Cooper
 */
public interface IngredientDAO {
    /**
     * Given an identifier, this method loads and returns a new instance of the
     * specified {@link Ingredient}.
     * 
     * @param id
     *            the identifier.
     * @return the specified {@code Ingredient}.
     *
     */
    Ingredient findIngredient(int id);

    /**
     * Given a Dish identifier and a connection, this method returns the
     * ingredients (and quantity) necessary to prepare that dish as a map.
     * 
     * @param dishId
     *            the dish to be prepared.
     * @return the mapping of ingredients-to-quantity required for the specified
     *         dish.
     * @throws DataAccessException
     *             if an error occurs while accessing the data store.
     */
    Map<Ingredient, Integer> selectDishIngredients(int dishId);
    
    /**
     * 
     * @return all ingredients in the database
     */
    ArrayList<Ingredient> selectIngredients();
    /**
     * 
     * @param id - id of ingredient.
     * @param number - stock level the user wishes to change the stock level to
     */
    void changeStockLevel(int id, int number);
}
