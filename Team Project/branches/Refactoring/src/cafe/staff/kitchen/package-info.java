/**
 * Defines classes related to the user interface for kitchen staff.
 * 
 * @author Michael Winter
 */
package cafe.staff.kitchen;