package cafe.dao.postgres;

import cafe.dao.jdbc.AbstractDAOFactoryImpl;

/**
 * This class is an abstract factory that defines connections to the underlying
 * PostgreSQL test database. The intention is that tests around DAO object
 * implementations can subclass this utility class.
 * 
 * @author Michael Winter
 */
public abstract class AbstractTestDAOFactoryImpl extends AbstractDAOFactoryImpl {
    /**
     * The JDBC connection string for the test database. The full host name
     * is used to allow remote connections.
     */
    private static final String URI = "jdbc:postgresql://teaching.cs.rhul.ac.uk:29503/test";

    
    /* (non-Javadoc)
     * @see cafe.dao.jdbc.AbstractDAOFactoryImpl#getConnectionUri()
     */
    @Override
    protected final String getConnectionUri() {
        return URI;
    }
}
