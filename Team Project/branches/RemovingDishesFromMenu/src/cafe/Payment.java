package cafe;

/**
 * Represents a payment to the cafe.
 * 
 * @author Robert Kardjaliev
 */
public class Payment {
	/**
	 * The payment ID.
	 */
	private int id;
	/**
	 * The customer's {@link Sitting} ID.
	 */
	private int sittingId;
	/**
	 * The card used to make the payment.
	 */
	private String card;

	/**
	 * The amount paid.
	 */
	private float amount;


	/**
	 * Constructs a new {@code Payment} instance.
	 * 
	 * @param id
	 *            the unique identifier for this instance.
	 * @param sittingId
	 *            the customer's sittingId
	 * @param card
	 *            the customer's 16 characters long card
	 * @param amount
	 * 			  the amount of money getting paid
	 */
	public Payment(final int id, final int sittingId, final String card, final float amount) {
		this.id = id;
		this.sittingId = sittingId;
		this.card = card;
		this.amount = amount;
	}

	/**
	 * Returns the unique identifier for this {@code Payment}. 
	 * @return the identifier
	 */
	public final int getId() {
		return id;
	}

	/**
	 * Returns the {@code sittingId} for this {@code Payment}.
	 * @return the {@code sittingId}
	 */
	public final int getSittingId() {
		return sittingId;
	}

	/**
	 * Sets the {@code sittingId} for this {@code Payment}.
	 * @param sittingId the {@code sittingId}.
	 */
	public final void setSittingId(final int sittingId) {
		this.sittingId = sittingId;
	}

	/**
	 * Returns the {@code card} used for this {@code Payment}.
	 * @return the {@code card}
	 */
	public final String getCard() {
		return card;
	}

	/**
	 * Sets the {@code card} used for this {@code Payment}.
	 * @param card the {@code card}.
	 */
	public final void setCard(final String card) {
		this.card = card;
	}

	/**
	 * Returns the {@code amount} paid for this {@code Payment}.
	 * @return the {@code amount}
	 */
	public final float getAmount() {
		return amount;
	}
	/**
	 * Sets the {@code amount} paid for this {@code Payment}.
	 * @param amount the {@code amount}.
	 */
	public final void setAmount(final float amount) {
		this.amount = amount;
	}
}
