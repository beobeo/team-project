package cafe.dao;

import java.util.List;

import cafe.Employee;

/**
 * A data access object used to manipulate persisted {@link Employee} objects.
 * 
 * @author Michael Winter
 */
public interface EmployeeDAO {
    /**
     * Given an identifier, this method loads and returns a new instance of the
     * specified {@link Employee}.
     * 
     * @param id
     *            the identifier.
     * @return the specified {@code Sitting}.
     * 
     */
    Employee findEmployee(int id);
    
    /**
     * Gets all the waiters from the database.
     * @return
     * 			A list containing the first name of all waiters.
     */
    List<String> getAllWaiterNames();
}
