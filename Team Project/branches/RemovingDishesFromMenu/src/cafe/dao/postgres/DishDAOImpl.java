package cafe.dao.postgres;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import cafe.Dish;
import cafe.dao.DataAccessException;
import cafe.dao.DishDAO;
import cafe.dao.IngredientDAO;
import cafe.dao.jdbc.ConnectionFactory;

/**
 * A data access object used to manipulate {@link Dish} objects that have
 * persisted to a PostgreSQL database.
 * 
 * @author Michael Winter
 * @author Hannah Cooper
 * @author Robert Kardjaliev
 */
public final class DishDAOImpl implements DishDAO {
	/**
	 * The source of JDBC connections used to perform queries against the
	 * underlying database.
	 */
	private ConnectionFactory connectionFactory;
	/**
	 * The data access object used to obtain {@link Ingredient} instances from the
	 * database.
	 */
	private IngredientDAO ingredientDAO;


	/**
	 * Constructs a data access object for {@link Dish}es that will operate over
	 * the given connection.
	 * 
	 * @param ingredientDAO
	 *            the data access object used to supply {@link Ingredient}
	 *            instances.
	 * @param connectionFactory
	 *            the factory object that will provide a database connection.
	 * @throws NullPointerException
	 *             if the specified factory is {@code null}.
	 */
	public DishDAOImpl(final IngredientDAO ingredientDAO,
			final ConnectionFactory connectionFactory) {
		if (ingredientDAO == null) {
			throw new NullPointerException("ingredientDAO is null");
		}
		if (connectionFactory == null) {
			throw new NullPointerException("connectionFactory is null");
		}
		this.ingredientDAO = ingredientDAO;
		this.connectionFactory = connectionFactory;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.DishDAO#selectDishesByCourse(java.lang.String)
	 */
	@Override
	public List<Dish> selectDishesByCourse(final String course) {
		final Connection connection = connectionFactory.getConnection();
		final List<Dish> dishes = new ArrayList<>();
		try (final PreparedStatement statement = connection.prepareStatement(
				"SELECT id, name, description, price, calories FROM dish WHERE category = ?")) {
			statement.setString(1, course);
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				final InputStream inputStream = result.getBinaryStream("image");
				BufferedImage image = null;
				if (inputStream != null) {
					image = ImageIO.read(inputStream);
				}
				final int id = result.getInt("id");
				dishes.add(new Dish(
						id,
						result.getString("name"),
						result.getString("description"),
						result.getFloat("price"),
						result.getInt("calories"),
						course,
						image,
						result.getBoolean("visible"),
						ingredientDAO.selectDishIngredients(id),
						DietaryInformationDAOImpl.selectDishInformation(id,
								connection)));
			}

		} catch (final SQLException e) {
			throw new DataAccessException(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dishes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.DishDAO#findDish(int)
	 */
	@Override
	public Dish findDish(final int id) {
		final Connection connection = connectionFactory.getConnection();
		Dish dish = null;
		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT dish.name AS name, description, calories, price,"
						+ " dish_category.name AS category, image, visible"
						+ " FROM dish"
						+ " INNER JOIN dish_category ON dish.category_id = dish_category.id"
						+ " WHERE dish.id = ?")) {
			statement.setInt(1, id);
			ResultSet result = statement.executeQuery();
			if (result.next()) {
				final InputStream inputStream = result.getBinaryStream("image");
				BufferedImage image = null;
				if (inputStream != null) {
					image = ImageIO.read(inputStream);
				}
				dish = new Dish(
						id,
						result.getString("name"),
						result.getString("description"),
						result.getFloat("price"),
						result.getInt("calories"),
						result.getString("category"),
						image,
						result.getBoolean("visible"),
						ingredientDAO.selectDishIngredients(id),
						DietaryInformationDAOImpl.selectDishInformation(id,
								connection));
			}

		}
		catch (final SQLException e) {
			throw new DataAccessException(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dish;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.DishDAO#selectAvailableDishes()
	 */
	@Override
	public List<Dish> selectAvailableDishes() {
		final Connection connection = connectionFactory.getConnection();
		final List<Dish> dishes = new LinkedList<>();
		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT DISTINCT dish.id AS id, dish.name AS name, description,"
						+ " dish.price AS price, calories, image, dish_category.name AS category"
						+ " FROM dish"
						+ " INNER JOIN dish_ingredient ON dish.id = dish_id"
						+ " INNER JOIN ingredient ON ingredient.id = ingredient_id"
						+ " INNER JOIN dish_category ON dish.category_id = dish_category.id"
						+ " WHERE visible = true AND stock >= quantity"
						+ " ORDER BY name")) {
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				final int id = result.getInt("id");
				final InputStream inputStream = result.getBinaryStream("image");
				BufferedImage image = null;
				if (inputStream != null) {
					image = ImageIO.read(inputStream);
				}
				dishes.add(new Dish(
						id,
						result.getString("name"),
						result.getString("description"),
						result.getFloat("price"),
						result.getInt("calories"),
						result.getString("category"),
						image,
						true,
						ingredientDAO.selectDishIngredients(id),
						DietaryInformationDAOImpl.selectDishInformation(id,
								connection)));
			}

		} catch (final SQLException e) {
			throw new DataAccessException(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dishes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.DishDAO#getAllDishNames()
	 */
	@Override
	public List<String> getAllDishNames() {
		final Connection connection = connectionFactory.getConnection();
		final List<String> names = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT name FROM dish")) {			
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				names.add(result.getString("name"));
			}			
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}
		return names;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.DishDAO#setVisible(String, boolean)
	 */
	@Override
	public void setVisible(final String name, final boolean visible) {
		final Connection connection = connectionFactory.getConnection();
		try (PreparedStatement statement = connection.prepareStatement(
				"UPDATE dish set visible = ? where name = ?")) {
			statement.setBoolean(1, visible);
			statement.setString(2, name);
			statement.executeUpdate();		
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.DishDAO#getVisible(String)
	 */
	@Override
	public boolean getVisible(final String name) {
		boolean visible = false;
		final Connection connection = connectionFactory.getConnection();
		try (PreparedStatement statement = connection.prepareStatement(
				"select visible from dish set where name = ?")) {
			statement.setString(1, name);
			ResultSet result = statement.executeQuery();
			if (result.next()) {
				visible = result.getBoolean("visible");
			}			
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}
		return visible;
	}
	
}
