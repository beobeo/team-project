package cafe.kitchen;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

/**
 * Presents a graphical window to kitchen staff from which they can view orders,
 * and update their progress.
 * 
 * @author Robert Kardjaliev
 * @author Michael Winter
 */
public final class KitchenView extends JFrame {
	/**
	 * Unique serialisation identifier.
	 */
	private static final long serialVersionUID = 206562399161115992L;
	/**
	 * Specifies how listed orders are arranged in this UI.
	 */
	private static final GridBagConstraints ITEM_PANEL_CONSTRAINTS = new GridBagConstraints();
	/**
	 * Provides a container within the scroll pane into which displayed orders
	 * are rendered.
	 */
	private JPanel container;

	static {
		ITEM_PANEL_CONSTRAINTS.anchor = GridBagConstraints.NORTHWEST;
		ITEM_PANEL_CONSTRAINTS.fill = GridBagConstraints.HORIZONTAL;
		// CHECKSTYLE IGNORE MagicNumberCheck FOR NEXT 1 LINES
		ITEM_PANEL_CONSTRAINTS.insets = new Insets(0, 0, 10, 0);
		ITEM_PANEL_CONSTRAINTS.gridx = 0;
		ITEM_PANEL_CONSTRAINTS.gridy = GridBagConstraints.RELATIVE;
	}

	/**
	 * Create the frame.
	 */
	public KitchenView() {
		initGUI();
	}

	/**
	 * Adds the given {@code OrderListItemView} to the current list of order
	 * panels.
	 * 
	 * @param listView
	 *            the view to add.
	 */
	public void addListItem(final OrderListItemView listView) {
		container.add(listView, ITEM_PANEL_CONSTRAINTS);
		container.revalidate();
		container.repaint();
	}

	/**
	 * Removes the given {@code OrderListItemView} from the current list of
	 * order panels.
	 * 
	 * @param listView
	 *            the view to remove.
	 */
	public void removeListItem(final OrderListItemView listView) {
		container.remove(listView);
		container.revalidate();
		container.repaint();
	}

	/**
	 * Initialises UI components.
	 */
	// CHECKSTYLE:OFF
	private void initGUI() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 416, 463);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		final JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new EmptyBorder(1, 0, 1, 0));
		scrollPane
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		contentPane.add(scrollPane);

		container = new JPanel();
		scrollPane.setViewportView(container);
		GridBagLayout containerGridBagLayout = new GridBagLayout();
		containerGridBagLayout.columnWidths = new int[] { 380 };
		containerGridBagLayout.rowHeights = new int[] { 0, 0, 0 };
		containerGridBagLayout.columnWeights = new double[] { 0.0 };
		containerGridBagLayout.rowWeights = new double[] { 0.0, 0.0,
				Double.MIN_VALUE };
		container.setLayout(containerGridBagLayout);
	}
	// CHECKSTYLE:ON
}
