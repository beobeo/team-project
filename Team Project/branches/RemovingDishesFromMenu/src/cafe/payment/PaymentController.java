package cafe.payment;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A controller for the payment GUI. It observes user behaviour and directs behaviour
 * accordingly.
 * @author Robert Kardjaliev
 *
 */
public class PaymentController {
	/** The view. */
	private PaymentView view;
	/** The price of the {@link Order}. */
	private float paymentPrice = 0.0f;
	
	/**
	 * Hides the view associated with this controller.
	 */
	public final void hideView() {
		view.setVisible(false);
	}

	/**
	 * Shows the view associated with this controller.
	 */
	public final void showView() {
		view.setVisible(true);
	}



	/**
	 * Instantiates the fields of the Track Order Controller.
	 *
	 * @param view - The View.
	 */
	public PaymentController(final PaymentView view) { 
		this.view = view;
		addPayWithCardListener(new PayWithCardListener());
		addPayWithCashListener(new PayWithCashListener());
	}
	/**
	 * Sets the price of an order to the payment view.
	 */
	public final void setPrice() {
		view.setPrice(getPrice());
	}

	/**
	 * Adds a listener to the Pay with card button.
	 * @param listener
	 * 					the pay with card button listener.
	 */
	public final void addPayWithCardListener(final ActionListener listener) {
		view.addPayWithCardListener(listener);
	}
	
	/**
	 * Adds a listener to the Pay with cash button.
	 * @param listener
	 * 					the pay with cash button listener.
	 */
	public final void addPayWithCashListener(final ActionListener listener) {
		view.addPayWithCashListener(listener);
	}

	/**
	 * A listener which, when the pay with card button is pressed, sets the appropriate
	 * message to the label in the view.
	 */
	private class PayWithCardListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {			
			view.setMessage("You paid with card");
		}
	}
	
	/**
	 * A listener which, when the pay with cash button is pressed, sets the appropriate
	 * message to the label in the view.
	 */
	private class PayWithCashListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {			
			view.setMessage("You paid with cash");
		}
	}

	/**
	 * Sets the price of an {@link Order} for the Payment GUI.
	 * @param price
	 * 			the {@link Order}'s price.
	 */
	public final void setPaymentPrice(final float price) {
		paymentPrice = price;
	}
	
	/**
	 * Gets the price of an order.
	 * @return
	 * 		the price of an order.
	 */
	public final float getPrice() {
		return paymentPrice;
	}
}
