/**
 * 
 */
package cafe.trackorder;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import cafe.payment.PaymentController;
import cafe.payment.PaymentView;


/**
 * Observes user events from the Track Order View and directs behaviour accordingly, 
 * invoking the model where appropriate and referring events to higher-level observing
 * controllers.
 * 
 * @author Robert Kardjaliev
 * @author Jonathan Hercock
 * @author Hannah Cooper
 */



public class TrackOrderController {
	/**
	 * Hides the view associated with this controller.
	 */
	public final void hideView() {
		view.setVisible(false);
	}

	/**
	 * Shows the view associated with this controller.
	 */
	public final void showView() {
		view.setVisible(true);
	}

	/** The model. */
	private TrackOrderModel model;
	/** The view. */
	private TrackOrderView view;
	
	/** A payment controller. */
	private PaymentController paymentController;
	/** A payment view. */
	private PaymentView paymentView;

	/**
	 * Instantiates the fields of the Track Order Controller.
	 *
	 * @param view - The View.
	 * @param model - The Model.
	 */
	public TrackOrderController(final TrackOrderView view, final TrackOrderModel model) { 
		this.view = view; 
		this.model = model;
		paymentView = new PaymentView();
		setPaymentController(new PaymentController(paymentView));
		setProgressValue();
		refreshProgress();
	}

	/**
	 * Adds a listener to the Edit Order button.
	 * @param listener - Edit Order Listener
	 */
	public final void addEditOrderListener(final ActionListener listener) {
		view.addEditOrderListener(listener);
	}
	/**
	 * Adds a listener to the Cancel Order button.
	 * @param listener - cancel order listener
	 */
	public final void addCancelOrderListener(final ActionListener listener) {
		view.addCancelOrderListener(listener);
	}
	
	/**
	 * Adds a listener to the Cancel Order button.
	 * @param listener - cancel order listener
	 */
	public final void addPaymentButtonListener(final ActionListener listener) {
		view.addPaymentButtonListener(listener);
	}
	
	/**
	 * Adds an action listener to the Call for Assistance button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public final void addWaiterCallListener(final ActionListener listener) {
		view.addWaiterCallListener(listener);
	}

	/**
	 * Sets the progress bar percentage in the view.
	 */
	public final void setProgressValue() {
		int progress = model.getOrderProgress();
		String progressLabel = model.getOrderState().toString();
		view.setProgressValue(progress);
		view.setProgressLabel(progressLabel);
	}
	
	/**
	 * Listens for Assign button clicks. When the Assign button is pressed after 
	 * selecting a table number and waiter name from the drop boxes, it will 
	 * assign a waiter to a table in the database and display in the label 
	 * which waiter was assigned to which table.
	 */
	private class PaymentButtonListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			hideView();
			getPaymentController().showView();
		}
	}
	/**
	 * Refreshes the progress of the {@link Order}.
	 */
	public final void refreshProgress() {
		Runnable refresh = new Runnable() {
			public void run() {
				setProgressValue();
				// CHECKSTYLE IGNORE MagicNumberCheck FOR NEXT 11 LINES
				if (model.getOrderProgress() > 40) {
					view.cancelOrderEnabled(false);
				} else {
					view.cancelOrderEnabled(true);
				}
				if (model.getOrderProgress() > 60) {
					view.editOrderEnabled(false);
				} else {
					view.editOrderEnabled(true);
				}
				if (model.getOrderProgress() > 80) {
					view.paymentButtonEnabled(true);
					addPaymentButtonListener(new PaymentButtonListener());
				}
			}
		};
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		executor.scheduleAtFixedRate(refresh, 0, 1, TimeUnit.SECONDS);
	
    }
	
	/**
	 * Gets the payment controller.
	 * @return
	 * 			the payment controller.
	 */
	public final PaymentController getPaymentController() {
		return paymentController;
	}

	/**
	 * Sets the payment controller.
	 * @param paymentController
	 * 			the payment controller.
	 */
	public final void setPaymentController(final PaymentController paymentController) {
		this.paymentController = paymentController;
	}
    

}


