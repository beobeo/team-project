package cafe;

/**
 * Represents the current progress of an {@link Order} from placement to
 * delivery or cancellation.
 * 
 * @author Michael Winter
 */
public enum OrderState implements Comparable<OrderState> {
    /**
     * A newly-added order that has not yet been placed by a customer.
     * Such an order can still be cancelled.
     */
    NEW {
        @Override
        public OrderState next() {
            return PENDING;
        }
    },
    /**
     * A newly-placed order that has not yet been accepted into the system.
     * Such an order can still be cancelled.
     */
    PENDING {
        @Override
        public OrderState next() {
            return CONFIRMED;
        }
    },
    /**
     * An order that has been confirmed by a waiter but has not yet begun
     * preparation by the kitchen. Such an order can still be cancelled.
     */
    CONFIRMED {
        @Override
        public OrderState next() {
            return PREPARING;
        }
    },
    /**
     * An order that is currently being prepared by the kitchen.
     */
    PREPARING {
        @Override
        public OrderState next() {
            return READY;
        }
    },
    /**
     * An order that has been completed and is ready for delivery.
     */
    READY {
        @Override
        public OrderState next() {
            return DELIVERED;
        }
    },
    /**
     * An order that has been delivered to the relevant table.
     */
    DELIVERED {
        @Override
        public boolean isFinal() {
            return true;
        }
    },
    /**
     * An order that has been cancelled and will not be charged.
     */
    CANCELLED {
        @Override
        public boolean isFinal() {
            return true;
        }
    };
    
    /**
     * Returns the next state that would apply to the progression of a typical
     * order.
     * 
     * @return the next typical order state.
     * @see #isFinal()
     */
    public OrderState next() {
        throw new IllegalStateException();
    }
    
    /**
     * Returns whether this state represents an end-point in the progression of
     * an order.
     * 
     * @return {@code true} if there are no further state transitions;
     *         {@code false} otherwise.
     */
    public boolean isFinal() {
        return false;
    }
}
