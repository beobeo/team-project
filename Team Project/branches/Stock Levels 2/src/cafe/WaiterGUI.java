package cafe;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import cafe.dao.OrderDAO;
import cafe.kitchen.OrderListItemView;

/**
 * 
 * @author Jonathan Hercock
 * @author Adam Lumber
 */
public class WaiterGUI extends JFrame {
    
    /**
     * The container for the Waiter View.
     */
    private JPanel contentPane;
    /**
     *  The container for the Orders.
     */
    private JPanel callContainer;
    
    /** Order being marked as delivered. */
    private Order order;
    
    /**used to update the database.   */
    private OrderDAO data;
    /** Example of a Waiter, used to feed a name and ID to the GUI. */
    public static final Waiter TEST_WAITER = new Waiter(1234321, "John",
            "Smith", true, null);

    /**
     *  Automated ID.
     */
    private static final long serialVersionUID = 1L;


    /**
     * 
     * Launch the application.
     * 
     * @param args
     *            Arguments.
     */
    public static void main(final String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    WaiterGUI frame = new WaiterGUI();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Specifies how listed orders are arranged in this UI.
     */
    private static final GridBagConstraints CONSTRAINTS = new GridBagConstraints();
    
    //CHECKSTYLE:OFF 
    static {
        CONSTRAINTS.anchor = GridBagConstraints.NORTHWEST;
        CONSTRAINTS.fill = GridBagConstraints.HORIZONTAL;
        CONSTRAINTS.insets = new Insets(0, 0, 10, 0);
        CONSTRAINTS.gridx = 0;
        CONSTRAINTS.gridy = GridBagConstraints.RELATIVE;
    }
  //CHECKSTYLE:ON
    
    /**
     * Adds the given {@code OrderListItemView} to the current list of order panels.
     * 
     * @param listView
     *            the view to add.
     */
    public final void addListItem(final OrderListItemView listView) {
        callContainer.add(listView, CONSTRAINTS);
        callContainer.revalidate();
        callContainer.repaint();
    }

    
    /**
     * Removes the given {@code OrderListItemView} from the current list of order panels.
     * 
     * @param listView
     *            the view to remove.
     */
    public final void removeListItem(final OrderListItemView listView) {
        callContainer.remove(listView);
        callContainer.revalidate();
        callContainer.repaint();
    }

    /**
     * Create the frame.
     */
    //CHECKSTYLE:OFF
    @SuppressWarnings({ "unchecked", "rawtypes", "serial" })
    public WaiterGUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 710, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JLabel lblWaiter = new JLabel("WAITER");
        lblWaiter.setFont(new Font("Tahoma", Font.BOLD, 16));
        lblWaiter.setBounds(20, 11, 78, 31);
        contentPane.add(lblWaiter);
        
        JLabel lblWaiterName = new JLabel("Logged in as:");
        lblWaiterName.setFont(new Font("Tahoma", Font.ITALIC, 11));
        lblWaiterName.setBounds(154, 21, 78, 14);
        contentPane.add(lblWaiterName);
        
        JLabel label = new JLabel();
        label.setForeground(Color.BLUE);
        label.setText(TEST_WAITER.getName());
        label.setBounds(230, 21, 78, 14);
        contentPane.add(label);
        
        JButton btnLogOut = new JButton("Log Out");
        btnLogOut.setBounds(595, 12, 89, 23);
        btnLogOut.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0)
            {
                
                System.exit(0);
            }
        });
       
        contentPane.add(btnLogOut);
        
        JLabel lblId = new JLabel("ID:");
        lblId.setFont(new Font("Tahoma", Font.ITALIC, 11));
        lblId.setBounds(318, 21, 26, 14);
        contentPane.add(lblId);
        
        JLabel label_1 = new JLabel();
        label_1.setForeground(Color.BLUE);
        label_1.setText(String.valueOf(TEST_WAITER.getId()));
        label_1.setBounds(345, 21, 78, 14);
        contentPane.add(label_1);
        
        final JSplitPane splitPane = new JSplitPane();
        splitPane.setBounds(40, 63, 286, 196);
        splitPane.setVisible(false);
        contentPane.add(splitPane);
        
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        splitPane.setLeftComponent(scrollPane);
        
    
        
        final JList list = new JList();
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setFont(new Font("Tahoma", Font.BOLD, 13));
        list.setVisibleRowCount(4);
        list.setValueIsAdjusting(true);
       list.setModel(new DefaultListModel() {
            String[] values = new String[] {"14:00:42  - Table 3", "14:01:30  - Table 4",
                    "14:01:58  - Table 2", "14:04:00  - Table 1", "14:10:05  - Table 6",
                    "14:11:53  - Table 8", "14:14:09  - Table 7", "14:17:34  - Table 3",
                   "14:23:09  - Table 2", "14:30:51  - Table 4", "14:30:59  - Table 1",
                    "14:33:01  - Table 1", "14:44:09  - Table 8", "14:49:37  - Table 3"};
            public int getSize() {
                return values.length;
            }
            public Object getElementAt(int index) {
                return values[index];
            }
        });
       list.setSelectedIndex(0);
       
        
        scrollPane.setViewportView(list);
        
        JPanel panel = new JPanel();
        splitPane.setRightComponent(panel);
        
        JButton btnCallList = new JButton("Call List");
        btnCallList.setBounds(39, 270, 89, 23);
        contentPane.add(btnCallList);
        
        JLabel lblAvailability = new JLabel("Available:");
        lblAvailability.setFont(new Font("Tahoma", Font.ITALIC, 11));
        lblAvailability.setBounds(410, 21, 78, 14);
        contentPane.add(lblAvailability);
        
        JLabel label_2 = new JLabel();
        if (TEST_WAITER.getAvailable() == false)
        {
            label_2.setText("No");  
        }
        if (TEST_WAITER.getAvailable() != false)
        {
            label_2.setText("Yes");  
        }
        label_2.setForeground(Color.BLUE);
        label_2.setBounds(467, 21, 78, 14);
        contentPane.add(label_2);
        
        callContainer = new JPanel();
        callContainer.setBounds(352, 63, 310, 254);
        contentPane.add(callContainer);
        
        btnCallList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                if (callContainer.isVisible() == true) {callContainer.setVisible(false);}
                else if (callContainer.isVisible() == false) {callContainer.setVisible(true);}
                    
            }
        });
        
      //create button for 'order delivered.
      		final JButton btnOrderDelivered = new JButton("Ordered Delivered");
      		btnOrderDelivered.setBounds(39, 310, 150, 23);
      		contentPane.add(btnOrderDelivered);
      		
      		// create action Listener for btnOrderDelivered
      		btnOrderDelivered.addActionListener(new ActionListener() {
      			@Override
      			public void actionPerformed(final ActionEvent arg0) {
      				// TODO Auto-generated method stub
      				
      				if (order.getProgress() == OrderState.READY) {
      					//progress order.
      					order.advanceProgress();  					
      					//update database 
      					data.updateOrder(order);
      				}	
      			}
      		});
    
    }

}

  //CHECKSTYLE:ON

