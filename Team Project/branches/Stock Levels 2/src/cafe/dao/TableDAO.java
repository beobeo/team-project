package cafe.dao;

import java.util.List;
import cafe.Table;

/**
 * A data access object used to manipulate persisted {@link Table} objects.
 * 
 * @author Michael Winter
 */
public interface TableDAO {
    /**
     * Given an identifier, this method loads and returns a new instance of the
     * specified {@link Table}.
     * 
     * @param id
     *            the identifier.
     * @return the specified {@code Sitting}.
     */
    Table findTable(final int id);
    
    /**
     * Creates a new {@link Table} and returns its ID. 
     * @return the specified {@link Table}'s ID.
     */
    int createTable();
    
    /**
     * Removes the last {@link Table} from the database.  			
     * @return 
     * 			returns the ID of the removed {@link Table}.
     */
	int removeTable();

	/**
	 * Gets a list of all {@link Table}s on the database.
	 * @return
	 * 		the list of tables.
	 */
	List<Table> getAllTables();
}
