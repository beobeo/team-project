package cafe.manager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import cafe.dao.TableDAO;

/**
 * Observes user events from the manager view(s) and directs behaviour accordingly.
 * @author Robert Kardjaliev
 *
 */
public class ManagerController {
	/**
	 * The controller for the Waiters for Tables GUI.
	 */
	private WaitersForTablesController tablesController;
	/**
	 * The model for the Waiters for Tables GUI.
	 */
	private WaitersForTablesModel tablesModel;
	/**
	 * The view for the Waiters for Tables GUI.
	 */
	private WaitersForTablesView tablesView;

	/**
	 * The View of the Manager GUI.
	 */
	private ManagerView view;
	
	/**
	 * Hides the view associated with this controller.
	 */
	public final void hideView() {
		view.setVisible(false);
	}

	/**
	 * Shows the view associated with this controller.
	 */
	public final void showView() {
		view.setVisible(true);
	}
	
	/**
	 * Observes the button to change to Stock Level View.
	 * @param listener
	 * 				the listener.
	 */
    public final void addStockLevelListener(final ActionListener listener) {
        view.addStockLevelListener(listener);        
    }
    /**
     * Observes the button to change to Waiters For Tables View.
     * @param listener
     * 				the listener
     */
    public final void addWaitersForTablesListener(final ActionListener listener) {
    	view.addWaitersForTablesListener(listener);
    }
	/**
	 * Instantiates the fields of the Manager Controller.
	 * @param view
	 *            The View
	 * @param tableData 
	 */

	public ManagerController(final ManagerView view, final TableDAO tableData) {
		this.view = view;
		tablesView = new WaitersForTablesView();
		tablesModel = new WaitersForTablesModel(tableData);
		tablesController = new WaitersForTablesController(tablesView, tablesModel);
		addWaitersForTablesListener(new ShowWaitersForTablesListener());
	}

    
    /**
     * Listens for button click events originating from the Waiters For Tables button.
     *  It hides the Manager View and displays the Waiter for Tables's view.
     */
	private class ShowWaitersForTablesListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			hideView();
			tablesController.showView();
		}
	}
}
