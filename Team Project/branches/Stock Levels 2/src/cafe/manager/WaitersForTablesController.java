package cafe.manager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Observes user events from the WaitersForTablesView and directs behaviour accordingly,
 * invoking the model where appropriate and referring events to higher-level
 * observing controllers.
 * @author Robert Kardjaliev
 *
 */
public class WaitersForTablesController {
	/**
	 * The view for the Waiters for Tables GUI.
	 */
	private WaitersForTablesView view;
	/**
	 * The model for the Waiters for Tables GUI.
	 */
	private WaitersForTablesModel model;
	
	
	/**
	 * Hides the view associated with this controller.
	 */
	public final void hideView() {
		view.setVisible(false);
	}

	/**
	 * Shows the view associated with this controller.
	 */
	public final void showView() {
		view.setVisible(true);
	}
	/**
	 * Observes the button to change back to Manager View.
	 * @param listener
	 * 				the listener.
	 */
	
    public final void addBackButtonListener(final ActionListener listener) {
        view.addBackButtonListener(listener);        
    }
    /**
     * Observes the button to assign to Waiters to Tables.
     * @param listener
     * 				the listener
     */
    public final void addAssignButtonListener(final ActionListener listener) {
    	view.addAssignButtonListener(listener);
    }
    
    /**
	 * Observes the Add table button to add more tables to the database.
	 * @param listener
	 * 				the listener.
	 */
	
    public final void addTableListener(final ActionListener listener) {
        view.addTableListener(listener);
    }
    
        /**
    	 * Observes the Remove Table button remove tables from the database.
    	 * @param listener
    	 * 				the listener.
    	 */
    	
        public final void addRemoveTableListener(final ActionListener listener) {
            view.addRemoveTableListener(listener);
    }
    
	/**
	 * Initializes the fiels for the Waiters for Tables controller.
	 * @param view 
	 * 			The View.
	 * @param model
	 * 			The Model.
	 */
	public WaitersForTablesController(final WaitersForTablesView view, 
			final WaitersForTablesModel model) {
		this.view = view;
		this.model = model;
		addTableListener(new AddTableListener());
		addRemoveTableListener(new RemoveTableListener());
	}
	/**
	 * When the add table button is clicked it automatically
	 * creates a new table.
	 */
	private class AddTableListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			model.createTable();
			view.setText("Table " + model.getTableId() + " created.");
		}
	}
	/**
	 * When the delete table button is clicked it automatically
	 * deletes the last {@link Table}(ordered by id) in the database.
	 */
	private class RemoveTableListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			model.removeTable();
			view.setText("Table " + model.getTableId() + " removed.");
		}
	}

}
