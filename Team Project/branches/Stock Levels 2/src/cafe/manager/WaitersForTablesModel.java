package cafe.manager;

import cafe.dao.TableDAO;

/**
 * Obtains a list of tables and a list of waiters
 * from the database.
 * 
 * @author Robert Kardjaliev
 */
public final class WaitersForTablesModel {
	/** A table data access object{@link TableDAO}. */
	private TableDAO tableData;
	/** A {@link Table}'s ID. */
	private int tableId;
	
	/**
	 * Initializes the field for the Waiters For Tables Model.
	 * @param tableData
	 * 				The Table data access object needed
	 * to allow modification of {@link Table}s in the database.
	 */
	public WaitersForTablesModel(final TableDAO tableData) {
		this.tableData = tableData;
	}
	
	
	
	/**
	 * Creates a new table in the database.
	 */
	public void createTable() {
		setTableId(tableData.createTable());
	}
	/**
	 * Removes the last table from the database.
	 */
	public void removeTable() {
		setTableId(tableData.removeTable());
		
	}
	/**
	 * Gets the ID of the removed {@link Table}.
	 * @return
	 * 		the table.
	 */
	public int getTableId() {
		return tableId;
	}
	/**
	 * Sets the ID of the removed {@link Table}.
	 * @param tableId
	 * 			the table's ID.
	 */
	public void setTableId(final int tableId) {
		this.tableId = tableId;
	}
	
}
