package cafe.manager;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
/**
 * Presents a graphical window to the manager(s) from which he can choose
 * to add/remove tables or assign waiters to tables.
 * @author Robert Kardjaliev
 *
 */
public class WaitersForTablesView extends JFrame {

	/** Unique serialisation identifier. */
	private static final long serialVersionUID = 8830889410240081933L;
	 /**  The Main GUI Panel.    */
    private JPanel contentPane;
    /**  The button that allows to return to the Manager View.  */
    private JButton btnBack;
    /**  The button that allows the Waiters for Tables View to be reached.  */
    private JButton btnAssign;
    /**  The button that allows new tables to be created.  */
	private JButton btnAddTable;
	/**  The button that allows tables to be removed.  */
    private JButton btnRemoveTable;
    /** 
     * A label that will add interactivty to the view, so that
     * a manager can see his button clicks have an effect.
     */
	private JLabel actionLabel;

    /**
     * Create the frame.
     */
    ///CHECKSTYLE:OFF
    public WaitersForTablesView() {
    	setTitle("Waiters For Tables");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 468, 327);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        btnAssign = new JButton();
        btnAssign.setFont(new Font("Tahoma", Font.BOLD, 13));
        btnAssign.setText("Assign");
        btnAssign.setBounds(182, 60, 80, 25);
        contentPane.add(btnAssign);
        
        btnBack = new JButton();
        btnBack.setFont(new Font("Tahoma", Font.BOLD, 10));
        btnBack.setText("Back");
        btnBack.setBounds(10, 11, 63, 21);
        contentPane.add(btnBack);
        
        btnAddTable = new JButton();
        btnAddTable.setText("Add Table");
        btnAddTable.setFont(new Font("Tahoma", Font.PLAIN, 12));
        btnAddTable.setBounds(58, 228, 105, 25);
        contentPane.add(btnAddTable);
        
        btnRemoveTable = new JButton();
        btnRemoveTable.setText("Remove Table");
        btnRemoveTable.setFont(new Font("Tahoma", Font.PLAIN, 12));
        btnRemoveTable.setBounds(292, 228, 112, 25);
        contentPane.add(btnRemoveTable);
        
        actionLabel = new JLabel("");
        actionLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
        actionLabel.setBounds(168, 253, 124, 25);
        contentPane.add(actionLabel);
        
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 56, 140, 144);
        contentPane.add(scrollPane);
        
        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setBounds(285, 56, 140, 144);
        contentPane.add(scrollPane_1);
        
    }
    //CHECKSTYLE:ON
    
    /**
     * Adds a listener to change back to the Manager View.
     * @param listener 
     * 				the listener for the Back button.
     */
    public final void addBackButtonListener(final ActionListener listener) {
        btnBack.addActionListener(listener);
    }
    
    /**
     * Adds a listener to the Assign button.
     * @param listener 
     * 				listener for the Assign button. 
     */
    
    public final void addAssignButtonListener(final ActionListener listener) {
        btnAssign.addActionListener(listener);
    }
    
    /**
     * Adds a listener to the Add Table button.
     * @param listener 
     * 				listener for the Add Table button. 
     */
    
    public final void addTableListener(final ActionListener listener) {
        btnAddTable.addActionListener(listener);
    }
    /**
     * Adds a listener to the Remove Table button.
     * @param listener 
     * 				listener for the Remove Table button. 
     */
    
    public final void addRemoveTableListener(final ActionListener listener) {
        btnRemoveTable.addActionListener(listener);
    }
    
    /**
     * A method that will set a different message depending on which button is clicked.
     * @param msg
     * 			the message.
     */
    public final void setText(final String msg) {
    	actionLabel.setText(msg);
    }
}