package cafe.dao.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import cafe.dao.AuthenticationException;
import cafe.dao.DAOFactory;
import cafe.dao.DataAccessException;

/**
 * Provides a skeleton for concrete data access object factory implementations.
 * 
 * @author Michael Winter
 */
public abstract class AbstractDAOFactoryImpl extends DAOFactory implements AutoCloseable,
        ConnectionFactory {
    /**
     * The database connection used by DAOs created by this object.
     */
    private Connection connection;
    /**
     * The user name used to authenticate the connection.
     */
    private String user;
    /**
     * The password used to authenticate the connection.
     */
    private String password;


    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.JdbcConnectionFactory#getConnection()
     */
    @Override
    public final Connection getConnection() {
        // If there's already a connection, validate it
        if (connection != null) {
            validateConnection();
        }
        // If validation failed, recreate and configure a new connection
        if (connection == null) {
            establishConnection();
            configureConnection();
        }
        return connection;
    }

    /**
     * Closes this resource, relinquishing any underlying resources. This method
     * is invoked automatically on objects managed by the try-with-resources
     * statement.
     * 
     * @see java.lang.AutoCloseable#close()
     */
    @Override
    public final void close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (final SQLException e) {
                // Should not raise an exception: close() should always succeed.
                throw new Error("Unanticipated SQL exception", e);
            }
        }
    }

    
    /**
     * Configures a newly-created connection.
     */
    protected void configureConnection() {
        try {
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
        } catch (final SQLException e) {
            // This exception should be impossible: the connection will be
            // newly-established (therefore valid) and the DBMS supports
            // serialisable transactions.
            throw new Error("Unanticipated SQL exception", e);
        }
    }

    /**
     * Returns a connection string that specifies how and to which database this
     * factory should connect.
     * 
     * @return the connection string.
     */
    protected abstract String getConnectionUri();
    
    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#setCredentials(String, String)
     */
    @Override
    protected void setCredentials(final String usr, final String pass) {
        if ((usr == null) || (pass == null)) {
            throw new AuthenticationException(
                    "a user name and password is required");
        }
        user = usr;
        password = pass;
        // Initialise a connection to check the credentials
        connection = getConnection();
    }


    /**
     * Creates a new connection to the database using the connection string
     * returned by {@link #getConnectionUri()} and the current credentials.
     * 
     * @throws AuthenticationException
     *             if authentication is required and the user name is unknown or
     *             {@code null}, or the password is incorrect or {@code null}.
     * @throws ConnectionException
     *             if a connection error occurs whilst attempting to
     *             authenticate the connection.
     * @throws DataAccessException
     *             if an error occurs while accessing the data store.
     */
    private void establishConnection() {
        try {
            connection = DriverManager.getConnection(getConnectionUri(), user,
                    password);
        } catch (final SQLException e) {
            switch (e.getSQLState()) {
            case "08001": // Client connection failure
                throw new ConnectionException(
                        "Unable to connect to the database", e);
            case "28000": // Invalid user name
                throw new AuthenticationException("Invalid user name", e);
            case "28P01": // Invalid password
                throw new AuthenticationException("Invalid password", e);
            default:
                // Re-raise the exception
                throw new DataAccessException(e);
            }
        }
    }

    /**
     * Validates the current connection, determining whether communication over
     * it is still possible. If not, the connection is closed.
     */
    private void validateConnection() {
        try {
            if (connection.isClosed() || !connection.isValid(1)) {
                connection.close();
                connection = null;
            }
        } catch (final SQLException e) {
            // None of isClosed(), isValid(int) and close() should raise an
            // exception: VALIDATION_TIMEOUT is an acceptable value, and
            // isClosed() and close() should always succeed.
            throw new Error("Unanticipated SQL exception", e);
        }
    }
}
