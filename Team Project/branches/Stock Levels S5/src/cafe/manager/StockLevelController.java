package cafe.manager;

/**
 * Observes user events from the StockLevelsView and directs behaviour accordingly,
 * invoking the model where appropriate and referring events to higher-level
 * observing controllers.
 * @author Robert Kardjaliev
 *
 */
public class StockLevelController {
	
	/**
	 * An instance of the stock level view.
	 */
	private StockLevelView view;
	/**
	 * An instance of the stock level model.
	 */
	private StockLevelModel model;

	
	/**
	 * Hides the view associated with this controller.
	 */
	public final void hideView() {
		view.setVisible(false);
	}

	/**
	 * Shows the view associated with this controller.
	 */
	public final void showView() {
		view.setVisible(true);
	}
	
	/**
	 * The constructor for the model.
	 * @param view
	 * 			The view associated with this controller.
	 * @param model
	 * 			the model associated with this controller.
	 */
	public StockLevelController(final StockLevelView view, final StockLevelModel model) {
		this.view = view;
		this.model = model;
	}
}
