package cafe.manager;

import cafe.dao.IngredientDAO;
/**
 * Obtains the list of all {@link Ingredient}s on the database and separates them
 * into a list of {@code names} and a list of {@code amount}.
 * @author Robert Kardjaliev
 *
 */
public class StockLevelModel {
	/**
	 * A database access object to modify persisted {@link Ingredient}
	 * objects.
	 */
	private IngredientDAO ingredientData;

	/**
	 * Initializes the fields for the Stock Level Model.
	 * @param ingredientData
	 * 				the {@link Ingredient} database access object.
	 */
	public StockLevelModel(final IngredientDAO ingredientData) {
		this.ingredientData = ingredientData;
	}

}
