package cafe.manager;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;

/**
 * Presents a graphical window to the manager(s) from which he can view Stock Levels
 * and modify them.
 * @author Robert Kardjaliev
 *
 */
public class StockLevelView extends JFrame {

	/** Unique serialisation identifier. */
	private static final long serialVersionUID = 8830889410240081933L;
	 /**  The Main GUI Panel.    */
    private JPanel contentPane;
    /**  The button that allows to return to the Manager View.  */
    private JButton btnBack;
    /**  The button that edits the current stock selected.  */
    private JButton btnModify;
    /** 
     * A label that will add interactivty to the view, so that
     * a manager can see his button clicks have an effect.
     */
	private JLabel actionLabel;
	
	/**
	 * Button for opening current stock levels view.
	 */
	private JButton btnViewStock;

	/**
	 * A text field for inputing the desired amount of an {@link Ingredient}.
	 */
	private JTextField stockAmountField;
	/**
	 * A combo box holding all the {@link Ingredient}s' names.
	 */
	private JComboBox<String> stockNameComboBox;

    /**
     * Create the frame.
     */
    ///CHECKSTYLE:OFF
    public StockLevelView() {
    	setTitle("Stock Levels");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 468, 327);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        btnModify = new JButton();
        btnModify.setFont(new Font("Tahoma", Font.BOLD, 13));
        btnModify.setText("Modify");
        btnModify.setBounds(180, 116, 80, 25);
        contentPane.add(btnModify);
        
        btnBack = new JButton();
        btnBack.setFont(new Font("Tahoma", Font.BOLD, 10));
        btnBack.setText("Back");
        btnBack.setBounds(10, 11, 63, 21);
        contentPane.add(btnBack);
        
        actionLabel = new JLabel("");
        actionLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
        actionLabel.setBounds(93, 243, 270, 25);
        contentPane.add(actionLabel);
        
        btnViewStock = new JButton();
        btnViewStock.setText("View Stock Levels");
        btnViewStock.setFont(new Font("Dialog", Font.BOLD, 10));
        btnViewStock.setBounds(303, 6, 149, 30);
        contentPane.add(btnViewStock);
        
        
        stockAmountField = new JTextField();
        stockAmountField.setBounds(270, 119, 86, 20);
        contentPane.add(stockAmountField);
        stockAmountField.setColumns(10);
        
        stockNameComboBox = new JComboBox<String>();
        stockNameComboBox.setBounds(126, 119, 28, 20);
        contentPane.add(stockNameComboBox);
        
    }
    //CHECKSTYLE:ON
    
    /**
     * Adds a listener to change back to the Manager View.
     * @param listener 
     * 				the listener for the Back button.
     */
    public final void addBackButtonListener(final ActionListener listener) {
        btnBack.addActionListener(listener);
    }
    
    /**
     * Adds a listener to the Assign button.
     * @param listener 
     * 				listener for the Assign button. 
     */
    
    public final void addModifyListener(final ActionListener listener) {
        btnModify.addActionListener(listener);
    }
    
    /**
     * Adds a listener to the Remove Table button.
     * @param listener 
     * 				listener for the Remove Table button. 
     */
    
    public final void addViewStockListener(final ActionListener listener) {
        btnViewStock.addActionListener(listener);
    }
    
    /**
     * A method that will set a different message depending on which button is clicked.
     * @param msg
     * 			the message.
     */
    public final void setLabel(final String msg) {
    	actionLabel.setText(msg);
    }
}