/**
 * 
 */
package cafe.waiter;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import java.awt.Font;
import java.awt.Scrollbar;

/** For reference: OLT = OrderListItem.
 * @author Jonathan Hercock
 *
 */
public final class WaiterOLIView extends JPanel {
    
     /** Unique serial ID. */
    private static final long serialVersionUID = -6888470920532585120L;
    /**
     * Displays the table number and waiter for this order.
     */
    private JLabel labelDestination;
    /**
     * Allows a waiter to view detailed information about the order.
     */
    private JButton buttonViewDetails;
    /**
     * Shows the progress of the order graphically.
     */
    private JProgressBar progressBar;
    /**
     * Updates the order to the next state (PENDING > CONFIRMED &
     *                                      READY > DELIVERED).
     */
    private JButton buttonAdvance;
    /**
     * Used to remove the current order from the list of orders.
     */
    private JButton btnCancelOrder;
    private Scrollbar scrollbar;

    /**
     * Create the panel.
     */
    public WaiterOLIView() {
        initGUI();
    }

    /**
     * Sets the text used to display the order number and the waiter responsible
     * for it.
     * 
     * @param text
     *            the string to display.
     */
    public void setDestination(final String text) {
        labelDestination.setText(text);
    }

    /**
     * Sets the current value for the progress bar based on the progression
     * state of the order represented by this view.
     * 
     * @param progress
     *            the progress value.
     */
    public void setProgress(final int progress) {
        progressBar.setValue(progress);
    }

    /**
     * Enables (and disables) the Advance progress button.
     * 
     * @param enabled
     *            if {@code true} the Advance button will be enabled; otherwise
     *            it will be disabled.
     */
    public void enableAdvanceButton(final boolean enabled) {
        buttonAdvance.setEnabled(enabled);
    }
    
    /**
     * Enables (and disables) the Cancel Order button.
     * 
     * @param enabled
     *            if {@code true} the Advance button will be enabled; otherwise
     *            it will be disabled.
     */
    public void enableCancelButton(final boolean enabled) {
        btnCancelOrder.setEnabled(enabled);
    }

    /**
     * Adds an action listener to the View Details button.
     * 
     * @param listener
     *            the event listener.
     */
    public void addViewDetailsButtonActionListener(final ActionListener listener) {
        buttonViewDetails.addActionListener(listener);
    }

    /**
     * Adds an action listener to the Advance button.
     * 
     * @param listener
     *            the event listener.
     */
    public void addAdvanceButtonActionListener(final ActionListener listener) {
        buttonAdvance.addActionListener(listener);
    }
    
    /**
     * Adds an action listener to the Cancel Order button.
     * 
     * @param listener
     *            the event listener.
     */
    public void addCancelButtonActionListener(final ActionListener listener) {
        btnCancelOrder.addActionListener(listener);
    }

    /**
     * Initialise the UI components.
     */
    // CHECKSTYLE:OFF
    private void initGUI() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 0 };
        gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
        gridBagLayout.columnWeights = new double[] { 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
        gridBagLayout.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
        setLayout(gridBagLayout);

        labelDestination = new JLabel("Table: # (Waiter)");
        GridBagConstraints labelDestinationGridBagConstraints = new GridBagConstraints();
        labelDestinationGridBagConstraints.insets = new Insets(0, 0, 5, 5);
        labelDestinationGridBagConstraints.gridx = 0;
        labelDestinationGridBagConstraints.gridy = 0;
        add(labelDestination, labelDestinationGridBagConstraints);
        
                buttonViewDetails = new JButton("View details...");
                GridBagConstraints buttonViewDetailsGridBagConstraints = new GridBagConstraints();
                buttonViewDetailsGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
                buttonViewDetailsGridBagConstraints.insets = new Insets(0, 0, 5, 5);
                buttonViewDetailsGridBagConstraints.gridx = 2;
                buttonViewDetailsGridBagConstraints.gridy = 0;
                add(buttonViewDetails, buttonViewDetailsGridBagConstraints);
        
        scrollbar = new Scrollbar();
        GridBagConstraints gbc_scrollbar = new GridBagConstraints();
        gbc_scrollbar.gridheight = 2;
        gbc_scrollbar.insets = new Insets(0, 0, 5, 0);
        gbc_scrollbar.gridx = 3;
        gbc_scrollbar.gridy = 0;
        add(scrollbar, gbc_scrollbar);
        
        btnCancelOrder = new JButton("Cancel Order");
        btnCancelOrder.setFont(new Font("Tahoma", Font.PLAIN, 9));
        GridBagConstraints gbc_btnDeleteOrder = new GridBagConstraints();
        gbc_btnDeleteOrder.insets = new Insets(0, 0, 0, 5);
        gbc_btnDeleteOrder.gridx = 1;
        gbc_btnDeleteOrder.gridy = 1;
        add(btnCancelOrder, gbc_btnDeleteOrder);

        progressBar = new JProgressBar();
        progressBar.setOpaque(true);
        progressBar.setMaximum(4);
        progressBar.setString("");
        progressBar.setStringPainted(true);
        GridBagConstraints progressBarGridBagConstraints = new GridBagConstraints();
        progressBarGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        progressBarGridBagConstraints.insets = new Insets(0, 0, 0, 5);
        progressBarGridBagConstraints.gridx = 0;
        progressBarGridBagConstraints.gridy = 1;
        add(progressBar, progressBarGridBagConstraints);
        
                buttonAdvance = new JButton("Advance");
                GridBagConstraints buttonAdvanceGridBagConstraints = new GridBagConstraints();
                buttonAdvanceGridBagConstraints.insets = new Insets(0, 0, 0, 5);
                buttonAdvanceGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
                buttonAdvanceGridBagConstraints.gridx = 2;
                buttonAdvanceGridBagConstraints.gridy = 1;
                add(buttonAdvance, buttonAdvanceGridBagConstraints);
    }
    // CHECKSTYLE:ON
}


