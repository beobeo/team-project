package cafe;

import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

/**
 * 
 * @author Hannah Cooper
 * Observes user events from the Edit Stock interface and directs behaviour accordingly
 */
public class EditStockController {
	 /**
	  * the view.
	  */
private EditStockGUI view;

	/**
	 * 
	 * @param view - EditStockGUI
	 */
	 public EditStockController(final EditStockGUI view) {
	       this.view = view;
	 }

	/**
	 * Edit stock interface visible.
	 */
	public final void showView() {
        view.setVisible(true);
    }
    /**
     * Edit stock interface not visible.
     */
    public final void hideView() {
    	view.setVisible(false);
    }
    
    public final void addEditStockListener(final ActionListener listener) {
    	view.addEditStockListener(listener);
    }
    
}
