package cafe;
import cafe.dao.postgres.*;
import cafe.dao.*;
import cafe.dao.jdbc.*;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;


/**
 * 
 * @author Hannah Cooper
 *
 */
//CHECKSTYLE:OFF



public class EditStockGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton button;
	private static JTextField textField;

	//private int ingredientListVisible = 0;
	
    //private ArrayList<Order> ingredience;
    
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EditStockGUI frame = new EditStockGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public EditStockGUI() {
				//list();
		        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		        setBounds(100, 100, 710, 400);
		        contentPane = new JPanel();
		        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		        setContentPane(contentPane);
		        contentPane.setLayout(null);
		        
		        JLabel lblEdit = new JLabel("EDIT STOCK");
		        lblEdit.setFont(new Font("Tahoma", Font.BOLD, 16));
		        lblEdit.setBounds(20, 11, 300, 31);
		        contentPane.add(lblEdit);
		        
		        final JSplitPane splitPane = new JSplitPane();
		        splitPane.setBounds(40, 63, 448, 196);
		        splitPane.setVisible(true);
		        contentPane.add(splitPane);
		        
		        JScrollPane scrollPane = new JScrollPane();
		        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		        splitPane.setLeftComponent(scrollPane);
		        
		        final JPanel panel = new JPanel();
			    splitPane.setRightComponent(panel);
			    panel.setLayout(null);
			    
			    final JLabel currentStockLevel = new JLabel("Current Stock Level:");
			    currentStockLevel.setFont(new Font("Tahoma", Font.BOLD, 14));
			    currentStockLevel.setBounds(0, 50, 200, 50);
			
			    //contentPane.add(currentStockLevel);
			    
			    final JLabel level = new JLabel();
			    level.setFont(new Font("Tahoma", Font.BOLD, 14));
			    level.setBounds(150, 50, 200, 50);

			    
			    final JLabel newStockLevel = new JLabel("New Stock Level:");
			    newStockLevel.setFont(new Font("Tahoma", Font.BOLD, 14));
			    newStockLevel.setBounds(0, 100, 150, 20);

			    
			    setTextField(new JTextField());
			    getTextField().setFont(new Font("Tahoma", Font.BOLD, 14));
			    getTextField().setBounds(120, 100, 100, 20);

			    
			    button = new JButton("Update");
			    button.setFont(new Font("Tahoma", Font.BOLD, 14));
			    button.setBounds(200, 130, 130, 20);

			    
		        final JList list = new JList();
		        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		        list.setFont(new Font("Tahoma", Font.BOLD, 13));
		        list.setVisibleRowCount(4);
		        list.setValueIsAdjusting(true);
		        list.setModel(new DefaultListModel() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;
					String[] values = new String[]{ "Pasta", "Cheese",
		                    "Tomatos", "Fries", "Bread sticks",
		                    "Raspberries", "Potatoes", "Lettuce",
		                   "Cod", "Salt", "Pepper"};
		            public int getSize() {
		                return values.length;
		            }
		            public Object getElementAt(int index) {
		                return values[index];
		            }
		            
		        });
		        
		        MouseListener mouseListener = new MouseAdapter(){
		        	public void mouseClicked(MouseEvent e) {
		        
			    	   if (e.getClickCount() == 1) {
			    		   panel.removeAll();
			               String selectedItem = (String) list.getSelectedValue();
			               JLabel selection = new JLabel(selectedItem);
				 	       selection.setFont(new Font("Tahoma", Font.BOLD, 16));
				 	       selection.setBounds(120, 0, 500, 20);
				 	       panel.add(button);
				 	       panel.add(getTextField());
				 	       panel.add(newStockLevel);
				 	       panel.add(level);
			               panel.add(selection);
			               panel.add(currentStockLevel);
			               //panel.revalidate();
			               panel.repaint();
			               
			    	   }
		        	}
		        };
			 
			   list.addMouseListener(mouseListener);     
		       list.setSelectedIndex(0);   		       
		       scrollPane.setViewportView(list);

	 }
	
    private ConnectionFactory connectionFactory;
	
	//public void IngredientDAOImpl(final ConnectionFactory connectionFactory) {
	//  this.connectionFactory = connectionFactory;
	//}
	
	public void list(){
		
		//*****Getting null pointer exception on ingredientDAO connectionFactory
		ArrayList<Ingredient> list = new ArrayList<>();

        //final Connection connection = connectionFactory.getConnection();
		IngredientDAOImpl in = new IngredientDAOImpl(connectionFactory);
		list = in.selectIngredients();
		System.out.println(list);
	}
	
	public final void addEditStockListener(final ActionListener listener) {
		button.addActionListener(listener);
	}
	
	public final void addTextFieldKeyListener(final KeyListener key){
		getTextField().addKeyListener(key);
	}


	public static JTextField getTextField() {
		return textField;
	}


	public void setTextField(JTextField textField) {
		EditStockGUI.textField = textField;
	}

}
//CHECKSTYLE:ON


