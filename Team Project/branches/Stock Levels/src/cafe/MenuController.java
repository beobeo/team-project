package cafe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Observes user events from the menu view and directs behaviour accordingly, invoking the model
 * where appropriate and referring events to higher-level observing controllers.
 * 
 * @author Michael Winter
 * @author Robert Kardjaliev
 */
public class MenuController {
	/**
	 * A list of event listeners that observe order change events caused by the Add to Order
	 * buttons.
	 */
	private List<ChangeListener> orderChangeListeners = new ArrayList<>();
	/**
	 * The current page of dishes shown in the view. Each page contains up to
	 * {@code MenuGUI.getDishesPerPage()} dishes.
	 */
	private int currentPage = 1;
	/**
	 * The menu model.
	 */
	private MenuModel model;
	/**
	 * The menu interface.
	 */
	private MenuGUI view;
	/**
	 * Associates action listeners used to add to orders with the respective dishes. This map uses
	 * weak references so that old action listeners can be garbage collected.
	 */
	private Map<ActionListener, Dish> monitoredDishes = new WeakHashMap<>();
	
	/**
	 * Constructs a controller for the given model and view.
	 * 
	 * @param model the model.
	 * @param view the view.
	 */
	public MenuController(final MenuModel model, final MenuGUI view) {
		this.view = view;
		this.model = model;
		
		final ActionListener paginationListener = new PaginationListener();
		view.addNextPageListener(paginationListener);
		view.addPreviousPageListener(paginationListener);
	    
		updateDishList();
		
		if (getPageCount() > 1) {
			view.enableNextPageButton(true);
		}
	}
	
	/**
	 * Hides the view associated with this controller.
	 */
	public final void hideView() {
		view.setVisible(false);
	}
	
	/**
	 * Shows the view associated with this controller.
	 */
	public final void showView() {
		view.setVisible(true);
	}
	
    /**
     * Adds a change listener to observe the order state. This event is
     * triggered when the client adds a dish to the current order.
     * 
     * @param listener
     *            the change listener to be added.
     */
	public final void addDishAddedListener(final ChangeListener listener) {
		orderChangeListeners.add(listener);
	}
	
	/**
	 * Adds an action listener to the Order button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public final void addShowOrderListener(final ActionListener listener) {
		view.addShowOrderListener(listener);
	}
	
	/**
	 * Adds an action listener to the Call for Assistance button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public final void addWaiterCallListener(final ActionListener listener) {
		view.addWaiterCallListener(listener);
	}
	
    /**
     * Calculates how many pages are necessary to display the dishes available
     * from the model given the available space in the view.
     * 
     * @return the pages necessary to display available dishes.
     */
	private int getPageCount() {
		return (int) Math.ceil((float) model.getDishes().size() 
				/ (float) MenuGUI.getDishesPerPage());
	}
	
    /**
     * Causes the view to switch to the next page of menu items, disabling the
     * Next Page button once the end of the list has been reached.
     */
	private void showNextPage() {
		++currentPage;
		view.enablePreviousPageButton(true);
		
		if (currentPage == getPageCount()) {
			view.enableNextPageButton(false);
		}

		updateDishList();
	}
	
    /**
     * Causes the view to switch to the previous page of menu items, disabling
     * the Previous Page button once the start of the list has been reached.
     */
	private void showPreviousPage() {
		--currentPage;
		view.enableNextPageButton(true);
		
		if (currentPage == 1) {
			view.enablePreviousPageButton(false);
		}

		updateDishList();
	}
	
    /**
     * Notifies all observers that a dish has been added to the current order.
     * The event object has, as its source, the dish that was added.
     * 
     * @param event
     *            the event.
     */
	private void notifyDishAddedListeners(final ChangeEvent event) {
		for (final ChangeListener listener : orderChangeListeners) {
			listener.stateChanged(event);
		}
	}
	
    /**
     * Based on the current page, this selects the dishes that should be
     * displayed in the view, adds appropriate action listeners to observe Add
     * to Order action events, and sends the list to the view.
     */
	private void updateDishList() {
		final List<Dish> dishes = model.getDishes();
		final int start = (currentPage - 1) * MenuGUI.getDishesPerPage();
		final int end = Math.min(currentPage * MenuGUI.getDishesPerPage(), dishes.size());

		final List<DishMenuView> viewableDishes = new ArrayList<>(MenuGUI.getDishesPerPage());
		for (final Dish dish : dishes.subList(start, end)) {
			final DishMenuView dishView = new DishMenuView(dish);
			final ActionListener orderListener = new OrderListener();
			monitoredDishes.put(orderListener, dish);
			dishView.addOrderListener(orderListener);
			viewableDishes.add(dishView);
		}
		view.setDishes(viewableDishes);
	}
	
	
    /**
     * Listens for button click events originating from the Add to Order button
     * associated with each dish, adding them to the current order.
     */
	private class OrderListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent actionEvent) {
			final Dish addedDish = monitoredDishes.get(this);
			final ChangeEvent changeEvent = new ChangeEvent(addedDish);
			notifyDishAddedListeners(changeEvent);
		}
	}

	
	/**
	 * Listens for button click events originating from the Next and Previous Page buttons,
	 * changing the displayed orders as appropriate.
	 */
	private class PaginationListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			if (e.getActionCommand().equals("next")) {
				showNextPage();
			} else {
				showPreviousPage();
			}
		}
	}
}
