package cafe.dao;

import java.util.ArrayList;

import cafe.Ingredient;

/**
 * A data access object used to manipulate persisted {@link Ingredient} objects.
 * 
 * @author Michael Winter
 * @author Hannah Cooper
 */
public interface IngredientDAO {
    /**
     * Given an identifier, this method loads and returns a new instance of the
     * specified {@link Ingredient}.
     * 
     * @param id
     *            the identifier.
     * @return the specified {@code Ingredient}.
     * @throws DataAccessException
     *             if an error occurs while accessing the data store.
     */
    Ingredient findIngredient(int id);
    /**
     * 
     * @param ingredient - the ingredient you want to update
     * @return specified ingredient 
     * @throws DataAccessException
     * 				if an error occurs while accessing the data store.
     * 
     */
    Ingredient updateIngredient(Ingredient ingredient);
    /**
     * 
     * @return all ingredients in the database
     */
    ArrayList<Ingredient> selectIngredients();
}
