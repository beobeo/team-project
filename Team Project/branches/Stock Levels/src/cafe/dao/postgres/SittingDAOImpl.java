/**
 * 
 */
package cafe.dao.postgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import cafe.Sitting;
import cafe.Table;
import cafe.dao.DataAccessException;
import cafe.dao.SittingDAO;
import cafe.dao.TableDAO;
import cafe.dao.jdbc.ConnectionFactory;

/**
 * A data access object used to manipulate {@link Sitting} objects that have
 * persisted to a PostgreSQL database.
 * 
 * @author Michael Winter
 */
public final class SittingDAOImpl implements SittingDAO {
    /**
     * The source of JDBC connections used to perform queries against the
     * underlying database.
     */
    private ConnectionFactory connectionFactory;
    /**
     * The data access object used to obtain {@link Table} instances from the
     * database.
     */
    private TableDAO tableData;

    
    /**
     * Constructs a data access object for {@link Sitting} objects that will operate over
     * the given connection.
     * 
     * @param tableDAO
     *            the data access object used to supply {@link Table}
     *            instances.
     * @param connectionFactory
     *            the factory object that will provide a database connection.
     * @throws NullPointerException
     *             if the specified factory or data access objects are {@code null}.
     */
	public SittingDAOImpl(final TableDAO tableDAO, final ConnectionFactory connectionFactory) {
        if (tableDAO == null) {
            throw new NullPointerException("tableDAO is null");
        }
        if (connectionFactory == null) {
            throw new NullPointerException("connectionFactory is null");
        }
	    this.tableData = tableDAO;
		this.connectionFactory = connectionFactory;
	}

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.SittingDAO#findSitting(int)
     */
    @Override
    public Sitting findSitting(final int id) {
        final Connection connection = connectionFactory.getConnection();
        Sitting sitting = null;
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT table_id, customer_id, placed, reserved_from, reserved_until"
                + " FROM sitting"
                + " WHERE id = ?")) {
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                // TODO: Complete implementation of method
                sitting = new Sitting(id,
                        tableData.findTable(result.getInt("table_id")));
            }
        } catch (final SQLException e) {
            throw new DataAccessException(e);
        }
        return sitting;
    }

}
