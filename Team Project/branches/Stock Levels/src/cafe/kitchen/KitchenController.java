package cafe.kitchen;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingUtilities;

import cafe.Order;
import cafe.OrderEvent;
import cafe.OrderEventListener;


/**
 * Observes user events from the kitchen view and directs behaviour accordingly,
 * invoking the model where appropriate and referring events to higher-level
 * observing controllers.
 * 
 * @author Robert Kardjaliev
 * @author Michael Winter
 */
public class KitchenController {
    /**
     * The model.
     */
    private KitchenModel model;
    /**
     * The view.
     */
    private KitchenView view;
    /**
     * A mapping between orders and the controller for its corresponding entry
     * in the list.
     */
    private Map<Order, OrderListItemController> controllerMap = new HashMap<>();


    /**
     * Constructs a controller for the given model and view.
     * 
     * @param model
     *            the model.
     * @param view
     *            the view.
     */
    public KitchenController(final KitchenModel model, final KitchenView view) {
        this.model = model;
        this.view = view;

        this.model.addOrderModelListener(new OrderStateListener());
        this.model.refresh();
    }

    
    /**
     * Shows the view associated with this controller.
     */
    public final void showView() {
        view.setVisible(true);
    }
    
    /**
     * KitchenControl visible.
     */
    public final void hideView() {
    	view.setVisible(false);
    }
    
    /**
     * 
     * @param listener - listener for the edit stock button
     */
    public final void addEditStockListener(final ActionListener listener) {
    	view.addEditStockListener(listener);
    }
    
    /**
     * Listens for updates to orders, invoking UI updates as appropriate.
     */
    private class OrderStateListener implements OrderEventListener {
        /*
         * (non-Javadoc)
         * @see cafe.OrderEventListener#orderAdded(cafe.OrderEvent)
         */
        @Override
        public void orderAdded(final OrderEvent e) {
            final Order addedOrder = (Order) e.getSource();
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    final OrderDetailTableModel tableModel = new OrderDetailTableModel(addedOrder);
                    OrderDetailView detailView = new OrderDetailView(tableModel);
                    OrderListItemView listView = new OrderListItemView();

                    OrderListItemController listController = new OrderListItemController(
                            listView, detailView);
                    listController.addAdvanceButtonActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(final ActionEvent e) {
                            addedOrder.advanceProgress();
                            model.updateOrder(addedOrder);
                        }
                    });
                    listController.refresh(addedOrder);
                    controllerMap.put(addedOrder, listController);
                    view.addListItem(listView);
                }
            });
        }

        /*
         * (non-Javadoc)
         * @see cafe.OrderEventListener#orderChanged(cafe.OrderEvent)
         */
        @Override
        public void orderChanged(final OrderEvent e) {
            final Order changedOrder = (Order) e.getSource();
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    controllerMap.get(changedOrder).refresh(changedOrder);
                }
            });
        }

        /*
         * (non-Javadoc)
         * @see cafe.OrderEventListener#orderRemoved(cafe.OrderEvent)
         */
        @Override
        public void orderRemoved(final OrderEvent e) {
            final Order removedOrder = (Order) e.getSource();
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    OrderListItemController controller = controllerMap.remove(removedOrder);
                    view.removeListItem(controller.getListView());
                }
            });
        }
    }

}
