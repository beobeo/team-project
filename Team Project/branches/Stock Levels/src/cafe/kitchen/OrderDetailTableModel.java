package cafe.kitchen;

import javax.swing.table.AbstractTableModel;

import cafe.Order;
import cafe.OrderItem;

/**
 * Provides the mechanism for populating tabular data within the detailed order
 * view used by kitchen staff.
 * 
 * @author Michael Winter
 */
public final class OrderDetailTableModel extends AbstractTableModel {
    /**
     * Unique serialisation identifier.
     */
    private static final long serialVersionUID = -6314730557977100210L;
    /**
     * The order to be queried for ordered dishes.
     */
    private Order order;

    /**
     * Constructs an {@code OrderDetailTableModel} using the specified
     * {@code Order}.
     * 
     * @param order
     *            the order from which dish information will be retrieved.
     */
    public OrderDetailTableModel(final Order order) {
        this.order = order;
    }

    /* (non-Javadoc)
     * @see javax.swing.table.TableModel#getRowCount()
     */
    @Override
    public int getRowCount() {
        return order.getItems().size();
    }

    /* (non-Javadoc)
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    @Override
    public int getColumnCount() {
        return 2;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        final OrderItem item = order.getItems().get(rowIndex);
        return (columnIndex == 0) ? item.getDish().getName()
                : item.getQuantity();
    }

    @Override
    public String getColumnName(final int column) {
        return (column == 0) ? "Dish" : "Quantity";
    }

}
