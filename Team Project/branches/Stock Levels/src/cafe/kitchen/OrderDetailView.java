package cafe.kitchen;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;

/**
 * Displays a detailed list the menu items, and the quantity therefore, ordered by a table.
 * 
 * @author Michael Winter
 */
public final class OrderDetailView extends JDialog {
    /**
     * Unique serialisation identifier.
     */
    private static final long serialVersionUID = 6278515119633230991L;
    /**
     * The table in which the order information will be displayed.
     */
    private JTable itemTable;
    /**
     * The button used to dismiss this dialogue box.
     */
    private JButton okButton;

    /**
     * Create the dialog.
     * 
     * @param tableModel
     *            the {@code TableModel} used to populate the database.
     */
    public OrderDetailView(final OrderDetailTableModel tableModel) {
        initGUI();
        itemTable.setModel(tableModel);
    }
    
    /**
     * Adds the specified {@code ActionListener} to the OK button within this
     * dialogue box.
     * 
     * @param listener
     *            the listener to add.
     */
    public void addOkButtonActionListener(final ActionListener listener) {
        okButton.addActionListener(listener);
    }
    
    //CHECKSTYLE:OFF
    private void initGUI() {
        setTitle("Order Details");
        setResizable(false);
        setModalityType(ModalityType.APPLICATION_MODAL);
        setType(Type.UTILITY);
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        {
            final JScrollPane scrollPane = new JScrollPane();
            scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            scrollPane.setBorder(null);
            getContentPane().add(scrollPane, BorderLayout.CENTER);
            {
                itemTable = new JTable();
                itemTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
                itemTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                itemTable.setShowVerticalLines(false);
                itemTable.setFillsViewportHeight(true);
                scrollPane.setViewportView(itemTable);
            }
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                okButton = new JButton("OK");
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
            }
        }
    }
    //CHECKSTYLE:ON
}
