package cafe;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.junit.Test;

/**
 * Unit tests for the {@link Dish} class.
 * 
 * @author Michael Winter
 */
public final class DishTest {
    /**
     * A visible dish should be available.
     */
    @Test
    public void visibleDishIsAvailable() {
        final Dish dish = new Dish(1, "Test", "This is a test!", 0, 0, "Test",
                null, true, Collections.<Ingredient, Integer> emptyMap(),
                Collections.<DietaryInformation> emptySet());
        assertTrue(dish.isAvailable());
    }
	
    /**
     * If a dish is marked as not visible, it should not be considered
     * available.
     */
    @Test
    public void invisibleDishIsNotAvailable() {
        final Dish dish = new Dish(1, "Test", "This is a test!", 0, 0, "Test",
                null, false, Collections.<Ingredient, Integer> emptyMap(),
                Collections.<DietaryInformation> emptySet());
        assertFalse(dish.isAvailable());
    }
	
    /**
     * If an ingredient is not in stock, it should not be considered available.
     */
    @Test
    public void dishWithInsufficientIngredientsIsNotAvailable() {
        final int stock = 0;
        final int requiredStock = 1;
        final Ingredient ingredient = new Ingredient(1, "Test", 0, stock);
        final Map<Ingredient, Integer> ingredients = Collections.singletonMap(
                ingredient, requiredStock);
        final Dish dish = new Dish(1, "Test", "This is a test!", 0, 0, "Test",
                null, true, ingredients, Collections.<DietaryInformation> emptySet());
        assertFalse(dish.isAvailable());
    }

	/**
	 * All ingredients necessary to prepare a dish must be in stock for for that dish to be
	 * considered available.
	 */
	@Test
	public void dishWithSomeInsufficientIngredientsIsNotAvailable() {
		final Ingredient stockedIngredient = new Ingredient(1, "Stocked", 0, 1);
		final Ingredient unstockedIngredient = new Ingredient(2, "Unstocked", 0, 0);
        final Map<Ingredient, Integer> ingredients = new HashMap<>();
        ingredients.put(stockedIngredient, 1);
        ingredients.put(unstockedIngredient, 1);
        final Dish dish = new Dish(1, "Test", "This is a test!", 0, 0, "Test",
                null, true, ingredients, Collections.<DietaryInformation> emptySet());
		assertFalse(dish.isAvailable());
	}

    /**
     * If a dish is visible and all of its ingredients are in stock, it should be considered
     * available.
     */
    @Test
    public void dishWithSufficientIngredientsIsAvailable() {
        final int stock = 1;
        final int requiredStock = 1;
        final Ingredient ingredient = new Ingredient(1, "Test", 0, stock);
        final Map<Ingredient, Integer> ingredients = Collections.singletonMap(
                ingredient, requiredStock);
        final Dish dish = new Dish(1, "Test", "This is a test!", 0, 0, "Test",
                null, true, ingredients, Collections.<DietaryInformation> emptySet());
        assertTrue(dish.isAvailable());
    }
}
    

