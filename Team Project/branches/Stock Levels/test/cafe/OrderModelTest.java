package cafe;
import static org.junit.Assert.assertEquals;

import java.util.Collections;

import org.junit.Test;


/**
 * Unit tests for the {@link Order} class.
 * 
 * @author Hannah Cooper
 */
public final class OrderModelTest {
	@Test
	public void test() {
		OrderModel orderTest = new OrderModel();
		Dish dish = new Dish(1, "Test 1", "This is a test!", 3.40f, 0, "Test",
                null, true, Collections.<Ingredient, Integer> emptyMap(),
                Collections.<DietaryInformation> emptySet());
		Dish dish1 = new Dish(2, "Test 2", "This is another test!", 2.40f, 0, "Test",
                null, true, Collections.<Ingredient, Integer> emptyMap(),
                Collections.<DietaryInformation> emptySet());
		
		orderTest.addDish(dish);
		orderTest.addDish(dish1);
		
		assertEquals("Test for correct addition of dish prices", 5.80f, orderTest.getTotalPrice(), 0.01f);
	}
}
