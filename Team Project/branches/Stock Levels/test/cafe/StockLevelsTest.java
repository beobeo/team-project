package cafe;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import cafe.dao.IngredientDAO;

/**
 * 
 * @author Hannah Cooper
 *
 */
public class StockLevelsTest {
	
	/**
	 * Initial value of stock.
	 */
	private int initialValue;
	
	/**
	 * After reduce stock has been called.
	 */
	private int expectedValue;
	
	/**
	 * Initialising values.
	 */
	@Before
	public final void setUp() {
	initialValue = 5;
	expectedValue = 3;
	}
	
	/**
	 * 
	 */
	@Test
	public final void reduceStocktest() {
		
		Ingredient ingredient = new Ingredient(0, "ingredient", 0, initialValue);
		ingredient.reduceStock(2);
		assertEquals(ingredient.getStock(), expectedValue);
	}
	
	/**
	 * A mock data source for testing purposes.
	 */
	
	private static class MockIngredientDAO implements IngredientDAO {
	    /**
	     * The unique identifier for the next generated fake object.
	     */
	
		private int nextId = 1;
		/**
		 * The orders that this data source can return.
		 */
		private List<Ingredient> ingredients = new ArrayList<>();
		
        /**
         * Creates a new fake object with a unique identifier, the current time
         * as placement time, and in a confirmed state.
         */
		/*
		public void addIngredient() {
			ingredients.add(new Ingredient(nextId, "ingredient", 0, 5));
			++nextId;
		}
		*/
		@Override
		public Ingredient findIngredient(final int id) {
			throw new UnsupportedOperationException();
		}

		@Override
		public Ingredient updateIngredient(final Ingredient ingredient) {
			throw new UnsupportedOperationException();
		}

		@Override
		public ArrayList<Ingredient> selectIngredients() {
			// TODO Auto-generated method stub
			return null;
		}

	}
}
