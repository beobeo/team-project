package cafe.kitchen;

import static org.junit.Assert.fail;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import cafe.Order;
import cafe.OrderEvent;
import cafe.OrderEventListener;
import cafe.OrderItem;
import cafe.OrderState;
import cafe.dao.OrderDAO;

/**
 * Unit tests for the {@link KitchenModel} class.
 * 
 * @author Michael Winter
 */
public final class KitchenModelTest {
	/**
	 * Used as a wait guard during concurrent tests. Is {@code true} if the test
	 * is regarded as complete (though not necessarily passed); {@code false}
	 * otherwise.
	 */
	private volatile boolean finished = false;
	/**
	 * The monitor object used for synchronisation during concurrent tests.
	 */
	private final Object monitor = new Object();
	
	
	/**
	 * Configures the test fixture.
	 */
	@Before
	public void setUp() {
		finished = false;
	}
	
    /**
     * Asserts that when an order has been added to the data source, this is
     * determined upon refresh by the model.
     * @throws InterruptedException 
     */
	@Test
	public void addedOrderFiresAddedEvent() throws InterruptedException {
		final MockOrderDAO orderData = new MockOrderDAO();
		final KitchenModel model = new KitchenModel(orderData);
		model.addOrderModelListener(new AbstractOrderEventListener() {
            @Override
            public void orderAdded(final OrderEvent e) {
                finished = true;
                synchronized (monitor) {
                    monitor.notifyAll();
                }
            }
		});
		model.setRefreshInterval(0);
		
		orderData.addConfirmedOrder();
		model.refresh();

        synchronized (monitor) {
            final long start = System.currentTimeMillis();
            final long delay = 500;
            while (!finished) {
                monitor.wait(delay);
            }
            if ((System.currentTimeMillis() - start) > delay) {
                fail();
            }
        }
	}
	
    /**
     * Asserts that when a second order has been added to the data source, this is
     * also determinable upon refresh by the model.
     * @throws InterruptedException 
     */
	@Test
	public void secondAddedOrderFiresAddedEvent() throws InterruptedException {
		final MockOrderDAO orderData = new MockOrderDAO();
		final KitchenModel model = new KitchenModel(orderData);
		model.addOrderModelListener(new AbstractOrderEventListener() {
		    private volatile int fired = 0;
		    
            @Override
            public void orderAdded(final OrderEvent e) {
                ++fired;
                if (fired == 2) {
        		    finished = true;
                    synchronized (monitor) {
                        monitor.notifyAll();
                    }
                }
            }
		});
		model.setRefreshInterval(0);

		orderData.addConfirmedOrder();
		model.refresh();
		
		orderData.addConfirmedOrder();
		model.refresh();

        synchronized (monitor) {
            final long start = System.currentTimeMillis();
            final long delay = 500;
            while (!finished) {
                monitor.wait(delay);
            }
            if ((System.currentTimeMillis() - start) > delay) {
                fail();
            }
        }
	}

    /**
     * Checks whether a change in progression state of an order can be
     * discovered by the model.
     * @throws InterruptedException 
     */
	@Test
	public void advancingOrderFiresChangedEvent() throws InterruptedException {
		final MockOrderDAO orderData = new MockOrderDAO();
		final KitchenModel model = new KitchenModel(orderData);
		model.setRefreshInterval(0);

		orderData.addConfirmedOrder();
		orderData.addConfirmedOrder();
		model.refresh();
				
		model.addOrderModelListener(new AbstractOrderEventListener() {
            @Override
            public void orderChanged(final OrderEvent e) {
                finished = true;
                synchronized (monitor) {
                    monitor.notifyAll();
                }
            }
        });
		orderData.advanceOrder(0);
		model.refresh();

        synchronized (monitor) {
            final long start = System.currentTimeMillis();
            final long delay = 500;
            while (!finished) {
                monitor.wait(delay);
            }
            if ((System.currentTimeMillis() - start) > delay) {
                fail();
            }
        }
	}

	
	/**
	 * A mock data source for testing purposes.
	 */
	private static class MockOrderDAO implements OrderDAO {
	    /**
	     * The unique identifier for the next generated fake object.
	     */
		private int nextId = 1;
		/**
		 * The orders that this data source can return.
		 */
		private List<Order> orders = new ArrayList<>();
		
        /**
         * Creates a new fake object with a unique identifier, the current time
         * as placement time, and in a confirmed state.
         */
		public void addConfirmedOrder() {
			orders.add(new Order(nextId, null, OrderState.CONFIRMED, new Timestamp(
					System.currentTimeMillis()), null, null, null, Collections
					.<OrderItem> emptyList()));
			++nextId;
		}
		
        /**
         * Updates the specified order (in terms of its position in the list,
         * rather than its own properties) such that it will take on the next
         * progression state.
         * 
         * @param index
         *            the order to update.
         */
		public void advanceOrder(final int index) {
			orders.get(index).advanceProgress();
		}
		
		@Override
		public Order findOrder(final int id) {
			throw new UnsupportedOperationException();
		}

        @Override
        public List<Order> selectUndeliveredOrders() {
            List<Order> list = new ArrayList<>(orders.size());
            for (Order order : orders) {
                list.add(new Order(order.getId(), order.getSitting(),
                        order.getProgress(), order.getWhenPlaced(),
                        order.getWhenReady(), order.getWhenDelivered(),
                        order.getWaiter(), order.getItems()));
            }
            return list;
        }

		@Override
		public int createOrder(final int sitting) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int createOrder(final int sitting, final int waiter) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void updateOrder(final Order order) {
			throw new UnsupportedOperationException();
		}
	}
	
	
    /**
     * Provides an adapter for implementing OrderEventListeners. The default
     * implementation for each event is to raise a test failure error.
     */
    private abstract class AbstractOrderEventListener implements
            OrderEventListener {
        /*
         * (non-Javadoc)
         * @see cafe.OrderEventListener#orderAdded(cafe.OrderEvent)
         */
        @Override
        public void orderAdded(final OrderEvent e) {
            fail("No order should be regarded as added in this test.");
            finished = true;
        }

        /*
         * (non-Javadoc)
         * @see cafe.OrderEventListener#orderChanged(cafe.OrderEvent)
         */
        @Override
        public void orderChanged(final OrderEvent e) {
            fail("No order should be regarded as changed in this test.");
            finished = true;
        }

        /*
         * (non-Javadoc)
         * @see cafe.OrderEventListener#orderRemoved(cafe.OrderEvent)
         */
        @Override
        public void orderRemoved(final OrderEvent e) {
            fail("No order should be regarded as removed in this test.");
            finished = true;
        }

    }
}
