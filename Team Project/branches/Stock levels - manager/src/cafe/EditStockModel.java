package cafe;

import java.util.ArrayList;

import cafe.dao.IngredientDAO;
/**
 * 
 * @author Hannah Cooper
 * Model for edit stock MVC
 */
public class EditStockModel {
	/**
	 * Ingredient access object.
	 */
	private static IngredientDAO ingredientData;
/**
 * 
 * @param ingredientData - an ingredientDAO
 * creates edit stock model
 */
	public EditStockModel(final IngredientDAO ingredientData) {
		EditStockModel.ingredientData = ingredientData;
	}
	
	/**
	 * Gets a list of ingredients from the database.
	 * @return i - each ingredient in the database
	 */
	public final ArrayList<Ingredient> getIngredientList() {
		ArrayList<Ingredient> ingreds = ingredientData.selectIngredients();
		return ingreds;
	}
	/**
	 * Changes the stock level of an ingredient in the database to the number the user provides.
	 * @param id - id of ingredient.
	 * @param number - number to change stock level to.
	 */
	public static final void changeStockLevel(final int id, final int number) {
		ingredientData.changeStockLevel(id, number);
	}
}
