package cafe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

/**
 * The Class OrderController.
 * 
 * @author Robert Kardjaliev
 * @author Jonathan Hercock
 * @author JuLi
 */
public final class OrderController {
	/** The model. */
	private OrderModel model;
	/** The view. */
	private OrderGUI view;
	/**
	 * A lazily-initialised listener for detecting the addition of menu items to
	 * the current order.
	 */
	private OrderItemEventListener orderItemListener = null;

	/**
	 * A Map to map action listeners to dishes, so removing and adding an item
	 * from/to dish list is possible.
	 */
	//private Map<ActionListener, Dish> displayedItems = new WeakHashMap<>();

	/**
	 * Instantiates the fields of the Order Controller.
	 * 
	 * @param model
	 *            The Model
	 * @param view
	 *            The View
	 */
	public OrderController(final OrderModel model, final OrderGUI view) {
		this.model = model;
		this.view = view;

		this.view.addConfirmOrderListener(new ConfirmOrderListener());
		populateView();
	}

	/**
	 * Hides the view associated with this controller.
	 */
	public void hideView() {
		view.setVisible(false);
	}

	/**
	 * Shows the view associated with this controller.
	 */
	public void showView() {
		view.setVisible(true);
	}

	/**
	 * Populates the GUI.
	 */
	public void populateView() {
		// view.populateGUI(model.getDishes(), model.getTotalPrice() , new
		// AListener());
		view.populateGUI(model.getDishes(), model.getTotalPrice(),
				new AListener());
	}

	/**
	 * Adds an action listener to the Order button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public void addShowMenuListener(final ActionListener listener) {
		view.addShowMenuListener(listener);
	}

	/**
	 * Adds an action listener to the Track Order button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public void addShowTrackOrderViewListener(final ActionListener listener) {
		view.addShowTrackOrderViewListener(listener);
	}

	/**
	 * Adds a waiter Listener to the view.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public void addWaiterCallListener(final ActionListener listener) {
		view.addWaiterCallListener(listener);
	}

	/**
	 * Returns a listener that observes the addition of menu items to an order.
	 * 
	 * @return the listener.
	 */
	public synchronized OrderItemEventListener getOrderItemListener() {
		if (orderItemListener == null) {
			orderItemListener = new DishAddedListener();
		}
		return orderItemListener;
	}

	/**
	 * ActionListener used in DishComponent. will either increase or decrease
	 * quantity
	 */
	private class AListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent arg0) { // remove dish
																// etc...
			//CHECKSTYLE:OFF									// unfinished
			if (arg0.getActionCommand().toString().equals("-")) {
				// model.getDishes().get()... ;
				// model.removeItem();
			} else if (arg0.getActionCommand().toString().equals("+")) {
			}
			//CHECKSTYLE:ON
		}
	}

	/**
	 * Listens for button click events originating from the Confirm Order
	 * button, checking that ordered items are available and, if so, placing the
	 * order.
	 */
	private class ConfirmOrderListener implements ActionListener {
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent
		 * )
		 */
		@Override
		public void actionPerformed(final ActionEvent event) {
			String message = "Thank you for your order. A waiter will be with you soon.";
			String title = "Order placed";
			int messageType = JOptionPane.INFORMATION_MESSAGE;
			try {
				model.placeOrder();
				view.enableTrackOrderButton(true);
				view.enableConfirmOrderButton(false);
			} catch (final DishesUnavailableException e) {
				final StringBuilder messageParts = new StringBuilder(
						"Unfortunately the following item(s) are out of stock"
								+ " and have been removed from your order."
								+ "\nPlease feel free to add additional items and try again.\n");
				title = "Item unavailable";
				messageType = JOptionPane.ERROR_MESSAGE;
				for (final Dish dish : e.getDishes()) {
					messageParts.append("\n");
					messageParts.append(dish.getName());
				}
				message = messageParts.toString();
				populateView();
			}
			JOptionPane.showMessageDialog(view, message, title, messageType);
			
		}
	}

	/**
	 * Observes the addition of order items from the menu to the order being
	 * presented to the user.
	 */
	private class DishAddedListener implements OrderItemEventListener {
		/*
		 * (non-Javadoc)
		 * 
		 * @see cafe.OrderItemEventListener#orderItemAdded(cafe.OrderItemEvent)
		 */
		@Override
		public void orderItemAdded(final OrderItemEvent event) {
			final Dish dish = (Dish) event.getSource();
			try {
				model.addItem(dish);
			} catch (final DishesUnavailableException e) {
				JOptionPane
						.showMessageDialog(
								view,
								"Unfortunately this item is out of stock and cannot be ordered.",
								"Item unavailable", JOptionPane.ERROR_MESSAGE);
			}
			populateView();
		}
	}
}
