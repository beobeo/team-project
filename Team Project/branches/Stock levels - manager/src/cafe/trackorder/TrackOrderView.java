package cafe.trackorder;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JProgressBar;

/**
 * A view for tracking a customer's {@link Order}'s progress.
 * 
 * @author Robert Kardjaliev
 *
 */
public class TrackOrderView extends JFrame {
	/** Unique serialisation identifier. */
	private static final long serialVersionUID = 4566928122223960827L;
	/** The content pane. */
	private JPanel contentPane;
	/** Cancel Order button. */
	private JButton cancelOrder;
	/** Edit Order button.	 */
	private JButton editOrder;
	/** The progress bar. */
	private JProgressBar progressBar;
	/** A button to request waiter's assistance. */
	private JButton requestAssistance;
	/** 
	 * A label that will show the {@link OrderState}
	 * in a String format.
	 */
	private JLabel progressLabel;
	/**
	 * A button for opening the payment GUI.
	 */
	private JButton btnPayment;
	/**
	 * A label that will give an estimate of how much time is remaining.
	 */
	private JLabel lblTime;
	
	/**
	 * Create the frame.
	 */
	public TrackOrderView() {
		//CHECKSTYLE:OFF
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
        contentPane.setOpaque(false);
        contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblMenu = new JLabel("TRACK ORDER");
		lblMenu.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblMenu.setBounds(10, 11, 123, 25);
		contentPane.add(lblMenu);

		cancelOrder = new JButton("Cancel Order");
		cancelOrder.setFont(new Font("Tahoma", Font.BOLD, 12));
		cancelOrder.setBounds(284, 210, 123, 25);
		contentPane.add(cancelOrder);

		editOrder = new JButton("Edit Order");
		editOrder.setFont(new Font("Tahoma", Font.BOLD, 12));
		editOrder.setBounds(284, 174, 123, 25);
		contentPane.add(editOrder);

		progressBar = new JProgressBar();
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		progressBar.setBounds(128, 47, 146, 14);
		contentPane.add(progressBar);

		JLabel lblOrderProgress = new JLabel("Order Progress:");
		lblOrderProgress.setBounds(20, 47, 113, 14);
		contentPane.add(lblOrderProgress);
		
		requestAssistance = new JButton("Call for Assistance");
		requestAssistance.setFont(new Font("Tahoma", Font.BOLD, 12));
		requestAssistance.setBounds(20, 226, 146, 25);
		contentPane.add(requestAssistance);
		
		progressLabel = new JLabel("");
		progressLabel.setBounds(128, 72, 146, 14);
		contentPane.add(progressLabel);
		
		btnPayment = new JButton("Payment");
		btnPayment.setEnabled(false);
		btnPayment.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnPayment.setBounds(284, 14, 123, 23);
		contentPane.add(btnPayment);
		
		JLabel label = new JLabel("Approx. Time Remaining:");
		label.setBounds(20, 97, 156, 14);
		contentPane.add(label);
		
		lblTime = new JLabel("");
		lblTime.setBounds(161, 97, 156, 14);
		contentPane.add(lblTime);
		
		JLabel lblCurrentState = new JLabel("Current State:");
		lblCurrentState.setBounds(20, 72, 101, 14);
		contentPane.add(lblCurrentState);
	}
	//CHECKSTYLE:ON
	/**
	 * 
	 * @param listener - listener for Cancel Order button on Track my order screen
	 */
	public final void addCancelOrderListener(final ActionListener listener) {
		cancelOrder.addActionListener(listener);
	}

	/**
	 * 
	 * @param listener - listener for Edit Order button on track my order screen 
	 */

	public final void addEditOrderListener(final ActionListener listener) {
		editOrder.addActionListener(listener);
	}

	/**
	 * A method to update the progress bar being displayed.
	 * @param value
	 * 			The progress's percentage.
	 */
	public final void setProgressValue(final int value) {
		progressBar.setValue(value);
	}
	
	/**
	 * A method to update the progress label being displayed.
	 * @param value
	 * 			The stage of progress an Order is at.
	 */
	public final void setProgressLabel(final String value) {
		progressLabel.setText(value);
	}
	/**
	 * A method that sets the time remaining until an {@code Order's} completion.
	 * @param value
	 * 			The String message of the time remaining.
	 */
	public final void setTimeLabel(final String value) {
		lblTime.setText(value);
	}
	
	/**
	 * A method to disable the cancelOrder button.
	 * @param enabled
	 * 				the boolean value for whether cancelOrder is enabled.
	 */
	public final void cancelOrderEnabled(final boolean enabled) {
		cancelOrder.setEnabled(enabled);
	}
	/**
	 * A method to disable the editOrder button.
	 * @param enabled
	 * 				the boolean value for whether editOrder is enabled.
	 *
	 */
	public final void editOrderEnabled(final boolean enabled) {
		editOrder.setEnabled(enabled);		
	}
	
	/**
	 * Adds an action listener to the Call for Assistance button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
    public final void addWaiterCallListener(final ActionListener listener) {
    	requestAssistance.addActionListener(listener);
    }
    
	/**
	 * Adds an action listener to the Call for Assistance button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
    public final void addPaymentButtonListener(final ActionListener listener) {
    	btnPayment.addActionListener(listener);
    }
    /**
     * Enables the Payment button.
     * @param b
     * 			the boolean value for enabling the button.
     */
	public final void paymentButtonEnabled(final boolean b) {
		btnPayment.setEnabled(b);		
	}
}


