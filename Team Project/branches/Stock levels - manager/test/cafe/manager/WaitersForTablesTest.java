package cafe.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import cafe.dao.DAOFactory;
import cafe.dao.TableDAO;
import cafe.dao.postgres.DefaultTestDAOFactoryImpl;

/**
 * Unit test for the Waiters for Tables GUI.
 * @author Robert Kardjaliev
 */
public class WaitersForTablesTest {
	/** An instance of the DAO factory. */
    private DAOFactory factory = DAOFactory.getInstance("zyvc215", "IYIP1845",
            DefaultTestDAOFactoryImpl.class);
	/** A table data access object. */
    private TableDAO tableData = factory.getTableDAO();
    /**
     * A {@code Waiter}'s first name.
     */
    private String name = "Harry";
    
    /**A {@code Table} number. */
    private int tableNum;
	@Before
	public final void setUp() {
		tableNum = tableData.createTable();
		tableData.assignWaiterToTable(1, "David");
	}
	
    
	/**
	 * Checks whether the last table in the database(the one created at the setUp()) will get removed.
	 */
	@Test
	public final void checkOrderState() {		
		assertEquals("Test 1: Check for correct removal of a table.", tableData.removeTable(), tableNum);
	}
	
	/**
	 * Checks whether removal of a table will decrease the size of the {@code Tables} list.
	 */
	@Test
	public final void testTableListSize() {
		int tableListSize =  tableData.getAllTables().size();
		tableData.removeTable();
		assertTrue("Test 2: Removal of table decreases the number of total tables in database.",
				tableListSize-1 == tableData.getAllTables().size());
	}
	/**
	 * Checks whether correct assignment of {@code Waiter} to {@code Table} occurs.
	 */
	@Test
	public final void assigningWaiterToTable() {
		tableData.assignWaiterToTable(1, name);
		assertTrue("Test 3: Assignment of waiter to table.",
				tableData.findTable(1).getWaiter().getFirstName().equals(name));
	}
	
}
