package cafe;


/**
 * @author Robert Kardjaliev
 * The Class KitchenController.
 */
public class KitchenController {
	
	/**
	 * Shows the view associated with this controller.
	 */
	public final void showView() {
		view.setVisible(true);
	}

	/** The model. */
	private KitchenModel model;
	/** The view. */
	private KitchenGUI view;


	/**
	 * Instantiates the fields of the Kitchen Controller.
	 * @param model - The Model
	 * @param view - The View
	 */
	public KitchenController(final KitchenModel model, final KitchenGUI view) { 
		this.model = model; 
		this.view = view; 
	}

}
