package cafe;

import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;

import cafe.dao.OrderDAO;

/**
 * Obtains orders from the database.
 * @author Robert Kardjaliev
 */
public class KitchenModel {
	/**
	 * The list of dishes last retrieved from the database.
	 */
	private final SortedSet<Order> orders = new TreeSet<>();
	/**
	 * An orderDish data access object.
	 */
	private OrderDAO orderData;

	/**
	 * Creates the Kitchen model.
	 * @param orderData - the data access object
	 */
	public KitchenModel(final OrderDAO orderData) {
		this.orderData = orderData;
		loadOrders();
	}

	/**
	 * Obtains an ordered list of OrderItem objects. The returned list is immutable.
	 * 
	 * @return an unmodifiable, ordered list of current Orders.
	 */
	public final SortedSet<Order> getOrders() {
		return Collections.unmodifiableSortedSet(orders);
	}

	/**
	 * Loads the dishes with the visible parameter set to true from the
	 * database in order to display them when later called.
	 */
	private void loadOrders() {
	    orders.clear();
	    orders.addAll(orderData.selectUndeliveredOrders());
	}
}