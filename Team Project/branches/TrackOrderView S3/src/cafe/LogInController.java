/**
 * 
 */
package cafe;

import java.awt.event.ActionListener;

/**
 * @author Jonathan Hercock
 * 
 */
public class LogInController {

    /**
     *  The Log In Screen interface.
     */
    private static LogInScreen view;

    /** A constructor for the given view.
     * 
     * @param view - the log in view.
     */
    public LogInController(final LogInScreen view) {
        LogInController.view = view;
    }
    
    /**
     * Shows the view associated with this controller.
     */
    public final void showView() {
       view.setVisible(true);
    }

    /**
     * Hides the view associated with this controller. 
     */
    public final void hideView() {
        LogInController.view.setVisible(false);
    }

    
    /** Adds an Action Listener for Management.
     * 
     * @param listener the listener that is added. 
     */
    public final void addManagementLogInListener(final ActionListener listener) {
        view.addManagementLogInListener(listener);
        
    } 
    
    /**
     * 
     * @param listener - the listener to be added for the Waiting Staff.
     */
    public final void addWaiterLogInListener(final ActionListener listener) {
        view.addWaitingStaffLogInListener(listener);
    } 
    
    /**
     * 
     * @param listener - the listener to be added for the Kitchen Staff.
     */
    public final void addKitchenLogInListener(final ActionListener listener) {
        view.addKitchenStaffLogInListener(listener);
    } 

    
}
