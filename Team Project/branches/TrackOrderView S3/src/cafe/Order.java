package cafe;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Represents an Order object that just shows its number and the table it was
 * ordered from.
 * 
 * @author Robert Kardjaliev
 * @author Michael Winter
 */
public final class Order implements Comparable<Order> {
	/**
	 * An integer that stores the order number.
	 */
	private final int id;
	/**
	 * The seating allocation to which this order relates.
	 */
	private final Sitting sitting;
	/**
	 * The current state of this order.
	 */
	private OrderState state;
    /**
     * The time when this order was placed.
     */
    private final Timestamp whenPlaced;
    /**
     * The time when this order was ready for delivery.
     */
    private Timestamp whenReady;
    /**
     * The time when this order was delivered to its table.
     */
    private Timestamp whenDelivered;
	/**
     * The waiter that confirmed this order (if applicable).
     */
	private Employee takenBy;
	/**
	 * The menu items that constitute this order.
	 */
	private final List<OrderItem> items = new ArrayList<>();
	
	
    /**
     * Constructs a new {@link Order} instance.
     * 
     * @param id
     *            the unique identifier for this instance.
     * @param sitting
     *            the seating allocation to which this order relates.
     * @param state
     *            the current state of the {@code Order}.
     * @param whenPlaced
     *            the time when this order was placed.
     * @param whenReady
     *            the time when this order became ready for delivery or
     *            {@code null} if that state has not yet been reached.
     * @param whenDelivered
     *            the time when this order was delivered to the table or
     *            {@code null} if that state has not yet been reached.
     * @param takenBy
     *            the {@link Waiter} that took this order or {@code null} if the
     *            order has not yet been confirmed.
     * @param items
     *            the {@linkplain OrderItem items} that constitute this order.
     */
    public Order(final int id, final Sitting sitting, final OrderState state,
            final Timestamp whenPlaced, final Timestamp whenReady,
            final Timestamp whenDelivered, final Employee takenBy,
            final List<OrderItem> items) {
        this.id = id;
        this.sitting = sitting;
        this.state = state;
        this.whenPlaced = whenPlaced;
        this.whenReady = whenReady;
        this.whenDelivered = whenDelivered;
        this.takenBy = takenBy;
        this.items.addAll(items);
    }

    /**
     * Returns the unique identifier for this order.
     * 
     * @return the identifier.
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the sitting to which this order is related.
     * 
     * @return the sitting.
     */
    public Sitting getSitting() {
        return sitting;
    }
    
    /**
     * Returns the current progression state of this {@code Order}.
     * 
     * @return the state of this {@code Order}.
     */
    public OrderState getProgress() {
        return state;
    }
    
    /**
     * Returns when this {@code Order} was originally placed.
     * 
     * @return a time stamp denoting when this {@code Order} was placed.
     */
    public Timestamp getWhenPlaced() {
        return whenPlaced;
    }
    
    /**
     * Returns when this {@code Order} became ready for delivery.
     * 
     * @return a time stamp denoting when this {@code Order} was completed by
     *         the kitchen, or {@code null} if it is incomplete.
     */
    public Timestamp getWhenReady() {
        return whenReady;
    }
    
    /**
     * Returns when this {@code Order} was delivered to its table.
     * 
     * @return a time stamp denoting when this {@code Order} was delivered or
     *         {@code null} if it has not yet been delivered.
     */
    public Timestamp getWhenDelivered() {
        return whenDelivered;
    }
    
    /**
     * Returns the waiter that confirmed this {@code Order}. Any suggested items
     * are credited to this employee.
     * 
     * @return the waiter that confirmed this {@code Order} or {@code null} if
     *         it has not yet been confirmed.
     */
    public Employee getWaiter() {
        return takenBy;
    }
    
    /**
     * Returns the set of order items that constitute this order. The set cannot
     * be modified.
     * 
     * @return an unmodifiable set of order items contained in this order.
     */
    public List<OrderItem> getItems() {
        return Collections.unmodifiableList(items);
    }
    
    /**
     * Updates the progress of this {@code Order} to the next appropriate state.
     * 
     * @throws IllegalStateException
     *             if this {@code Order} is in an end state.
     * @see OrderState
     */
    public void advanceProgress() {
        state = state.next();
    }
    
    /**
     * Marks this {@code Order} as cancelled.
     */
    public void cancel() {
        state = OrderState.CANCELLED;
    }
    
    /**
     * Adds an {@linkplain OrderItem item} to this order.
     * 
     * @param item
     *            the item to be added.
     * @return {@code true} if the item was added; {@code false} otherwise.
     */
    public boolean addItem(final OrderItem item) {
        return items.add(item);
    }
    
    /**
     * Removes an {@linkplain OrderItem item} from this order.
     * 
     * @param item
     *            the item to be removed.
     * @return {@code true} if the item was removed; {@code false} otherwise.
     */
    public boolean removeItem(final OrderItem item) {
        return items.remove(item);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !(obj instanceof Order)) {
            return false;
        }
        final Order other = (Order) obj;
        if (id != other.id) {
            return false;
        }
        return true;
    }

    /**
     * Compares this {@code Order} object to the given {@code Order} object.
     * 
     * @param o
     *            the {@code Order} object to be compared to this {@code Order}
     *            object.
     * @return the value {@code 0} if the two {@code Order} objects were placed
     *         at the same time, and they either have the same progression state
     *         or one has been cancelled; a value less than {@code 0} if this
     *         {@code Order} object is in an earlier progression state or was
     *         placed before the given argument; and a value greater than
     *         {@code 0} if this {@code Order} object is in a later progression
     *         state or was placed after the given argument.
     * @see OrderState
     */
    @Override
    public int compareTo(final Order o) {
        return whenPlaced.compareTo(o.whenPlaced);
    }
}
