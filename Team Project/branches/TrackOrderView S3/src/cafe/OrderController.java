package cafe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



/**
 * @author Robert Kardjaliev
 * @author Jonathan Hercock
 * The Class OrderController.
 */
public class OrderController {
	/**
	 * Hides the view associated with this controller.
	 */
	public final void hideView() {
		view.setVisible(false);
	}
	
	/**
	 * Shows the view associated with this controller.
	 */
	public final void showView() {
		view.setVisible(true);
	}

	/** The model. */
	private OrderModel model;
	/** The view. */
	private OrderGUI view;


	/**
	 * Listener for the first remove button.
	 * When pressed it will remove the first panel
	 * from the view.
	 */
	

	class ButtonListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent a) {
			view.hidePanel(1);
		}

	}

	/**
	 * Listener for the second remove button.
	 * When pressed it will remove the second panel
	 * from the view.
	 */
	class ButtonListenerA implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent b) {
			view.hidePanel(2);
		}
	}

	/**
	 * Listener for the third remove button.
	 * When pressed it will remove the third panel
	 * from the view.
	 */
	class ButtonListenerB implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent c) {
			view.hidePanel(3);
		}
	}


	/**
	 * Instantiates the fields of the Order Controller.
	 * @param model - The Model
	 * @param view - The View
	 */
	public OrderController(final OrderModel model, final OrderGUI view) { 
		this.model = model; 
		this.view = view; 
	
		view.addButtonListener(new ButtonListener());
		view.addButtonListenerA(new ButtonListenerA());
		view.addButtonListenerB(new ButtonListenerB());
	
	}

	/**
	 * Adds an action listener to the Order button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public final void addShowMenuListener(final ActionListener listener) {
		view.addShowMenuListener(listener);
	} 
	
	/**
	 * Adds an action listener to the Track Order button.
	 * @param listener
	 *            the action listener to be added.
	 */
	public final void addShowTrackOrderViewListener(final ActionListener listener) {
	    view.addShowTrackOrderViewListener(listener);
	}

	/**
	 * 
	 * @param listener
	 */
    public void addWaiterCallListener(final ActionListener listener) {
        view.addWaiterCallListener(listener);
        
    }
}
