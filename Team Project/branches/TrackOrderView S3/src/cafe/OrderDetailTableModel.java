package cafe;

import javax.swing.table.AbstractTableModel;

/**
 * 
 * @author Michael Winter
 */
public final class OrderDetailTableModel extends AbstractTableModel {

    private Order order;

    /**
     * 
     */
    public OrderDetailTableModel(final Order order) {
        this.order = order;
    }

    /* (non-Javadoc)
     * @see javax.swing.table.TableModel#getRowCount()
     */
    @Override
    public int getRowCount() {
        return order.getItems().size();
    }

    /* (non-Javadoc)
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    @Override
    public int getColumnCount() {
        return 2;
    }

    /* (non-Javadoc)
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        final OrderItem item = order.getItems().get(rowIndex);
        return (columnIndex == 0) ? item.getDish().getName() : item.getQuantity();
    }

    @Override
    public String getColumnName(int column) {
        return (column == 0) ? "Dish" : "Quantity";
    }

}
