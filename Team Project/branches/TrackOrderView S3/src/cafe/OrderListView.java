package cafe;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JProgressBar;
import java.awt.Insets;
import javax.swing.JButton;

/**
 * 
 * @author Michael Winter
 */
public final class OrderListView extends JPanel {
    /**
     * Unique serialisation identifier.
     */
    private static final long serialVersionUID = 1853827954396435733L;
    
    /**
     * Displays the table number and waiter for this order.
     */
    private JLabel labelDestination;
    /**
     * Allows a kitchen staff member to view detailed information about the order.
     */
    private JButton buttonViewDetails;
    /**
     * Shows the progress of the order graphically.
     */
    private JProgressBar progressBar;
    /**
     * Updates the order to the next state (CONFIRMED > PREPARING > READY).
     */
    private JButton buttonAdvance;
    /**
     * The order summarised by this view.
     */
    private final Order order;
    
    
    /**
     * Create the panel.
     * 
     * @param order
     *            the order to summarise.
     */
    public OrderListView(final Order order) {
        initGUI();

        this.order = order;
        
        buttonAdvance = new JButton("Advance");
        GridBagConstraints buttonAdvanceGridBagConstraints = new GridBagConstraints();
        buttonAdvanceGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        buttonAdvanceGridBagConstraints.gridx = 1;
        buttonAdvanceGridBagConstraints.gridy = 1;
        add(buttonAdvance, buttonAdvanceGridBagConstraints);
        
    }
    
    
    /**
     * Initialise the UI components.
     */
    //CHECKSTYLE:OFF
    private void initGUI() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0};
        gridBagLayout.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
        setLayout(gridBagLayout);
        
        labelDestination = new JLabel("Table: # (Waiter)");
        GridBagConstraints labelDestinationGridBagConstraints = new GridBagConstraints();
        labelDestinationGridBagConstraints.insets = new Insets(0, 0, 5, 5);
        labelDestinationGridBagConstraints.gridx = 0;
        labelDestinationGridBagConstraints.gridy = 0;
        add(labelDestination, labelDestinationGridBagConstraints);
        
        buttonViewDetails = new JButton("View details...");
        GridBagConstraints buttonViewDetailsGridBagConstraints = new GridBagConstraints();
        buttonViewDetailsGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        buttonViewDetailsGridBagConstraints.insets = new Insets(0, 0, 5, 0);
        buttonViewDetailsGridBagConstraints.gridx = 1;
        buttonViewDetailsGridBagConstraints.gridy = 0;
        add(buttonViewDetails, buttonViewDetailsGridBagConstraints);
        
        progressBar = new JProgressBar();
        progressBar.setOpaque(true);
        progressBar.setMaximum(2);
        progressBar.setString("");
        progressBar.setStringPainted(true);
        GridBagConstraints progressBarGridBagConstraints = new GridBagConstraints();
        progressBarGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        progressBarGridBagConstraints.insets = new Insets(0, 0, 0, 5);
        progressBarGridBagConstraints.gridx = 0;
        progressBarGridBagConstraints.gridy = 1;
        add(progressBar, progressBarGridBagConstraints);
    }
    //CHECKSTYLE:ON
}
