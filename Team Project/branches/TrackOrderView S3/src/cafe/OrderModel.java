package cafe;
import java.util.ArrayList;

/**
 * The Model for Order.
 *
 * @author Robert Kardjaliev
 */
public class OrderModel {
	/** the variable in which the total price of the order is saved. */
	private float totalPrice = 0;
	/** The list of dishes ordered. */
	private ArrayList<Dish> dishes = new ArrayList<Dish>();
	
	/**
	 * Iterates over entire list of dishes and adds
	 * up their prices.
	 * @return the total price of the order
	 */
	public final float getTotalPrice() {

		for (int i = 0; i < dishes.size(); i++) {
			totalPrice += dishes.get(i).getPrice();
			
		}
		return totalPrice;
	}
	
	/**
	 * Removes a dish from the list of dishes.
	 */
	public final void removeDish() {
		//for that certain dish that is selected
		dishes.remove(dishes);
	}
	
	/**
	 * Displays the list of dishes.
	 */
	public final void showDishes() {
		for (int i = 0; i < dishes.size(); i++) {
			dishes.get(i).getName();
			dishes.get(i).getPrice();
		}
	}
	
	/**
	 * Adds a dish to the dish list.
	 *
	 * @param d the dish to be added
	 */
	public final void addDish(final Dish d) {
		dishes.add(d);
	}
}
