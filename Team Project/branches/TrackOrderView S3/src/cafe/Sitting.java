package cafe;

/**
 * A sitting encapsulates an allocated table, its orders, and its payments. A
 * sitting may be booked in advance or occur as a result of passing trade. The
 * default length of a sitting is two hours.
 * 
 * @author Michael Winter
 */
public final class Sitting {
    /**
     * The unique identifier for this instance.
     */
    private int id;
    
    
    /**
     * Returns the unique identifier for this sitting.
     * 
     * @return the identifier.
     */
    public int getId() {
        return id;
    }

}
