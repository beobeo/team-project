/**
 * 
 */
package cafe;

import java.util.ArrayList;

/**
 * @author Jonathan Hercock
 */
public class Waiter {

    /** The name of the waiter. */
    private static String name;
    
    /** The unique identifier for the waiter.   */
    private int employeeID;
    
    /** The list of tables that the waiter is helping.     */          
    @SuppressWarnings("unused")
    private ArrayList<Table> tables = new ArrayList<Table>();
    
    /** The state that will be returned for whether the waiter can assist a new table.    */
    private boolean available;

    /** Constructor that passes two arguments in from the above variables.
     * 
     * @param name  The waiter's name.
     * @param employeeID    The numerical ID by which the waiter is identified.
     * @param tables    The list of tables that the Waiter is assigned to.
     */
    public Waiter(final String name, final int employeeID, final boolean available, final ArrayList<Table> tables) {
        Waiter.name = name;
        this.employeeID = employeeID;
        this.available = available;
        this.tables = tables;
    }
    
    /** Returns the name of this Waiter.
     * @return the Waiter's name.
     */
    public final String getName() {
        return name;
    }
    
    /** Returns the unique ID of this Waiter.
     * @return the employee's ID.
     */
    public final int getId() {
        return employeeID;
    }
    
    /** Method to return whether the Waiter can help any other tables.
     * 
     * @return the Waiter's availability status.
     */
    public final boolean getAvailable() {
        return available;
    }

    /** Constant state, checking whether the Waiter needs help from another employee or not.
     * 
     * @return true at the moment.
     */
    public final boolean needsAssistance() {
        return true;
        
    }

    /** Constant state, returns true when the Waiter signals that a table is ready to pay,
     *                      or false otherwise.
     * 
     * @return false, for now.
     */
    public final boolean readyToPay() {
        return false;
        
    }

    /**
     * @return false at the moment.
     */
    public final boolean collectDish() {
        return true;
                
    }

}
