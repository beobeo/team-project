package cafe;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Component to hold the information about a dish.
 * 
 * @author JuLi
 * @author Adam
 * 
 */
public class DishComponent extends JPanel {
	
	/** Sets serial Version UID to 1L. */
	private static final long serialVersionUID = 1L;
	
	/** Button for remove. */
	private final JButton btnRemove = new JButton("-");
	
	/** Prints out the amount of food. */
	private JLabel lblQuantity = new JLabel("1");
	
	/** Presents price. */
	private JLabel lblPrice = new JLabel("price");
	
	/** The label for the description of the Dish. */
	private JLabel lblDescription = new JLabel("Description");
	
	/** The Name of the dish. */
	private JLabel lblName = new JLabel("Name");
	
	/** Where the picture is displayed. */
	private final JLabel lblPicture = new JLabel("");

	
	//Below are some variables for the positions of labels
	/** sets the 'Y' label position to 41. */
	private final int yPosition = 41;
	
	/** sets the 'X' label position to 348.  */
	private final int xLblPosition = 348;
	
	/** sets the width to 41.*/
	private final int buttonWidth = 41;
	
	/** sets the hight to 23. */
	private final int buttonHight = 23;
	
	/** The 'X' position for the removeButton. */
	private final int btnRemoveXPos = 399;
	
	/**sets label width to 46. */
	private final int lblWidth = 46;
	
	/** sets label height to 14. */
	private final int lblHeight = 14;

	/** sets label 'Y' position to 11.*/
	private final int lblYPos = 11;
	
	/** sets the X position of the remove button to 374. */
	private final int btnRemoveX = 374;
	
	/** sets the 'X' position of Quantity to 250. */
	private final int lblQuantityXPos = 250;
	
	/** sets the label 'X' position to 109. */
	private final int lblX = 109;
	
	/** sets the description 'Y' position to 36. */
	private final int descriptionY = 36;
	
	/** sets the description width to 201. */
	private final int descriptionWidth = 201;
	
	/** sets the description height to 34.*/
	private final int descriptionHeight = 34;
	
	/** sets width of picture to 99.  */
	private final int picWidth = 99;
	
	/** sets hight of picture to 70.*/
	private final int picHight = 70;
	
	
	/**
	 * Create the panel. sets the label values to the dish values that are
	 * passed to the method
	 * 
	 * @param name
	 *            - dish name.
	 * @param description
	 *            - dish description.
	 * @param price
	 *            - dish price.
	 * @param quantity
	 * 			- dish quantity
	 * @param aListener
	 * 			  - Action listener for dish.
	 
	 */
	public DishComponent(final String name, final String description,
			final float price, final int quantity, final ActionListener aListener) { 
		// add the picture as well String image
		
		setLayout(null);

		JButton btnAdd = new JButton("+");

		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) { 
				// for now it just increases the
				// number displayed on the label
				
				int temp = Integer.parseInt(lblQuantity.getText()) + 1;
				lblQuantity.setText(Integer.toString(temp));
			}
		});
		btnAdd.addActionListener(aListener); 
		/* aListener from controller -  
		should increase quantity and refresh labels
		and refresh price */

		btnAdd.setBounds(xLblPosition, yPosition, buttonWidth, buttonHight);
		add(btnAdd);
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent arg0) {
				// temporary, just decreasing quantity
				
				int temp = Integer.parseInt(lblQuantity.getText()) - 1;
				lblQuantity.setText(Integer.toString(temp));	
			}
		});

		btnRemove.addActionListener(aListener); 
		/*aListener from controller -
		 should decrease quantity and/or remove item
		 and refresh labels and refresh price */
		int temp = Integer.parseInt(lblQuantity.getText()) + 1;
		lblQuantity.setText(Integer.toString(temp));	

		btnRemove.setBounds(btnRemoveXPos, yPosition, buttonWidth, buttonHight);

		add(btnRemove);
		lblQuantity.setBounds(btnRemoveX, lblYPos, lblWidth, lblHeight);

		add(lblQuantity);
		lblPrice.setBounds(lblQuantityXPos, lblYPos, lblWidth, lblHeight);

		add(lblPrice);
		lblDescription.setVerticalAlignment(SwingConstants.TOP);
		lblDescription.setBounds(lblX, descriptionY, descriptionWidth, descriptionHeight);

		add(lblDescription);
		lblName.setBounds(lblX, lblYPos, lblWidth, lblHeight);

		add(lblName);
		lblPicture.setBounds(0, 0, picWidth, picHight);

		add(lblPicture);

		// lblPicture has to display the dish picture
		/*
		 * example:
		 * 			JLabel dice1 = new JLabel();
		 *			ImageIcon one = new ImageIcon("dice/1.png");
		 *			dice1.setIcon(one);
		 */
		
		ImageIcon test = new ImageIcon("Team Project/resources/dish.png");
		lblPicture.setIcon(test);
		
		lblName.setText(name);
		lblDescription.setText(description);
		lblPrice.setText(Float.toString(price));

	}
}
