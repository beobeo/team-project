package cafe.dao;

import java.sql.Timestamp;

import cafe.Sitting;


/**
 * A data access object used to manipulate persisted {@link Sitting} objects.
 * 
 * @author Michael Winter
 */
public interface SittingDAO {
    /**
     * Creates a new {@link Sitting} relating to the specified {@link Table},
     * reserving the latter from the current time over the next two hours,
     * returning its identifier. This is equivalent to:
     * 
     * <pre>
     * long currentTime = System.currentTimeMillis();
     * createSitting(tableId, -1, new Timestamp(currentTime),
     *         new Timestamp(currentTime + 2 * 60 * 60 * 1000))</pre>
     * 
     * @param tableId
     *            the identifier of the table to which this sitting relates.
     * @return the identifier for the new sitting.
     *
     */
    int createSitting(int tableId);

    /**
     * Creates a new {@link Sitting} relating to the specified {@link Table},
     * returning its identifier.
     * 
     * @param tableId
     *            the identifier of the table to which this sitting relates.
     * @param customerId
     *            the identifier of the customer that booked this sitting or
     *            {@code -1} if it is ad-hoc.
     * @param from
     *            the time that this booking starts.
     * @param until
     *            the time that this booking ends.
     * @return the identifier for the new sitting.
     * @throws DataAccessException
     *             if an error occurs while accessing the data store.
     */
    int createSitting(int tableId, int customerId, Timestamp from,
            Timestamp until);
    
    /**
     * Given an identifier, this method loads and returns a new instance of the
     * specified {@link Sitting}.
     * 
     * @param id
     *            the identifier.
     * @return the specified {@code Sitting}.
     * @throws DataAccessException
     *             if an error occurs while accessing the data store.
     */
    Sitting findSitting(final int id);
}
