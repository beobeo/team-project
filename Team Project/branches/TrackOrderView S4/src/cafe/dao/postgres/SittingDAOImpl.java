package cafe.dao.postgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import cafe.Sitting;
import cafe.dao.DataAccessException;
import cafe.dao.SittingDAO;
import cafe.dao.TableDAO;
import cafe.dao.jdbc.ConnectionFactory;

/**
 * A data access object used to manipulate {@link Sitting} objects that have
 * persisted to a PostgreSQL database.
 * 
 * @author Michael Winter
 */
public final class SittingDAOImpl implements SittingDAO {
    /**
     * The source of JDBC connections used to perform queries against the
     * underlying database.
     */
    private ConnectionFactory connectionFactory;
    /**
     * The data access object used to obtain {@link Table} instances from the
     * database.
     */
    private TableDAO tableData;

    
    /**
     * Constructs a data access object for {@link Sitting} objects that will operate over
     * the given connection.
     * 
     * @param tableDAO
     *            the data access object used to supply {@link Table}
     *            instances.
     * @param connectionFactory
     *            the factory object that will provide a database connection.
     * @throws NullPointerException
     *             if the specified factory or data access objects are {@code null}.
     */
	public SittingDAOImpl(final TableDAO tableDAO, final ConnectionFactory connectionFactory) {
        if (tableDAO == null) {
            throw new NullPointerException("tableDAO is null");
        }
        if (connectionFactory == null) {
            throw new NullPointerException("connectionFactory is null");
        }
	    this.tableData = tableDAO;
		this.connectionFactory = connectionFactory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.SittingDAO#createSitting(int, Timestamp, Timestamp)
	 */
	@Override
	public int createSitting(final int tableId) {
        final long now = System.currentTimeMillis();
        final long twoHourMs = 7200000;
		return createSitting(tableId, -1, new Timestamp(now),
		        new Timestamp(now + twoHourMs));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.SittingDAO#createSitting(int, int, Timestamp, Timestamp)
	 */
	@Override
	public int createSitting(final int tableId, final int customerId,
			final Timestamp from, final Timestamp until) {
		final Connection connection = connectionFactory.getConnection();
		try (PreparedStatement statement = connection
				.prepareStatement(
						"INSERT INTO sitting"
								+ " (table_id, customer_id, reserved_from, reserved_until)"
								+ " VALUES (?, ?, ?, ?)",
						PreparedStatement.RETURN_GENERATED_KEYS)) {
			// CHECKSTYLE IGNORE MagicNumberCheck FOR NEXT 8 LINES
			statement.setInt(1, tableId);
			if (customerId == -1) {
				statement.setNull(2, Types.INTEGER);
			} else {
				statement.setInt(2, customerId);
			}
			statement.setTimestamp(3, from);
			statement.setTimestamp(4, until);
			statement.executeUpdate();

			final ResultSet result = statement.getGeneratedKeys();
			result.next();
			return result.getInt(1);
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}
	}

	/*
     * (non-Javadoc)
     * 
     * @see cafe.dao.SittingDAO#findSitting(int)
     */
    @Override
    public Sitting findSitting(final int id) {
        final Connection connection = connectionFactory.getConnection();
        Sitting sitting = null;
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT table_id, customer_id, placed, reserved_from, reserved_until"
                + " FROM sitting"
                + " WHERE id = ?")) {
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                // TODO: Complete implementation of method
                sitting = new Sitting(id,
                        tableData.findTable(result.getInt("table_id")));
            }
        } catch (final SQLException e) {
            throw new DataAccessException(e);
        }
        return sitting;
    }

}
