package cafe;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit tests for the {@link OrderState} enumeration.
 * 
 * @author Michael Winter
 */
public final class OrderStateTest {
    /**
     * Ensures that the <em>new</em> state compares such that it precedes
     * the <em>pending</em> state.
     */
    @Test
    public void newPrecedesPending() {
        assertTrue(OrderState.NEW.compareTo(OrderState.PENDING) < 0);
    }

    /**
     * Ensures that the <em>pending</em> state compares such that it precedes
     * the <em>confirmed</em> state.
     */
    @Test
    public void pendingPrecedesConfirmed() {
        assertTrue(OrderState.PENDING.compareTo(OrderState.CONFIRMED) < 0);
    }

    /**
     * Ensures that the <em>confirmed</em> state compares such that it precedes
     * the <em>preparing</em> state.
     */
    @Test
    public void confirmedPrecedesPreparing() {
        assertTrue(OrderState.CONFIRMED.compareTo(OrderState.PREPARING) < 0);
    }
    
    /**
     * Ensures that the <em>preparing</em> state compares such that it precedes
     * the <em>ready</em> state.
     */
    @Test
    public void preparingPrecedesReady() {
        assertTrue(OrderState.PREPARING.compareTo(OrderState.READY) < 0);
    }

    /**
     * Ensures that the <em>ready</em> state compares such that it precedes the
     * <em>delivered</em> state.
     */
    @Test
    public void readyPrecedesDelivered() {
        assertTrue(OrderState.READY.compareTo(OrderState.DELIVERED) < 0);
    }
}
