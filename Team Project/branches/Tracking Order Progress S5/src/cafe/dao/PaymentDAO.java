package cafe.dao;

import cafe.Payment;

/**
 * A data access object used to manipulate payments in the database.
 * @author Robert Kardjaliev
 *
 */
public interface PaymentDAO {
	/**
	 * Makes a payment by cash given a customer's {@link Sitting} ID and
	 * the amount paid.
	 * @param sittingId
	 * 			the {@link Sitting} ID of a customer.
	 * @param amount
	 * 			the amount paid.
	 */
	void makePayment(int sittingId, int amount);
	
	/**
	 * Makes a payment by cash given a customer's {@link Sitting} ID and
	 * the amount paid.
	 * @param sittingId
	 * 			the {@link Sitting} ID of a customer.
	 * @param card
	 * 			a 16 characters long String representing a card.
	 * @param amount
	 * 			the amount paid.
	 */
	void makePayment(int sittingId, String card, int amount);

	/**
	 * Finds and returns a {@link Payment} given its {@code id}.
	 * @param id
	 * 			the {@link Payment} ID.
	 * @return 
	 * 			the {@link Payment} object.
	 */
	Payment findPayment(int id);
}
