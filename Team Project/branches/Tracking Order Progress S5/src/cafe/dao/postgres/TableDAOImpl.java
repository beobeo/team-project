package cafe.dao.postgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cafe.Table;
import cafe.dao.DataAccessException;
import cafe.dao.EmployeeDAO;
import cafe.dao.TableDAO;
import cafe.dao.jdbc.ConnectionFactory;

/**
 * A data access object used to manipulate {@link Table} objects that have
 * persisted to a PostgreSQL database.
 * 
 * @author Michael Winter
 * @author Robert Kardjaliev
 */
public final class TableDAOImpl implements TableDAO {
	/**
	 * The source of JDBC connections used to perform queries against the
	 * underlying database.
	 */
	private ConnectionFactory connectionFactory;
	/**
	 * The data access object used to obtain {@link Employee} instances from the
	 * database.
	 */
	private EmployeeDAO employeeDataSource;
	/**
	 * A table's ID.
	 */
	private int tableId;
	/** A waiter's ID. */
	private int waiterId;


	/**
	 * Constructs a data access object for {@link Sitting} objects that will operate over
	 * the given connection.
	 * 
	 * @param employeeDAO
	 *            the data access object used to supply {@link Employee}
	 *            instances.
	 * @param connectionFactory
	 *            the factory object that will provide a database connection.
	 * @throws NullPointerException
	 *             if the specified factory or data access objects are {@code null}.
	 */
	public TableDAOImpl(final EmployeeDAO employeeDAO, final ConnectionFactory connectionFactory) {
		if (employeeDAO == null) {
			throw new NullPointerException("employeeDataSource is null");
		}
		if (connectionFactory == null) {
			throw new NullPointerException("connectionFactory is null");
		}
		this.employeeDataSource = employeeDAO;
		this.connectionFactory = connectionFactory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.TableDAO#findTable(int)
	 */
	@Override
	public Table findTable(final int id) {
		final Connection connection = connectionFactory.getConnection();
		Table table = null;
		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT waiter_id FROM \"table\" WHERE id = ?")) {
			statement.setInt(1, id);
			ResultSet result = statement.executeQuery();
			if (result.next()) {
				// TODO: Complete implementation of method
				table = new Table(id,
						employeeDataSource.findEmployee(result.getInt("waiter_id")));
			}
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}
		return table;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.TableDAO#createTable()
	 */
	@Override
	public int createTable() {
		final Connection connection = connectionFactory.getConnection();        
		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT * FROM \"table\" ORDER BY id DESC LIMIT 1")) {
			ResultSet result = statement.executeQuery();
			if (result.next()) {
				tableId = result.getInt("id") + 1;
				waiterId = result.getInt("waiter_id");
			}
		}
		catch (final SQLException e) {
			throw new DataAccessException(e);
		}
		try (PreparedStatement statement = connection.prepareStatement(
				"INSERT INTO \"table\" (id, waiter_id) VALUES(?, ?)")) {
			statement.setInt(1, tableId);
			statement.setInt(2, waiterId);
			statement.executeUpdate();
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}
		return tableId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.TableDAO#removeTable()
	 */
	@Override
	public int removeTable() {
		final Connection connection = connectionFactory.getConnection();

		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT id FROM \"table\" ORDER BY id DESC LIMIT 1")) {
			ResultSet result = statement.executeQuery();
			if (result.next()) {
				tableId = result.getInt("id");
			}
		}
		catch (final SQLException e) {
			throw new DataAccessException(e);
		}
		try (PreparedStatement statement = connection.prepareStatement(
				"DELETE FROM \"table\" WHERE id = ?")) {
			statement.setInt(1, tableId);
			statement.executeUpdate();
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}
		return tableId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.TableDAO#getAllTables()
	 */
	@Override
	public List<Table> getAllTables() {
		final Connection connection = connectionFactory.getConnection();
		final List<Table> tables = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT * FROM \"table\" ORDER BY id")) {			
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				tables.add(new Table(result.getInt("id"),
						employeeDataSource.findEmployee(result.getInt("waiter_id"))));
			}			
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}
		return tables;
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.TableDAO#assignWaiterToTable(int, String)
	 */
	@Override
	public void assignWaiterToTable(final int id, final String waiter) {
		final Connection connection = connectionFactory.getConnection();
		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT id FROM employee WHERE first_name = ? and role = \'Waiter\'")) {
			statement.setString(1, waiter);
			ResultSet result = statement.executeQuery();
			if (result.next()) {
				waiterId = result.getInt("id");
				try (PreparedStatement statement2 = connection.prepareStatement(
						"UPDATE \"table\" SET waiter_id = ? WHERE id = ?")) {
					statement2.setInt(1, waiterId);
					statement2.setInt(2, id);
					statement2.executeUpdate();
				}
			}
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}
	}
}
