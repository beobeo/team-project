package cafe;

import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.util.Collections;

import org.junit.Test;

/**
 * Unit tests for the {@link Order} class.
 * 
 * @author Michael Winter
 */
public final class OrderTest {
    
    /**
     * Ensures that orders are ranked relative to their respective placement
     * time.
     */
    @Test
    public void ranksByTime() {
        final Order earlier = new Order(0, null, null, new Timestamp(0), null,
                null, null, Collections.<OrderItem> emptyList());
        final Order later = new Order(0, null, null, new Timestamp(1), null,
                null, null, Collections.<OrderItem> emptyList());
        assertTrue(earlier.compareTo(later) < 0);
    }

    /**
     * Ensures that orders with the same placement time are ranked equal.
     */
    @Test
    public void sameStateAndTimeRanksEqual() {
        final Order earlier = new Order(0, null, null, new Timestamp(0), null,
                null, null, Collections.<OrderItem> emptyList());
        final Order later = new Order(0, null, null, new Timestamp(0), null,
                null, null, Collections.<OrderItem> emptyList());
        assertTrue(earlier.compareTo(later) == 0);
    }
}
