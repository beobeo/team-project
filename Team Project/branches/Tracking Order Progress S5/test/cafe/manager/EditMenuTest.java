package cafe.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.Test;


import cafe.dao.DAOFactory;
import cafe.dao.DishDAO;
import cafe.dao.postgres.DefaultTestDAOFactoryImpl;

/**
 * Unit test for the Edit Menu GUI.
 * @author Robert Kardjaliev
 */
public class EditMenuTest {
	/** An instance of the DAO factory. */
    private DAOFactory factory = DAOFactory.getInstance("zyvc215", "IYIP1845",
            DefaultTestDAOFactoryImpl.class);
	/** An Order data access object. */
    private DishDAO dishData = factory.getDishDAO();
    
    /**
     * A {@code Dish}'s name.
     */
    private String dishName = "Lasagne";
    /** A boolean value for a {@code Dish}.*/
    private boolean visibility = true;
    
	@Before
	public final void setUp() {
		dishData.setVisible(dishName, visibility);
	}
	
	/**
	 * Checks whether the visibility of a {@link Dish} is what this test was designed for.
	 */
	@Test
	public final void checkOrderState() {		
		assertEquals("Test 1: Check for correct visibility of a Dish.", dishData.getVisible(dishName),visibility);
	}
	
	/**
	 * Checks for correct change of visibility.
	 */
	@Test
	public final void testVisibleChange() {
		dishData.setVisible(dishName, false);
		assertFalse("Test 2: Test for correct correct change of visibility.", dishData.getVisible(dishName) == visibility);
	}
}
