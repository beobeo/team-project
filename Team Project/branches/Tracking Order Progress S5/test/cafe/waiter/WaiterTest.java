/**
 * 
 */
package cafe.waiter;
//import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


/**
 * @author Jonathan Hercock
 * 
 *
 */
public final class WaiterTest {

	/**
	 *  First test, checking the needsAssistance() method of the class.
	 */
	@Test
	public void testNeedsAssistance() {
		final Waiter waiterTest = new Waiter(123421, "John", "Smith", true, null);

		waiterTest.needsAssistance();
		assertTrue(waiterTest.needsAssistance());
	}

	/**
	 *  Second test, checking the readyToPay() method of the class.
	 */
	@Test
	public void testReadyToPay() {
		final Waiter testWaiter = new Waiter(123422, "Phil", "Jones", true, null);

		testWaiter.readyToPay();
		assertFalse(testWaiter.readyToPay());
	}

	/**
	 * 
	 */
	@Test
	public void testCollectDish() {
		final Waiter waiterTest = new Waiter(123423, "Mark", "Jones", true, null);

		waiterTest.collectDish();
		assertTrue(waiterTest.collectDish());
	}

	/**
	 *  Ensures that the availability of the Waiter registers properly.
	 */
	@Test
	public void isAvailable() {
		final Waiter waiterTest = new Waiter(123424, "Tom", "Jones", false, null);

		waiterTest.getAvailable();
		assertFalse(waiterTest.getAvailable());

	}

	@Test
	public void testOrderDelPressed() {
		final Waiter waiterTest = new Waiter(123423, "Brian", "King", true, null);

		waiterTest.orderDelivered();
	}
}

