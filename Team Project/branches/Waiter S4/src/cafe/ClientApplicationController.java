package cafe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import cafe.dao.AuthenticationException;
import cafe.dao.DAOFactory;
import cafe.dao.DataAccessException;
import cafe.dao.DishDAO;
import cafe.dao.postgres.DAOFactoryImpl;

/**
 * Provides a driver for the client's application, handling the relationship
 * between the menu, order status display, and payment processing.
 * 
 * @author Michael Winter
 * @author Jonathan Hercock
 * @author Hannah Cooper
 */
public final class ClientApplicationController {
    /**
     * Initialises and runs the application. The actual launching of the
     * application is delegated to this method in order to ensure initialisation
     * occurs on Swing's event-dispatching thread.
     */
	private static void launch() {
		try {
			if (SwingUtilities.isEventDispatchThread()) {
				final ClientApplicationController application = new ClientApplicationController();
				application.run();
			} else {
				SwingUtilities.invokeAndWait(new Runnable() {
					@Override
					public void run() {
						launch();
					}
				});
			}
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * The main entry point for the application.
	 * 
	 * @param args the command line arguments passed to the application
	 */
	public static void main(final String[] args) {
		launch();
	}

    /**
     * The currently active order. This instance retains the state of client's
     * selection during their visit and is reset once the client has paid and
     * left.
     */
	//private Order currentOrder;
	/**
	 * The controller for the client menu.
	 */
	private MenuController menuController;
	/**
	 * The controller for the client order display.
	 */
	private OrderController orderController;
	/**
	 * The controller for the client track order display.
	 */
	private TrackOrderController trackOrderController;
    /**
     * The controller for the client payment system.
     */
    // private PaymentController paymentController;
	private OrderGUI orderView;
	
    /**
     * Prepares the UI and any resources required prior to starting the
     * application. The constructor must be invoked on the event-dispatching
     * thread.
     */
    private ClientApplicationController() {
        DAOFactory factory;
        try {
            /* Initialise the data object factory using the PostgreSQL
             * implementation, using the given credentials to authenticate the
             * database connection. These details could be provided by the user
             * when the application loads, rather than hard-coded. For example,
             * guest details would only allow users read access to the menu,
             * insert access to service requests, and insert/update to orders.
             * If a waiter logs in, they also can read and update service
             * requests, insert and update bookings, etc.
             */
            factory = DAOFactory.getInstance("zyvc215", "IYIP1845",
                    DAOFactoryImpl.class);

            /*
             * Obtain the data object for dish information. The menu model
             * should only take data sources that it actually requires, which is
             * why the model now only takes the dish data object as an argument.
             * If it requires more information from other parts of the database,
             * then those access objects should be created separately and passed
             * in (not the whole factory: see dependency injection).
             */
            final DishDAO dishData = factory.getDishDAO();

            final OrderModel orderModel = new OrderModel();
            orderView = new OrderGUI();
            orderController = new OrderController(orderModel, orderView);
            orderController.addShowMenuListener(new ShowMenuListener());
            orderController.addWaiterCallListener(new WaiterCallListener());

            final MenuGUI menuView = new MenuGUI();
            final MenuModel menuModel = new MenuModel(dishData);
            menuController = new MenuController(menuModel, menuView);
            // TODO: Listen for this event in the OrderController.
            menuController.addDishAddedListener(new DishAddedListener());
            menuController.addShowOrderListener(new ShowOrderListener());
            menuController.addWaiterCallListener(new WaiterCallListener());
        } catch (final AuthenticationException e) {
            JOptionPane.showMessageDialog(
                    null,
                    "The user name or password used to access information is incorrect."
                            + "\nPlease speak to a member of staff for assistance.",
                    "Bad user name/password", JOptionPane.ERROR_MESSAGE);
            // TODO: Re-prompt authentication (if appropriate)
            return;
        } catch (final DataAccessException e) {
            JOptionPane.showMessageDialog(
                    null,
                    "The application is unable to get some required information and must close."
                            + "\nPlease speak to a member of staff for assistance.",
                    "Connection failed", JOptionPane.ERROR_MESSAGE);
            // TODO: Add logging
            
            return;
        }
        orderController.addShowTrackOrderViewListener(new ShowTrackOrderViewListener());
        
        final TrackMyOrderGUI trackView = new TrackMyOrderGUI();

        trackOrderController = new TrackOrderController(trackView);
        trackOrderController.addCancelOrderListener(new CancelOrder());
        trackOrderController.addEditOrderListener(new EditOrder());
        
        
    }
	
	/**
	 * Starts the application.
	 */
	public void run() {
		menuController.showView();
	}
	
	
	/**
	 * Listens for order change events originating from the Add to Order button associated with
	 * each dish, adding them to the current order.
	 */
	private class DishAddedListener implements ChangeListener {
		@Override
		public void stateChanged(final ChangeEvent e) {
			final Dish addedDish = (Dish) e.getSource();
			//currentOrder.addDish(addedDish);
			System.out.println("Added " + addedDish.getName() + " to order.");
		}
	}
	
	/**
	 * Listens for button click events originating from the Order button, hiding the menu view
	 * and displaying the order status view.
	 */
	private class ShowOrderListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			orderController.showView();
			menuController.hideView();
		}
	}
	
	/**
	 * Listens for button click events originating from the Menu button, hiding the order view
	 * and displaying the menu.
	 */
	private class ShowMenuListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			menuController.showView();
			orderController.hideView();
		}
	}
	
	
	
	/**
	 * Listens for button click events originating from the Track my Order button, 
	 * hiding the order view                           //and displaying the Track Order view.
	 * 
	 *
	 */
	private class ShowTrackOrderViewListener implements ActionListener {
	    @Override
	    public void actionPerformed(final ActionEvent e) {
	        orderController.hideView();
	        trackOrderController.showView();
	       
	    }
	}
	
	
	/**
	 * Listens for button click events originating from the Call for Assistance button,
	 * alerting the waiters that a table requires attention.
	 */
	private class WaiterCallListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
		    // TODO: Implement write to database
			System.out.println("Service request sent...");
		}
	}
		/**
		 * 
		 * @author Hannah Cooper
		 * ActionListener for Cancel Order button of Track My Order GUI. 
		 * Actions include:
		 * presenting user with a confirm pop-up once the button is clicked 
		 * and changes the user back to the menu screen.
		 */
		class CancelOrder implements ActionListener {
	
			@Override
			public void actionPerformed(final ActionEvent e) {
				
				int option = JOptionPane.showConfirmDialog(
						null, "Are you sure you want to cancel your order?");
				
				if (option == 0) {
					trackOrderController.hideView();
					menuController.showView();
				}
				
				//To Do:
				//change order status to cancelled in database
				
			}
			
		}
	    
		/**
		 * 
		 * @author Hannah Cooper
		 * ActionListener for EditOrder button of Track My Order GUI. 
		 * Actions include:
		 * presenting user with a confirm pop-up once the button is clicked 
		 * and changes the user back to order screen so they can begin to edit their order again.
		 */
	class EditOrder implements ActionListener {
	
		@Override
		public void actionPerformed(final ActionEvent e) {
			
			
			int option1 = JOptionPane.showConfirmDialog(
					null, "Editing your order will increase"
							+ " the time that it will take for"
							+ " your food to be delivered."
							+ " Would you like to continue?");
			
			if (option1 == 0) {
				trackOrderController.hideView();
				orderController.showView();
				orderView.makeConfirmOrderVisible();
			}
			
			
			//To Do:
			//currently not working - must make it so the 'confirm order'
			//button works once you return to the order screen.
			//change status of order

		}
		
	}
	
	
}
