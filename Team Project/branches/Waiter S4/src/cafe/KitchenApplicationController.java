package cafe;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import cafe.dao.AuthenticationException;
import cafe.dao.DAOFactory;
import cafe.dao.DataAccessException;
import cafe.dao.OrderDAO;
import cafe.dao.postgres.DAOFactoryImpl;
import cafe.kitchen.KitchenController;
import cafe.kitchen.KitchenModel;
import cafe.kitchen.KitchenView;

/**
 * Provides a driver for the kitchen's application.
 * 
 * @author Michael Winter
 */
public final class KitchenApplicationController {
    /**
     * Initialises and runs the application. The actual launching of the
     * application is delegated to this method in order to ensure initialisation
     * occurs on Swing's event-dispatching thread.
     */
	private static void launch() {
		try {
			if (SwingUtilities.isEventDispatchThread()) {
				final KitchenApplicationController application = new KitchenApplicationController();
				application.run();
			} else {
				SwingUtilities.invokeAndWait(new Runnable() {
					@Override
					public void run() {
						launch();
					}
				});
			}
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * The main entry point for the application.
	 * 
	 * @param args the command line arguments passed to the application
	 */
	public static void main(final String[] args) {
		launch();
	}

	
	/**
	 * The controller for the kitchen interface.
	 */
	private KitchenController kitchenController;


    /**
     * Prepares the UI and any resources required prior to starting the
     * application. The constructor must be invoked on the event-dispatching
     * thread.
     */
    private KitchenApplicationController() {
        DAOFactory factory;
        try {
            /* Initialise the data object factory using the PostgreSQL
             * implementation, using the given credentials to authenticate the
             * database connection. These details could be provided by the user
             * when the application loads, rather than hard-coded. For example,
             * guest details would only allow users read access to the menu,
             * insert access to service requests, and insert/update to orders.
             * If a waiter logs in, they also can read and update service
             * requests, insert and update bookings, etc.
             */
            factory = DAOFactory.getInstance("zyvc215", "IYIP1845",
                    DAOFactoryImpl.class);

            final OrderDAO orderData = factory.getOrderDAO();
            final KitchenModel kitchenModel = new KitchenModel(orderData);
            final KitchenView kitchenView = new KitchenView();
            kitchenController = new KitchenController(kitchenModel, kitchenView);
        } catch (final AuthenticationException e) {
            JOptionPane.showMessageDialog(
                    null,
                    "The user name or password used to access information is incorrect."
                            + "\nPlease speak to a manager for assistance.",
                    "Bad user name/password", JOptionPane.ERROR_MESSAGE);
            // TODO: Re-prompt authentication (if appropriate)
            return;
        } catch (final DataAccessException e) {
            JOptionPane.showMessageDialog(
                    null,
                    "The application is unable to get some required information and must close."
                            + "\nPlease speak to a manager for assistance.",
                    "Connection failed", JOptionPane.ERROR_MESSAGE);
            // TODO: Add logging
            return;
        }
    }
	
	/**
	 * Starts the application.
	 */
	public void run() {
		kitchenController.showView();
	}
}
