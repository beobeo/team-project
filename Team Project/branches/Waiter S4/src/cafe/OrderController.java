package cafe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**The Class OrderController.
 * @author Robert Kardjaliev 
 */
public class OrderController {
	/**
	 * Hides the view associated with this controller.
	 */
	public final void hideView() {
		view.setVisible(false);
	}

	/**
	 * Shows the view associated with this controller.
	 */
	public final void showView() {
		view.setVisible(true);
	}
	
	
	
	/** The model. */
	private OrderModel model;
	/** The view. */
	private OrderGUI view;

	/**
	 * Populates the GUI.
	 * 
	 */
	public final void populateView() {
		model.populateOrder();
		view.populateGUI(model.getDishes(), new AListener());
	}

	/**
	 * ActionListener used in DishComponent. will either increase or decrease
	 * quantity
	 * 
	 * @author JuLi
	 * 
	 */
	private static class AListener implements ActionListener { 

		@Override
		public void actionPerformed(final ActionEvent arg0) { //remove dish etc... unfinished

			/*if (arg0.getActionCommand().toString().equals("-")) {
				//model.getDishes().get()... ;
			}*/
		}
	}

	/**
	 * Instantiates the fields of the Order Controller.
	 * 
	 * @param model
	 *            - The Model
	 * @param view
	 *            - The View
	 */
	public OrderController(final OrderModel model, final OrderGUI view) {
		this.model = model;
		this.view = view;
		populateView();
	}

	/**
	 * Adds an action listener to the Order button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public final void addShowMenuListener(final ActionListener listener) {
		view.addShowMenuListener(listener);
	}
	/**
	 * Waiter Call listener.
	 * @param listener
	 * 					action listener
	 */
	public final  void addWaiterCallListener(final ActionListener listener) {
		view.addWaiterCallListener(listener);
	}
	/**
	 * Track Order Listener.
	 * @param listener
	 * 					action listener
	 */
	public final void addShowTrackOrderViewListener(final ActionListener listener) {
		view.addShowTrackOrderViewListener(listener);
		
	}

	

}
