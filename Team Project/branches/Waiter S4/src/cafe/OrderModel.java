package cafe;

import java.util.ArrayList;
import java.util.List;

/**
 * The Model for Order.
 * 
 * @author Robert Kardjaliev
 */
public class OrderModel {
	/** the variable in which the total price of the order is saved. */
	private float totalPrice = 0;
	/** The list of dishes ordered. */
	private List<OrderItem> orderItems = new ArrayList<OrderItem>();

	/**
	 * Fills up the List.
	 * 
	 */
	public final void populateOrder() {
		// database connection start
		// get orders and fill up arrayList
		// close connection
	}

	/**
	 * Iterates over entire list of dishes and adds up their prices.
	 * 
	 * @return the total price of the order
	 */
	public final float getTotalPrice() {
		int size = orderItems.size();
		for (int i = 0; i < size; i++) {
			totalPrice += orderItems.get(i).getDish().getPrice()
					* orderItems.get(i).getQuantity();
		}
		return totalPrice;
	}

	/**
	 * Removes a dish from the list of dishes.
	 * 
	 * @param i
	 *            item to be removed
	 */
	public final void removeItem(final OrderItem i) {
		orderItems.remove(i);
	}


	/**
	 * Adds a item to the order list.
	 * 
	 * @param i
	 *            the item to be added
	 */
	public final void addItem(final OrderItem i) {
		orderItems.add(i);
	}

	/**
	 * Returns the list of orderItems (containing the dishes).
	 * 
	 * @return orderItems - list of type OrderItem
	 */
	public final List<OrderItem> getDishes() {
		return orderItems;
	}

}
/*
 * public void SendToDatabase(ArrayList<Dish> dishes){
 * Database.getInstance(String username, String password){ }
 * 
 * 
 * 
 * } }
 */

