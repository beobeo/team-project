package cafe;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

/**
 * GUI class of Track Order.
 * @author JuLi
 *
 */
public class TrackMyOrderGUI extends JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * JPanel.
	 */
	private JPanel contentPane;

    /**
     * Launch the application.
     * @param args
     * 				arguments
     */
    public static void main(final String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    TrackMyOrderGUI frame = new TrackMyOrderGUI();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    /**
     * Cancel Order button.
     */
    private JButton cancelOrder;
    /**
     * Edit Order button.
     */
    private JButton editOrder;
 
    /**
     * Create the frame.
     */
    //CHECKSTYLE:OFF
    public TrackMyOrderGUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JLabel lblMenu = new JLabel("TRACK ORDER");
        lblMenu.setFont(new Font("Tahoma", Font.BOLD, 15));
        lblMenu.setBounds(10, 11, 123, 25);
        contentPane.add(lblMenu);
        
        cancelOrder = new JButton("Cancel Order");
        cancelOrder.setFont(new Font("Tahoma", Font.BOLD, 12));
        cancelOrder.setBounds(284, 210, 123, 25);
        contentPane.add(cancelOrder);
        
        editOrder = new JButton("Edit Order");
        /* THIS IS STUPID
        editOrder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            	OrderGUI.enableTrackOrder();
                OrderGUI.btnTrackOrder.setEnabled(false);
                OrderGUI.btnConfirmOrder.setEnabled(false);
            }
        });*/
        editOrder.setFont(new Font("Tahoma", Font.BOLD, 12));
        editOrder.setBounds(284, 174, 123, 25);
        contentPane.add(editOrder);
        
        JProgressBar progressBar = new JProgressBar();
        progressBar.setValue(60);
        progressBar.setStringPainted(true);
        progressBar.setBounds(128, 47, 146, 14);
        contentPane.add(progressBar);
        
        JLabel lblOrderProgress = new JLabel("Order Progress:");
        lblOrderProgress.setBounds(20, 47, 113, 14);
        contentPane.add(lblOrderProgress);
    }
    //CHECKSTYLE:ON
    //public void addShowTrackOrderViewListener(ActionListener listener) {      
    //}
	/**
	 * 
	 * @param listener - listener for Cancel Order button on Track my order screen
	 */
	public final void addCancelOrderListener(final ActionListener listener) {
		cancelOrder.addActionListener(listener);
	}
	
	/**
	 * 
	 * @param listener - listener for Edit Order button on track my order screen 
	 */
	
	public final void addEditOrderListener(final ActionListener listener) {
		editOrder.addActionListener(listener);
	}
}


