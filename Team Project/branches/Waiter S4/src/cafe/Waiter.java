package cafe;

import java.util.ArrayList;

/**
 * @author Jonathan Hercock
 */
public class Waiter extends Employee {

	/** Example of a Waiter, used to fees a name and ID to the GUI. */
	public static final Waiter TEST_WAITER = new Waiter(1234321, "John",
			"Smith", true, null);

	/** The list of tables that the waiter is helping. */
	private ArrayList<Table> tables = new ArrayList<Table>();

	/**
	 * The state that will be returned for whether the waiter can assist a new
	 * table.
	 */
	private boolean available;

	/**
	 * Constructor that passes two arguments in from the above variables.
	 * 

	 * @param id    The numerical ID by which the waiter is identified.
	 * @param firstName  The waiter's first name.
	 * @param lastName  The waiter's surname.
	 * @param tables    The list of tables that the Waiter is assigned to.
	 * @param available  A boolean, used to represent whether the Waiter is busy or not.
	 */

	public Waiter(final int id, final String firstName, final String lastName,
			final boolean available, final ArrayList<Table> tables) {
		super(id, firstName, lastName);
		this.available = available;
		this.tables = tables;
	}

	/**
	 * Returns the name of this Waiter.
	 * 
	 * @return the Waiter's name.
	 */
	public final String getName() {
		return getFirstName() + " " + getLastName();
	}

	/**
	 * Method to return whether the Waiter can help any other tables.
	 * 
	 * @return the Waiter's availability status.
	 */
	public final boolean getAvailable() {
	    return available;
	}
	
	//CHECKSTYLE:OFF
	/** Method to determine whether the Waiter is available or not.
	 *  Sets Available to false - if the Waiter is dealing with more than 4 tables currently.
	 *                    true - otherwise.
	 */
	public final void setAvailable() {
	    if (tables.size() > 4) {
	        available = false; 
	    } else {
            available = true;
        }
	}
	//CHECKSTYLE:ON
	
	/**
	 * Constant state, checking whether the Waiter needs help from another
	 * employee or not.
	 * 
	 * @return true at the moment.
	 */
	public final boolean needsAssistance() {
		return true;

	}

	/**
	 * Constant state, returns true when the Waiter signals that a table is
	 * ready to pay, or false otherwise.
	 * 
	 * @return false, for now.
	 */
	public final boolean readyToPay() {
		return false;

	}

	/**
	 * @param dish - the dish in question.
	 * @return true at the moment.
	 */
	public final boolean collectDish(final Dish dish) {
		return true;

	}

}
