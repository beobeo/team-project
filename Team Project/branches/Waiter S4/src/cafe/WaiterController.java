package cafe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingUtilities;

import cafe.kitchen.OrderDetailTableModel;
import cafe.kitchen.OrderDetailView;
import cafe.kitchen.OrderListItemController;
import cafe.kitchen.OrderListItemView;

/**
 * 
 */

/**
 * @author Jonathan Hercock
 *
 */
public class WaiterController {
    
    /**
     *  The Waiter's interface.
     */
    private static WaiterGUI view;
    /**
     *  The model.
     */
    private static WaiterModel model;
    
    /**
     * A mapping between orders and the controller for its corresponding entry
     * in the list.
     */
    private Map<Order, OrderListItemController> controllerMap = new HashMap<>();


    /** A constructor for the given view.
     * 
     * @param view - the waiter's view.
     * @param model - the waiter model.
     */
    public WaiterController(final WaiterGUI view, final WaiterModel model) {
        WaiterController.view = view;
        WaiterController.model = model;
        
        WaiterController.model.addOrderModelListener(new OrderStateListener());
        WaiterController.model.refresh();
    }
    
    /**
     * Listens for updates to orders, invoking UI updates as appropriate.
     */
    private class OrderStateListener implements OrderEventListener {
        /*
         * (non-Javadoc)
         * @see cafe.OrderEventListener#orderAdded(cafe.OrderEvent)
         */
        @Override
        public void orderAdded(final OrderEvent e) {
            final Order addedOrder = (Order) e.getSource();
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    final OrderDetailTableModel tableModel = new OrderDetailTableModel(addedOrder);
                    OrderDetailView detailView = new OrderDetailView(tableModel);
                    OrderListItemView listView = new OrderListItemView();

                    OrderListItemController listController = new OrderListItemController(
                            listView, detailView);
                    listController.addAdvanceButtonActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(final ActionEvent e) {
                            if (addedOrder.getProgress() == OrderState.READY
                                || addedOrder.getProgress() == OrderState.PENDING)
                                {    
                                    addedOrder.advanceProgress();
                                    model.updateOrder(addedOrder);
                                }
                        }
                    });
                    listController.refresh(addedOrder);
                    controllerMap.put(addedOrder, listController);
                    view.addListItem(listView);
                }
            });
        }

        @Override
        public void orderChanged(final OrderEvent e) {
            final Order changedOrder = (Order) e.getSource();
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    controllerMap.get(changedOrder).refresh(changedOrder);
                }
            });
        }

        @Override
        public void orderRemoved(final OrderEvent e) {
                final Order removedOrder = (Order) e.getSource();
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        OrderListItemController controller = controllerMap.remove(removedOrder);
                        view.removeListItem(controller.getListView());
                    }
                });
            }
                
            
        
    }
        
    /**
     * Shows the view associated with this controller.
     */
    public final void showView() {
       view.setVisible(true);
    }

    /**
     * Hides the view associated with this controller. 
     */
    public final void hideView() {
        WaiterController.view.setVisible(false);
    }


}

