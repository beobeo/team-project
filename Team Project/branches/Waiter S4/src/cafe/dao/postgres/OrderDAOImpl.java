package cafe.dao.postgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cafe.Employee;
import cafe.Order;
import cafe.OrderState;
import cafe.dao.DataAccessException;
import cafe.dao.EmployeeDAO;
import cafe.dao.OrderDAO;
import cafe.dao.OrderItemDAO;
import cafe.dao.SittingDAO;
import cafe.dao.jdbc.ConnectionFactory;

/**
 * A data access object used to manipulate {@link Order} objects that have
 * persisted to a PostgreSQL database.
 * 
 * @author Robert Kardjaliev
 * @author Michael Winter
 */
public final class OrderDAOImpl implements OrderDAO {
	/**
	 * The source of JDBC connections used to perform queries against the
	 * underlying database.
	 */
	private ConnectionFactory connectionFactory;
    /**
     * The data access object used to obtain {@link Sitting} instances from the
     * database.
     */
	private SittingDAO sittingDAO;
    /**
     * The data access object used to obtain {@link Employee} instances from the
     * database.
     */
    private EmployeeDAO employeeDAO;
    /**
     * The data access object used to obtain {@link OrderItem} instances from the
     * database.
     */
    private OrderItemDAO orderItemDAO;

	
    /**
     * Constructs a data access object for {@link Order}s that will operate over
     * the given connection.
     * 
     * @param connectionFactory
     *            the factory object that will provide a database connection.
     * @param orderItemDAO
     *            the data access object used to supply {@link Sitting}
     *            instances.
     * @param sittingDAO
     *            the data access object used to supply {@link Employee}
     *            instances.
     * @param employeeDAO
     *            the data access object used to supply {@link OrderItem}
     *            instances.
     * @throws NullPointerException
     *             if the specified factory or data access objects are
     *             {@code null}.
     */
    public OrderDAOImpl(final OrderItemDAO orderItemDAO, final SittingDAO sittingDAO,
            final EmployeeDAO employeeDAO, final ConnectionFactory connectionFactory) {
        if (orderItemDAO == null) {
            throw new NullPointerException("orderItemDAO is null");
        }
        if (sittingDAO == null) {
            throw new NullPointerException("sittingDAO is null");
        }
        if (employeeDAO == null) {
            throw new NullPointerException("employeeDAO is null");
        }
        if (connectionFactory == null) {
            throw new NullPointerException("connectionFactory is null");
        }
        this.orderItemDAO = orderItemDAO;
        this.sittingDAO = sittingDAO;
        this.employeeDAO = employeeDAO;
        this.connectionFactory = connectionFactory;
    }

	
    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DishDAO#findOrder(int)
     */
    @Override
    public Order findOrder(final int id) {
        final Connection connection = connectionFactory.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT sitting_id, progress, time_placed, time_ready, time_delivered, taken_by"
                + " FROM \"order\" WHERE id = ?")) {
            statement.setInt(1, id);

            final ResultSet result = statement.executeQuery();
            if (result.next()) {
                return new Order(id,
                        sittingDAO.findSitting(result.getInt("sitting_id")),
                        OrderState.valueOf(result.getString("progress")),
                        result.getTimestamp("time_placed"),
                        result.getTimestamp("time_ready"),
                        result.getTimestamp("time_delivered"),
                        employeeDAO.findEmployee(result.getInt("taken_by")),
                        orderItemDAO.selectOrderItems(id));
            }
        } catch (final SQLException e) {
            throw new DataAccessException(e);
        }
        return null;
    }
	
	/*
	 * (non-Javadoc)
	 * @see cafe.dao.OrderDAO#selectUndeliveredOrders()
	 */
	@Override
	public List<Order> selectUndeliveredOrders() {
	    final Connection connection = connectionFactory.getConnection();
		final List<Order> orders = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement(
				"SELECT id, sitting_id, progress, time_placed, time_ready, time_delivered, taken_by"
		        + " FROM \"order\""
		        + " WHERE progress IN ('CONFIRMED', 'PREPARING', 'READY')"
		        + " ORDER BY time_placed")) {
			ResultSet result = statement.executeQuery();
			while (result.next()) {
			    final int id = result.getInt("id");
			    orders.add(new Order(id,
			            sittingDAO.findSitting(result.getInt("sitting_id")),
			            OrderState.valueOf(result.getString("progress")),
			            result.getTimestamp("time_placed"),
			            result.getTimestamp("time_ready"),
			            result.getTimestamp("time_delivered"),
			            employeeDAO.findEmployee(result.getInt("taken_by")),
			            orderItemDAO.selectOrderItems(id)));
			}
			Collections.sort(orders);
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}
		return orders;
	}

    /*
     * (non-Javadoc)
     * @see cafe.dao.OrderDAO#createOrder(int)
     */
    @Override
    public int createOrder(final int sitting) {
        return createOrder(sitting, -1);
    }
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.dao.OrderDAO#createOrder(int, int)
	 */
	@Override
	public int createOrder(final int sittingId, final int waiterId) {
		final Connection connection = connectionFactory.getConnection();
		try (PreparedStatement statement = connection.prepareStatement(
				"INSERT INTO \"order\""
						+ " (sitting_id, progress, taken_by)"
						+ " VALUES (?, ?, ?)",
				PreparedStatement.RETURN_GENERATED_KEYS)) {
			// CHECKSTYLE IGNORE MagicNumberCheck FOR NEXT 8 LINES
			statement.setInt(1, sittingId);
			if (waiterId == -1) {
				statement.setString(2, OrderState.PENDING.name());
				statement.setNull(3, Types.INTEGER);
			} else {
				statement.setString(2, OrderState.CONFIRMED.name());
				statement.setInt(3, waiterId);
			}
			statement.executeUpdate();

			final ResultSet result = statement.getGeneratedKeys();
			result.next();
			return result.getInt(1);
		} catch (final SQLException e) {
			throw new DataAccessException(e);
		}
	}


    /* (non-Javadoc)
     * @see cafe.dao.OrderDAO#updateOrder(cafe.Order)
     */
    @Override
    public void updateOrder(final Order order) {
        final Connection connection = connectionFactory.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE \"order\""
                + " SET progress = CAST(? AS order_progress), time_ready = ?,"
                        + " time_delivered = ?, taken_by = ?"
                + " WHERE id = ?")) {
            final Timestamp whenReady = order.getWhenReady();
            final Timestamp whenDelivered = order.getWhenDelivered();
            final Employee waiter = order.getWaiter();
            //CHECKSTYLE IGNORE MagicNumberCheck FOR NEXT 17 LINES
            statement.setString(1, order.getProgress().name());
            if (whenReady != null) {
                statement.setTimestamp(2, whenReady);
            } else {
                statement.setNull(2, Types.TIMESTAMP);
            }
            if (whenDelivered != null) {
                statement.setTimestamp(3, whenDelivered);
            } else {
                statement.setNull(3, Types.TIMESTAMP);
            }
            if (waiter != null) {
                statement.setInt(4, waiter.getId());
            } else {
                statement.setNull(4, Types.INTEGER);
            }
            statement.setInt(5, order.getId());
            statement.executeUpdate();
        } catch (final SQLException e) {
            throw new DataAccessException(e);
        }
    }
}
