package cafe;

import static org.junit.Assert.assertEquals;

import java.util.Collections;

import org.junit.Test;

/**
 * Unit tests for the {@link Order} class.
 * 
 * @author Hannah Cooper
 */
public final class OrderModelTest {
	@Test
	/*
	 * public void test() { OrderModel orderTest = new OrderModel(); Dish dish =
	 * new Dish(1, "Test 1", "This is a test!", 3.40f, 0, "Test", true,
	 * Collections.<Ingredient, Integer> emptyMap(),
	 * Collections.<DietaryInformation> emptySet()); Dish dish1 = new Dish(2,
	 * "Test 2", "This is another test!", 2.40f, 0, "Test", true,
	 * Collections.<Ingredient, Integer> emptyMap(),
	 * Collections.<DietaryInformation> emptySet());
	 * 
	 * orderTest.addItem(dish); orderTest.addItem(dish1);
	 * 
	 * assertEquals("Test for correct addition of dish prices", 5.80f,
	 * orderTest.getTotalPrice(), 0.01f); }
	 */
	public void test2() {
		OrderModel orderTest2 = new OrderModel();
		Dish dish = new Dish(1, "Test 1", "This is a test!", 3.40f, 0, "Test",
				null, true, Collections.<Ingredient, Integer> emptyMap(),
				Collections.<DietaryInformation> emptySet());
		OrderItem item = new OrderItem(dish, 1, true);

		Dish dish1 = new Dish(2, "Test 2", "This is another test!", 2.40f, 0,
				"Test", null, true, Collections.<Ingredient, Integer> emptyMap(),
				Collections.<DietaryInformation> emptySet());
		OrderItem item1 = new OrderItem(dish1, 2, false);

		orderTest2.addItem(item);
		orderTest2.addItem(item1);

		assertEquals("Test for correct addition of dish prices", 8.20f,
				orderTest2.getTotalPrice(), 0.01f);
	}
}
