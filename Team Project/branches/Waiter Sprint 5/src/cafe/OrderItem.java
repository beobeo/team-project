package cafe;

/**
 * Represents an OrderItem object that contains the dishes in an Order, its
 * current progress, the time at which it was taken, time spent on it so far,
 * time taken to finish it, whether it was suggested by a waiter, and the waiter
 * that took the order.
 * 
 * @author Robert Kardjaliev
 */
public final class OrderItem {
	/**
	 * An integer that stores a dish's id.
	 */
	private final Dish dish;
	/**
	 * The number of times a single dish is ordered in an Order.
	 */
	private int quantity;
    /**
     * Indicates whether a dish was suggested and if {@code true} it's
     * automatically assumed that it was by the waiter who took the order.
     */
	private final boolean suggested;
	

    /**
     * Constructs a new {@code OrderItem} instance.
     * 
     * @param dish
     *            the ordered {@code Dish}.
     * @param quantity
     *            the number of times a single dish is ordered in an Order.
     *            related to an Order.
     * @param suggested
     *            if {@code true}, this item was suggested by the waiter that
     *            took the order; if {@code false}, the customer chose this
     *            item.
     */
    public OrderItem(final Dish dish, final int quantity,
            final boolean suggested) {
        this.dish = dish;
        this.quantity = quantity;
        this.suggested = suggested;
    }

	/**
	 * Returns the ordered {@code Dish}.
	 * 
	 * @return the ordered {@code Dish}.
	 */
	public Dish getDish() {
		return dish;
	}
    
    /**
     * Returns the number of times this dish was requested in the respective
     * {@code Order}.
     * 
     * @return the number of times a dish was ordered.
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Sets the number of times this dish is requested in the respective
     * {@code Order}.
     * 
     * @param quantity
     *            the number of dishes ordered (must be greater-than zero).
     * @throws IllegalArgumentException
     *             if {@code quantity} is not positive.
     */
    public void setQuantity(final int quantity) {
        if (quantity < 1) {
            throw new IllegalArgumentException("quantity must be at least 1");
        }
        this.quantity = quantity;
    }

    /**
     * Returns whether this item was at the suggestion of a waiter.
     * 
     * @return {@true} if the waiter that took the order suggested this item;
     *         {@code false} otherwise.
     */
	public boolean isSuggested() {
		return suggested;
	}
}
