package cafe;

/**
 * A sitting encapsulates an allocated table, its orders, and its payments. A
 * sitting may be booked in advance or occur as a result of passing trade. The
 * default length of a sitting is two hours.
 * 
 * @author Michael Winter
 */
public final class Sitting {
    /**
     * The unique identifier for this instance.
     */
    private int id;
    /**
     * The allocated table.
     */
    private Table table;
    
    
    /**
     * Constructs a new {@link Sitting} instance.
     * 
     * @param id
     *            the unique identifier for this instance.
     * @param table
     *            the allocated table.
     */
    public Sitting(final int id, final Table table) {
        this.id = id;
        this.table = table;
    }
    
    /**
     * Returns the unique identifier for this sitting.
     * 
     * @return the identifier.
     */
    public int getId() {
        return id;
    }


    /**
     * Returns the table allocated for this sitting.
     * 
     * @return the table.
     */
    public Table getTable() {
        return table;
    }
}
