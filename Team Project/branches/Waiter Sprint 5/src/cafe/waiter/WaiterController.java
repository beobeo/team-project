package cafe.waiter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.SwingUtilities;

import cafe.Employee;
import cafe.Order;
import cafe.OrderEvent;
import cafe.OrderEventListener;
import cafe.OrderItem;
import cafe.OrderState;
import cafe.Sitting;
import cafe.Table;
import cafe.dao.OrderDAO;

/**
 * 
 */

/**
 * @author Jonathan Hercock
 *
 */
public class WaiterController {
	
	/**used to update the database.   */
    private static OrderDAO data;
    
	/** constant value for test order. */
    private static final List<OrderItem> OWNEDWINDOWS =
    		new ArrayList<OrderItem>();
	
    /** employee created for table. */
    private static final Employee EMP1 = new Employee(12345, "Will", "Smith");
   
    /** The table for order. */
    private static final Table TABLE = new Table(1, EMP1);
     
    /** Adds a sitting for order. */
    private static Sitting sitting = new Sitting(1, TABLE);
    
    /** An order to be tested for when it's ready. */
    private static final Order TEST_ORDER = new Order(1, sitting, OrderState.READY,
    		null, null, null, null, OWNEDWINDOWS);
	/**
	 *  The Waiter's interface.
	 */
	private static WaiterGUI view;
	/**
	 *  The model.
	 */
	private static WaiterModel model;

	/**
	 * A mapping between orders and the controller for its corresponding entry
	 * in the list.
	 */
	private Map<Order, WaiterOLIController> controllerMap = new HashMap<>();


	/** A constructor for the given view.
	 * 
	 * @param view - the waiter's view.
	 * @param model - the waiter model.
	 */
	public WaiterController(final WaiterGUI view, final WaiterModel model) {
		WaiterController.view = view;
		WaiterController.model = model;

		WaiterController.model.addOrderModelListener(new OrderStateListener());
		WaiterController.model.refresh();
	}

	/**
	 * Listens for updates to orders, invoking UI updates as appropriate.
	 */
	private class OrderStateListener implements OrderEventListener {
		/*
		 * (non-Javadoc)
		 * @see cafe.OrderEventListener#orderAdded(cafe.OrderEvent)
		 */
		@Override
		public void orderAdded(final OrderEvent e) {
			final Order addedOrder = (Order) e.getSource();
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					final WaiterODTModel tableModel = new WaiterODTModel(addedOrder);
					WaiterODView detailView = new WaiterODView(tableModel);
					WaiterOLIView listView = new WaiterOLIView();

					WaiterOLIController listController = new WaiterOLIController(
							listView, detailView);
					listController.addAdvanceButtonActionListener(new ActionListener() {
						@Override
						public void actionPerformed(final ActionEvent e) {
							//if (addedOrder.getProgress() == OrderState.READY
									//    || addedOrder.getProgress() == OrderState.PENDING)
								//     {    
							addedOrder.advanceProgress();
							model.updateOrder(addedOrder);
							//     }
						}
					});

					listController.addCancelButtonActionListener(new ActionListener() {
						@Override
						public void actionPerformed(final ActionEvent e) {
							addedOrder.cancel();
							model.updateOrder(addedOrder);
						}
					});

					listController.refresh(addedOrder);
					controllerMap.put(addedOrder, listController);
					view.addListItem(listView);
				}
			});
		}

		@Override
		public void orderChanged(final OrderEvent e) {
			final Order changedOrder = (Order) e.getSource();
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					controllerMap.get(changedOrder).refresh(changedOrder);
				}
			});
		}

		@Override
		public void orderRemoved(final OrderEvent e) {
			final Order removedOrder = (Order) e.getSource();
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					WaiterOLIController controller = controllerMap.remove(removedOrder);
					view.removeListItem(controller.getListView());
				}
			});
		}



	}

	/**
	 * Shows the view associated with this controller.
	 */
	public final void showView() {
		view.setVisible(true);
	}

	/**
	 * Hides the view associated with this controller. 
	 */
	public final void hideView() {
		WaiterController.view.setVisible(false);
	}

	/**
	 * called when order comfirmed is called.
	 */
	public static final void comfirmOrder() {
		if (TEST_ORDER.getProgress() == OrderState.READY) {
			//progress order.
			TEST_ORDER.advanceProgress();  					
			//update database 
			data.updateOrder(TEST_ORDER);
		}	
	}
}

