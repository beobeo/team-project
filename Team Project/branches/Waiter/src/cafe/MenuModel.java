package cafe;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Obtains available dishes from the database.
 * 
 * TODO: Provide filtering and sorting.
 * 
 * @author Michael Winter
 */
public class MenuModel {
	/**
	 * The list of dishes last retrieved from the database.
	 */
	private List<Dish> dishes = new java.util.LinkedList<>();
	
	/**
	 * Creates the model.
	 */
	public MenuModel() {
		loadDishes();
	}
	
	/**
	 * Obtained an ordered list of dishes available from the menu. The returned list is immutable.
	 * 
	 * @return an unmodifiable, ordered list of available dishes.
	 */
	public final List<Dish> getDishes() {
		return Collections.unmodifiableList(dishes);
	}
	
	
    /**
     * Generates a dummy set of dishes.
     * 
     * TODO: Load items from the database.
     */
    private void loadDishes() {
        Dish dish = null;
        Map<Ingredient, Integer> ingredients = null;

        ingredients = new HashMap<>();
        ingredients.put(new Ingredient(1, "Minced beef", 0, 2000), 120);
        ingredients.put(new Ingredient(2, "Maris Piper potatoes", 0, 100), 1);
        ingredients.put(new Ingredient(3, "Cheddar cheese", 0, 1000), 50);
        ingredients.put(new Ingredient(4, "Egg pasta", 0, 100), 5);
        dish = new Dish(1, "Lasagne",
                "Homemade lasagne. Served with chips and salad.", 8.95f, 1080,
                "Main course", true, ingredients);
        dishes.add(dish);

        ingredients = new HashMap<>();
        ingredients.put(new Ingredient(5, "Red Onion", 0, 50), 1);
        ingredients.put(new Ingredient(6, "Nutmeg", 0, 100), 1);
        ingredients.put(new Ingredient(3, "Cheddar cheese", 0, 1000), 25);
        ingredients.put(new Ingredient(7, "Egg", 0, 120), 3);
        ingredients.put(new Ingredient(8, "Tomatoes", 0, 50), 2);
        dish = new Dish(2, "Butternut squash ravioli",
                "Homemade Butternut squash ravioli with fried sage leaves and tomatoes.", 7.45f, 900,
                "Main course", true, ingredients);
        dishes.add(dish);

        ingredients = new HashMap<>();
        ingredients.put(new Ingredient(9, "Mozzarella", 0, 1000), 50);
        ingredients.put(new Ingredient(10, "ricotta", 0, 1000), 50);
        ingredients.put(new Ingredient(3, "Cheddar cheese", 0, 1000), 50);
        ingredients.put(new Ingredient(11, "Gorgonzola", 0, 1000), 50);
        dish = new Dish(3, "4 Cheese Pizza",
                "Freshly made base with 4 toppings of cheese, Mozzarellam, Ricotta, Gorgonzola and Chedder.", 8.95f, 1000,
                "Main course", true, ingredients);
        dishes.add(dish);

        ingredients = new HashMap<>();
        ingredients.put(new Ingredient(12, "Sammon", 0, 500), 1);
        ingredients.put(new Ingredient(13, "Garlic", 0, 100), 1);
        ingredients.put(new Ingredient(14, "Mixed herbs", 0, 250), 1);
        dish = new Dish(4, "Grilled Sammon",
                "Grilled Sammon on a bed of salad.", 7.95f, 850,
                "Main course", true, ingredients);
        dishes.add(dish);
    }
}
