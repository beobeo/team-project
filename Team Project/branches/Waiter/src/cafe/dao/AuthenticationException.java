package cafe.dao;

/**
 * Thrown to indicate that the authentication details were rejected by the
 * database server.
 * 
 * @author Michael Winter
 */
public final class AuthenticationException extends RuntimeException {
    /**
     * Unique serialisation identifier.
     */
    private static final long serialVersionUID = -7950274648871881276L;

    /**
     * Constructs an {@code AuthenticationException} with no detail message.
     */
    public AuthenticationException() {
        super();
    }

    /**
     * Constructs an {@code AuthenticationException} with the specified detail
     * message.
     * 
     * @param message
     *            the detail message.
     */
    public AuthenticationException(final String message) {
        super(message);
    }

    /**
     * Constructs an {@code AuthenticationException} with the specified detail
     * message and cause.
     * 
     * <p>
     * Note that the detail message associated with {@code cause} is not
     * automatically incorporated in this exception's detail message.
     * 
     * @param message
     *            the detail message.
     * @param cause
     *            the cause. A value of {@code null} is acceptable and indicates
     *            that the cause is either unknown or nonexistent.
     */
    public AuthenticationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs an {@code AuthenticationException} with the specified cause
     * and a detail message of {@code (cause == null) ? null : cause.toString()}
     * (which typically contains the class and detail message of {@code cause}).
     * 
     * @param cause
     *            the cause. A value of {@code null} is acceptable and indicates
     *            that the cause is either unknown or nonexistent.
     */
    public AuthenticationException(final Throwable cause) {
        this((cause == null) ? null : cause.toString(), cause);
    }
}
