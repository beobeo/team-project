package cafe.dao;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;

/**
 * This class is an abstract factory that is responsible for creating an
 * appropriate driver-specific factory that, in turn, produces data access
 * objects (DAOs) used to interact with the underlying storage mechanism.
 * 
 * <p>
 * Initially, an instance is obtained by calling the
 * {@link #getInstance(String, String, Class)} method with appropriate
 * connection credentials, and a reference to a concrete class that implements
 * the factory:
 * 
 * <pre>
 * DAOFactory factory = DAOFactory.getInstance(&lt;user&gt;, &lt;password&gt;,
 *         DAOFactoryImpl.class);
 * </pre>
 * 
 * <p>
 * From here, the required DAOs can be used to manipulate the underlying data:
 * 
 * <pre>
 * IngredientDAO ingredientDAO = factory.getIngredientDAO();
 * // Create a new ingredient within the data store
 * int id = ingredientDAO.createIngredient("Parsnips", 0.25f, 50);
 * // Load an instance of the newly-created ingredient
 * Ingredient parsnip = ingredientDAO.findIngredient(id);
 * // Work with and modify the ingredient
 * ...
 * // Update the data store with changes in the ingredient
 * ingredientDAO.update(parsnip);
 * </pre>
 * 
 * @author Michael Winter
 */
public abstract class DAOFactory implements AutoCloseable {
    /**
     * A map for pooling factory instances that share the same the authentication credentials.
     */
    private static final Map<Class<? extends DAOFactory>, Map<String, DAOFactory>> IMPLEMENTATIONS
            = new HashMap<>();

    /**
     * Creates a data access object factory that will use the specified user
     * credentials.
     * 
     * @param user
     *            the name used to authenticate access to the data store (can be
     *            {@code null}).
     * @param password
     *            the password used to authenticate the given user (can be
     *            {@code null}).
     * @param implementationClass
     *            the concrete type to instantiate.
     * @return the data access object factory.
     * @throws AuthenticationException
     *             if authentication is required and the user name is unknown or
     *             {@code null}, or the password is incorrect or {@code null}.
     * @throws NullPointerException
     *             if {@code implementationClass} is {@code null}.
     * @throws ConnectionException
     *             if a connection error occurs whilst attempting to
     *             authenticate the connection.
     */
    public static DAOFactory getInstance(final String user,
            final String password,
            final Class<? extends DAOFactory> implementationClass)
            throws ConnectionException {
        if (implementationClass == null) {
            throw new NullPointerException("implementationClass cannot be null");
        }

        // Hash credentials so they're not stored as plain text in the instance
        // map.
        final String credentials = getObfuscatedCredentials(user, password);
        // Determine whether there's existing factory instances using the
        // provided class.
        final Map<String, DAOFactory> instances = getAuthenticatedInstances(implementationClass);
        // Obtain an existing instance using the specified credentials.
        DAOFactory instance = instances.get(credentials);
        // If there isn't an instantiated implementation, create one and store
        // it.
        if (instance == null) {
            try {
                instance = implementationClass.newInstance();
            } catch (final IllegalAccessException | InstantiationException e) {
                throw new DAOConfigurationError(e);
            }
            instance.setCredentials(user, password);
            instances.put(credentials, instance);
        }
        return instance;
    }

    
    /**
     * Returns a mapping of authenticated factory instances for the specified
     * implementation type.
     * 
     * @param implementationClass
     *            the class of a particular type of factory implementation.
     * @return the authenticated factory instances.
     */
    private static Map<String, DAOFactory> getAuthenticatedInstances(
            final Class<? extends DAOFactory> implementationClass) {
        Map<String, DAOFactory> instances;
        if (IMPLEMENTATIONS.containsKey(implementationClass)) {
            instances = IMPLEMENTATIONS.get(implementationClass);
        } else {
            instances = new HashMap<>();
            IMPLEMENTATIONS.put(implementationClass, instances);
        }
        return instances;
    }
    
    /**
     * Obfuscates a given user name and password pair.
     * 
     * @param user
     *            the user name.
     * @param password
     *            the password.
     * @return an obfuscated string representing the given credentials.
     */
    private static String getObfuscatedCredentials(final String user,
            final String password) {
        MessageDigest hash = null;
        try {
            hash = MessageDigest.getInstance("SHA-1");
            hash.update((user + '#' + password).getBytes("UTF-8"));
        } catch (final Exception e) {
            // Impossible: Implementation of SHA-1 and support for the UTF-8
            // character set is guaranteed.
            throw new InternalError(e.getMessage());
        }
        return Base64.encodeBase64String(hash.digest());
    }

    /**
     * Returns a data access object for manipulating persisted {@link cafe.Dish
     * Dish} objects.
     * 
     * @return the {@code Dish} data access object.
     */
    public abstract DishDAO getDishDAO();

    /**
     * Returns a data access object for manipulating persisted
     * {@link cafe.Ingredient Ingredient} objects.
     * 
     * @return the {@code Ingredient} data access object.
     */
    public abstract IngredientDAO getIngredientDAO();

    /**
     * Sets the credentials used to access the underlying data store. This
     * method should not be called once operations have begun on the data store.
     * 
     * @param user
     *            the name used to authenticate access to the data store.
     * @param password
     *            the password used to authenticate the given user.
     * @throws AuthenticationException
     *             if authentication is required and the user name is unknown or
     *             {@code null}, or the password is incorrect or {@code null}.
     * @throws ConnectionException
     *             if a connection error occurs whilst attempting to
     *             authenticate the connection.
     */
    protected abstract void setCredentials(String user, String password)
            throws ConnectionException;
}
