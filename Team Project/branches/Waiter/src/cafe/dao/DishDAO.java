package cafe.dao;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import cafe.Dish;

/**
 * A data access object used to manipulate persisted {@link Dish} objects.
 * 
 * TODO: Wrap IOException exceptions as another, more friendly exception where
 * possible. Lookup-type methods should only need to raise an exception if the
 * connection is lost.
 * 
 * @author Michael Winter
 * @author Robert Kardjaliev
 */
public interface DishDAO {
    /**
     * Given an identifier, this method loads and returns a new instance of the
     * specified {@link Dish}.
     * 
     * @param id
     *            the identifier.
     * @return the specified {@code Dish}.
     * @throws IOException
     *             if an error occurs while accessing the data store.
     */
    Dish findDish(int id) throws IOException;
    /**
     * @param course - dish category
     * @return the dishes in the course
     * @throws IOException 
     */
    List<Dish> selectDishesByCourse(String course) throws IOException;
    /**
     * 
     * @return the dishes set to visible from the database
     * @throws IOException 
     */
    Set<Dish> selectAvailableDishes() throws IOException;
}
