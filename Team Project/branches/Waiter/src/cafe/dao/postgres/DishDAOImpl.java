package cafe.dao.postgres;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cafe.Dish;
import cafe.Ingredient;
import cafe.dao.DishDAO;
import cafe.dao.JdbcConnectionFactory;

/**
 * A data access object used to manipulate {@link Dish} objects that have
 * persisted to a PostgreSQL database.
 * 
 * TODO: Wrap IOException exceptions as another, more friendly exception where
 * possible. Lookup-type methods should only need to raise an exception if the
 * connection is lost.
 * 
 * @author Michael Winter
 * @author Hannah Cooper
 * @author Robert Kardjaliev
 */
public final class DishDAOImpl implements DishDAO {
    /**
     * The source of JDBC connections used to perform queries against the
     * underlying database.
     */
    private JdbcConnectionFactory connectionFactory;

    /**
     * Given a Dish identifier and a connection, this method returns the
     * ingredients (and quantity) necessary to prepare that dish as a map.
     * 
     * @param dish
     *            the dish to be prepared.
     * @param connection
     *            a connection over which the query will be performed.
     * @return the mapping of ingredients-to-quantity required for the specified
     *         dish.
     * @throws IOException
     *             if an error occurs while accessing the data store.
     */
    private static Map<Ingredient, Integer> selectIngredients(final int dish,
            final Connection connection) throws IOException {
        final Map<Ingredient, Integer> ingredients = new HashMap<>();
        try (final PreparedStatement statement = connection.prepareStatement(
                "SELECT id, name, price, stock, amount"
                + " FROM ingredient JOIN dish_ingredient ON id = ingredient_id"
                + " WHERE dish_id = ? ORDER BY price")) {
            statement.setInt(1, dish);
            final ResultSet result = statement.executeQuery();
            while (result.next()) {
                ingredients.put(new Ingredient(result.getInt("id"),
                        result.getString("name"), result.getFloat("price"),
                        result.getInt("stock")), result.getInt("amount"));
            }
        } catch (final SQLException e) {
            throw new IOException(e);
        }
        return ingredients;
    }

    /**
     * Constructs a data access object for {@link Dish}es that will operate over
     * the given connection.
     * 
     * @param connectionFactory
     *            the factory object that will provide a database connection.
     * @throws NullPointerException
     *             if the specified factory is {@code null}.
     */
    public DishDAOImpl(final JdbcConnectionFactory connectionFactory) {
        if (connectionFactory == null) {
            throw new NullPointerException();
        }
        this.connectionFactory = connectionFactory;
    }


    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DishDAO#selectDishesByCourse(java.lang.String)
     */
    @Override
    public List<Dish> selectDishesByCourse(final String course)
            throws IOException {
        final Connection connection = connectionFactory.getConnection();
        final List<Dish> dishes = new ArrayList<>();
        try (final PreparedStatement statement = connection.prepareStatement(
                "SELECT id, name, description, price, calories FROM dish WHERE category = ?")) {
            statement.setString(1, course);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                final int id = result.getInt("id");
                dishes.add(new Dish(id, result.getString("name"),
                        result.getString("description"),
                        result.getFloat("price"), result.getInt("calories"),
                        course, result.getBoolean("visible"),
                        selectIngredients(id, connection)));
            }
        } catch (final SQLException e) {
            throw new IOException(e);
        }
        return dishes;
    }

    @Override
    public Dish findDish(final int id) throws IOException {
        Connection connection = connectionFactory.getConnection();
        Dish dish = null;
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM dish WHERE id = ?")) {
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                dish = new Dish(id, result.getString("name"),
                        result.getString("description"),
                        result.getFloat("price"), result.getInt("calories"),
                        result.getString("category"),
                        result.getBoolean("visible"), selectIngredients(id, connection));
            }
        } catch (final SQLException e) {
            throw new IOException(e);
        }
        return dish;
    }
    
    @Override
    public Set<Dish> selectAvailableDishes() throws IOException {
        Connection connection = connectionFactory.getConnection();
        Set<Dish> dishes = new HashSet<>();
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM dish WHERE visible = true ORDER BY name")) {
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                final int id = result.getInt("id");
                dishes.add(new Dish(id, result.getString("name"),
                        result.getString("description"),
                        result.getFloat("price"), result.getInt("calories"),
                        result.getString("category"),
                        result.getBoolean("visible"), selectIngredients(id, connection)));
            }
        } catch (final SQLException e) {
            throw new IOException(e);
        }
        return dishes;
    }
}