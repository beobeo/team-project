/**
 * Defines concrete implementations of the DAOs declared in the parent package.
 * 
 * @author Michael Winter
 */
package cafe.dao.postgres;