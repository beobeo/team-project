package cafe.dao;

import java.util.List;

import cafe.Dish;

/**
 * A data access object used to manipulate persisted {@link Dish} objects.
 * 
 * @author Michael Winter
 * @author Robert Kardjaliev
 */
public interface DishDAO {
    /**
     * Given an identifier, this method loads and returns a new instance of the
     * specified {@link Dish}.
     * 
     * @param id
     *            the identifier.
     * @return the specified {@code Dish}.
     * 
     */
    Dish findDish(int id);
    
    /**
     * @param course - dish category as in main, dessert, etc
     * @return the dishes in the selected course
     * @throws DataAccessException
     *             if an error occurs while accessing the data store.
     */
    List<Dish> selectDishesByCourse(String course);
    
    /**
     * 
     * @return the set of dishes set to visible from the database
     * @throws DataAccessException
     *             if an error occurs while accessing the data store.
     */
    List<Dish> selectAvailableDishes();
}
