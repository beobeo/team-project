/**
 * 
 */
package cafe.dao.postgres;

import cafe.dao.DishDAO;
import cafe.dao.EmployeeDAO;
import cafe.dao.IngredientDAO;
import cafe.dao.OrderDAO;
import cafe.dao.OrderItemDAO;
import cafe.dao.SittingDAO;
import cafe.dao.TableDAO;

/**
 * A simplified DAOFactory implementation that uses the real data access
 * object implementations, but targets the test database.
 * 
 * @author Michael Winter
 */
public final class DefaultTestDAOFactoryImpl extends AbstractTestDAOFactoryImpl {
    /* (non-Javadoc)
     * @see cafe.dao.DAOFactory#getDishDAO()
     */
    @Override
    public DishDAO getDishDAO() {
        return new DishDAOImpl(getIngredientDAO(), this);
    }

    /* (non-Javadoc)
     * @see cafe.dao.DAOFactory#getIngredientDAO()
     */
    @Override
    public IngredientDAO getIngredientDAO() {
        return new IngredientDAOImpl(this);
    }

    /* (non-Javadoc)
     * @see cafe.dao.DAOFactory#getSittingDAO()
     */
    @Override
    public SittingDAO getSittingDAO() {
        return new SittingDAOImpl(getTableDAO(), this);
    }

    /* (non-Javadoc)
     * @see cafe.dao.DAOFactory#getTableDAO()
     */
    @Override
    public TableDAO getTableDAO() {
        return new TableDAOImpl(getEmployeeDAO(), this);
    }

    /* (non-Javadoc)
     * @see cafe.dao.DAOFactory#getOrderDAO()
     */
    @Override
    public OrderDAO getOrderDAO() {
        return new OrderDAOImpl(getOrderItemDAO(), getSittingDAO(), getEmployeeDAO(), this);
    }

    /* (non-Javadoc)
     * @see cafe.dao.DAOFactory#getOrderItemDAO()
     */
    @Override
    public OrderItemDAO getOrderItemDAO() {
        return new OrderItemDAOImpl(getDishDAO(), this);
    }

    /* (non-Javadoc)
     * @see cafe.dao.DAOFactory#getEmployeeDAO()
     */
    @Override
    public EmployeeDAO getEmployeeDAO() {
        return new EmployeeDAOImpl(this);
    }
}
