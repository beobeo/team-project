package cafe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;

import cafe.dao.DAOFactory;
import cafe.dao.OrderDAO;
import cafe.dao.postgres.DAOFactoryImpl;
import cafe.kitchen.KitchenController;
import cafe.kitchen.KitchenModel;
import cafe.kitchen.KitchenView;

/**
 * @author Jonathan Hercock
 *
 */
public final class StaffApplicationController {

    /** A carbon copy of the Client method (including the following javadoc message) :
     * Initialises and runs the application. The actual launching of the
     * application is delegated to this method in order to ensure initialisation
     * occurs on Swing's event-dispatching thread.  
     */
    private static void launch() {
        try {
            if (SwingUtilities.isEventDispatchThread()) {
                final StaffApplicationController application = new StaffApplicationController();
                application.run();
            } else {
                SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                        launch();
                    }
                });
            }
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * The Controller for the employee's Log In Screen.
     */
    private LogInController loginController;
    
    /**
     * The Controller for the Waiter's view.
     */
    private WaiterController waiterController;
    
    /**
     * The Controller for the Kitchen's view.
     */
    private KitchenController kitchenController;
    
    /** The constructor for the Application. Adds all relevant listeners to 
     *  a new instance of the controllers.
     */
    private StaffApplicationController() {
        final LogInScreen loginScreen = new LogInScreen();
        loginController = new LogInController(loginScreen);
        loginController.addManagementLogInListener(new ManagementLogInListener());
        loginController.addKitchenLogInListener(new KitchenLogInListener());
        loginController.addWaiterLogInListener(new WaiterLogInListener());
        
        final WaiterGUI waiterScreen = new WaiterGUI();
        waiterController = new WaiterController(waiterScreen);        
        
        DAOFactory factory = DAOFactory.getInstance("zyvc215", "IYIP1845",
                DAOFactoryImpl.class);

        final OrderDAO orderData = factory.getOrderDAO();
        final KitchenView kitchenScreen = new KitchenView();
        final KitchenModel kitchenModel = new KitchenModel(orderData);
        kitchenController = new KitchenController(kitchenModel, kitchenScreen);
    }       
    
    /**
     * Listens for button click events originating from the Management button.
     *  It hides the log in screen and (TODO:) displays the Manager's view.
     */
    private class ManagementLogInListener implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent e) {
            loginController.hideView();
            //TODO: Show Management View.
        }
    }
    
    /**
     * Listens for button click events originating from the Kitchen Staff button.
     *  It hides the log in screen and displays the Kitchen Staff's view.
     */
    private class KitchenLogInListener implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent e) {
            loginController.hideView();
          kitchenController.showView();
        }
    }   
    
    /**
     * Listens for button click events originating from the Kitchen Staff button.
     *  It hides the log in screen and (TODO:) displays the Kitchen Staff's view.     *
     */
    private class WaiterLogInListener implements ActionListener {
        @Override
        public void actionPerformed(final ActionEvent e) {
            loginController.hideView();
            waiterController.showView();
           
        }
    }
    
    /** Starts up the application.     */
    private void run() {
        loginController.showView();
    }

    /**
     * The main entry point for the application.
     * 
     * @param args the command line arguments passed to the application.
     */
    public static void main(final String[] args) {
        launch();
    }

}
