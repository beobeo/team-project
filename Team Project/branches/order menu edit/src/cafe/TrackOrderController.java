/**
 * 
 */
package cafe;

import java.awt.event.ActionListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Observes user events from the Track Order View and directs behaviour accordingly, 
 * invoking the model where appropriate and referring events to higher-level observing
 * controllers.
 * 
 * @author Robert Kardjaliev
 * @author Jonathan Hercock
 * @author Hannah Cooper
 */



public class TrackOrderController {
	/**
	 * Hides the view associated with this controller.
	 */
	public final void hideView() {
		view.setVisible(false);
	}

	/**
	 * Shows the view associated with this controller.
	 */
	public final void showView() {
		view.setVisible(true);
	}

	/** The model. */
	private TrackOrderModel model;
	/** The view. */
	private TrackOrderView view;

	/**
	 * Instantiates the fields of the Track Order Controller.
	 *
	 * @param view - The View.
	 * @param model - The Model.
	 */
	public TrackOrderController(final TrackOrderView view, final TrackOrderModel model) { 
		this.view = view; 
		this.model = model;
		setProgressValue();
		refreshProgress();
	}

	/**
	 * 
	 * @param listener - Edit Order Listener
	 */
	public final void addEditOrderListener(final ActionListener listener) {
		view.addEditOrderListener(listener);
	}
	/**
	 * 
	 * @param listener - cancel order listener
	 */
	public final void addCancelOrderListener(final ActionListener listener) {
		view.addCancelOrderListener(listener);
	}
	
	/**
	 * Adds an action listener to the Call for Assistance button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public final void addWaiterCallListener(final ActionListener listener) {
		view.addWaiterCallListener(listener);
	}

	/**
	 * Sets the progressbar percentage in the view.
	 */
	public final void setProgressValue() {
		int progress = model.getOrderProgress();
		String progressLabel = model.getOrderState().toString();
		view.setProgressValue(progress);
		view.setProgressLabel(progressLabel);
	}

	/**
	 * Refreshes the progress of the {@link Order}.
	 */
	public final void refreshProgress() {
		Runnable refresh = new Runnable() {
			public void run() {
				setProgressValue();
				// CHECKSTYLE IGNORE MagicNumberCheck FOR NEXT 6 LINES
				if (model.getOrderProgress() > 40) {
					view.cancelOrderEnabled(false);
				} else {
					view.cancelOrderEnabled(true);
				}
				if (model.getOrderProgress() > 60) {
					view.editOrderEnabled(false);
				} else {
					view.editOrderEnabled(true);
				}
			}
		};
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		executor.scheduleAtFixedRate(refresh, 0, 1, TimeUnit.SECONDS);
	}
}


