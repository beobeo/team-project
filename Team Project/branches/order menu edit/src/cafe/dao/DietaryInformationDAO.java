package cafe.dao;

import cafe.DietaryInformation;

/**
 * A data access object used to manipulate persisted {@link DietaryInformation}
 * objects.
 * 
 * @author Michael Winter
 */
public interface DietaryInformationDAO {
    /**
     * Given an identifier, this method loads and returns a new instance of the
     * specified {@link DietaryInformation}.
     * 
     * @param id
     *            the identifier.
     * @return the specified {@code DietaryInformation}.
     * 
     */
    DietaryInformation findDietaryInformation(int id);
}
