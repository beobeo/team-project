package cafe.dao;

import java.util.List;

import cafe.OrderItem;

/**
 * A data access object used to manipulate persisted {@link OrderItem} objects.
 * 
 * @author Robert Kardjaliev
 * @author Michael Winter
 */
public interface OrderItemDAO {
    /**
     * Given {@code Order} and {@code Dish} identifiers, this method loads and
     * returns a new instance of the specified {@link OrderItem}.
     * 
     * @param order
     *            the identifier of the order which the desired item
     *            constitutes.
     * @param dish
     *            the identifier of the dish that was ordered.
     * @return the specified order item.
     *
     */
	OrderItem findOrderItem(int order, int dish);
    
    /**
     * Given an {@code Order} identifier, this method returns the items that
     * constitute that order.
     * 
     * @param id
     *            the order identifier to be matched.
     * @return the set of items that constitute the given {@code Order}.
     *
     */
    List<OrderItem> selectOrderItems(int id);
    
    /**
     * Creates a new order item.
     * 
     * @param orderId
     *            the identifier of the {@code Order} to which this item
     *            relates.
     * @param dishId
     *            the identifier of the {@code Dish} that was ordered.
     * @param quantity
     *            how many of specified dishes were ordered (must be greater
     *            than 0).
     * @param suggested
     *            if {@code true}, this item was suggested by the waiter that
     *            took the order; if {@code false}, the customer chose this
     *            item.
     *
     */
    void createOrderItem(int orderId, int dishId, int quantity, boolean suggested);
    
    /**
     * Deletes an existing order item.
     * 
     * @param orderId
     *            the identifier of the {@code Order} to which this item
     *            relates.
     * @param dishId
     *            the identifier of the {@code Dish} that was ordered.
     */
    void deleteOrderItem(int orderId, int dishId);

    /**
     * Updates an existing order item.
     *
     * @param orderId
     *            the identifier of the {@code Order} to which this item
     *            relates.
     * @param item
     *            the {@code OrderItem} to update.
     */
    void updateOrderItem(int orderId, OrderItem item);
}
