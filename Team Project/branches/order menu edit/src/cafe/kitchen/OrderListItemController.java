package cafe.kitchen;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import cafe.Order;

/**
 * Observes user events from the order items displayed as part of the kitchen
 * view and directs behaviour accordingly.
 * 
 * @author Michael Winter
 */
public final class OrderListItemController {
	/**
	 * The panel displayed as part of the active order list.
	 */
	private OrderListItemView listView;
	/**
	 * The dialogue box that details the items within an order.
	 */
	private OrderDetailView detailView;

	/**
	 * Constructs a controller for the given views.
	 * 
	 * @param listView
	 *            the order panel view.
	 * @param detailView
	 *            the order details view.
	 */
	public OrderListItemController(final OrderListItemView listView,
			final OrderDetailView detailView) {
		this.listView = listView;
		this.listView
				.addViewDetailsButtonActionListener(new ViewDetailsButtonActionListener());

		this.detailView = detailView;
		this.detailView.addOkButtonActionListener(new OkButtonActionListener());
	}

	/**
	 * Returns the panel view used to display an object.
	 * 
	 * @return the panel view.
	 */
	public OrderListItemView getListView() {
		return listView;
	}

	/**
	 * This method notifies the controller that the order has changed and its
	 * view needs to be refreshed.
	 * 
	 * @param order
	 *            the order that was updated.
	 */
	public void refresh(final Order order) {
		listView.setDestination(String.format("Table: %d (%s)", order
				.getSitting().getTable().getId(), order.getWaiter()
				.getFirstName()));

		int progress;
		switch (order.getProgress()) {
		case PREPARING:
			progress = 1;
			break;
		case READY:
			progress = 2;
			listView.enableAdvanceButton(false);
			break;
		default:
			progress = 0;
			break;
		}
		listView.setProgress(progress);
	}

	/**
	 * Adds an {@code ActionListener} to the Advance button within the panel
	 * view associated with this controller.
	 * 
	 * @param listener
	 *            the listener.
	 */
	public void addAdvanceButtonActionListener(final ActionListener listener) {
		listView.addAdvanceButtonActionListener(listener);
	}

	/**
	 * Observes button presses on the View Details button within the panel view.
	 */
	private class ViewDetailsButtonActionListener implements ActionListener {
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent
		 * )
		 */
		@Override
		public void actionPerformed(final ActionEvent e) {
			detailView.setVisible(true);
		}
	}

	/**
	 * Observes button presses on the OK button within the detail view.
	 */
	private class OkButtonActionListener implements ActionListener {
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent
		 * )
		 */
		@Override
		public void actionPerformed(final ActionEvent e) {
			detailView.setVisible(false);
		}
	}
}
