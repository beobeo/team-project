package cafe.client.menu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

/**
 * Presents a graphical window to the client from which they can view menu items, add those items
 * to an order, and call for assistance from the attending waiters.
 * 
 * @author Jonny Hercock
 * @author Michael Winter
 */
public class MenuGUI extends JFrame {
	/**
	 * Represents the number of dishes that should be displayed at once in the
	 * view due to size limitations.
	 */
	private static final int DISHES_PER_PAGE = 3;
	/**
	 * Unique serialisation identifier.
	 */
	private static final long serialVersionUID = 2922450639815942656L;

	
	/**
	 * Returns the number of dishes that are expected to fit in the container component.
	 * 
	 * @return the maximum number of dishes that should be added to the view.
	 */
	public static int getDishesPerPage() {
    	return DISHES_PER_PAGE;
    }
	
	
    /**
	 * Provides a container within the (normally hidden) scroll pane into which displayed menu
	 * items are rendered.
	 */
    private JPanel dishContainer;
    /**
     * Allows the client to move to later pages in the menu.
     */
    private JButton nextPage;
    /**
     * Allows the client to move to earlier pages in the menu.
     */
    private JButton previousPage;
    /**
     * Alerts attending waiters that this table requires assistance.
     */
    private JButton requestAssistance;

    /**
     * Hides this view and notifies the application controller that the order status view needs to
     * be displayed.
     */
    private JButton showOrder;
    
	/**
     * Create the frame.
     */
    //CHECKSTYLE:OFF
    public MenuGUI() {
	    initGUI();
    }
	private void initGUI() {
	    setResizable(false);
	    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    setBounds(100, 100, 650, 520);
	    JPanel contentPane = new JPanel();
	    contentPane.setOpaque(false);
	    contentPane.setBackground(Color.LIGHT_GRAY);
	    contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
	    setContentPane(contentPane);
	    GridBagLayout contentPaneGridBagLayout = new GridBagLayout();
	    contentPaneGridBagLayout.columnWidths = new int[]{50, 109, 83, 89, 89, 50, 0};
	    contentPaneGridBagLayout.rowHeights = new int[]{23, 312, 22, 30, 0};
	    contentPaneGridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
	    contentPaneGridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
	    contentPane.setLayout(contentPaneGridBagLayout);
	    
	    JLabel menu = new JLabel("MENU");
	    menu.setFont(new Font("Tahoma", Font.BOLD, 15));
	    GridBagConstraints menuGridBagConstraints = new GridBagConstraints();
	    menuGridBagConstraints.anchor = GridBagConstraints.BELOW_BASELINE;
	    menuGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
	    menuGridBagConstraints.insets = new Insets(0, 0, 5, 5);
	    menuGridBagConstraints.gridx = 0;
	    menuGridBagConstraints.gridy = 0;
	    contentPane.add(menu, menuGridBagConstraints);
	    
	    JMenuBar sortingMenuBar = new JMenuBar();
	    sortingMenuBar.setOpaque(false);
	    sortingMenuBar.setBorderPainted(false);
	    GridBagConstraints sortingMenuBarGridBagConstraints = new GridBagConstraints();
	    sortingMenuBarGridBagConstraints.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
	    sortingMenuBarGridBagConstraints.insets = new Insets(0, 0, 5, 5);
	    sortingMenuBarGridBagConstraints.gridx = 1;
	    sortingMenuBarGridBagConstraints.gridy = 0;
	    contentPane.add(sortingMenuBar, sortingMenuBarGridBagConstraints);
	    
	    final JMenu sortingMenu = new JMenu("Sort by");
	    sortingMenu.setEnabled(false);
	    sortingMenu.setMnemonic('S');
	    sortingMenuBar.add(sortingMenu);
	    
	    JRadioButtonMenuItem rdbtnmntmCalories = new JRadioButtonMenuItem("Calories");
	    rdbtnmntmCalories.setSelected(true);
	    sortingMenu.add(rdbtnmntmCalories);
	    
	    JRadioButtonMenuItem rdbtnmntmPopularity = new JRadioButtonMenuItem("Popularity");
	    sortingMenu.add(rdbtnmntmPopularity);
	    
	    JRadioButtonMenuItem rdbtnmntmPrice = new JRadioButtonMenuItem("Price");
	    sortingMenu.add(rdbtnmntmPrice);
	    
	    JMenuBar filterMenuBar = new JMenuBar();
	    filterMenuBar.setOpaque(false);
	    filterMenuBar.setBorderPainted(false);
	    GridBagConstraints filterMenuBarGridBagConstraints = new GridBagConstraints();
	    filterMenuBarGridBagConstraints.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
	    filterMenuBarGridBagConstraints.insets = new Insets(0, 0, 5, 5);
	    filterMenuBarGridBagConstraints.gridx = 2;
	    filterMenuBarGridBagConstraints.gridy = 0;
	    contentPane.add(filterMenuBar, filterMenuBarGridBagConstraints);
	    
	    JMenu filterMenu = new JMenu("Show only");
	    filterMenu.setMnemonic('O');
	    filterMenuBar.add(filterMenu);
	    
	    JRadioButtonMenuItem radioButtonMenuItem = new JRadioButtonMenuItem("Meal Deals");
	    radioButtonMenuItem.setVisible(false);
	    filterMenu.add(radioButtonMenuItem);
	    
	    JSeparator separator = new JSeparator();
	    separator.setVisible(false);
	    filterMenu.add(separator);
	    
	    JRadioButtonMenuItem radioButtonMenuItem_1 =
	    		new JRadioButtonMenuItem("Desserts");
	    radioButtonMenuItem_1.setVisible(false);
	    filterMenu.add(radioButtonMenuItem_1);
	    
	    JRadioButtonMenuItem radioButtonMenuItem_2 =
	    		new JRadioButtonMenuItem("Mains");
	    radioButtonMenuItem_2.setSelected(true);
	    filterMenu.add(radioButtonMenuItem_2);
	    
	    JRadioButtonMenuItem radioButtonMenuItem_3 =
	    		new JRadioButtonMenuItem("Starters");
	    radioButtonMenuItem_3.setVisible(false);
	    filterMenu.add(radioButtonMenuItem_3);
	    
	    JSeparator separator_1 = new JSeparator();
	    separator_1.setVisible(false);
	    filterMenu.add(separator_1);
	    
	    JRadioButtonMenuItem radioButtonMenuItem_4 = 
	    		new JRadioButtonMenuItem("Fish & Seafood");
	    radioButtonMenuItem_4.setVisible(false);
	    filterMenu.add(radioButtonMenuItem_4);
	    
	    JRadioButtonMenuItem radioButtonMenuItem_5 =
	    		new JRadioButtonMenuItem("Halal Meats ");
	    radioButtonMenuItem_5.setVisible(false);
	    filterMenu.add(radioButtonMenuItem_5);
	    
	    JRadioButtonMenuItem radioButtonMenuItem_6 =
	    		new JRadioButtonMenuItem("Lactose Free");
	    radioButtonMenuItem_6.setVisible(false);
	    filterMenu.add(radioButtonMenuItem_6);
	    
	    JRadioButtonMenuItem radioButtonMenuItem_7 =
	    		new JRadioButtonMenuItem("Vegan");
	    radioButtonMenuItem_7.setVisible(false);
	    filterMenu.add(radioButtonMenuItem_7);
	    
	    JRadioButtonMenuItem radioButtonMenuItem_8 =
	    		new JRadioButtonMenuItem("Vegetarian");
	    radioButtonMenuItem_8.setVisible(false);
	    filterMenu.add(radioButtonMenuItem_8);
	    
	    JButton showMenu = new JButton("Menu");
	    showMenu.setEnabled(false);
	    GridBagConstraints showMenuGridBagConstraints = new GridBagConstraints();
	    showMenuGridBagConstraints.anchor = GridBagConstraints.BELOW_BASELINE;
	    showMenuGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
	    showMenuGridBagConstraints.insets = new Insets(0, 0, 5, 5);
	    showMenuGridBagConstraints.gridx = 3;
	    showMenuGridBagConstraints.gridy = 0;
	    contentPane.add(showMenu, showMenuGridBagConstraints);
	    
	    showOrder = new JButton("Order");
	    GridBagConstraints showOrderGridBagConstraints = new GridBagConstraints();
	    showOrderGridBagConstraints.anchor = GridBagConstraints.BELOW_BASELINE;
	    showOrderGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
	    showOrderGridBagConstraints.insets = new Insets(0, 0, 5, 5);
	    showOrderGridBagConstraints.gridx = 4;
	    showOrderGridBagConstraints.gridy = 0;
	    contentPane.add(showOrder, showOrderGridBagConstraints);
	    
	    JScrollPane dishContainerScroller = new JScrollPane();
	    dishContainerScroller.setBackground(SystemColor.activeCaption);
	    dishContainerScroller.setBorder(null);
	    dishContainerScroller.setHorizontalScrollBarPolicy(
	    		ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	    GridBagConstraints dishContainerScrollerGridBagConstraints = new GridBagConstraints();
	    dishContainerScrollerGridBagConstraints.fill = GridBagConstraints.BOTH;
	    dishContainerScrollerGridBagConstraints.insets = new Insets(5, 5, 10, 10);
	    dishContainerScrollerGridBagConstraints.gridwidth = 4;
	    dishContainerScrollerGridBagConstraints.gridx = 1;
	    dishContainerScrollerGridBagConstraints.gridy = 1;
	    contentPane.add(dishContainerScroller, dishContainerScrollerGridBagConstraints);
	    
	    dishContainer = new JPanel();
	    dishContainer.setOpaque(false);
	    dishContainer.setBackground(Color.LIGHT_GRAY);
	    dishContainer.setMaximumSize(new Dimension(546, 32767));
	    dishContainer.setBorder(null);
	    dishContainerScroller.setViewportView(dishContainer);
	    dishContainer.setLayout(new GridLayout(3, 1, 0, 10));
	    
	    previousPage = new JButton("<");
	    previousPage.setMargin(new Insets(1, 1, 1, 1));
	    previousPage.setBorder(UIManager.getBorder("Button.border"));
	    previousPage.setActionCommand("previous");
	    previousPage.setEnabled(false);
	    GridBagConstraints previousPageGridBagConstraints = new GridBagConstraints();
	    previousPageGridBagConstraints.ipadx = 15;
	    previousPageGridBagConstraints.anchor = GridBagConstraints.ABOVE_BASELINE_TRAILING;
	    previousPageGridBagConstraints.insets = new Insets(0, 0, 5, 5);
	    previousPageGridBagConstraints.gridx = 3;
	    previousPageGridBagConstraints.gridy = 2;
	    contentPane.add(previousPage, previousPageGridBagConstraints);
	    
	    nextPage = new JButton(">");
	    nextPage.setMargin(new Insets(1, 1, 1, 1));
	    nextPage.setBorder(UIManager.getBorder("Button.border"));
	    nextPage.setEnabled(false);
	    nextPage.setActionCommand("next");
	    GridBagConstraints nextPageGridBagConstraints = new GridBagConstraints();
	    nextPageGridBagConstraints.ipadx = 15;
	    nextPageGridBagConstraints.anchor = GridBagConstraints.ABOVE_BASELINE_LEADING;
	    nextPageGridBagConstraints.insets = new Insets(0, 0, 5, 5);
	    nextPageGridBagConstraints.gridx = 4;
	    nextPageGridBagConstraints.gridy = 2;
	    contentPane.add(nextPage, nextPageGridBagConstraints);
	    
	    requestAssistance = new JButton("Call for Assistance");
	    GridBagConstraints requestAssistanceGridBagConstraints = new GridBagConstraints();
	    requestAssistanceGridBagConstraints.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
	    requestAssistanceGridBagConstraints.insets = new Insets(0, 0, 0, 5);
	    requestAssistanceGridBagConstraints.gridwidth = 2;
	    requestAssistanceGridBagConstraints.gridx = 0;
	    requestAssistanceGridBagConstraints.gridy = 3;
	    contentPane.add(requestAssistance, requestAssistanceGridBagConstraints);
	}
    //CHECKSTYLE:ON
    
	/**
	 * Adds an action listener to the Next Page button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
    public final void addNextPageListener(final ActionListener listener) {
    	nextPage.addActionListener(listener);
    }
    
	/**
	 * Adds an action listener to the Previous Page button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
    public final void addPreviousPageListener(final ActionListener listener) {
    	previousPage.addActionListener(listener);
    }
    
	/**
	 * Adds an action listener to the Order button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
    public final void addShowOrderListener(final ActionListener listener) {
    	showOrder.addActionListener(listener);
    }
    
	/**
	 * Adds an action listener to the Call for Assistance button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
    public final void addWaiterCallListener(final ActionListener listener) {
    	requestAssistance.addActionListener(listener);
    }
    
	/**
	 * Enables (or disables) the Next Page button.
	 * 
	 * @param enabled
	 * @param enabled	
	 *             {@code true} to enable the button; {@code false} otherwise.
	 */
    public final void enableNextPageButton(final boolean enabled) {
    	nextPage.setEnabled(enabled);
    }
    
    
    /**
	 * Enables (or disables) the Previous Page button.
	 * 
	 * @param enabled
	 * @param enabled
	 *             {@code true} to enable the button; {@code false} otherwise.
	 */

    public final void enablePreviousPageButton(final boolean enabled) {
    	previousPage.setEnabled(enabled);
    }
    
    /**
     * Sets which dishes should be displayed by this view and redraws the display.
     * 
     * @param dishes
     * 			   an ordered list containing the dishes to display.
     */
    public final void setDishes(final List<DishMenuView> dishes) {
    	dishContainer.removeAll();

    	for (final DishMenuView dish : dishes) {
    		dishContainer.add(dish);
    	}
    	
    	revalidate();
    	repaint();
    }
}
