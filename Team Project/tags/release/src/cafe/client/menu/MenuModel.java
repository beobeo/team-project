package cafe.client.menu;

import java.util.Collections;
import java.util.List;

import cafe.Dish;
import cafe.dao.DishDAO;

/**
 * Obtains available dishes from the database.
 * 
 * @author Michael Winter
 * @author Robert Kardjaliev
 */
public class MenuModel {
	/**
	 * The list of dishes last retrieved from the database.
	 */
	private List<Dish> dishes;
	/**
	 * A dish data access object.
	 */
	private DishDAO dishData;
	
	/**
	 * Creates the menu model.
	 * @param dishData - the data access object
	 */
	public MenuModel(final DishDAO dishData) {
		this.dishData = dishData;
		loadDishes();
	}
	
    /**
     * Obtained an ordered list of dishes available from the menu. The returned
     * list is immutable.
     * 
     * @return an unmodifiable, ordered list of available dishes.
     */
	public final List<Dish> getDishes() {
		return Collections.unmodifiableList(dishes);
	}
	
    /**
     * Loads the dishes with the visible parameter set to true from the database
     * in order to display them when later called.
     */
	final void loadDishes() {
	    dishes = dishData.selectAvailableDishes();
    }
}
