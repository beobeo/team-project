/**
 * Defines classes related to the Menu user interface for customers.
 * 
 * @author Robert Kardjaliev
 *
 */
package cafe.client.menu;