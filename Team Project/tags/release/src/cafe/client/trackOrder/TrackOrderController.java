package cafe.client.trackOrder;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import cafe.OrderEvent;
import cafe.OrderEventListener;
import cafe.OrderState;


/**
 * Observes user events from the Track Order View and directs behaviour accordingly, 
 * invoking the model where appropriate and referring events to higher-level observing
 * controllers.
 * 
 * @author Robert Kardjaliev
 * @author Jonathan Hercock
 * @author Hannah Cooper
 */
public final class TrackOrderController {
	/** An integer constant used for the timer. */	
	private static final int MINUTE = 60;
	/**
	 * The scheduler service used to perform refreshes.
	 */
	private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
	/**
	 * A list of event listeners that observe order change events caused by
	 * progress updates.
	 */
	private final List<OrderEventListener> listeners = new ArrayList<>();
	/**
	 * The task used for scheduling updates.
	 */
	private ScheduledFuture<?> task;
	/** 
	 * An integer that will act as 10 minute a timer in 
	 * {@code refreshProgress()}. 
	 */
	private int timer;
	/** The model. */
	private TrackOrderModel model;
	/** The view. */
	private TrackOrderView view;


	/**
	 * Instantiates the fields of the Track Order Controller.
	 *
	 * @param view - The View.
	 * @param model - The Model.
	 */
	public TrackOrderController(final TrackOrderView view, final TrackOrderModel model) { 
		this.view = view; 
		this.model = model;
		updateProgress();
	}

	/**
	 * Shows the view associated with this controller.
	 */
	public void showView() {
		view.setVisible(true);
	}

	/**
	 * Hides the view associated with this controller.
	 */
	public void hideView() {
		view.setVisible(false);
	}
	
	/**
	 * Returns the view associated with this controller.
	 * 
	 * @return the view.
	 */
	public Component getView() {
		return view;
	}

	/**
	 * Adds a listener to the Edit Order button.
	 * @param listener - Edit Order Listener
	 */
	public void addEditOrderListener(final ActionListener listener) {
		view.addEditOrderListener(listener);
	}
	/**
	 * Adds a listener to the Cancel Order button.
	 * @param listener - cancel order listener
	 */
	public void addCancelOrderListener(final ActionListener listener) {
		view.addCancelOrderListener(listener);
	}

	/**
	 * Adds an action listener to the Call for Assistance button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public void addWaiterCallListener(final ActionListener listener) {
		view.addWaiterCallListener(listener);
	}
	
	/**
	 * Adds an {@linkplain OrderEventListener order event listener} for
	 * {@code OrderEvent}s fired after the list of active orders changes in some
	 * way.
	 * 
	 * @param listener
	 *            the listener.
	 */
	public void addOrderModelListener(final OrderEventListener listener) {
		listeners.add(listener);
	}

	/**
	 * Reloads the progress of the Order, sets buttons to enabled/disabled where appropriate
	 * and displays appropriate text to the client in the view about the progress and
	 * approximate time remaining until its completion.
	 */
	public void refreshProgress() {
		task = executor.scheduleAtFixedRate(new Runnable() {
			private boolean sem = true;
			
			@Override
			public void run() {
				updateProgress();
				if (getTimer() / MINUTE >= 2 && model.getOrderProgress() <= MINUTE) {
					if (!sem) {
						sem = true;
					}
					view.setTimeLabel(getTimer() / MINUTE + " minutes");
					setTimer(getTimer() - 1);
				} else if (getTimer() / MINUTE < 2 && model.getOrderProgress() <= MINUTE) {
					view.setTimeLabel("Your order's taking longer "
							+ "than expected.");
				}
				switch(model.getOrderProgress()) {
				case 20:
					view.cancelOrderEnabled(true);
					view.editOrderEnabled(true);
					break;
				case 40: 
					break;
				case 60:
					view.cancelOrderEnabled(false);
					break;
				case 80:
					if (sem) {
						sem = false;
						setTimer(MINUTE);
					}
					if (getTimer() >= 1) {
						view.setTimeLabel(getTimer() + " seconds"); 
						setTimer(getTimer() - 1);
					} else {
						view.setTimeLabel("Your order should be here "
								+ "soon.");
					}
					view.editOrderEnabled(false);
					break;
				case 100:
					view.setTimeLabel("Your order has arrived.");
					view.paymentButtonEnabled(true);
					break;
				default:
					break;
				}
				if (model.getOrderState() == OrderState.DELIVERED) {
					task.cancel(true);
					fireOrderChangedEvent(new OrderEvent(model.getOrder()));
				}
			}
		}, 0, 1, TimeUnit.SECONDS);
	}


	/**
	 * Gets the current value of {@code timer}.
	 * @return the {@code timer} - integer representation of
	 * the time in seconds
	 */
	public int getTimer() {
		return timer;
	}

	/**
	 * Sets the seconds the timer should have.
	 * @param timer
	 * 			the variable keeping track of the seconds.
	 */
	public void setTimer(final int timer) {
		this.timer = timer;
	}

	
	/**
	 * Notifies interested parties that an existing order has changed state. The
	 * changed order is passed as the source object.
	 * 
	 * @param e
	 *            the event.
	 */
	private void fireOrderChangedEvent(final OrderEvent e) {
		for (OrderEventListener listener : listeners) {
			listener.orderChanged(e);
		}
	}

	/**
	 * Sets the progress bar percentage in the view.
	 */
	private void updateProgress() {
		int progress = model.getOrderProgress();
		String progressLabel = model.getOrderState().toString();
		view.setProgressValue(progress);
		view.setProgressLabel(progressLabel);
	}
}