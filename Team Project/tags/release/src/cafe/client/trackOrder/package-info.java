/**
 * Defines classes related to the Track Order user interface for customers.
 * 
 * @author Robert Kardjaliev
 *
 */
package cafe.client.trackOrder;