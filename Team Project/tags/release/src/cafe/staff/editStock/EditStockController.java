package cafe.staff.editStock;

import java.awt.event.ActionListener;

/**
 * 
 * @author Hannah Cooper
 * Observes user events from the Edit Stock interface and directs behaviour accordingly
 */
public class EditStockController {
	 /**
	  * the view.
	  */
private static EditStockGUI view;
/**
 * the model.
 */
private static EditStockModel model;

	/**
	 * 
	 * @param v - EditStockGUI
	 * @param esm - EditStockModel
	 */
	 public EditStockController(final EditStockGUI v, final EditStockModel esm) {
	      
		 EditStockController.view = v;
	     model = esm;

	 }

	/**
	 * Edit stock interface visible.
	 */
	public final void showView() {
        view.setVisible(true);
    }
    /**
     * Edit stock interface not visible.
     */
    public final void hideView() {
    	view.setVisible(false);
    }
    /**
     *  Listens for edit stock button press.
     * @param listener - listens
     */
    public final void addUpdateStockListener(final ActionListener listener) {
    	view.addUpdateStockListener(listener);
    	
    }
    /**
     * Listens for back button press.
     * @param listener 
     */
    public final void addBackButtonListener(final ActionListener listener) {
    	view.addBackButtonListener(listener);
    }
}