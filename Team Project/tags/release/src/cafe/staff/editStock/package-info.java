/**
 * Defines classes related to the Editing Stock user interface
 * for kitchen staff and managers.
 * 
 * @author Robert Kardjaliev
 */
package cafe.staff.editStock;