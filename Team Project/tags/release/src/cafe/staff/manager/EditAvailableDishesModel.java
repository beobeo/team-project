package cafe.staff.manager;

import java.util.ArrayList;
import java.util.List;

import cafe.dao.DishDAO;

/**
 * Obtains the list of {@link cafe.Dish Dish} names on the database and contains
 * the methods that perform operations on them. 
 * @author Robert Kardjaliev
 */
public class EditAvailableDishesModel {
	/** A data access object for modifying persisted {@link cafe.Dish Dish} objects. */
	private DishDAO dishData;
	/**
	 * The {@code list} that will hold the {@link cafe.Dish Dish} names.
	 */
	private List<String> dishNames = new ArrayList<>();

	/**
	 * Initializes the field for the Edit Menu Model.
	 * @param dishData
	 * 				the Dish data access object needed
	 * to allow modification of {@link cafe.Table Table}s from the database.
	 */
	public EditAvailableDishesModel(final DishDAO dishData) {
		this.dishData = dishData;
		setDishNames();
	}
	
	/**
	 * Gets all the {@code Dish} names from the database and saves them to the local
	 * list {@code dishNames}.
	 */
	private void setDishNames() {
		dishNames = dishData.getAllDishNames();
	}

	/**
	 * Gets the list of {@code Dish} names.
	 * @return
	 * 			the list of dish names.
	 */
	public final List<String> getDishNames() {
		return dishNames;
	}

	/**
	 * Sets the {@code visibility) of currently selected {@code Dish} from the view
	 * depending on which button was pressed.
	 * @param dishName
	 * 				the {@code Dish}'s name.
	 * @param visible
	 * 				the boolean value for visibility.
	 */
	public final void setVisible(final String dishName, final boolean visible) {
		dishData.setVisible(dishName, visible);
	}
	
	/**
	 * Gets the {@code visible} field of the currently selected {@link cafe.Dish Dish} from
	 * the database.
	 * @param dishName
	 * 				the {@code Dish}'s name.
	 * @return
	 * 				boolean value of visible for the {@code Dish}.
	 */
	public final boolean getVisible(final String dishName) {
		boolean visible;
		visible = dishData.getVisible(dishName);
		return visible;
	}
}