/**
 * Defines classes related to the user interface for manager(s).
 * 
 * @author Robert Kardjaliev
 */
package cafe.staff.manager;