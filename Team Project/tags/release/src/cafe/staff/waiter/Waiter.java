package cafe.staff.waiter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cafe.Employee;
import cafe.Table;

/**
 * @author Jonathan Hercock
 */
public class Waiter extends Employee {

	/** Example of a Waiter, used to fees a name and ID to the GUI. */
	public static final Waiter TEST_WAITER = new Waiter(3, "David", "Jones",
			true, Collections.<Table> emptyList());

	/** The list of tables that the waiter is helping. */
	@SuppressWarnings("unused")
	private List<Table> tables = new ArrayList<Table>();

	/**
	 * The state that will be returned for whether the waiter can assist a new
	 * table.
	 */
	private boolean available;

	/**
	 * Constructor that passes two arguments in from the above variables.
	 * 
	 * @param id
	 *            The numerical ID by which the waiter is identified.
	 * @param firstName
	 *            Waiter's first name.
	 * @param lastName
	 *            Waiter's last name.
	 * @param tables
	 *            The list of tables that the Waiter is assigned to.
	 * @param available
	 *            Whether the waiter is currently available.
	 */
	public Waiter(final int id, final String firstName, final String lastName,
			final boolean available, final List<Table> tables) {
		super(id, firstName, lastName);
		this.available = available;
		this.tables = tables;
	}

	/**
	 * Returns the name of this Waiter.
	 * 
	 * @return the Waiter's name.
	 */
	public final String getName() {
		return getFirstName() + " " + getLastName();
	}

	/**
	 * Method to return whether the Waiter can help any other tables.
	 * 
	 * @return the Waiter's availability status.
	 */
	public final boolean getAvailable() {
		return available;
	}

	/**
	 * Constant state, checking whether the Waiter needs help from another
	 * employee or not.
	 * 
	 * @return true at the moment.
	 */
	public final boolean needsAssistance() {
		return true;

	}

	/**
	 * Constant state, returns true when the Waiter signals that a table is
	 * ready to pay, or false otherwise.
	 * 
	 * @return false, for now.
	 */
	public final boolean readyToPay() {
		return false;

	}

	/**
	 * @return false at the moment.
	 */
	public final boolean collectDish() {
		return true;

	}

	/**
	 * @return Odered Delivered method. Should return when button is pressed.
	 */
	public final boolean orderDelivered() {
		return true;
	}
}
