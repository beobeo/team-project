package cafe.staff.waiter;

import javax.swing.table.AbstractTableModel;

import cafe.Order;
import cafe.OrderItem;

/** Provides the mechanism for populating tabular data within the detailed order
 * view used by Waiting staff.
 * For reference: ODT = OrderDetailTable.
 * 
 * @author Jonathan Hercock
 *
 */
public final class WaiterODTModel extends AbstractTableModel {

    /**
     * Unique serialisation identifier.
     */
    private static final long serialVersionUID = 6959136478540511762L;
    /**
     * The order to be queried for ordered dishes.
     */
    private Order order;

    /**
     * Constructs an {@code WaiterODTModel} using the specified
     * {@code Order}.
     * 
     * @param order
     *            the order from which dish information will be retrieved.
     */
    public WaiterODTModel(final Order order) {
        this.order = order;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getRowCount()
     */
    @Override
    public int getRowCount() {
        return order.getItems().size();
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    @Override
    public int getColumnCount() {
        return 2;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    //CHECKSTYLE:OFF
    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        final OrderItem item = order.getItems().get(rowIndex);
        return (columnIndex == 0) ? item.getDish().getName() : item
                .getQuantity();
    }

    @Override
    public String getColumnName(final int column) {
        return (column == 0) ? "Dish" : "Quantity";
    }

}
