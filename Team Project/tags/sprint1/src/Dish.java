import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Represents an item that can be ordered by a client and is listed in the menu.
 * 
 * TODO: Add remove ingredient method.
 * 
 * @author Michael Winter
 */
public class Dish {
	/**
	 * The unique identifier for this instance.
	 */
	private int id;
	/**
	 * The name of the dish.
	 */
	private String name;
	/**
	 * A brief description of the contents of the dish.
	 */
	private String description;
	/**
	 * Determines whether this dish should be displayed apart from ingredient availability. This
	 * may be used to temporarily remove a dish from the menu without deleting it entirely from
	 * the database.
	 */
	private boolean visible = true;
	/**
	 * The number of calories, in kcal, of this dish.
	 */
	private int calories;
	/**
	 * The price, in pounds stirling, of this dish.
	 */
	private float price;
	/**
	 * The category, such as the type of course, for this dish.
	 */
	private String category;
	/**
	 * A collection of ingredients, and the respective quantity, required to prepare this dish.
	 */
	private Map<Ingredient, Integer> requirements = new HashMap<>();
	
	/**
	 * Returns the number of calories of this dish (in kcal).
	 * 
	 * @return the number of calories.
	 */
	public int getCalories() {
		return calories;
	}
	
	/**
	 * Returns the category, such as the type of course, of this dish.
	 * 
	 * @return the category for this dish.
	 */
	public String getCategory() {
		return category;
	}
	
	/**
	 * Returns a brief description of the contents or style of this dish.
	 * 
	 * @return a description of this dish.
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Returns a set of ingredients used to prepare this dish. The set cannot be modified.
	 * 
	 * @return an unmodifiable set of ingredients contained in this dish.
	 */
	public Set<Ingredient> getIngredients() {
		return Collections.unmodifiableSet(requirements.keySet());
	}
	
	/**
	 * Returns the name of this dish.
	 * 
	 * @return the name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the price of this dish (in pounds stirling).
	 * 
	 * @return the price.
	 */
	public float getPrice() {
		return price;
	}
	
	/**
	 * Returns whether this dish is available and should be displayed in the menu. Availability is
	 * determined both by the available ingredient stock and whether the dish has been explicitly
	 * hidden.
	 * 
	 * @return {@code true} if this dish is available for ordering; {@code false} otherwise.
	 */
	public boolean isAvailable() {
		if (!visible) {
			return false;
		}
		for (final Map.Entry<Ingredient, Integer> ingredientQuantityRequired : requirements.entrySet()) {
			final Ingredient ingredient = ingredientQuantityRequired.getKey();
			final int quantityRequired = ingredientQuantityRequired.getValue();
			if (!ingredient.isAvailable(quantityRequired)) {
				return false;
			}
		}
		return true;
	}
	
	
	/**
	 * Returns the unique identifier for this dish.
	 * 
	 * @return the identifier.
	 */
	int getId() {
		return id;
	}

	/**
	 * Sets the number of calories in this dish. This must be greater than, or equal to, zero.
	 * 
	 * @param amount the number of calories (must be >= 0).
	 * @throws IllegalArgumentException if {@code amount} is negative.
	 */
	void setCalories(final int amount) {
		if (amount < 0) {
			throw new IllegalArgumentException();
		}
		calories = amount;
	}

	/**
	 * Sets the category for this dish.
	 * 
	 * @param string the category (must not be {@code null}).
	 * @throws NullPointerException if {@code string} is {@code null}.
	 */
	void setCategory(final String string) {
		if (string == null) {
			throw new NullPointerException();
		}
		category = string;
	}
	
	/**
	 * Sets the description for this dish.
	 * 
	 * @param string the description (must not be {@code null}).
	 * @throws NullPointerException if {@code string} is {@code null}.
	 */
	void setDescription(final String string) {
		if (string == null) {
			throw new NullPointerException();
		}
		description = string;
	}

	/**
	 * Adds an ingredient to this dish, and quantity required to prepare it.
	 * 
	 * @param ingredient the ingredient (must not be {@code null}).
	 * @param requiredAmount the quantity of the ingredient needed (must be > 0).
	 * @throws IllegalArgumentException if {@code requiredAmount} is less than, or equal to, zero.
	 * @throws NullPointerException if {@code ingredient} is {@code null}.
	 */
	void addIngredient(final Ingredient ingredient, final int requiredAmount) {
		if (ingredient == null) {
			throw new NullPointerException();
		}
		if (requiredAmount <= 0) {
			throw new IllegalArgumentException();
		}
		requirements.put(ingredient, requiredAmount);
	}
	
	/**
	 * Sets the name of this dish.
	 * 
	 * @param string the name (must not be {@code null}).
	 * @throws NullPointerException if {@code string} is {@code null}.
	 */
	void setName(final String string) {
		if (string == null) {
			throw new NullPointerException();
		}
		name = string;
	}
	
	/**
	 * Sets the price of this dish. This must be greater than, or equal to, zero.
	 * 
	 * @param amount the price (must be >= 0).
	 * @throws IllegalArgumentException if {@code amount} is negative.
	 */
	void setPrice(final float amount) {
		if (amount < 0) {
			throw new IllegalArgumentException();
		}
		price = amount;
	}
	
	/**
	 * Sets the visibility of this dish, independent of ingredient stock levels, allowing it to be
	 * temporarily hidden. A visible dish is subject to ingredient checks, but an invisible dish
	 * will always be hidden.
	 * 
	 * @param visible {@code true} if this dish should be visible; {@code false} otherwise.
	 * @throws IllegalArgumentException if {@code amount} is negative.
	 */
	void setVisible(final boolean visible) {
		this.visible = visible;
	}
}
