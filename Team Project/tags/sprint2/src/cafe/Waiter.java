/**
 * 
 */
package cafe;

//import java.util.ArrayList;

/**
 * @author Jonathan Hercock
 */
public class Waiter {
    
    /** Example of a Waiter, used to fees a name and ID to the GUI. */
    public static final Waiter TEST_WAITER = new Waiter("John Smith", 1234321);

    /** The name of the waiter. */
    private static String name;
    
    /** The unique identifier for the waiter.   */
    private int employeeID;
    
   // /** The list of tables that the waiter is helping.     */          //??
   // private ArrayList<Table> tables = new ArrayList<Table>();
    
    /** The state that will be returned for whether the waiter can assist a new table.    */
    private boolean available;

    /** Constructor that passes two arguments in from the above variables.
     * 
     * @param name  The waiter's name.
     * @param employeeID    The numerical ID by which the waiter is identified.
     */
    public Waiter(final String name, final int employeeID) {
        Waiter.name = name;
        this.employeeID = employeeID;
    }
    
    /** Returns the name of this Waiter.
     * @return the Waiter's name.
     */
    public final String getName() {
        return name;
    }
    
    /** Returns the unique ID of this Waiter.
     * @return the employee's ID.
     */
    public final int getID() {
        return employeeID;
    }
    
    /** Method to return whether the Waiter can help any other tables.
     * 
     * @return the Waiter's availability status.
     */
    public final boolean getAvailable() {
        return available;
    }

    /** Constant state, checking whether the Waiter needs help from another employee or not.
     * 
     * @return true at the moment.
     */
    public final boolean needsAssistance() {
        return true;
        
    }

    /** Constant state, returns true when the Waiter signals that a table is ready to pay,
     *                      or false otherwise.
     * 
     * @return false, for now.
     */
    public final boolean readyToPay() {
        return false;
        
    }

    /**
     * @return false at the moment.
     */
    public final boolean collectDish() {
        return true;
                
    }

}
