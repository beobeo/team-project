package cafe.dao;
import java.io.IOException;

/**
 * Thrown to indicate that the client was unable to establish a connection with
 * the database server.
 * 
 * @author Michael Winter
 */
public final class ConnectionException extends IOException {
    /**
     * Unique serialisation identifier.
     */
    private static final long serialVersionUID = -2961009743558128109L;

    /**
     * Constructs a {@code ConnectionException} with no detail message.
     */
    public ConnectionException() {
        super();
    }

    /**
     * Constructs a {@code ConnectionException} with the specified detail
     * message.
     * 
     * @param message
     *            the detail message.
     */
    public ConnectionException(final String message) {
        super(message);
    }

    /**
     * Constructs a {@code ConnectionException} with the specified detail
     * message and cause.
     * 
     * <p>
     * Note that the detail message associated with {@code cause} is not
     * automatically incorporated in this exception's detail message.
     * 
     * @param message
     *            the detail message.
     * @param cause
     *            the cause. A value of {@code null} is acceptable and indicates
     *            that the cause is either unknown or nonexistent.
     */
    public ConnectionException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a {@code ConnectionException} with the specified cause and a
     * detail message of {@code (cause == null) ? null : cause.toString()}
     * (which typically contains the class and detail message of {@code cause}).
     * 
     * @param cause
     *            the cause. A value of {@code null} is acceptable and indicates
     *            that the cause is either unknown or nonexistent.
     */
    public ConnectionException(final Throwable cause) {
        this((cause == null) ? null : cause.toString(), cause);
    }
}
