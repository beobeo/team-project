package cafe.dao;

import java.io.IOException;

import cafe.DietaryInformation;

/**
 * A data access object used to manipulate persisted {@link DietaryInformation}
 * objects.
 * 
 * TODO: Wrap IOException exceptions as another, more friendly exception where
 * possible. Lookup-type methods should only need to raise an exception if the
 * connection is lost.
 * 
 * @author Michael Winter
 */
public interface DietaryInformationDAO {
    /**
     * Given an identifier, this method loads and returns a new instance of the
     * specified {@link DietaryInformation}.
     * 
     * @param id
     *            the identifier.
     * @return the specified {@code DietaryInformation}.
     * @throws IOException
     *             if an error occurs while accessing the data store.
     */
    DietaryInformation findDietaryInformation(int id) throws IOException;
}
