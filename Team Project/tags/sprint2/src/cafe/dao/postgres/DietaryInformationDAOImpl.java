package cafe.dao.postgres;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import cafe.DietaryInformation;
import cafe.dao.AuthenticationException;
import cafe.dao.DietaryInformationDAO;
import cafe.dao.JdbcConnectionFactory;

/**
 * A data access object used to manipulate {@link DietaryInformation} objects
 * that have persisted to a PostgreSQL database.
 * 
 * TODO: Wrap IOException exceptions as another, more friendly exception where
 * possible. Lookup-type methods should only need to raise an exception if the
 * connection is lost.
 * 
 * @author Michael Winter
 */
public final class DietaryInformationDAOImpl implements DietaryInformationDAO {
    /**
     * The source of JDBC connections used to perform queries against the
     * underlying database.
     */
    private JdbcConnectionFactory connectionFactory;
    
    /**
     * Constructs a data access object for {@link DietaryInformation}s that will
     * operate over the given connection.
     * 
     * @param connectionFactory
     *            the factory object that will provide a database connection.
     * @throws NullPointerException
     *             if the specified factory is {@code null}.
     */
    public DietaryInformationDAOImpl(final JdbcConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DietaryInformationDAO#findDietaryInformation(int)
     */
    @Override
    public DietaryInformation findDietaryInformation(final int id) throws IOException {
        Connection connection = null;
        try {
            connection = connectionFactory.getConnection();
        } catch (final AuthenticationException e) {
            // Ignore: Authentication would have been verified earlier in the program.
        }
        DietaryInformation requirement = null;
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT name FROM dietary_information WHERE id = ?")) {
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                requirement = new DietaryInformation(id, result.getString("name"));
            }
        } catch (final SQLException e) {
            throw new IOException(e);
        }
        return requirement;
    }
}
