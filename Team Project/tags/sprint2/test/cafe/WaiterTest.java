/**
 * 
 */
package cafe;
//import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


/**
 * @author Jonathan Hercock
 * 
 *
 */
public final class WaiterTest {

    /**
     *  First test, checking the needsAssistance() method of the class.
     */
    @Test
    public void testNeedsAssistance() {
       final Waiter waiterTest = new Waiter("John Smith", 123421);
       
       waiterTest.needsAssistance();
       assertTrue(waiterTest.needsAssistance());
    }

    /**
     *  Second test, checking the readyToPay() method of the class.
     */
    @Test
    public void testReadyToPay() {
        final Waiter testWaiter = new Waiter("Phil Jones", 123422);
        
        testWaiter.readyToPay();
        assertFalse(testWaiter.readyToPay());
    }
    
    /**
     * 
     */
    @Test
    public void testCollectDish() {
        final Waiter waiterTest = new Waiter("Mark Jones", 123423);
        
        waiterTest.collectDish();
        assertTrue(waiterTest.collectDish());
    }

}
