package cafe;

import java.util.EventObject;

/**
 * {@code OrderEvent} is used to notify interested parties that the state
 * has changed in the event source.
 * 
 * @author Michael Winter
 */
public class OrderEvent extends EventObject {
	/**
	 * Unique serialisation identifier.
	 */
	private static final long serialVersionUID = 221769177215533399L;

	
	/**
	 * Constructs an order model event.
	 * 
	 * @param source
	 *            The object on which the {@code OrderEvent} initially
	 *            occurred.
	 * @throws IllegalArgumentException
	 *             if source is {@code null}.
	 */
	public OrderEvent(final Object source) {
		super(source);
	}
}
