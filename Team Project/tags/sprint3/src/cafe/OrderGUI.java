package cafe;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;
import javax.swing.JTextPane;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JScrollPane;

import java.awt.Color;
import java.awt.SystemColor;

import javax.swing.SwingConstants;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Presents a graphical window to the client from which they can view and edit
 * their order, and call for assistance from the attending waiters.
 * 
 * @author Robert Kardjaliev
 * @author Jonathan Hercock
 * @author Hannah Cooper
 * @author Adam Lumber
 * @author Ace
 */
public final class OrderGUI extends JFrame {
    /**
     * Unique serialisation identifier.
     */
    private static final long serialVersionUID = -8615250900651957024L;


    /**
     * GUI panel.
     */
    private JPanel contentPane;
    

    /**
     * value for total price.
     */
    private JLabel totalPrice;

	/**
	 * dish which has been selected.
	 */
	private JLabel picture3;

	/**
	 * dish which has been selected.
	 */
	private JTextPane txtpnFourCheesePizza;

	/**
	 * dish which has been selected.
	 */
	private JTextPane txtpnFreshlyMadeBase;

	/**
	 * display quantity for item 1.
	 */
	private JLabel label6;

	/**
	 * display quantity for item 2.
	 */
	private JLabel label7;

	/**
	 * display quantitiy for item 3.
	 */
	private JLabel label8;

	/**
	 * panel for individual dish(1).
	 */
	private JPanel panel1;
	/**
	 * panel for individual dish(3).
	 */
	private JPanel panel3;

	/**
	 * /**
	 * panel for individual dish(2).
	 */
	private JPanel panel2;

	/**
	 * remove button 1.
	 */
	private JButton button1;

	/**
	 * remove button 2.
	 */
	private JButton button2;


	/**
	 * remove button 3.
	 */
	private JButton button3;

	
	/**
	 * shows menu.
	 */
	private JButton showMenu;
	/**
	 * Send a call for help from Waiters.
	 */
	private JButton requestAssistance;
	/**
	 * shows track order view.
	 */
	 static JButton trackOrder;
	 /**
     * Create the frame.
     */	
	static JButton btnConfirmOrder;
	
    /**
     * Create the frame.
     */
	//CHECKSTYLE:OFF
    public OrderGUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 693, 463);
        
        contentPane = new JPanel();
        contentPane.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent arg0) {
        	}
        });
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JButton btnViewBasket = new JButton("Order");
        btnViewBasket.setEnabled(false);
        btnViewBasket.setBounds(578, 11, 89, 23);
        contentPane.add(btnViewBasket);
        
        showMenu = new JButton("Menu");
        showMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            }
        });
        showMenu.setBounds(479, 11, 89, 23);
        contentPane.add(showMenu);
        
        requestAssistance = new JButton("Call for Assistance");
        requestAssistance.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            }
        });
        requestAssistance.setBounds(10, 383, 152, 30);
        contentPane.add(requestAssistance);
        
        JMenu mnSortBy = new JMenu("Sort by");
        mnSortBy.setBounds(92, 11, 89, 22);
        contentPane.add(mnSortBy);
        
        JRadioButtonMenuItem rdbtnmntmCalories = new JRadioButtonMenuItem("Calories");
        rdbtnmntmCalories.setSelected(true);
        mnSortBy.add(rdbtnmntmCalories);
        
        JRadioButtonMenuItem rdbtnmntmPopularity = new JRadioButtonMenuItem("Popularity");
        mnSortBy.add(rdbtnmntmPopularity);
        
        JRadioButtonMenuItem rdbtnmntmPrice = new JRadioButtonMenuItem("Price");
        mnSortBy.add(rdbtnmntmPrice);
        
        JMenu mnShowOnly = new JMenu("Show only");
        mnShowOnly.setHorizontalAlignment(SwingConstants.CENTER);
        mnShowOnly.setBounds(255, 11, 107, 22);
        contentPane.add(mnShowOnly);
        
        JRadioButtonMenuItem rdbtnmntmNewRadioItem_1 = new JRadioButtonMenuItem("Meal Deals");
        rdbtnmntmNewRadioItem_1.setSelected(true);
        mnShowOnly.add(rdbtnmntmNewRadioItem_1);
        
        JSeparator separator_1 = new JSeparator();
        mnShowOnly.add(separator_1);
        
        JRadioButtonMenuItem rdbtnmntmDesserts = new JRadioButtonMenuItem("Desserts");
        mnShowOnly.add(rdbtnmntmDesserts);
        
        JRadioButtonMenuItem rdbtnmntmMains = new JRadioButtonMenuItem("Mains");
        mnShowOnly.add(rdbtnmntmMains);
        
        JRadioButtonMenuItem rdbtnmntmStarters = new JRadioButtonMenuItem("Starters");
        mnShowOnly.add(rdbtnmntmStarters);
        
        JSeparator separator = new JSeparator();
        mnShowOnly.add(separator);
        
        panel3 = new JPanel();
        panel3.setOpaque(false);
        panel3.setBounds(20, 79, 647, 50);
        contentPane.add(panel3);
        panel3.setLayout(null);
        
        JTextPane txtpnSampleDish = new JTextPane();
        txtpnSampleDish.setOpaque(false);
        txtpnSampleDish.setBounds(82, 0, 99, 20);
        panel3.add(txtpnSampleDish);
        txtpnSampleDish.setForeground(Color.BLACK);
        txtpnSampleDish.setFont(new Font("Tahoma", Font.PLAIN, 12));
        txtpnSampleDish.setBackground(SystemColor.activeCaption);
        txtpnSampleDish.setText("Grilled Salmon");
        
        JTextPane txtpnDescription = new JTextPane();
        txtpnDescription.setForeground(Color.BLACK);
        txtpnDescription.setOpaque(false);
        txtpnDescription.setBounds(109, 19, 249, 20);
        panel3.add(txtpnDescription);
        txtpnDescription.setFont(new Font("Tahoma", Font.ITALIC, 11));
        txtpnDescription.setBackground(SystemColor.activeCaption);
        txtpnDescription.setText("Grilled Salmon on a bed of salad");
        
        JLabel picture = new JLabel();
        picture.setBounds(10, 0, 50, 50);
        panel3.add(picture);
        picture.setIcon(new ImageIcon("images/grisalm.jpg"));
        
        JLabel label_2 = new JLabel();
        label_2.setText("7.95");
        label_2.setBounds(424, 6, 58, 14);
        panel3.add(label_2);
        label_2.setForeground(Color.WHITE);
        
        JLabel lblQuantity = new JLabel();
        lblQuantity.setText("2");
        lblQuantity.setBounds(492, 6, 18, 14);
        panel3.add(lblQuantity);
        lblQuantity.setForeground(Color.WHITE);
        
        JLabel lblDishPrice = new JLabel();
        lblDishPrice.setText(String.format("£%1$3.2f", Double.valueOf(label_2.getText())
                 * Double.valueOf(lblQuantity.getText())));
        lblDishPrice.setBounds(520, 6, 58, 14);
        panel3.add(lblDishPrice);
        lblDishPrice.setForeground(SystemColor.window);
        
        
        setButton1(new JButton("Remove"));
        getButton1().setBounds(574, 0, 73, 20);
        panel3.add(getButton1());
        getButton1().setFont(new Font("Tahoma", Font.PLAIN, 9));
        
        panel2 = new JPanel();
        panel2.setOpaque(false);
        panel2.setBounds(20, 139, 649, 50);
        contentPane.add(panel2);
        panel2.setLayout(null);
        
        JTextPane txtpnExampleDish = new JTextPane();
        txtpnExampleDish.setOpaque(false);
        txtpnExampleDish.setBounds(82, 0, 58, 20);
        panel2.add(txtpnExampleDish);
        txtpnExampleDish.setFont(new Font("Tahoma", Font.PLAIN, 12));
        txtpnExampleDish.setForeground(Color.BLACK);
        txtpnExampleDish.setBackground(SystemColor.activeCaption);
        txtpnExampleDish.setText("Lasagne");
        
        JTextPane txtpnDescriptionIngredients = new JTextPane();
        txtpnDescriptionIngredients.setForeground(Color.BLACK);
        txtpnDescriptionIngredients.setOpaque(false);
        txtpnDescriptionIngredients.setBounds(109, 19, 270, 20);
        panel2.add(txtpnDescriptionIngredients);
        txtpnDescriptionIngredients.setFont(new Font("Tahoma", Font.ITALIC, 11));
        txtpnDescriptionIngredients.setBackground(SystemColor.activeCaption);
        txtpnDescriptionIngredients.setText("Homemade Lasagne. Served with chips and salad.");
        
        JLabel label_5 = new JLabel();
        label_5.setText("8.95");
        label_5.setBounds(424, 6, 46, 14);
        panel2.add(label_5);
        label_5.setForeground(Color.WHITE);
        button2 = new JButton("Remove");
        button2.setBounds(576, 0, 73, 20);
        panel2.add(button2);
        
        JLabel label_4 = new JLabel();
        label_4.setText("1");
        label_4.setBounds(492, 6, 18, 14);
        panel2.add(label_4);
        label_4.setForeground(Color.WHITE);
        
        JLabel label = new JLabel();
        label.setText(String.format("£%1$3.2f", Double.valueOf(label_5.getText())
                * Double.valueOf(label_4.getText())));
        label.setBounds(520, 6, 46, 14);
        panel2.add(label);
        label.setForeground(SystemColor.text);
        
        JLabel picture_2 = new JLabel();
        picture_2.setBounds(10, 0, 50, 50);
        picture_2.setIcon(new ImageIcon("images/lasagne.jpg"));
        panel2.add(picture_2);
      
        button2.setFont(new Font("Tahoma", Font.PLAIN, 9));
        
        JLabel lblMenu = new JLabel("ORDER");
        lblMenu.setFont(new Font("Tahoma", Font.BOLD, 15));
        lblMenu.setBounds(10, 13, 58, 14);
        contentPane.add(lblMenu);
        
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(594, 122, 2, 2);
        contentPane.add(scrollPane);
        
        panel1 = new JPanel();
        panel1.setOpaque(false);
        panel1.setBackground(Color.BLACK);
        panel1.setBounds(20, 200, 647, 50);
        contentPane.add(panel1);
        panel1.setLayout(null);
        
        picture3 = new JLabel();
        picture3.setBounds(10, 0, 50, 50);
        picture3.setIcon(new ImageIcon("Images/margPizza.jpg"));
        panel1.add(picture3);
        
        
        txtpnFourCheesePizza = new JTextPane();
        txtpnFourCheesePizza.setOpaque(false);
        txtpnFourCheesePizza.setBounds(82, 5, 139, 20);
        panel1.add(txtpnFourCheesePizza);
        txtpnFourCheesePizza.setText("Four Cheese Pizza");
        txtpnFourCheesePizza.setForeground(Color.BLACK);
        txtpnFourCheesePizza.setFont(new Font("Tahoma", Font.PLAIN, 12));
        
        txtpnFreshlyMadeBase = new JTextPane();
        txtpnFreshlyMadeBase.setOpaque(false);
        txtpnFreshlyMadeBase.setForeground(Color.BLACK);
        txtpnFreshlyMadeBase.setBounds(109, 23, 284, 20);
        panel1.add(txtpnFreshlyMadeBase);
        txtpnFreshlyMadeBase.setText("Freshly made pizza with 4 toppings of cheese.");
        txtpnFreshlyMadeBase.setFont(new Font("Tahoma", Font.ITALIC, 11));
        
        label6 = new JLabel("8.95");
        label6.setBounds(424, 4, 58, 14);
        panel1.add(label6);
        label6.setForeground(Color.WHITE);
        
        label7 = new JLabel();
        label7.setText("1");
        label7.setBounds(492, 4, 18, 14);
        panel1.add(label7);
        label7.setForeground(Color.WHITE);
        
        label8 = new JLabel();
        label8.setText(String.format("£%1$3.2f", Double.valueOf(label6.getText())
                 * Double.valueOf(label7.getText())  ));
        label8.setBounds(520, 4, 58, 14);
        panel1.add(label8);
        label8.setForeground(Color.WHITE);
       
        button3 = new JButton("Remove");
        button3.setBounds(574, 0, 73, 20);
        panel1.add(button3);


        button3.setFont(new Font("Tahoma", Font.PLAIN, 9));
        
        trackOrder = new JButton("Track My Order");
        trackOrder.setEnabled(false);
        trackOrder.setBounds(512, 368, 128, 30);
        contentPane.add(trackOrder);
        
        JLabel lblTotalPrice = new JLabel();
        lblTotalPrice.setText(String.format("£%1$3.2f", Double.valueOf(lblDishPrice.getText().substring(1))
                 + Double.valueOf(label8.getText().substring(1)) 
                 + Double.valueOf(label.getText().substring(1))));
        lblTotalPrice.setForeground(Color.WHITE);
        lblTotalPrice.setBounds(512, 313, 56, 14);
        contentPane.add(lblTotalPrice);
        
        final long time = System.currentTimeMillis();
        
        btnConfirmOrder = new JButton("Confirm Order");
        btnConfirmOrder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                trackOrder.setEnabled(true);
                btnConfirmOrder.setEnabled(false);
               // Order thisTableOrder = new Order(1, 1, PENDING, time, null, null, )
                System.out.println("Order placed at :" + time);
            } 
        });
        btnConfirmOrder.setBounds(512, 332, 128, 30);
        contentPane.add(btnConfirmOrder);
        
        totalPrice = new JLabel();
        totalPrice.setBounds(578, 310, 58, 20);
        contentPane.add(totalPrice);
    }
    //CHECKSTYLE:ON
    
    
    public void makeConfirmOrderVisible(){
    	btnConfirmOrder.setEnabled(true);
    	trackOrder.setEnabled(false);
    }
    
	/**
	 * Adds an action listener to the Menu button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
    public void addShowMenuListener(final ActionListener listener) {
    	showMenu.addActionListener(listener);
    }
    
    /**
     * Adds an action listener to the Track Order button.
     * 
     * @param listener
     *            the action listener to be added.
     */
    public final void addShowTrackOrderViewListener(final ActionListener listener) {
        trackOrder.addActionListener(listener);
    }
	/**
	 * Adds an action listener to the first remove button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public void addButtonListener(final ActionListener listener) {
		getButton1().addActionListener(listener);	
	}
	
	/**
	 * Adds an action listener to the second remove button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public void addButtonListenerA(final ActionListener listener) {
		button2.addActionListener(listener);
	}
	/**
	 * Adds an action listener to the third remove button.
	 * 
	 * @param listener
	 *            the action listener to be added.
	 */
	public void addButtonListenerB(final ActionListener listener) {
		button3.addActionListener(listener);
	}
	
	/**
     * Adds an action listener to the Call for Assistance button.
     * 
     * @param listener
     *            the action listener to be added.
     */
    public final void addWaiterCallListener(final ActionListener listener) {
        requestAssistance.addActionListener(listener);
    }
    
	/**
	 * Adds an action listener to the Menu button.
	 * 
	 * @param panel
	 *            the panel to be hidden from the Order View.
	 */
	public void hidePanel(final int panel) {
		switch(panel) {
		case 1:
			panel3.setVisible(false);
			break;
		case 2:
			panel2.setVisible(false);
			break;
		case 3:
			panel1.setVisible(false);
			break;
		default:
			break;
	}
}
/**
 * 
 * @return - returns button1
 */
	public JButton getButton1() {
		return button1;
	}
/**
 * 
 * @param button1 
 */
	public void setButton1(final JButton button1) {
		this.button1 = button1;
	}
}

