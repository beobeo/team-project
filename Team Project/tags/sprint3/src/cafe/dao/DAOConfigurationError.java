package cafe.dao;

/**
 * Thrown to indicate that the static data access configuration is incorrect.
 * 
 * @author Michael Winter
 */
public final class DAOConfigurationError extends Error {
    /**
     * Unique serialisation identifier.
     */
    private static final long serialVersionUID = -6488959584138576453L;

    /**
     * Constructs a {@code DAOConfigurationError} with no detail message.
     */
    public DAOConfigurationError() {
        super();
    }

    /**
     * Constructs a {@code DAOConfigurationError} with the specified detail
     * message.
     * 
     * @param message
     *            the detail message.
     */
    public DAOConfigurationError(final String message) {
        super(message);
    }

    /**
     * Constructs a {@code DAOConfigurationError} with the specified detail
     * message and cause.
     * 
     * <p>
     * Note that the detail message associated with {@code cause} is not
     * automatically incorporated in this exception's detail message.
     * 
     * @param message
     *            the detail message.
     * @param cause
     *            the cause. A value of {@code null} is acceptable and indicates
     *            that the cause is either unknown or nonexistent.
     */
    public DAOConfigurationError(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a {@code DAOConfigurationError} with the specified cause
     * and a detail message of {@code (cause == null) ? null : cause.toString()}
     * (which typically contains the class and detail message of {@code cause}).
     * 
     * @param cause
     *            the cause. A value of {@code null} is acceptable and indicates
     *            that the cause is either unknown or nonexistent.
     */
    public DAOConfigurationError(final Throwable cause) {
        this((cause == null) ? null : cause.toString(), cause);
    }
}
