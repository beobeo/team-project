package cafe.dao;

import cafe.Ingredient;

/**
 * A data access object used to manipulate persisted {@link Ingredient} objects.
 * 
 * @author Michael Winter
 */
public interface IngredientDAO {
    /**
     * Given an identifier, this method loads and returns a new instance of the
     * specified {@link Ingredient}.
     * 
     * @param id
     *            the identifier.
     * @return the specified {@code Ingredient}.
     * @throws DataAccessException
     *             if an error occurs while accessing the data store.
     */
    Ingredient findIngredient(int id);
}
