package cafe.dao;

import cafe.Table;

/**
 * A data access object used to manipulate persisted {@link Table} objects.
 * 
 * @author Michael Winter
 */
public interface TableDAO {
    /**
     * Given an identifier, this method loads and returns a new instance of the
     * specified {@link Table}.
     * 
     * @param id
     *            the identifier.
     * @return the specified {@code Sitting}.
     * @throws DataAccessException
     *             if an error occurs while accessing the data store.
     */
    Table findTable(final int id);
}
