package cafe.dao.postgres;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import cafe.dao.AuthenticationException;
import cafe.dao.DAOFactory;
import cafe.dao.DataAccessException;
import cafe.dao.DishDAO;
import cafe.dao.EmployeeDAO;
import cafe.dao.IngredientDAO;
import cafe.dao.OrderDAO;
import cafe.dao.OrderItemDAO;
import cafe.dao.SittingDAO;
import cafe.dao.TableDAO;
import cafe.dao.jdbc.ConnectionException;
import cafe.dao.jdbc.ConnectionFactory;

/**
 * This class is a factory that is responsible for creating data access objects
 * (DAOs) used to interact with an underlying PostgreSQL database.
 * 
 * @author Michael Winter
 */
public final class DAOFactoryImpl extends DAOFactory implements AutoCloseable,
        ConnectionFactory {
    /**
     * The JDBC connection string for the project database. The full host name
     * is used to allow remote connections.
     */
    private static final String URI = "jdbc:postgresql://teaching.cs.rhul.ac.uk:29503/teamproject";

    /**
     * The database connection used by DAOs created by this object.
     */
    private Connection connection;
    /**
     * The user name used to authenticate the connection.
     */
    private String user;
    /**
     * The password used to authenticate the connection.
     */
    private String password;

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#getDishDAO()
     */
    @Override
    public DishDAO getDishDAO() {
        return new DishDAOImpl(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#getIngredientDAO()
     */
    @Override
    public IngredientDAO getIngredientDAO() {
        return new IngredientDAOImpl(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#getSittingDAO()
     */
    @Override
    public SittingDAO getSittingDAO() {
        return new SittingDAOImpl(getTableDAO(), this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#getSittingDAO()
     */
    @Override
    public TableDAO getTableDAO() {
        return new TableDAOImpl(getEmployeeDAO(), this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#getOrderDAO()
     */
    @Override
    public OrderDAO getOrderDAO() {
        return new OrderDAOImpl(getOrderItemDAO(), getSittingDAO(), getEmployeeDAO(), this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#getOrderItemDAO()
     */
    @Override
    public OrderItemDAO getOrderItemDAO() {
        return new OrderItemDAOImpl(getDishDAO(), this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#getEmployeeDAO()
     */
    @Override
    public EmployeeDAO getEmployeeDAO() {
        return new EmployeeDAOImpl(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.JdbcConnectionFactory#getConnection()
     */
    @Override
    public Connection getConnection() {
        if (connection != null) {
            try {
                if (connection.isClosed() || !connection.isValid(1)) {
                    connection.close();
                    connection = null;
                }
            } catch (final SQLException e) {
                // None of isClosed(), isValid(int) and close() should raise an
                // exception: VALIDATION_TIMEOUT is an acceptable value, and
                // isClosed() and close() should always succeed.
                throw new Error("Unanticipated SQL exception", e);
            }
        }

        if (connection == null) {
            try {
                connection = DriverManager.getConnection(URI, user, password);
            } catch (final SQLException e) {
                switch (e.getSQLState()) {
                case "08001": // Client connection failure
                    throw new ConnectionException(
                            "Unable to connect to the database", e);
                case "28000": // Invalid user name
                    throw new AuthenticationException("Invalid user name", e);
                case "28P01": // Invalid password
                    throw new AuthenticationException("Invalid password", e);
                default:
                    // Re-raise the exception
                    throw new DataAccessException(e);
                }
            }
            try {
                connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            } catch (final SQLException e) {
                // This exception should be impossible: the connection will be
                // newly-established (therefore valid) and the DBMS supports
                // serialisable transactions.
                throw new Error("Unanticipated SQL exception", e);
            }
        }
        return connection;
    }

    /**
     * Closes this resource, relinquishing any underlying resources. This method
     * is invoked automatically on objects managed by the try-with-resources
     * statement.
     * 
     * @see java.lang.AutoCloseable#close()
     */
    @Override
    public void close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (final SQLException e) {
                // Should not raise an exception: close() should always succeed.
                throw new Error("Unanticipated SQL exception", e);
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DAOFactory#setCredentials(String, String)
     */
    @Override
    protected void setCredentials(final String usr, final String pass) {
        if ((usr == null) || (pass == null)) {
            throw new AuthenticationException(
                    "a user name and password is required");
        }
        user = usr;
        password = pass;
        // Initialise a connection to check the credentials
        connection = getConnection();
    }
}
