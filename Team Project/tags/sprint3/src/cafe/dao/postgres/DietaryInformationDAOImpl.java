package cafe.dao.postgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import cafe.DietaryInformation;
import cafe.dao.DataAccessException;
import cafe.dao.DietaryInformationDAO;
import cafe.dao.jdbc.ConnectionFactory;

/**
 * A data access object used to manipulate {@link DietaryInformation} objects
 * that have persisted to a PostgreSQL database.
 * 
 * @author Michael Winter
 */
public final class DietaryInformationDAOImpl implements DietaryInformationDAO {
    /**
     * Given a Dish identifier and a connection, this method returns the dietary
     * information for that dish.
     * 
     * @param dish
     *            the dish to be prepared.
     * @param connection
     *            a connection over which the query will be performed.
     * @return the set of dietary and allergy information for the specified
     *         dish.
     * @throws DataAccessException
     *             if an error occurs while accessing the data store.
     */
    static Set<DietaryInformation> selectDishInformation(
            final int dish, final Connection connection) {
        final Set<DietaryInformation> information = new HashSet<>();
        try (final PreparedStatement statement = connection.prepareStatement(
                "SELECT id, description"
                + " FROM dietary_information"
                + " INNER JOIN dish_information ON id = information_id"
                + " WHERE dish_id = ?"
                + " ORDER BY description")) {
            statement.setInt(1, dish);
            final ResultSet result = statement.executeQuery();
            while (result.next()) {
                information.add(new DietaryInformation(result.getInt("id"),
                        result.getString("description")));
            }
        } catch (final SQLException e) {
            throw new DataAccessException(e);
        }
        return information;
    }
    
    
    /**
     * The source of JDBC connections used to perform queries against the
     * underlying database.
     */
    private ConnectionFactory connectionFactory;

    
    /**
     * Constructs a data access object for {@link DietaryInformation}s that will
     * operate over the given connection.
     * 
     * @param connectionFactory
     *            the factory object that will provide a database connection.
     * @throws NullPointerException
     *             if the specified factory is {@code null}.
     */
    public DietaryInformationDAOImpl(final ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }
    
    
    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.DietaryInformationDAO#findDietaryInformation(int)
     */
    @Override
    public DietaryInformation findDietaryInformation(final int id) {
        final Connection connection = connectionFactory.getConnection();
        DietaryInformation requirement = null;
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT description FROM dietary_information WHERE id = ?")) {
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                requirement = new DietaryInformation(id, result.getString("description"));
            }
        } catch (final SQLException e) {
            throw new DataAccessException(e);
        }
        return requirement;
    }
}
