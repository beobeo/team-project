/**
 * 
 */
package cafe.dao.postgres;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import cafe.Employee;
import cafe.Sitting;
import cafe.Table;
import cafe.dao.DataAccessException;
import cafe.dao.EmployeeDAO;
import cafe.dao.TableDAO;
import cafe.dao.jdbc.ConnectionFactory;

/**
 * A data access object used to manipulate {@link Table} objects that have
 * persisted to a PostgreSQL database.
 * 
 * @author Michael Winter
 */
public final class TableDAOImpl implements TableDAO {
    /**
     * The source of JDBC connections used to perform queries against the
     * underlying database.
     */
    private ConnectionFactory connectionFactory;
    /**
     * The data access object used to obtain {@link Employee} instances from the
     * database.
     */
    private EmployeeDAO employeeDataSource;

    
    /**
     * Constructs a data access object for {@link Sitting} objects that will operate over
     * the given connection.
     * 
     * @param employeeDAO
     *            the data access object used to supply {@link Employee}
     *            instances.
     * @param connectionFactory
     *            the factory object that will provide a database connection.
     * @throws NullPointerException
     *             if the specified factory or data access objects are {@code null}.
     */
	public TableDAOImpl(final EmployeeDAO employeeDAO, final ConnectionFactory connectionFactory) {
        if (employeeDAO == null) {
            throw new NullPointerException("employeeDataSource is null");
        }
        if (connectionFactory == null) {
            throw new NullPointerException("connectionFactory is null");
        }
        this.employeeDataSource = employeeDAO;
		this.connectionFactory = connectionFactory;
	}

    /*
     * (non-Javadoc)
     * 
     * @see cafe.dao.TableDAO#findTable(int)
     */
    @Override
    public Table findTable(final int id) {
        final Connection connection = connectionFactory.getConnection();
        Table table = null;
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT waiter_id FROM \"table\" WHERE id = ?")) {
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                // TODO: Complete implementation of method
                table = new Table(id,
                        employeeDataSource.findEmployee(result.getInt("waiter_id")));
            }
        } catch (final SQLException e) {
            throw new DataAccessException(e);
        }
        return table;
    }

}
