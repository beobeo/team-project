package cafe;

/**
 * Represents allergy and dietary preference information that applies to a
 * {@link Dish}.
 * 
 * @author Michael Winter
 */
public final class DietaryInformation {
	/**
	 * The unique identifier for this instance.
	 */
	private int id;
	/**
	 * The name of the requirement.
	 */
	private String name;

	
    /**
     * Constructs a new dietary requirement instance.
     * 
     * @param id
     *            the unique identifier for this instance.
     * @param name
     *            the name of the requirement.
     */
    public DietaryInformation(final int id, final String name) {
        this.id = id;
        this.name = name;
    }
    
    /**
     * Returns the unique identifier for this instance.
     * 
     * @return the identifier.
     */
    public int getId() {
        return id;
    }

	/**
	 * Returns the name of this dietary requirement.
	 * 
	 * @return the name.
	 */
	public String getName() {
		return name;
	}
	

    /**
     * Sets the name of this dietary requirement.
     * 
     * @param string
     *            the name (must not be {@code null}).
     * @throws NullPointerException
     *             if {@code string} is {@code null}.
     */
	void setName(final String string) {
		if (string == null) {
			throw new NullPointerException();
		}
		name = string;
	}
}
