package cafe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Thrown to indicate that one or more ordered dishes are not available due to low stock.
 * 
 * @author Michael Winter
 */
public final class DishesUnavailableException extends Exception {
    /**
     * Unique serialisation identifier.
     */
    private static final long serialVersionUID = 355194352413230003L;
    /**
     * The collection of dishes that are unavailable.
     */
    private final Collection<Dish> dishes;
    
    
    /**
     * Constructs a {@code DishesUnavailableException} with the specified
     * collection of unavailable dishes.
     * 
     * @param unavailableDishes
     *            the dishes that are unavailable.
     */
    public DishesUnavailableException(final Collection<Dish> unavailableDishes) {
        dishes = Collections.unmodifiableList(new ArrayList<>(unavailableDishes));
    }
    
    /**
     * Returns the unavailable dishes.
     * 
     * @return the unavailable dishes.
     */
    public Collection<Dish> getDishes() {
        return dishes;
    }
}
