package cafe;

import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Represents an item that can be ordered by a client and is listed in the menu.
 * 
 * TODO: Add remove ingredient method.
 * 
 * @author Michael Winter
 */
public final class Dish {
	/**
	 * The unique identifier for this instance.
	 */
	private int id;
	/**
	 * The name of the dish.
	 */
	private String name;
	/**
	 * A brief description of the contents of the dish.
	 */
	private String description;
	/**
	 * Determines whether this dish should be displayed apart from ingredient availability. This
	 * may be used to temporarily remove a dish from the menu without deleting it entirely from
	 * the database.
	 */
	private boolean visible = true;
	/**
	 * The number of calories, in kcal, of this dish.
	 */
	private int calories;
	/**
	 * The price, in pounds sterling, of this dish.
	 */
	private float price;
	/**
	 * The category, such as the type of course, for this dish.
	 */
	private String category;
	/**
	 * A mapping of ingredients to the quantity required to prepare this dish.
	 */
	private Map<Ingredient, Integer> requiredIngredients = new HashMap<>();
	/**
	 * A set of allergy- and diet-related information for this dish.
	 */
	private Set<DietaryInformation> dietaryInformation = new HashSet<>();
	
	/**
	 * Image displaying the dish.
	 */
	private BufferedImage image;

	
    /**
     * Constructs a new {@code Dish} instance.
     * 
     * @param id
     *            the unique identifier for this instance.
     * @param name
     *            the name of the dish.
     * @param description
     *            a brief description of the contents of the dish.
     * @param price
     *            the price, in pounds sterling, of this dish.
     * @param calories
     *            the number of calories, in kcal, of this dish.
     * @param category
     *            the category for this dish.
     * @param image 
     * @param visible
     *            {@code true} if this dish should be displayed;
     *            {@code false otherwise}.
     * @param requiredIngredients
     *            a map of ingredients to the quantity required to prepare this
     *            dish.
     * @param dietaryInformation
     *            the set of allergy- and diet-related information for this
     *            dish.
     */
	public Dish(final int id, final String name, final String description,
			final float price, final int calories, final String category,
			final BufferedImage image, final boolean visible,
			final Map<Ingredient, Integer> requiredIngredients,
			final Set<DietaryInformation> dietaryInformation) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.calories = calories;
        this.category = category;
        this.image = image;
        this.visible = visible;
        this.requiredIngredients.putAll(requiredIngredients);
        this.dietaryInformation.addAll(dietaryInformation);
    }

    /**
	 * Returns the number of calories of this dish (in kcal).
	 * 
	 * @return the number of calories.
	 */
	public int getCalories() {
		return calories;
	}
	
	/**
	 * Returns the category, such as the type of course, of this dish.
	 * 
	 * @return the category for this dish.
	 */
	public String getCategory() {
		return category;
	}
	
	/**
	 * Returns a brief description of the contents or style of this dish.
	 * 
	 * @return a description of this dish.
	 */
	public String getDescription() {
		return description;
	}
    
    /**
     * Returns a set of allergy and dietary information descriptions for this
     * dish. The set cannot be modified.
     * 
     * @return an unmodifiable set of descriptive information for this dish.
     */
    public Set<DietaryInformation> getDietaryInformation() {
        return Collections.unmodifiableSet(dietaryInformation);
    }
    
    /**
     * Returns a set of ingredients used to prepare this dish. The set cannot be modified.
     * 
     * @return an unmodifiable set of ingredients contained in this dish.
     */
    public Set<Ingredient> getIngredients() {
        return Collections.unmodifiableSet(requiredIngredients.keySet());
    }
	
	/**
	 * Returns the name of this dish.
	 * 
	 * @return the name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the price of this dish (in pounds stirling).
	 * 
	 * @return the price.
	 */
	public float getPrice() {
		return price;
	}
	/**
	 * returns image on dish.
	 * @return dish image
	 */
	public BufferedImage getImage() {
		return image;
	}
	
    /**
     * Returns whether this dish is available and should be displayed in the
     * menu. Availability is determined both by the available ingredient stock
     * and whether the dish has been explicitly hidden.
     * 
     * @return {@code true} if this dish is available for ordering;
     *         {@code false} otherwise.
     */
    public boolean isAvailable() {
        if (!visible) {
            return false;
        }
        for (final Map.Entry<Ingredient, Integer> ingredientQuantityRequired
                : requiredIngredients.entrySet()) {
            final Ingredient ingredient = ingredientQuantityRequired.getKey();
            final int quantityRequired = ingredientQuantityRequired.getValue();
            if (!ingredient.isAvailable(quantityRequired)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns whether this dish has been explicitly hidden from the menu.
     * 
     * @return {@code true} if this dish is visible; {@code false} otherwise.
     */
	public boolean isVisible() {
		return visible;
	}
    
    /**
     * Returns whether this dish is suitable with respect to a given type of
     * allergy or dietary requirement.
     * 
     * @param requirement
     *            the dietary need to be tested.
     * @return {@code true} if the requirement is satisfied; {@code false}
     *         otherwise.
     */
    public boolean isSuitableFor(final DietaryInformation requirement) {
        return dietaryInformation.contains(requirement);
    }
	
	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !(obj instanceof Dish)) {
            return false;
        }
        Dish other = (Dish) obj;
        if (id != other.id) {
            return false;
        }
        return true;
    }

    /**
	 * Returns the unique identifier for this dish.
	 * 
	 * @return the identifier.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the number of calories in this dish. This must be greater than, or equal to, zero.
	 * 
	 * @param amount the number of calories (must be >= 0).
	 * @throws IllegalArgumentException if {@code amount} is negative.
	 */
	void setCalories(final int amount) {
		if (amount < 0) {
			throw new IllegalArgumentException();
		}
		calories = amount;
	}

	/**
	 * Sets the category for this dish.
	 * 
	 * @param string the category (must not be {@code null}).
	 * @throws NullPointerException if {@code string} is {@code null}.
	 */
	void setCategory(final String string) {
		if (string == null) {
			throw new NullPointerException();
		}
		category = string;
	}
	
	/**
	 * Sets the description for this dish.
	 * 
	 * @param string the description (must not be {@code null}).
	 * @throws NullPointerException if {@code string} is {@code null}.
	 */
	void setDescription(final String string) {
		if (string == null) {
			throw new NullPointerException();
		}
		description = string;
	}

	/**
	 * Adds an ingredient to this dish, and quantity required to prepare it.
	 * 
	 * @param ingredient the ingredient (must not be {@code null}).
	 * @param requiredAmount the quantity of the ingredient needed (must be > 0).
	 * @throws IllegalArgumentException if {@code requiredAmount} is less than, or equal to, zero.
	 * @throws NullPointerException if {@code ingredient} is {@code null}.
	 */
	void addIngredient(final Ingredient ingredient, final int requiredAmount) {
		if (ingredient == null) {
			throw new NullPointerException();
		}
		if (requiredAmount <= 0) {
			throw new IllegalArgumentException();
		}
		requiredIngredients.put(ingredient, requiredAmount);
	}

    /**
     * Removes an ingredient from this dish.
     * 
     * @param ingredient
     *            the ingredient.
     */
    void removeIngredient(final Ingredient ingredient) {
        requiredIngredients.remove(ingredient);
    }
	
	/**
	 * Sets the name of this dish.
	 * 
	 * @param string the name (must not be {@code null}).
	 * @throws NullPointerException if {@code string} is {@code null}.
	 */
	void setName(final String string) {
		if (string == null) {
			throw new NullPointerException();
		}
		name = string;
	}
	
	/**
	 * Sets the price of this dish. This must be greater than, or equal to, zero.
	 * 
	 * @param amount the price (must be >= 0).
	 * @throws IllegalArgumentException if {@code amount} is negative.
	 */
	void setPrice(final float amount) {
		if (amount < 0) {
			throw new IllegalArgumentException();
		}
		price = amount;
	}
	
	/**
	 * Sets the visibility of this dish, independent of ingredient stock levels, allowing it to be
	 * temporarily hidden. A visible dish is subject to ingredient checks, but an invisible dish
	 * will always be hidden.
	 * 
	 * @param visible {@code true} if this dish should be visible; {@code false} otherwise.
	 * @throws IllegalArgumentException if {@code amount} is negative.
	 */
	void setVisible(final boolean visible) {
		this.visible = visible;
	}
}
