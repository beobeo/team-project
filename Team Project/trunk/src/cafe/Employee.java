package cafe;

/**
 * Represents an employee of the cafe.
 * 
 * @author Michael Winter
 */
public class Employee {
    /**
     * The unique identifier for this instance.
     */
    private int id;
    /**
     * The first name of this employee.
     */
    private String firstName;
    /**
     * The last name of this employee.
     */
    private String lastName;
    
    
    /**
     * Constructs a new {@code Employee} instance.
     * 
     * @param id
     *            the unique identifier for this instance.
     * @param firstName
     *            the employee's first name.
     * @param lastName
     *            the employee's last name. 
     */
    public Employee(final int id, final String firstName, final String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Returns the unique identifier for this {@code Employee}.
     * 
     * @return the identifier
     */
    public final int getId() {
        return id;
    }

    /**
     * Returns the first name of this {@code Employee}.
     * 
     * @return the employee's first name.
     */
    public final String getFirstName() {
        return firstName;
    }

    /**
     * Returns the last name of this {@code Employee}.
     * 
     * @return the employee's last name
     */
    public final String getLastName() {
        return lastName;
    }
}
