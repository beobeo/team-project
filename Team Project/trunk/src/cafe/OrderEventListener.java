package cafe;

import java.util.EventListener;

/**
 * Defines the interface for an object that listens to changes in an
 * {@link cafe.client.order.OrderModel OrderModel}.
 * 
 * @author Michael Winter
 */
public interface OrderEventListener extends EventListener {
	/**
	 * Invoked after an order has been added to the list of active orders.
	 * 
	 * @param e an order added event.
	 */
	void orderAdded(OrderEvent e);
    
    /**
     * Invoked after an order has changed in some way.
     * 
     * @param e an order changed event.
     */
    void orderChanged(OrderEvent e);
    
    /**
     * Invoked after an order has removed from the active list.
     * 
     * @param e an order removed event.
     */
    void orderRemoved(OrderEvent e);
}
