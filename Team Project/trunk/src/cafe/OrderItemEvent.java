package cafe;

import java.util.EventObject;

/**
 * {@code OrderItemEvent} is used to notify interested parties that the state
 * has changed in the event source.
 * 
 * @author Michael Winter
 */
public class OrderItemEvent extends EventObject {
	/**
	 * Unique serialisation identifier.
	 */
	private static final long serialVersionUID = 4149494823188722199L;

	/**
	 * Constructs an order model event.
	 * 
	 * @param source
	 *            The object on which the {@code OrderItemEvent} initially
	 *            occurred.
	 * @throws IllegalArgumentException
	 *             if source is {@code null}.
	 */
	public OrderItemEvent(final Object source) {
		super(source);
	}
}
