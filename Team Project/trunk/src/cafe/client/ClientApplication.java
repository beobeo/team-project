package cafe.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.TableModel;

import cafe.Application;
import cafe.Order;
import cafe.OrderEvent;
import cafe.OrderEventListener;
import cafe.Sitting;
import cafe.Table;
import cafe.client.menu.MenuController;
import cafe.client.menu.MenuGUI;
import cafe.client.menu.MenuModel;
import cafe.client.order.OrderController;
import cafe.client.order.OrderGUI;
import cafe.client.order.OrderModel;
import cafe.client.payment.PaymentController;
import cafe.client.payment.PaymentEvent;
import cafe.client.payment.PaymentListener;
import cafe.client.payment.PaymentModel;
import cafe.client.payment.PaymentOverviewTableModel;
import cafe.client.payment.PaymentView;
import cafe.client.trackOrder.TrackOrderController;
import cafe.client.trackOrder.TrackOrderModel;
import cafe.client.trackOrder.TrackOrderView;
import cafe.dao.AuthenticationException;
import cafe.dao.DAOFactory;
import cafe.dao.DataAccessException;
import cafe.dao.OrderDAO;
import cafe.dao.PaymentDAO;
import cafe.dao.SittingDAO;
import cafe.dao.postgres.DAOFactoryImpl;

/**
 * Provides a driver for the client's application, handling the relationship
 * between the menu, order status display, and payment processing.
 * 
 * @author Michael Winter
 * @author Jonathan Hercock
 * @author Hannah Cooper
 * @author Robert Kardjaliev
 */
public final class ClientApplication extends Application {
	/**
	 * A logging instance used to record errors and debugging information during
	 * execution.
	 */
	private static final Logger LOGGER = Logger.getLogger(ClientApplication.class.getName());
	/**
	 * The current sitting. This instance references the client's orders during
	 * their visit and is reset once the client has paid and left.
	 */
	private Sitting currentSitting;
	/**
	 * The controller for the client menu.
	 */
	private MenuController menuController;
	/**
	 * The controller for the client order display.
	 */
	private OrderController orderController;
	/**
	 * The controller for the client track order display.
	 */
	private TrackOrderController trackOrderController;
    /**
     * The model for the {@link OrderGUI}.
     */
	private OrderModel orderModel;
	/**
	 * The data model used by the menu interface.
	 */
	private MenuModel menuModel;
	/**
	 * The data model used by the order tracking interface.
	 */
	private TrackOrderModel trackOrderModel;
	/**
	 * The data access object used to obtain and persist {@code Order} instances.
	 */
	private OrderDAO orderDataSource;
	/**
	 * The data access object used to obtain and persist {@code Sitting} instances.
	 */
	private SittingDAO sittingDataSource;
	/**
	 * The data access object used to obtain and persist {@code Payment} instances.
	 */
	private PaymentDAO paymentDataSource;
	/**
	 * The {@code Table} on which this application is running.
	 */
	private Table table;
	/**
	 * The ID of the {@link Order}.
	 */
	private int orderId;

	
	/**
	 * The main entry point for the application. The application expects that 
	 * 
	 * @param args the command line arguments passed to the application.
	 */
	public static void main(final String[] args) {
		launch(ClientApplication.class, args);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see cafe.Application#initialise(java.lang.String[])
	 */
	@Override
	protected void initialise(final String[] args) {
		int tableId = 0;
		try {
			tableId = parseTableNumber(args);
		} catch (final RuntimeException e) {
			showUsage();
			return;
		}

		/*
		 * Initialise the data object factory using the PostgreSQL
		 * implementation, using the given credentials to authenticate the
		 * database connection. These details could be provided by the user when
		 * the application loads, rather than hard-coded. For example, guest
		 * details would only allow users read access to the menu, insert access
		 * to service requests, and insert/update to orders. If a waiter logs
		 * in, they also can read and update service requests, insert and update
		 * bookings, etc.
		 */
		DAOFactory factory;
		try {									// won't work without database connection here
			factory = DAOFactory.getInstance(System.getenv("USERNAME"), System.getenv("PASSWORD"), DAOFactoryImpl.class);
		} catch (final AuthenticationException e) {
            JOptionPane.showMessageDialog(
                    null,
                    "The user name or password used to access information is incorrect."
                            + "\nPlease speak to a member of staff for assistance.",
                    "Bad user name/password", JOptionPane.ERROR_MESSAGE);
            String message = "Database authentication failure";
            LOGGER.log(Level.SEVERE, message, e);
            throw new Error(message, e);
        } catch (final DataAccessException e) {
            JOptionPane.showMessageDialog(
                    null,
                    "The application is unable to get some required information and must close."
                            + "\nPlease speak to a member of staff for assistance.",
                    "Connection failed", JOptionPane.ERROR_MESSAGE);
            String message = "Database connection failed";
            LOGGER.log(Level.SEVERE, message, e);
            throw new Error(message, e);
        }

		table = factory.getTableDAO().findTable(tableId);
		if (table == null) {
			throw new IllegalArgumentException("Table does not exist (id: "
					+ tableId + ")");
		}
		
        sittingDataSource = factory.getSittingDAO();
        orderDataSource = factory.getOrderDAO();
        paymentDataSource = factory.getPaymentDAO();
        
        orderModel = new OrderModel(orderDataSource, factory.getOrderItemDAO(),
                factory.getIngredientDAO());
        menuModel = new MenuModel(factory.getDishDAO());
        trackOrderModel = new TrackOrderModel(orderDataSource);

        beginSitting();
	}
		
	/**
	 * Starts the application.
	 */
    @Override
	protected void run() {
        final OrderGUI orderView = new OrderGUI();
        orderController = new OrderController(orderModel, orderView);
        orderController.addShowMenuListener(new ShowMenuListener());
        orderController.addWaiterCallListener(new WaiterCallListener());
        orderController.addShowTrackOrderViewListener(new ShowTrackOrderViewListener());

        final MenuGUI menuView = new MenuGUI();
        menuController = new MenuController(menuModel, menuView);
        menuController.addDishAddedListener(orderController.getOrderItemListener());
        menuController.addShowOrderListener(new ShowOrderListener());
        menuController.addWaiterCallListener(new WaiterCallListener());
        
        final TrackOrderView trackView = new TrackOrderView();
        trackOrderController = new TrackOrderController(trackView, trackOrderModel);
        trackOrderController.addCancelOrderListener(new CancelOrder());
        trackOrderController.addEditOrderListener(new EditOrder());
        trackOrderController.addOrderModelListener(new OrderDeliveredListener());

        menuController.showView();
	}
	
	
	/**
	 * Interprets command line arguments received when the application was
	 * invoked.
	 * 
	 * @param args
	 *            the arguments received from the command line as a series of
	 *            words or quoted strings.
	 * @return the identifier of the table on which this application is running.
	 */
	private int parseTableNumber(final String[] args) {
		try {
			return Integer.parseInt(args[0]);
		} catch (final ArrayIndexOutOfBoundsException e) {
			final String message = "Required argument TABLE was missing.";
			LOGGER.log(Level.SEVERE, message);
			throw e;
		} catch (final NumberFormatException e) {
			final String message = "TABLE was not a number.";
			LOGGER.log(Level.SEVERE, message);
			throw e;
		}
	}
	
	
	/**
	 * Creates a new {@code Sitting} and prepares a new order for the client.
	 */
	private void beginSitting() {
        final int sittingId = sittingDataSource.createSitting(table.getId());
        currentSitting = sittingDataSource.findSitting(sittingId);
		beginOrder();
	}
	
	/**
	 * Creates a new {@code Order}, updating the order editing and tracking
	 * interfaces accordingly.
	 */
	private void beginOrder() {
		orderId = orderDataSource.createOrder(currentSitting.getId());
		final Order order = orderDataSource.findOrder(orderId);
		orderModel.setOrder(order);
		trackOrderModel.setOrder(order);
	}
	
	/**
	 * Sends invocation instructions to standard output.
	 */
	private void showUsage() {
		final String message = "Usage: java %s TABLE\n"
				+ "       TABLE is the table number on which this application is running.";
		System.out.println(String.format(message, ClientApplication.class.getName()));
	}

	
	/**
	 * Listens for order-related events originating from the Order Tracking
	 * component, prompting for either payment or further orders.
	 */
	private class OrderDeliveredListener implements OrderEventListener {
		@Override
		public void orderAdded(final OrderEvent e) {
		}

		@Override
		public void orderChanged(final OrderEvent e) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					int option = JOptionPane.showConfirmDialog(
							trackOrderController.getView(),
							"We hope you enjoy your order. Would you like to place another?",
							"Order Again?",
							JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE);
					if (option == JOptionPane.YES_OPTION) {
						beginOrder();
						orderController.populateView();
						trackOrderController.hideView();
						menuController.showView();
					} else {
						trackOrderController.hideView();
						orderController.hideView();
						menuController.showView();
						
						PaymentModel paymentModel = new PaymentModel(currentSitting,
								orderDataSource, paymentDataSource);
						TableModel tableModel = new PaymentOverviewTableModel(
								paymentModel.getDishQuantities());
						PaymentView view = new PaymentView(tableModel);
						PaymentController controller = new PaymentController(paymentModel, view);
						controller.addPaymentCompletedListener(new PaymentCompletedListener());
						controller.showView();
					}
					orderController.resetControls();
				}
			});
		}

		@Override
		public void orderRemoved(final OrderEvent e) {
		}
    }
	
	
	/**
	 * Listens for button click events originating from the Order button, hiding the menu view
	 * and displaying the order status view.
	 */
	private class ShowOrderListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			orderController.showView();
			menuController.hideView();
		}
	}
	
	/**
	 * Listens for button click events originating from the Menu button, hiding the order view
	 * and displaying the menu.
	 */
	private class ShowMenuListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			menuController.showView();
			orderController.hideView();
		}
	}
	
	
	
	/**
	 * Listens for button click events originating from the Track My Order button, 
	 * hiding the order view and displaying the Track Order view.
	 */
	private class ShowTrackOrderViewListener implements ActionListener {
	    @Override
	    public void actionPerformed(final ActionEvent e) {
	        orderController.hideView();
	        trackOrderController.setTimer(600); // Sets timer to 10 minutes
	        trackOrderController.showView();
			trackOrderController.refreshProgress();
	    }
	}
	
	
	/**
	 * Listens for button click events originating from the Call for Assistance button,
	 * alerting the waiters that a table requires attention.
	 */
	private class WaiterCallListener implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			System.out.println("Service request sent...");
		}
	}

	
	/**
	 * Listens for completion of the payment process before resetting the menu
	 * for the next customer.
	 */
	private class PaymentCompletedListener implements PaymentListener {
		@Override
		public void paymentCompleted(final PaymentEvent e) {
			JOptionPane.showMessageDialog(
					null,
					"Thank you! Your payment has been received."
							+ "\nWe hope you enjoyed your meal and"
							+ " look forward to seeing you again.",
					"Payment complete",
					JOptionPane.INFORMATION_MESSAGE);
			beginSitting();
			menuController.showView();
		}
	}
	
	/**
	 * 
	 * @author Hannah Cooper
	 * ActionListener for Cancel Order button of TrackOrder GUI. 
	 * Actions include:
	 * presenting user with a confirm pop-up once the button is clicked 
	 * and changes the user back to the menu screen.
	 */
	private class CancelOrder implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			int option = JOptionPane.showConfirmDialog(null,
					"Are you sure you want to cancel your order?",
					"Cancel Order", JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);

			if (option == JOptionPane.YES_OPTION) {
				trackOrderController.hideView();
				menuController.showView();
				orderController.resetControls();
				final Order order = orderDataSource.findOrder(orderId);
				order.cancel();
				orderDataSource.updateOrder(order);
				beginOrder();
			}
		}
	}


	/**
	 * 
	 * @author Hannah Cooper
	 * ActionListener for EditOrder button of TrackOrder GUI. 
	 * Actions include:
	 * presenting user with a confirm pop-up once the button is clicked 
	 * and changes the user back to order screen so they can begin to edit their order again.
	 */
	private class EditOrder implements ActionListener {
		@Override
		public void actionPerformed(final ActionEvent e) {
			int option = JOptionPane.showConfirmDialog(
					null,
					"Editing your order will increase the time that it will take for"
					+ " your food to be delivered.\nWould you like to continue?",
					"Edit Order",
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);
			
			if (option == JOptionPane.YES_OPTION) {
				trackOrderController.hideView();
				orderController.showView();
				orderController.resetControls();
				orderModel.newProgress();
			}
		}	
	}
}
