package cafe.client.payment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cafe.Dish;
import cafe.Order;
import cafe.OrderItem;
import cafe.Sitting;
import cafe.dao.OrderDAO;
import cafe.dao.PaymentDAO;

/**
 * Obtains information required to determine payment for a {@code Sitting} from the database.
 * 
 * @author Michael Winter
 */
public final class PaymentModel {
	/**
	 * An Order data access object, used to obtain information about previously persisted orders.
	 */
	private final OrderDAO orderDataSource;
	/**
	 * A Payment data access object, used to create and persist payments.
	 */
	private final PaymentDAO paymentDataSource;
	/**
	 * The sitting to be queried for ordered dishes.
	 */
	private final Sitting sitting;
	/**
	 * A list of all delivered orders made during the sitting.
	 */
	private List<Order> orders = null;
	
	
	/**
	 * Creates the Payment model.
	 * 
	 * @param sitting
	 * 			  the sitting to be queried for ordered dishes.
	 * @param orderDataSource
	 *            the data access object used to obtain order information.
	 * @param paymentDataSource 
	 *            the data access object used to create payment records.
	 */
	public PaymentModel(final Sitting sitting, final OrderDAO orderDataSource,
			final PaymentDAO paymentDataSource) {
		this.sitting = sitting;
		this.orderDataSource = orderDataSource;
		this.paymentDataSource = paymentDataSource;
	}

	/**
	 * Constructs a map of ordered dishes to the quantity ordered across a sitting.
	 * 
	 * @return the dish quantity mapping.
	 */
	public Map<Dish, Integer> getDishQuantities() {
		synchronized (this) {
			if (orders == null) {
				orders = orderDataSource.selectOrdersBySitting(sitting.getId());
			}
		}
		final Map<Dish, Integer> orderedDishes = new HashMap<>();
		for (Order order : orders) {
			final List<OrderItem> items = order.getItems();
			for (OrderItem item : items) {
				final Dish dish = item.getDish();
				final int quantity = item.getQuantity();
				if (!orderedDishes.containsKey(dish)) {
					orderedDishes.put(dish, quantity);
				} else {
					orderedDishes.put(dish, quantity + orderedDishes.get(dish));
				}
			}
		}
		return orderedDishes;
	}

	/**
	 * Returns the total bill for the {@code Sitting}.
	 * 
	 * @return the total cost.
	 */
	public float getTotalPrice() {
		float total = 0;
		for (Order order : orders) {
			for (OrderItem item : order.getItems()) {
				total += item.getDish().getPrice() * item.getQuantity();
			}
		}
		return total;
	}

	/**
	 * Returns the {@code Sitting} that is being paid.
	 * 
	 * @return the sitting.
	 */
	public Sitting getSitting() {
		return sitting;
	}

	/**
	 * Records full payment against the sitting using the specific credit card
	 * number (if applicable).
	 * 
	 * @param cardNumber
	 *            the card number as a 16-digit string, or {@code null} if
	 *            payment was by cash.
	 */
	public void addPayment(final String cardNumber) {
		paymentDataSource.createPayment(sitting.getId(), cardNumber, getTotalPrice());
	}
}
