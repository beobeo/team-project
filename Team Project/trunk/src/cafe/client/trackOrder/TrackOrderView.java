package cafe.client.trackOrder;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

/**
 * 
 * Presents a graphical window to the customer(s) from which he view
 * his {@code Order}'s progress.
 * 
 * @author Robert Kardjaliev
 *
 */
public class TrackOrderView extends JFrame {
	/** Unique serialisation identifier. */
	private static final long serialVersionUID = 4566928122223960827L;
	/** The content pane. */
	private JPanel contentPane;
	/** Cancel Order button. */
	private JButton cancelOrder;
	/** Edit Order button.	 */
	private JButton editOrder;
	/** The progress bar. */
	private JProgressBar progressBar;
	/** A button to request waiter's assistance. */
	private JButton requestAssistance;
	
	/** 
	 * A label that will show the {@code OrderState} as a String.
	 */
	private JLabel progressLabel;
	/**
	 * A button for opening the Payment view.
	 */
	private JButton btnPayment;
	/**
	 * A label that gives an estimate of how much time is remaining
	 * or a text message.
	 */
	private JLabel lblTime;
	
	/**
	 * Create the frame.
	 */
	public TrackOrderView() {
		//CHECKSTYLE:OFF
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
        contentPane.setOpaque(false);
        contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblMenu = new JLabel("TRACK ORDER");
		lblMenu.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblMenu.setBounds(10, 11, 123, 25);
		contentPane.add(lblMenu);

		cancelOrder = new JButton("Cancel Order");
		cancelOrder.setFont(new Font("Tahoma", Font.BOLD, 12));
		cancelOrder.setBounds(284, 210, 123, 25);
		contentPane.add(cancelOrder);

		editOrder = new JButton("Edit Order");
		editOrder.setFont(new Font("Tahoma", Font.BOLD, 12));
		editOrder.setBounds(284, 174, 123, 25);
		contentPane.add(editOrder);

		progressBar = new JProgressBar();
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		progressBar.setBounds(128, 47, 146, 14);
		contentPane.add(progressBar);

		JLabel lblOrderProgress = new JLabel("Order Progress:");
		lblOrderProgress.setBounds(10, 47, 116, 14);
		contentPane.add(lblOrderProgress);
		
		requestAssistance = new JButton("Call for Assistance");
		requestAssistance.setFont(new Font("Tahoma", Font.BOLD, 12));
		requestAssistance.setBounds(20, 226, 146, 25);
		contentPane.add(requestAssistance);
		
		progressLabel = new JLabel("");
		progressLabel.setBounds(128, 72, 146, 14);
		contentPane.add(progressLabel);
		
		btnPayment = new JButton("Payment");
		btnPayment.setEnabled(false);
		btnPayment.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnPayment.setBounds(284, 14, 123, 23);
		contentPane.add(btnPayment);
		
		JLabel label = new JLabel("Approx. Time Remaining:");
		label.setBounds(10, 97, 156, 14);
		contentPane.add(label);
		
		lblTime = new JLabel("");
		lblTime.setBounds(161, 97, 246, 14);
		contentPane.add(lblTime);
		
		JLabel lblCurrentState = new JLabel("Current State:");
		lblCurrentState.setBounds(10, 72, 111, 14);
		contentPane.add(lblCurrentState);
	}
	//CHECKSTYLE:ON
	/**
	 * Adds a listener to the Cancel Order button.
	 * @param listener the listener
	 */
	public final void addCancelOrderListener(final ActionListener listener) {
		cancelOrder.addActionListener(listener);
	}

	/**
	 * Adds a listener to the Edit Order button.
	 * @param listener 
	 * 				the listener
	 */

	public final void addEditOrderListener(final ActionListener listener) {
		editOrder.addActionListener(listener);
	}

	/**
	 * A method to update the progress bar being displayed.
	 * @param value
	 * 			The progress's percentage.
	 */
	public final void setProgressValue(final int value) {
		progressBar.setValue(value);
	}
	
	/**
	 * A method to update the progress label being displayed.
	 * @param value
	 * 			The stage of progress an Order is at.
	 */
	public final void setProgressLabel(final String value) {
		progressLabel.setText(value);
	}
	/**
	 * A method that sets the time remaining until an {@code Order's} completion.
	 * @param value
	 * The String message of the time remaining or an actual message if an order
	 * is taking too long.
	 */
	public final void setTimeLabel(final String value) {
		lblTime.setText(value);
	}
	
	/**
	 * A method to enable/disable the cancelOrder button.
	 * @param enabled
	 * 				{@code true} to enable the button; {@code false} otherwise.
	 * 
	 */
	public final void cancelOrderEnabled(final boolean enabled) {
		cancelOrder.setEnabled(enabled);
	}
	/**
	 * A method to disable the editOrder button.
	 * @param enabled
	 * 				{@code true} to enable the button; {@code false} otherwise.
	 *
	 */
	public final void editOrderEnabled(final boolean enabled) {
		editOrder.setEnabled(enabled);		
	}
	
	/**
	 * Adds an action listener to the Call for Assistance button.
	 * 
	 * @param listener
	 *            the listener to be added.
	 */
    public final void addWaiterCallListener(final ActionListener listener) {
    	requestAssistance.addActionListener(listener);
    }
    
	/**
	 * Adds an action listener to the Call for Assistance button.
	 * 
	 * @param listener
	 *            the listener to be added.
	 */
    public final void addPaymentButtonListener(final ActionListener listener) {
    	btnPayment.addActionListener(listener);
    }
    /**
     * Enables/disables the Payment button.
     * @param enabled
     * 			{@code true} to enable the button; {@code false} otherwise.
     */
	public final void paymentButtonEnabled(final boolean enabled) {
		btnPayment.setEnabled(enabled);		
	}
}


