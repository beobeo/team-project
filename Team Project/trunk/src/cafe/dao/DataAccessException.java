package cafe.dao;

/**
 * Thrown to indicate that an error occurred while trying to obtain data.
 * 
 * @author Michael Winter
 */
public class DataAccessException extends RuntimeException {
    /**
     * Unique serialisation identifier.
     */
    private static final long serialVersionUID = -2961009743558128109L;

    /**
     * Constructs a {@code ConnectionException} with no detail message.
     */
    public DataAccessException() {
        super();
    }

    /**
     * Constructs a {@code ConnectionException} with the specified detail
     * message.
     * 
     * @param message
     *            the detail message.
     */
    public DataAccessException(final String message) {
        super(message);
    }

    /**
     * Constructs a {@code ConnectionException} with the specified detail
     * message and cause.
     * 
     * <p>
     * Note that the detail message associated with {@code cause} is not
     * automatically incorporated in this exception's detail message.
     * 
     * @param message
     *            the detail message.
     * @param cause
     *            the cause. A value of {@code null} is acceptable and indicates
     *            that the cause is either unknown or nonexistent.
     */
    public DataAccessException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a {@code ConnectionException} with the specified cause and a
     * detail message of {@code (cause == null) ? null : cause.toString()}
     * (which typically contains the class and detail message of {@code cause}).
     * 
     * @param cause
     *            the cause. A value of {@code null} is acceptable and indicates
     *            that the cause is either unknown or nonexistent.
     */
    //CHECKSTYLE:OFF
    public DataAccessException(final Throwable cause) {
        this((cause == null) ? null : cause.toString(), cause);
    }
}
