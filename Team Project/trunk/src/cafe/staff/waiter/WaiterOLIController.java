package cafe.staff.waiter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import cafe.Employee;
import cafe.Order;

/**
 * Observes user events from the order items displayed as part of the waiter
 * view and directs behaviour accordingly. For reference: OLI = OrderListItem.
 * 
 * @author Jonathan Hercock
 * 
 */
public class WaiterOLIController {

    /**
     * The panel displayed as part of the active order list.
     */
    private WaiterOLIView listView;
    /**
     * The dialogue box that details the items within an order.
     */
    private WaiterODView detailView;

    /**
     * Constructs a controller for the given views.
     * 
     * @param listView
     *            the order panel view.
     * @param detailView
     *            the order details view.
     */
    public WaiterOLIController(final WaiterOLIView listView,
            final WaiterODView detailView) {
        this.listView = listView;
        this.listView
                .addViewDetailsButtonActionListener(new ViewDetailsButtonActionListener());

        this.detailView = detailView;
        this.detailView.addOkButtonActionListener(new OkButtonActionListener());
    }

    /**
     * Returns the panel view used to display an object.
     * 
     * @return the panel view.
     */
    public final WaiterOLIView getListView() {
        return listView;
    }

    /**
     * This method notifies the controller that the order has changed and its
     * view needs to be refreshed.
     * 
     * @param order
     *            the order that was updated.
     */
 // CHECKSTYLE:OFF
    public final void refresh(final Order order) {
        final Employee waiter = order.getWaiter();
        listView.setDestination(String.format("Table: %d (%s)", order
                .getSitting().getTable().getId(),
                (waiter != null) ? waiter.getFirstName() : "<Unspecified>"));
        
        int progress;
        switch (order.getProgress()) {
        case PENDING:
            progress = 1;
            JOptionPane.showMessageDialog(null, "New Pending Order(s).");
            break;
        case CONFIRMED:
            progress = 2;
            listView.enableAdvanceButton(false);
            break;
        case PREPARING:
            progress = 3;
            listView.enableAdvanceButton(false);
            break;
        case READY:
            progress = 4;
            listView.enableAdvanceButton(true);
            JOptionPane.showMessageDialog(null, "There is an Order(s) waiting to be delivered.");
            break;
        case DELIVERED:
            progress = 5;
            listView.enableAdvanceButton(false);
            listView.enableCancelButton(false);
            break;
        case CANCELLED:
            progress = -1;
            listView.enableAdvanceButton(false);
            listView.enableCancelButton(false);
            JOptionPane.showMessageDialog(null, "An Order has been cancelled.");
            break;
        case NEW:
            progress = 0;
            listView.enableAdvanceButton(false);
            break;
        default:
            progress = 9;
            listView.enableAdvanceButton(false);
            break;

        // CHECKSTYLE:ON
        }
        listView.setProgress(progress);
    }

    /**
     * Adds an {@code ActionListener} to the Advance button within the panel
     * view associated with this controller.
     * 
     * @param listener
     *            the listener.
     */
    public final void addAdvanceButtonActionListener(
            final ActionListener listener) {
        listView.addAdvanceButtonActionListener(listener);
    }

    /**
     * Removes an {@code ActionListener} from the Advance button within the panel
     * view associated with this controller.
     * 
     * @param listener
     *            the listener.
     */
	public final void removeAdvanceButtonActionListener(final ActionListener listener) {
		listView.removeAdvanceButtonActionListener(listener);
	}

    /**
     * Adds an {@code ActionListener} to the Cancel Order button within the
     * panel view associated with this controller.
     * 
     * @param listener
     *            the listener.
     */
    public final void addCancelButtonActionListener(
            final ActionListener listener) {
        listView.addCancelButtonActionListener(listener);
    }
    

    /**
     * Observes button presses on the View Details button within the panel view.
     */
    private class ViewDetailsButtonActionListener implements ActionListener {
        /*
         * (non-Javadoc)
         * 
         * @see
         * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent
         * )
         */
        @Override
        public void actionPerformed(final ActionEvent e) {
            detailView.setVisible(true);
        }
    }

    /**
     * Observes button presses on the OK button within the detail view.
     */
    private class OkButtonActionListener implements ActionListener {
        /*
         * (non-Javadoc)
         * 
         * @see
         * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent
         * )
         */
        @Override
        public void actionPerformed(final ActionEvent e) {
            detailView.setVisible(false);
        }
    }
}