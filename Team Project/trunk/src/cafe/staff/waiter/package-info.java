/**
 * Defines classes related to the user interface for waiter staff.
 * 
 * @author Robert Kardjaliev
 */
package cafe.staff.waiter;